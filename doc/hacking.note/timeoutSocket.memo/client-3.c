#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

int main(int argc, char **argv) {
    char *addr;
    int port;
    fd_set fdset;
    int so_error;
    socklen_t len;
    int seconds;

    if (argc != 4) {
        fprintf(stderr, "Usage: %s <ip> <port> <timeout_seconds>\n", argv[0]);
        return 1;
    }

    addr = argv[1];
    port = atoi(argv[2]);
    seconds = atoi(argv[3]);

	
	//-------------------------------------------------------
    int sock;

    struct timeval Timeout;
    Timeout.tv_sec = seconds;
    Timeout.tv_usec = 0;
    struct sockaddr_in address;
	
    struct timespec tstart={0,0}, tend={0,0};
    clock_gettime(CLOCK_MONOTONIC, &tstart);

    sock = socket(AF_INET, SOCK_STREAM, 0);

	address.sin_addr.s_addr = inet_addr(addr);
    address.sin_port = htons(port);
    address.sin_family = AF_INET; // utilizzo IPv4

    fcntl(sock, F_SETFL, O_NONBLOCK); // setup non blocking socket

	//--------------------------------------------------
    unsigned long iMode = 1;
    int iResult ;

    int rc;
	rc = connect(sock,(struct sockaddr *)&address,sizeof(address));
	
    if ( rc < 0 && errno != EINPROGRESS) //false)
    {
         switch ( errno )
         {
                  case EADDRNOTAVAIL : printf("EADDRNOTAVAIL"); 
				                       break;
									   
                  case EAFNOSUPPORT  : printf("EAFNOSUPPORT");
                                       break;

                  case EALREADY      : printf("EALREADY");
                                       break;

                  case EBADF         : printf("EBADF");
                                       break;

                  case ECONNREFUSED  : printf("ECONNREFUSED");
                                       break;
 
                  case EINPROGRESS   : printf("EINPROGRESS");
                                       break;

                  case EINTR         : printf("EINTR");
                                       break;

                  case EISCONN       : printf("EISCONN");
                                       break;

                  case ENETUNREACH   : printf("ENETUNREACH");
                                       break;

                  case ENOTSOCK      : printf("ENOTSOCK");
                                       break;

                  case EPROTOTYPE    : printf("EPROTOTYPE");
                                       break;

                  case ETIMEDOUT     : printf("ETIMEDOUT");
                                       break;

                  case EIO           : printf("EIO");
                                       break;

                  case ELOOP         : printf("ELOOP");
                                       break;

                  case ENAMETOOLONG  : printf("ENAMETOOLONG");
                                       break;

                  case ENOENT        : printf("ENOENT");
                                       break;

                  case ENOTDIR       : printf("ENOTDIR");
                                       break;

                  case EACCES        : printf("EACCES");
                                       break;

                  case EADDRINUSE    : printf("EADDRINUSE");
                                       break;

                  case ECONNRESET    : printf("ECONNRESET");
                                       break;

                  case EHOSTUNREACH  : printf("EHOSTUNREACH");
                                       break;

                  case EINVAL        : printf("EINVAL");
                                       break;

                  case ENETDOWN      : printf("ENETDOWN");
                                       break;

                  case ENOBUFS       : printf("ENOBUFS");
                                       break;

                  case EOPNOTSUPP    : printf("EOPNOTSUPP");
                                       break;

                  default            : printf("CONNECT_SOCKET_UNKNOWN_ERROR");
                                       break;
         };

        return -1 ; 
    }	
	//--------------------------------------------------
	
    // make the connection  ------------------------------
/*
    rc = connect(sock, (struct sockaddr *)&address, sizeof(address));
    if ((rc == -1) && (errno != EINPROGRESS)) {
        fprintf(stderr, "Error: %s\n", strerror(errno));
        close(sock);
        return 1;
    } */
    if (rc == 0) { 
        // connection has succeeded immediately
        clock_gettime(CLOCK_MONOTONIC, &tend);
        printf("socket %s:%d connected. It took %.5f seconds\n",
            addr, port, (((double)tend.tv_sec + 1.0e-9*tend.tv_nsec) - ((double)tstart.tv_sec + 1.0e-9*tstart.tv_nsec)));

        close(sock);
        return 0;
    }
	
    FD_ZERO(&fdset);
    FD_SET(sock, &fdset);

    rc = select(sock + 1, NULL, &fdset, NULL, &Timeout);

    switch(rc) {
    case 1: // data to read
        len = sizeof(so_error);

        getsockopt(sock, SOL_SOCKET, SO_ERROR, &so_error, &len);

        if (so_error == 0) {
            clock_gettime(CLOCK_MONOTONIC, &tend);
            printf("socket %s:%d connecteddddd. It took %.5f seconds\n",
                addr, port, (((double)tend.tv_sec + 1.0e-9*tend.tv_nsec) - ((double)tstart.tv_sec + 1.0e-9*tstart.tv_nsec)));
            close(sock);
            return 0;
        } else { // error
            printf("socket %s:%d NOT connected: %s\n", addr, port, strerror(so_error));
        }
        break;
    case 0: //timeout
        fprintf(stderr, "connection timeout trying to connect to %s:%d\n", addr, port);
        break;
    }

    close(sock);
    return 0;
}
