#include <sys/ioctl.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <unistd.h>

int connectTCP(char *host, int port, int timeout); 

int main(int argc, char **argv[])
{
    char *host  = "127.0.0.1";
    int port    = 80;
    int timeout = 0; 

    if (argc != 4)
    {
       printf ("\nformat: comand <ip> <port> <timeout>");
       return 0; 
    }
 
    printf("\ntcp://%s:%d * timeout: %d", argv[1], atoi(argv[2]), atoi(argv[3]));

    host = malloc(100);
    strcpy(host, argv[1]); 
    port = atoi(argv[2]); 
    timeout = atoi(argv[3]);

    connectTCP(host, port, timeout);
} 

/*
.---------------------------------------------------.
|                                                   |
|                                                   |
|                                                   |
'---------------------------------------------------' */ 
int connectTCP(char *host, int port, int timeout)
{
    int sock;

    struct  timeval Timeout;
    Timeout.tv_sec = timeout;
    Timeout.tv_usec = 0; 
    struct sockaddr_in address;     

//--- ADD
    struct timespec tstart={0,0}, tend={0,0};
    clock_gettime(CLOCK_MONOTONIC, &tstart);
//--- ADD 

    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    address.sin_addr.s_addr = inet_addr(host); /* assign the address */
    address.sin_port = htons(port);            /* translate int2port num */	
    address.sin_family = AF_INET;

//--- ADD
    fcntl(sock, F_SETFL, O_NONBLOCK); // setup non blocking socket

    unsigned long iMode = 1;
    int iResult ;
//--- ADD

//--- DEL
/*
    //set the socket in non-blocking
    unsigned long iMode = 1;
    int iResult = ioctl(sock, FIONBIO, &iMode);   
    if (iResult != 0) //NO_ERROR)
    {	
        printf("ioctlsocket failed with error: %ld\n", iResult);
    }
*/
//--- DEL 
	    
//  if(connect(sock,(struct sockaddr *)&address,sizeof(address))<0) //false)

    int rc = 0 ;
    rc = connect(sock,(struct sockaddr *)&address,sizeof(address)) ; 
    if ( rc < 0 && errno != EINPROGRESS ) 
    {
         switch ( errno )
         {
                  case EADDRNOTAVAIL : printf("EADDRNOTAVAIL"); 
                                       break;

                  case EAFNOSUPPORT  : printf("EAFNOSUPPORT");
                                       break;

                  case EALREADY      : printf("EALREADY");
                                       break;

                  case EBADF         : printf("EBADF");
                                       break;

                  case ECONNREFUSED  : printf("ECONNREFUSED");
                                       break;
 
                  case EINPROGRESS   : printf("EINPROGRESS");
                                       break;

                  case EINTR         : printf("EINTR");
                                       break;

                  case EISCONN       : printf("EISCONN");
                                       break;

                  case ENETUNREACH   : printf("ENETUNREACH");
                                       break;

                  case ENOTSOCK      : printf("ENOTSOCK");
                                       break;

                  case EPROTOTYPE    : printf("EPROTOTYPE");
                                       break;

                  case ETIMEDOUT     : printf("ETIMEDOUT");
                                       break;

                  case EIO           : printf("EIO");
                                       break;

                  case ELOOP         : printf("ELOOP");
                                       break;

                  case ENAMETOOLONG  : printf("ENAMETOOLONG");
                                       break;

                  case ENOENT        : printf("ENOENT");
                                       break;

                  case ENOTDIR       : printf("ENOTDIR");
                                       break;

                  case EACCES        : printf("EACCES");
                                       break;

                  case EADDRINUSE    : printf("EADDRINUSE");
                                       break;

                  case ECONNRESET    : printf("ECONNRESET");
                                       break;

                  case EHOSTUNREACH  : printf("EHOSTUNREACH");
                                       break;

                  case EINVAL        : printf("EINVAL");
                                       break;

                  case ENETDOWN      : printf("ENETDOWN");
                                       break;

                  case ENOBUFS       : printf("ENOBUFS");
                                       break;

                  case EOPNOTSUPP    : printf("EOPNOTSUPP");
                                       break;

                  default            : printf("CONNECT_SOCKET_UNKNOWN_ERROR");
                                       break;
         };

        return -1 ; 
    }	

//----------------------------------------------- - - - - - - - - - - 
	
    // restart the socket mode
    iMode = 0;
    iResult = ioctl(sock, FIONBIO, &iMode); 
    if (iResult != 0) // NO_ERROR)
    {	
        printf("ioctlsocket failed with error: %ld\n", iResult);
    }

    fd_set Write, Err;
    FD_ZERO(&Write);
    FD_ZERO(&Err);
    FD_SET(sock, &Write);
    FD_SET(sock, &Err);

    // check if the socket is ready
    select(0,NULL,&Write,&Err,&Timeout);			
    if(FD_ISSET(sock, &Write)) 
    {	
        return 1; //true;
    }

    return 0; //false;

}
