/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/


/*
.-----------------------------------------------------.
| AnalizzaRiga - Prototipo e Struttura dati di OUTPUT |
'-----------------------------------------------------' */
typedef struct tCol 
{
        char paragrafo[80]                ;
        char parola[80]                   ;
} tCol;

typedef struct tAnalizzaRiga 
{
        int  sezione                      ;
        char paragrafo_noclose[80]        ;             
        tCol Tab[10]                      ;
        int  DimTab                       ;
        int  return_code                  ;
} tAnalizzaRiga;

typedef struct t_caratteri_di_controllo 
{
        char tag_paragrafo_inizio         ;
        char tag_paragrafo_fine           ;
        char tag_paragrafo_coda           ;
        char non_considerati[5]           ;
        char separa_parole[5]             ;
        char fine_riga                    ;
} t_caratteri_di_controllo;

tAnalizzaRiga AnalizzaRiga ( char frase[1000], int sezione, t_caratteri_di_controllo caratteri_di_controllo  ); 


/*
.-------------------------------------------------.
| LoadConf - Prototipo e Struttura dati di OUTPUT | 
'-------------------------------------------------' */ 
typedef struct tRow { 
        char paragrafo[80]; 
        char variabile[80];
        char valore[80]; 
} tRow;

typedef struct tLoadConf {  
        tRow Tab[1000];
        int dimensioneTab;
        int return_code;
} tLoadConf;

tLoadConf LoadConf (char *fconf);


/*
.-------------------------------------------------.
| GetVar - Prototipo e Struttura dati di OUTPUT   |
'-------------------------------------------------' */
char *getVar (tLoadConf rLoadConf, char paragrafo[80], char variabile[80]);


// __EOF__
