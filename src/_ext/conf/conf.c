/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/*
.--------------.
| headers LIBC |
'--------------' */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
.--------------------.
| header Locale      |
'--------------------' */
#include "conf.h"


/*
.-------------------------------------------------------------.
|                                                             |                                                          
|   Descrizione : Interpreta il file di configurazione        |
|                 le verifica eventualmente popola            |
|                 una tabella con tali istruzioni, in un      |
|                 formato comprensibile alla routine          |
|                 che inizia i controlli.                     |
|                                                             |
`-------------------------------------------------------------' */ 
tLoadConf LoadConf(char *fconf)
{
    FILE *fp                                             ;   
    char carattere[1]                                    ;   
    char frase[1000]                                     ;   
    int  byte                 = 0                        ;
    int  incommento           = 0                        ;
       
    tAnalizzaRiga       rAnalizzaRiga                    ;
    tLoadConf           rLoadConf                        ;

    /*
    .----------------------------------------------.
    | Tabella dei Caratteri di Controllo           | 
    '----------------------------------------------' */
    t_caratteri_di_controllo  caratteri_di_controllo ;
    caratteri_di_controllo.tag_paragrafo_inizio   =  '<' ;
    caratteri_di_controllo.tag_paragrafo_fine     =  '>' ;
    caratteri_di_controllo.tag_paragrafo_coda     =  '/';
    caratteri_di_controllo.non_considerati[0]     =  '[' ;
    caratteri_di_controllo.non_considerati[1]     =  ']' ;
    caratteri_di_controllo.non_considerati[2]     =  ']' ;
    caratteri_di_controllo.non_considerati[3]     =  ']' ;
    caratteri_di_controllo.non_considerati[4]     =  ' ' ;
    caratteri_di_controllo.separa_parole[0]       =  '=' ;
    caratteri_di_controllo.separa_parole[1]       =  '=' ;
    caratteri_di_controllo.separa_parole[2]       =  '=' ;
    caratteri_di_controllo.separa_parole[3]       =  '=' ;
    caratteri_di_controllo.separa_parole[4]       =  '=' ;
    caratteri_di_controllo.fine_riga              =  ';' ;

    /*
    .----------------------------------------------.
    | Inizializzazioni                             |
    '----------------------------------------------' */
    rLoadConf.dimensioneTab = 0; 
    rAnalizzaRiga.sezione = 0;   

    /*    
    .----------------------------------------------.
    | Apre il file di configurazione               |
    '----------------------------------------------' */
    fp = fopen(fconf, "r");
    if (!fp) {
        rLoadConf.return_code = -1; 
        return rLoadConf ;
    };

    /*
    .---------------------------------------.
    |  Analizza il file di configurazione.  |
    '---------------------------------------' */
    byte     = 0;
    frase[0] = 0; 
    while (fread(carattere, 1, sizeof(carattere), fp)) 
    {      
          /*
          .---------------------------------.
          | Riempi frase (riga file) fin    |
          | quando non trovi fine riga (10) |
          | oppure inizio commento (#).     |
          '---------------------------------' */
          if (carattere[0]!=10)
          {
              if ( carattere[0] == '#') {  
                   incommento = 1; };
                                   
              if ( incommento != 1) {
                   frase[byte]=carattere[0];
                   frase[byte+1]='\0'; 
                   byte++; };
          }
          else 
          {
             /*
             .----------------------------------------------.
             | Se la frase e' terminata e non nulla (vuota) |
             | allora si puo' analizzare.                   |
             '----------------------------------------------' */
             if ( frase[0] != 0 ) 
             {
                  rAnalizzaRiga = 
                     AnalizzaRiga ( frase                   ,
                                    rAnalizzaRiga.sezione   ,
                                    caratteri_di_controllo  );  
                  
                  if (rAnalizzaRiga.return_code == 0) 
                  {
                      /*
                      .------------------------------------------.
                      | Muove nella tabella variabili globali :  |
                      | - Nome variabile                         |
                      | - Valore della variabile                 |
                      '------------------------------------------' */
                      if (rAnalizzaRiga.DimTab > 0)
                      {
                         strcpy(rLoadConf.Tab[rLoadConf.dimensioneTab].paragrafo, rAnalizzaRiga.Tab[0].paragrafo);
                         strcpy(rLoadConf.Tab[rLoadConf.dimensioneTab].variabile, rAnalizzaRiga.Tab[0].parola);
                         strcpy(rLoadConf.Tab[rLoadConf.dimensioneTab].valore,    rAnalizzaRiga.Tab[1].parola);
 
		         rLoadConf.dimensioneTab++;  
                      };
                  }; 
             };

             /*
             .-----------------------------------------.
             | Fine analisi di una frase (riga fconf)  |
             | Inizializzazione delle variabili di     |
             | appoggio.                               |
             '-----------------------------------------' */                    
             byte=0;
             incommento = 0; 
             frase[0] = 0;

          }; 

       };

       /*
       .----------------------------------------------------.
       |         chiude il file di configurazione           |
       '----------------------------------------------------' */ 
       fclose(fp);

       return rLoadConf;
};



/*
.-----------------------------------------------------------------------------------------------------.
|                                                                                                     |
|  Nome        : analizza_riga                                                                        |
|                                                                                                     |
|  Autore      : raffaele.granito@tiscali.it                                                          |
|                                                                                                     |
|  Descrizione : Analizza un buffer dati.                                                             |
|                Estrapola le parole in base a delle regole date dai caratteri                        |
|                di controllo passati.                                                                |
|                                                                                                     |
|                1:  $(tag-paragrafo-inizio) paragrafo $(tag-paragrafo-fine)                          |
|                                                                                                     |
|                2:      parola-1 $(separa-parole) parola-2 $(separa-parole) parola-3 $(fine-riga)    |
|                3:      parola-4 $(separa-parole) parola-5 $(separa-parole) parola-6 $(fine-riga)    |
|                4:      parola-7 $(separa-parole) parola-8 $(separa-parole) parola-9 $(fine-riga)    |
|                5:      ...                                                                          |
|                6:  $(tag-paragrafo-inizio)$(tag-paragrafo-coda) paragrafo $(tag-paragrafo-fine)     |
|                                                                                                     |
|  Parametri   :                                                                                      |
|                buffer                                ===> stringa da analizzare                     |
|                                                                                                     |
|                caratteri di controllo                                                               |
|                          |                                                                          |
|                          |                                                                          |
|                          +--- tag-paragrafo-inizio   ===> identifica l'inizio del nome del          |
|                          |                                paragrafo sia nella TESTA che nella       |
|                          |                                coda.                                     |
|                          |                                                                          |
|                          +--- tag-paragrafo-fine     ===> idenifica la fine del nome del            |
|                          |                                paragrafo sia nella TESTA che nella       |
|                          |                                coda.                                     |
|                          |                                                                          |
|                          +--- tag-paragrafo-coda     ===> va inserito dopo il                       |
|                          |                                tag-paragrafo-inizio nella CODA per       |
|                          |                                differenziarlo dal record di TESTA.       |
|                          |                                                                          |
|                          +--- separa-parole          ===> nel CORPO e' il separatore di PAROLE.     |
|                          |                                Fine di una PAROLA ed inizio di           |
|                          |                                un'altra.                                 |
|                          |                                                                          |
|                          +--- non-considerati        ===> caratteri che nell'intero buffer          |
|                          |                                non vengono considerati.                  |
|                          |                                                                          |
|                          '--- fine-riga              ===> Identifica la fine di un raggruppameto    |
|                                                           di parole.                                |
|                                                                                                     |
|  Return-code :                                                                                      |
|                paragrafo              ===>  Nome del paragrafo se e' rimasto aperto                 |
|                                                                                                     |
|                sezione                ===>  Nome della sezione in cui si trova a fine buffer        |
|                                                                                                     |
|                                             [0] In nessun paragrafo                                 |
|                                             [1] INIZIO TESTA PARAGRAFO                              |
|                                             [2] FINE TESTA PARAGRAFO                                |
|                                             [3] CORPO                                               |
|                                             [4] INIZIO CODA PARAGRAFO                               |
|                                             [5] FINE CODA PARAGRAFO                                 |
|                                                                                                     |
|                Tabella parole         ===>  Elenco parole del CORPO (campi: A, B, C, D)             |
|                                                                                                     |
|                                             [A] paragrafo                                           |
|                                             [B] numero parole                                       |
|                                             [C] parola                                              |
|                                                                                                     |
|                Numero parole          ===>  Numero di parole trovate nel CORPO                      |
|                                                                                                     |
|                Return-code            ===>  Esito                                                   |
|                                                                                                     |
|                                             [0] OK                                                  |
|                                                                                                     |
|  Esempio     :                                                                                      |
|                                                                                                     |
|                INPUT <=====                                                                         |
|                              $(tag-paragrafo-inizio) <===    <                                      |
|                              $(tag-paragrafo-coda)   <===    /                                      |
|                              $(tag-paragrafo-fine)   <===    >                                      |
|                              $(separa-parole)        <===    =                                      |
|                              $(fine-riga)            <===    ;                                      |
|                                                                                                     |
|                              $(buffer)               <===    1:  <colori>                           |
|                                                              2:            sfondo =  bianco  ;      |
|                                                              3:            testo  =  nero    ;      |
|                                                              4:            bordi  =  rosso   ;      |
|                                                              5:            titolo =  blu     ;      |
|                                                              6:  </colori>                          |
|                                                              7:                                     |
|                                                              8:  <stile>                            |
|                                                              9:            font   =  arial   ;      |
|                                                             10:            stile  =  corsivo ;      |
|                                                             11:  </stile>                           |
|                                                                                                     |
|                 OUTPUT <====                                                                        |
|                              $(sezione)              <===    0                                      |
|                                                                                                     |
|                              $(paragrafo)            <===    null                                   |
|                                                                                                     |
|                              $(Tab)                  <===    A:  paragrafo                          |
|                                                              B:  parola                             |
|                                                                                                     |
|                                                                  A       B                          |
|                                                                  --------------------               |
|                                                              1:  colori  sfondo                     |
|                                                              2:  colori  bianco                     |
|                                                              3:  colori  testo                      |
|                                                              4:  colori  nero                       |
|                                                              5:  colori  bordi                      |
|                                                              6:  colori  rosso                      |
|                                                              7:  colori  titolo                     |
|                                                              8:  colori  blu                        |
|                                                              9:  stile   font                       |
|                                                             10:  stile   arial                      | 
|                                                             11:  stile   stile                      |
|                                                             12:  stile   corsivo                    | 
|                                                                                                     | 
|                              $(DimTab)               <===   12                                      |
|                                                                                                     |
|                                                                                                     |
'-----------------------------------------------------------------------------------------------------' */ 
tAnalizzaRiga AnalizzaRiga
(
       char                     frase[1000]             , 
       int                      sezione                 ,
       t_caratteri_di_controllo caratteri_di_controllo   
)
{
          #define _LEN_FRASE_      1000  

          int  byte                = 0 ;
          int  byte_parola         = 0 ;

          char parola[80]              ;
       
          tAnalizzaRiga rAnalizzaRiga  ; 

          /*    
          .--------------------------------------------.
          |                                            |
          |  Inizializzazione RETURN CODE              |
          |                                            |
          '--------------------------------------------' */ 
          parola[0]                 = 0       ;
	 
          rAnalizzaRiga.sezione     = sezione ;
          rAnalizzaRiga.return_code = 0       ;
          rAnalizzaRiga.DimTab      = 0       ; 

          /*
          .--------------------------------------------.
          |                                            |
          |  Legge fino alla fine... oppure per        |
          |  un massimo di _LEN_FRASE_                 |
          |                                            |
          `--------------------------------------------' */ 
          while (byte < _LEN_FRASE_ && frase[byte] != 0) 
          {    
       
	         /*
                 .-------------------------------------------.
	         |                                           |
                 |  Esclude i caratteri di controllo         |
	         |                                           |
                 '-------------------------------------------' */ 
                 if (frase[byte]!= caratteri_di_controllo.tag_paragrafo_inizio    &&
                     frase[byte]!= caratteri_di_controllo.tag_paragrafo_fine      &&
               //    frase[byte]!= caratteri_di_controllo.tag_paragrafo_coda      &&
			       
	             frase[byte]!= caratteri_di_controllo.non_considerati[0]      &&
	             frase[byte]!= caratteri_di_controllo.non_considerati[1]      &&
		     frase[byte]!= caratteri_di_controllo.non_considerati[2]      &&
		     frase[byte]!= caratteri_di_controllo.non_considerati[3]      &&
		     frase[byte]!= caratteri_di_controllo.non_considerati[4]      &&
			       
                     frase[byte]!= caratteri_di_controllo.separa_parole[0]        &&
		     frase[byte]!= caratteri_di_controllo.separa_parole[1]        &&
		     frase[byte]!= caratteri_di_controllo.separa_parole[2]        &&
		     frase[byte]!= caratteri_di_controllo.separa_parole[3]        &&
		     frase[byte]!= caratteri_di_controllo.separa_parole[4]        &&
                              
                     frase[byte]!= 9                                              &&  // ...forse il TAB 
		     frase[byte]!= 10                                             &&  // ...forse return-carriage
			       
	             frase[byte]!= caratteri_di_controllo.fine_riga ) 
                 {
                     parola[byte_parola]   = frase[byte];
                     parola[byte_parola+1] = 0;
                     byte_parola++;
                 };


                 /*
                 .------------------------------------------.
                 |                                          |
                 |  Se ha appena letto un paragrafo         |
                 |  si è di nuovo fuori da ogni paragrafo   |
                 |  quindi il valore della sezione torna    |
                 |  a ZERO e si ricomincia.                 |
                 |                                          |
                 |           <inizio paragrafo>             |
                 |               ...                        |
                 |           <\fine paragrafo> [5]          |
                 |                              |           |
                 |     [0] <--------------------'           |
                 |                                          |
                 |           <inizio paragrafo>             |
                 |               ...                        |
                 |           <\fine paragrafo>              |
                 |                                          |
                 `------------------------------------------' */
                 if ( rAnalizzaRiga.sezione == 5 ) 
                 {
                     rAnalizzaRiga.sezione = 0;
                 };


	         /*	  
                 .------------------------------------------.
  	         |                                          |
                 |  inizio etichetta di inizio paragrafo    |
	         |                                          |
	         |   (1) --> <inizio paragrafo>             |
	         |               ...                        |
	         |           <\fine paragrafo>              |
	         |                                          |
	         `------------------------------------------' */ 
	         if (rAnalizzaRiga.sezione         == 0   &&
                     frase[byte]                   == caratteri_di_controllo.tag_paragrafo_inizio && 
                     frase[byte+1]                 != caratteri_di_controllo.tag_paragrafo_coda) 
                 {
	             /*
                     .-----------------------------------------.    
                     | Siamo nell'intestazione di un Paragrafo |
                     '-----------------------------------------' */
	             rAnalizzaRiga.sezione = 1; 
	         };


                 /*
	         .------------------------------------------.
	         |                                          |
	         |  fine etichetta di inizio paragrafo      |
	         |                                          |
	         |           <inizio paragrafo> <-- (2)     |
	         |               ...                        |
	         |           <\fine paragrafo>              |
	         |                                          |
	         `------------------------------------------' */ 
                 if (rAnalizzaRiga.sezione         == 1   &&
                     frase[byte]                   == caratteri_di_controllo.tag_paragrafo_fine) 
                 {

                     /*
                     .-----------------------------------------.
                     | Salvataggio dell'intestazione Paragrafo |
                     '-----------------------------------------' */
	             strcpy(rAnalizzaRiga.paragrafo_noclose,parola);
			   
                     /*
                     .-----------------------------------------.
                     | Inizializzazione paragrafo              |
                     '-----------------------------------------' */
		     parola[0]   = 0;
		     byte_parola = 0;
                               
                     /*
                     .-----------------------------------------.
                     | Siamo a fine intestazione Paragrafo     |
                     '-----------------------------------------' */
		     rAnalizzaRiga.sezione = 2; 
		 };


                 /*
	         .------------------------------------------.
	         |                                          |
                 |  Fine parola all'interno del paragrafo   | 
                 |                                          |
	         |            <inizio paragrafo>            |
	         |                ... parola <-- (3)        |
	         |            <\fine paragrafo>             |
	         |                                          |
                 '------------------------------------------' */ 
                 if ( 
                      ( rAnalizzaRiga.sezione == 2 || rAnalizzaRiga.sezione == 3 ) &&
                      ( frase[byte] != caratteri_di_controllo.tag_paragrafo_fine ) &&  
                      ( frase[byte] == caratteri_di_controllo.fine_riga              || 
                        frase[byte] == caratteri_di_controllo.separa_parole[0]       ||
	    	        frase[byte] == caratteri_di_controllo.separa_parole[1]       ||
	                frase[byte] == caratteri_di_controllo.separa_parole[2]       ||
		        frase[byte] == caratteri_di_controllo.separa_parole[3]       ||
		        frase[byte] == caratteri_di_controllo.separa_parole[4] ))
		 { 

		      /*
                      .-----------------------.
                      | Muove paragrafo PADRE |
                      '-----------------------' */  
		      strcpy(rAnalizzaRiga.Tab[rAnalizzaRiga.DimTab].paragrafo,rAnalizzaRiga.paragrafo_noclose);   
			      
		      /*
                      .--------------------------------.
                      | Muove parola in tabella PAROLE |
                      '--------------------------------' */ 
		      strcpy(rAnalizzaRiga.Tab[rAnalizzaRiga.DimTab].parola, parola);

		      /*
                      .------------------------------------.
                      | Aggiunge TAPPO alla tabella PAROLE | 
                      '------------------------------------' */
                      strcpy(rAnalizzaRiga.Tab[rAnalizzaRiga.DimTab+1].parola, "EOT\0");

		      /*
                      .---------------------------.
                      | Incrementa tabella PAROLE |
                      '---------------------------' */ 
                      rAnalizzaRiga.DimTab++;

		      /*
                      .--------------------.
                      | Inizializza PAROLA | 
                      '--------------------' */
                      parola[0] = 0;
                      byte_parola = 0;

                      /*
                      .--------------------------------------------------.
                      | Siamo nel corpo del paragrafo. Letta una parola. |
                      '--------------------------------------------------' */
                      rAnalizzaRiga.sezione = 3;
                 };

                 /*
	         .------------------------------------------.
	         |                                          |
	         |  inizio etichetta di fine paragrafo      |
	         |                                          |
	         |            <inizio paragrafo>            |
	         |                ...                       |
	         |   (4)  --> <\fine paragrafo>             |
	         |                                          |
	         `------------------------------------------' */ 
                 if ((rAnalizzaRiga.sezione == 2 || rAnalizzaRiga.sezione == 3) &&
                     frase[byte]   == caratteri_di_controllo.tag_paragrafo_inizio && 
                     frase[byte+1] == caratteri_di_controllo.tag_paragrafo_coda) 
                 {
                     /*
                     .--------------------------------.
                     | Siamo ad inizio CODA Paragrafo |
                     '--------------------------------' */
	             rAnalizzaRiga.sezione = 4; 
	         };

                 /*
	         .------------------------------------------.
	         |                                          |
	         |  fine etichetta di fine paragrafo        |
	         |                                          |
	         |            <inizio paragrafo>            |
	         |                ...                       |
	         |            <\fine paragrafo> <-- (5)     |
	         |                                          |          
	         `------------------------------------------' */ 
                 if (rAnalizzaRiga.sezione == 4 &&
                     frase[byte]           == caratteri_di_controllo.tag_paragrafo_fine) 
                 {
                     /*
                     .---------------------------.
		     | Salvataggio del paragrafo |
                     '---------------------------' */ 
		     strcpy(rAnalizzaRiga.paragrafo_noclose,parola);
			      
                     /*
                     .-----------------------. 
		     | Inizializza la parola |
                     '-----------------------' */ 
		     parola[0] = 0;
		     byte_parola = 0;

                     /*
                     .------------------------------------------.
		     | Azzera il PARAGRAFO perche' si e' usciti |
                     '------------------------------------------' */ 
		     rAnalizzaRiga.paragrafo_noclose[0] = 0;

                     /*
                     .-----------------------------------------------------. 
		     | Siamo usciti dal paragrafo                          | 
                     '-----------------------------------------------------' */ 
                     rAnalizzaRiga.sezione = 5; 
		 };
		 
                 byte++;
       
         };
									     
         return rAnalizzaRiga;
};


/*
.-------------------------------------------------------------.
|                                                             |
|   Descrizione : Cerca una variabile di un paragrafo e       |
|                 se la trova restituisce il suo valore.      |
|                 Se nella tabella passata la stessa          |
|                 variabile ricercata è presente più volte    |
|                 la funzione considera solamente la prima.   |
|                 Se la variavile è trovata restituisce 0     |
|                 altrimenti torna -1.                        |
|                                                             |
`-------------------------------------------------------------' */
char *getVar ( tLoadConf rLoadConf, char paragrafo[80], char variabile[80])
{

        char *rGetVar ;

        rGetVar = malloc(80);
        *rGetVar = 0;

        /*
        .--------------------------------.
        | Inizialize variable *NotFound* |
        '--------------------------------' */


        int i = 0; 
        while ( i < rLoadConf.dimensioneTab )
        {
                /*
                .--------.
                | search |
                '--------' */ 
                if (strcmp(rLoadConf.Tab[i].paragrafo,paragrafo)==0 && 
                    strcmp(rLoadConf.Tab[i].variabile,variabile)==0)
                {
                    strcpy(rGetVar,rLoadConf.Tab[i].valore);
                    /*
                    .-------.
                    | Found |  
                    '-------' */
                    return rGetVar; 
                }; 
                i++;
        };

        return rGetVar ;
};

// __EOF__  
