/*
# Copyright (C) 2008-2011 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 

/* 
.=====================================================================. 
|                                                                     |
    Libreria         : client.common.output

    Descrizione      : funzioni di uso comune di visualizzazione 
                       della componente client.

    Elenco funzioni

       funzione                 descrizione
    ------------------------------------------------------------
    1. PrintMessage             Visualizza in STDIO un messaggio  
                                elemento del DB message. 
    ------------------------------------------------------------
    2. Dump                     Visualizza in STDIO una stringa 
                                in esadecimale formato dump mem. 
    ------------------------------------------------------------
    3. GetDebugLevel            estrapola livello di logging per
                                un certo protocollo dal             
                                parametro debug level 
    ------------------------------------------------------------
    4. Uri2SingleFieldConnect   converte una URI string in            
                                struttura dati.
|                                                                    |
'====================================================================' 
*/

 
/*
.--------------.
| headers LIBC |
'--------------' */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
.--------------------.
| header Locale      |
'--------------------' */
#include "output.h"


///////////////////////////////////////////////////////////////////////////////////////////////
//=====================================================================================
// libreria string 
//============================================================================================
 
/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione    : mReplaceStr                                      |
|                                                                 |
|  Descrizione : Sostituzione multipla di sottostringhe.          |
|                                                                 |
|  Esempio                                                        |
|  <input>                                                        |
|  str         : la temperatura è di %temperatura%                |
|  a           : %temperatura%                                    |
|  b           : 100                                              |
|  <output>                                                       |
|  strd        : la temperatura è di 100                          |
|                                                                 |
|  return-code : stringa aggiornata                               |
|                                                                 |
|  dipendenze  : nessuna                                          |
|                                                                 |
'-----------------------------------------------------------------' */
char *mReplaceStr (const char *str, const char *TabVariabili[][3])
{
         register int DEBUG = 0;
         register int i     = 0;

         char* strd ;
         strd = strdup(str);

         while ( strcmp(TabVariabili[i][0],"EOT") != 0 )
         {
                 if (DEBUG==1) { 
                       printf("\n%c", strd[strlen(strd)]-1);
                       printf("\nmReplaceStr[prima] -- stringaIN : [%s] - variabile: [%s] - valore: [%s]", 
                                  strd, TabVariabili[i][0], TabVariabili[i][1]);  
                 }

                 /*
                 .--------------------------------------------------------------------------------.
                 | Se il nuovo valore è NULL (0) lo sostituisce con "\0" altrimenti va in crash   | 
                 | la funzione di replaceStr.... INDAGARE                                         |
                 '--------------------------------------------------------------------------------' */  
                 if (TabVariabili[i][1]==0) { 
                       strd=replaceStr(strd, TabVariabili[i][0], "\0");
                 } 
                 else { 
                       strd=replaceStr(strd, TabVariabili[i][0], TabVariabili[i][1]);
                 };

                 if (DEBUG==1) { 
                       printf("\n%c", strd[strlen(strd)]-1);  
                       printf("\nmReplaceStr[dopo] -- stringaOUT: [%s] - variabile: [%s] - valore: [%s]", 
                                           strd, TabVariabili[i][0], TabVariabili[i][1]);  
                 }

                 i++;
         };
         return strd;
}

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione    : replaceStr                                       |
|                                                                 |
|  Descrizione : Sostituisce una substr con un'altra.             |
|                                                                 |
|  Esempio                                                        |
|  <input>                                                        |
|  str         : la temperatura è di %temperatura%                |
|  a           : %temperatura%                                    |
|  b           : 100                                              |
|  <output>                                                       |
|  strd        : la temperatura è di 100                          |
|                                                                 |
|  return-code : stringa aggiornbata                              |
|                                                                 |
|  dipendenze  : nessuna                                          |
|                                                                 |
'-----------------------------------------------------------------' */
char *replaceStr(const char *str, const char *strOLD, const char *strNEW) 
{
      register int DEBUG = 0;

      if (DEBUG==1) { 
            printf("\n--BEFORE------------------------------------------------------"); 
            printf("\n>> replaceStr [PRIMA]-- stringa IN: [%s(%d)] [%s(%d)->%s(%d)]", 
                           str, strlen(str), strOLD, strlen(strOLD), strNEW, strlen(strNEW));
      }
      char* strd ;
      strd=malloc(4096);
      memset(strd,0,4096);
      strd=str;  

      char *cursor; 
      cursor = malloc(4096);

      /*
      .-------------------------------------------.
      | posiziona il cursore alla posizione in    |
      | cui inizia la stringa vecchia             |
      '-------------------------------------------' */ 
      for (cursor = strd; (cursor = strstr(cursor, strOLD)) != NULL;)
      {
          memmove(cursor + strlen(strNEW), cursor + strlen(strOLD), strlen(cursor) - strlen(strOLD) + 1);

          /*
          .-------------------------------------.
          | accoda la nuova stringa a cursor    |
          | dal punto in cui iniziava la        |
          | vecchia stringa.                    |
          '-------------------------------------' */
          for (int i = 0; strNEW[i] != '\0'; i++)
               cursor[i] = strNEW[i];

          /*
          .--------------------------------.
          | sposta il puntatore di cursor  |
          | alla fine della stringa nuova  |
          '--------------------------------' */ 
          cursor += strlen(strNEW);
      }

      free(cursor);

      if (DEBUG==1) { 
            printf("\n--AFTER---------------------------------------------------------");
            printf("\n>> replaceStr [DOPO]-- stringa IN: [%s(%d)] [%s(%d)->%s(%d)]",
                           strd, strlen(strd), strOLD, strlen(strOLD), strNEW, strlen(strNEW));
      }
      return strd;
}


/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione    : my_itoa                                          |
|                                                                 |
|  Descrizione : converte un intero in stringa a base n (2-16)    |
|                                                                 |
|  parametri   :                                                  |
|                val   * valore numerico da convertire in stringa |
|                base  * base da utilizzare per la conversione    |
|                                                                 |
|  return-code : stringa                                          |
|                                                                 |
|  dipendenze  : nessuna                                          |
|                                                                 |
'-----------------------------------------------------------------' */
char *my_itoa_OLD ( int val, int base )
{
    int DEBUG=0;

    static char str[32] = {0};
    int i = 30;
    int valbak = val ;

    /*  
    .-------------------------------------------.
    | nel caso di valore numerico uguale a zero |
    | non funzionava il meccanismo di           |
    | restituenzo in out un NULL. Ho aggiunto   | 
    | quindi questa condizione particolare.     |
    | (eventualmente approfondire)              |
    '-------------------------------------------' */
    if (val==0)
        return "0\0";

    /* 
    .----------------------------.
    | meccanismo di conversione  |
    '----------------------------' */
    for(; val && i ; --i, val /= base)
        str[i] = "0123456789abcdef"[val % base];

    /*  
    .-------------------------------------------------.
    | sposta la stringa di output &str[i+1]           | 
    | in un puntatore a stringa (strout) perchè       | 
    | effettuando la return direttamente del valore   | 
    | originale al chiamato non arrivava il valore    |  
    | corretto. (eventualmente approfondire)          |
    '-------------------------------------------------' */
    char *strout = malloc(32);
    memset(strout, 0, 32);
    strcpy(strout, &str[i+1]);    

    if (DEBUG==1)
        printf("\n>> itoa - int: [%d] / str: [%s]", valbak, &str[i+1]);

    return strout; 
}

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione    : my_itoa                                          |
|                                                                 |
|  Descrizione : converte un intero in stringa.                   |
|                                                                 |
|  parametri   :                                                  |
|                val   * valore intero da convertire in stringa.  |
|                mask  * maschera nel formato *printf.            |
|                                                                 |
|  return-code : stringa                                          |
|                                                                 |
|  dipendenze  : nessuna                                          |
|                                                                 |
'-----------------------------------------------------------------' */
char *my_itoa ( int val, char *mask )
{
    char *str = malloc (20) ;

    int strDim = 20 ;

    snprintf ( str, strDim, mask, val );

    return str;
}

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione    : my_dtoa                                          |
|                                                                 |
|  Descrizione : converte un intero in stringa.                   |
|                                                                 |
|  parametri   :                                                  |
|                val   * valore double da convertire in stringa.  |
|                mask  * maschera nel formato *printf.            |
|                                                                 |
|  return-code : stringa                                          |
|                                                                 |
|  dipendenze  : nessuna                                          |
|                                                                 |
'-----------------------------------------------------------------' */
char *my_dtoa ( double val, char *mask )
{
    char *str = malloc (20) ;

    int strDim = 20 ;

    snprintf ( str, strDim, mask, val ); 

    return str; 
}


/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione    : my_ltoa                                          |
|                                                                 |
|  Descrizione : converte un long in stringa.                     |
|                                                                 |
|  parametri   :                                                  |
|                val   * valore long da convertire in stringa.    |
|                mask  * maschera nel formato *printf.            |
|                                                                 |
|  return-code : stringa                                          |
|                                                                 |
|  dipendenze  : nessuna                                          |
|                                                                 |
'-----------------------------------------------------------------' */
char *my_ltoa ( long val, char *mask )
{
    char *str = malloc (20) ;

    int strDim = 20 ;

    snprintf ( str, strDim, mask, val );

    return str;
}

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione    : my_stos                                          |
|                                                                 |
|  Descrizione : converte una stringa in stringa differente.      |
|                                                                 |
|  parametri   :                                                  |
|                strin * valore stringa da convertire.            |
|                mask  * maschera nel formato *printf.            |
|                                                                 |
|  return-code : strout                                           |
|                                                                 |
|  dipendenze  : nessuna                                          |
|                                                                 |
'-----------------------------------------------------------------' */
char *my_stos ( char *strin, char *mask )
{
    char *strout = malloc (1024) ;

    int stroutDim = 1024 ;

    snprintf ( strout, stroutDim, mask, strin );

    return strout;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// FORSE VA CREATA UNA UNICA FUNZIONE DI CONVERSIONE A STRINGA, UNA MY_SNPRINTF PER CAPIRCI
//// PER AVERE LA STRINGA DI OUTPUT NEL RETURN  
///////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione   : PrintMessage                                      |
|                                                                 |
|  Descrizione: Visualizza un messaggio                           |
|                                                                 |
|  Viene passata l'ID del messaggio da visualizzare ed una        |
|  "tabella messaggi". Quindi cerca il messaggio nella            |
|  tabella per id [colonna 1].                                    |
|                                                                 |
|     TABMSG                                                      |
|     +------------------+ O         o id msg                     |
|     | id | type | desc | |        /                             |
|     |  <-------------------------'                              |
|     |    |      |      | |                                      |
|     |    |      |      | |                                      |
|     |    |      |      | |                                      |
|     +------------------+ V {EOT}                                |
|                                                                 |
|  Si da per sconatto che nella tabella al massimo ci sia un      |
|  unico messaggio con quell'id, ed infatti legge fintanto non    |
|  trova l'id oppure se la tabella finisce, cioè per non trovato  |
|  quindi se nella tabella ci fosse più di un messaggio con quel  |
|  id i successivi al primo verrebbero ignorati. Ok?              |
|                                                                 |
|  o Se non viene trovato nessuna occorenza con quell'id la       |
|    la funzione termina senza far nulla e senza tornare sulla    |
|    al chiamante (il return code è fisso a 0.                    |
|                                                                 |
|  o Se invece è stata trovata un occorrenza per quell'idmsg non  |
|    è detto che questo sia da visualizzare. Faccio un passo      |
|    indietro il messaggio sulla tabella altre all'id ha 2        |
|    attributi [1] tipo messaggio e [2] descrizione. Il tipo      |
|    messaggio specifica se si tratta di "information","warning"  |
|    o "errore". Il messaggio può anche non essere classificato.  |
|                                                                 |
|  Il messaggio viene visualizzato solamente se è compatibile     |
|  con il debug_level specificato in input, cioè se di livello    |
|  identico o più basso del livello di debug impostato, secondo   |
|  lo schema :                                                    |
|                                                                 |
|  >> debug_level                                                 |
|     dominio debug_level                                         |
|                                                                 |
|     0 * None                                                    |
|     1 * Information                                             |
|     2 * Warning                                                 |
|     3 * Error                                                   |
|                                                                 |
|  >> TABMSG                                                      |
|     dominio tabmsg.type                                         |
|                                                                 |
|     1 * Information                                             |
|     2 * Warning                                                 |
|     3 * Error                                                   |
|                                                                 |
|  Ad esempio se il livello di debug è 2 [warning] il messaggio   |
|  verrà visualizzato solamente se anch'esso definito come        |
|  warning [2] o di livello più basso: Information [1] o None [0] |
|  Per non visualizzare nulla è necessario impostare il tabmsg    |
|  al valore None [0].                                            |
|                                                                 |
|  >> IDMsg                                                       |
|                                                                 |
|  ID del messaggio da visualizzare                               |
|                                                                 |
|                                                                 |
|  >> TabVariabili                                                |
|                                                                 |
|  Tabella delle variabili/valori                                 |
|                                                                 |
|      esempio. char *TabVariabili[][2] =  {                      |
|                                    {"%A%", "100", "string" },   |
|                                    {"%B%", "200", "numeric"},   |
|                                    {"EOT", ""   , ""       }    |
|                                   };                            |
|                                                                 |
|  >> rows-before                                                 |
|		righe vuote da lasciare prima                             |
|                                                                 |
|  >> rows-after                                                  |
|		righe vuote da lasciare dopo                              |
|                                                                 |
|  >> viewType                                                    |
|		Tipo di vista                                             |
|     0 Stringa                                                   |
|     1 Stringa                                                   |
|                                                                 |
|  return-code : 0 [costante].                                    |
|                                                                 |
|  dipendenza  : nessuna.                                         |
|                                                                 |
'-----------------------------------------------------------------'*/
int PrintMessage(int debug_level, char *TABMSG[][3], char *IDMsg, char *TabVariabili[][3], 
						short int rowsBefore, short int rowsAfter, short int viewType )
{
    int i = 0;

    /*
    .------------------------------------------------.
    | Quit, from debug level __NONE__                |
    '------------------------------------------------' */
    if (debug_level==0)
        return 0;

    while ( strcmp(TABMSG[i][0],"END") != 0 && strcmp(TABMSG[i][0],IDMsg) != 0 )
    {
            i++;
    };

    int message_debug_level = 0;
    if ( strcmp(TABMSG[i][1],"__NONE__")==0 ) { message_debug_level = 0; };
    if ( strcmp(TABMSG[i][1],"__INFO__")==0 ) { message_debug_level = 1; };
    if ( strcmp(TABMSG[i][1],"__WARN__")==0 ) { message_debug_level = 2; };
    if ( strcmp(TABMSG[i][1],"__ERR__" )==0 ) { message_debug_level = 3; };

    if ( message_debug_level <= debug_level )
    {
         char *str;
         str = malloc(8096);
         memset(str,0,8096);
         str= mReplaceStr(TABMSG[i][2], TabVariabili);

         /*
         ========================================================================================
		 super pezza per eliminare la stringa ssl in caso di connessione senza 
		 ========================================================================================= */
	   //str= replaceStr(str, "\[      (null) \|               (null) \|   0 \]    ","") ;
		 str= replaceStr(str, "\[      (null)","");
		 str= replaceStr(str, "               (null) ","");
         str= replaceStr(str, "   0 \]","");
         str= replaceStr(str, "\|\|","");
         str= replaceStr(str, "[0]","");

         /* SPAZI PRIMA */		 
         register short int i1 = 0 ;
         while ( i1 < rowsBefore ) {
			printf ("\n");
			i1++;
		 }
				
         /* PRINT */
         switch(viewType)
		 {
			case 0: printf ("%s", str);
			   break;
			
			case 1: dump (str, strlen(str));
			   break;
			   
			default: 
			   printf ("%s", str);
			   break;
         }
		 
         /* SPAZI DOPO */		 
         register short int i2 = 0 ;
         while ( i2 < rowsAfter ) {
			printf ("\n");
			i2++;
		 }

		 /* */
		 //dump (str, strlen(str));

         //fflush(stdout);
         //free(str);
    };
  
    return 0;
}


/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione   : dump                                              |
|                                                                 |
|  Descrizione: Visualizza buffer in stile dump memoria.          | 
|                                                                 |
|  Visualizza un buffer passato in formato esadecimale e nel      |
|  formato originale. Questo layout di visualizzazione viene di   |
|  solito usato per i dump di memoria.                            |
|                                                                 |
|  Il codice è stato scritto da John Erikson, preso da un suo     |
|  libro "l'arte dell'hacking" ;) Riporto commento originale:     |
|                                                                 | 
|  "Dumps raw memory in hex byte and printable split format.      |
|  -- written John Erikson                                        |
|                                                                 |
|  <input>                                                        |
|  buffer.value  : abcdefghilmenopqrstuvz1234567890               | 
|  buffer.lenght : 32                                             | 
|                                                                 |
|  <output>                                                       |
|  61 62 63 64 65 66 67 68 | abcdefgh                             | 
|  69 6c 6d 65 6e 6f 70 71 | ilmenopq                             |
|  72 73 74 75 76 7a 31 32 | rstuvz12                             | 
|  33 34 35 36 37 38 39 30 | 34567890                             |
|                                                                 |
|  return-code : nessuno.                                         |
|                                                                 |
|  dipendenze  : nessuna                                          |
|                                                                 |
'-----------------------------------------------------------------' */ 
void dump (const unsigned char *data_buffer, const unsigned int length) 
{
     unsigned char byte;
     unsigned int i, j;

     for (i=0; i < length; i++) 
     {
         byte = data_buffer[i];
         printf("%02x ", data_buffer[i]); // Display byte in hex.
         if(((i%16)==15) || (i==length-1)) 
         {
            for(j=0; j < 15-(i%16); j++)
               printf("   ");

            printf("| ");

            for(j=(i-(i%16)); j <= i; j++) 
            { // Display printable bytes from line.
               byte = data_buffer[j];
               if((byte > 31) && (byte < 127)) // Outside printable char range
                   printf("%c", byte);
               else
                   printf(".");
            }

            printf("\n"); // End of the dump line (each line is 16 bytes)
         } 
     } 
};


/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione    : GetDebugLevel                                    |
|                                                                 |
|  Descrizione : Converte il livello di debug dal formato         |
|                stringa a numerico                               |
|                                                                 |
|  Traduce il livello di debug per il particolare protocollo      |
|  specificato in input dal formato stringa al formato numerico.  |
|  Per far questo cerca il protocollo nella stringa (strstr) e    |
|  conta quante + si trovano alla sinistra. Se alla sinistra      |
|  trova un solo + il livello è __err__ se ne trova due di + il   |
|  livello è __warn__, se ne trova tre il livello + __info__.     |
|  In tutti gli altri casi il livello è impostato a _none__,      |
|  ossia se non trova il protocollo nella stringa di debug, se    |
|  lo trova ma alla sinistra non trova nessun +, se non viene     |
|  specificato alcun protocollo la verificare.                    |
|                                                                 |
|  same examples                                                  |
|                                                                 |
|  ======== =============                                         | 
|  prot.    strDebug          ret                                 |
|  -------- -------------     --------                            |
|  tcp      +ssl          =>  __none__   not found protocol       | 
|  tcp      +++tcp+++ssl  =>  __info__                            |
|  tcp      ++tcp         =>  __warn__                            |
|  tcp      +tcp+ssl+http =>  __err__                             |
|                                                                 |
|                                                                 |
|  return-code : { __NONE__|__INFO__|__WARN__|__ERR__ }           |
|                                                                 |
|  dipendenze  : nessuna                                          |
|                                                                 |
'-----------------------------------------------------------------' */
int GetDebugLevel ( char *strDebug, char *protocollo )
{
    if ( strDebug == NULL )
         return __NONE__ ;

    if ( protocollo == NULL )
         return __NONE__ ;

    char *strDebugLevel ;

    strDebugLevel = malloc ( strlen(strDebug) + 5 );

    sprintf(strDebugLevel, "+++%s", protocollo);  if ( strstr(strDebug, strDebugLevel ) != NULL ) return __ERR__ ;
    sprintf(strDebugLevel,  "++%s", protocollo);  if ( strstr(strDebug, strDebugLevel ) != NULL ) return __WARN__ ; 
    sprintf(strDebugLevel,   "+%s", protocollo);  if ( strstr(strDebug, strDebugLevel ) != NULL ) return __INFO__  ; 

    return __NONE__ ;
};  



/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione   : Uri2SingleFieldConnect                            |
|                                                                 |
|  Descrizione : Verifica URI.                                    |
|                                                                 |
|  Verifica la URI passata tramite il parametro --connect e       |
|  restituisce i singoli campi : protocollo, host, porta,         |
|  percorso e risorsa. Al momento i primi 3 risultano             |
|  abbligatori.                                                   |
|                                                                 |
|  Return Code.                                                   |  
|                                                                 |
|    -1 err: Protocol * Non Specificato (len 0)                   | 
|    -2 err: Protocol * Missing Separatore con Hostname (://)     | 
|    -3 err: Hostname * Non Specificato (len 0)                   | 
|    -4 err: Porta    * Valore non numerico                       |
|    -5 err: Porta    * Valore non valido ( ! 1..65535 )          | 
|    -6 err: QueryStr * Missing separatore URL/Query [?]          |              
|    -7 err: QueryStr * Missing QueryString (len 0)               |              
|    -8 err: QueryStr * Lunga più di __DIM_QUERY_STRING__ caratt  |
|    -9 err: QueryStr * Missing separatore variabile              |              
|   -10 err: QueryStr * Missing variable (len 0)                  |               
|   -11 err: QueryStr * Missing carattere di assegnazione valore  |
|   -12 err: Netmask  * Valore non numerico                       |
|   -13 err: Netmask  * Valore non valido ( ! 1..32 )             | 
|                                                                 |
|  dipendenze  : nessuna                                          |
|                                                                 |
|                                                                 |
|  TODO.                                                          |  
|                                                                 |
|  Bisogna aggiungere il controllo sui caratteri validi per       |
|  protocollo, hostname e query string (alfabetici + soli         |
|  caratteri consentiti...)                                       |
|                                                                 |
|                                                                 |
'-----------------------------------------------------------------'*/
extern struct tUri2SingleFieldConnect Uri2SingleFieldConnect(char *uripar) // uri[1024])
{

#define __DIM_QUERY_STRING__ 1024

       int indice_uri = 0;
       int indice = 0;

       char uri[1024];
       char porta[1024];
	   char netmask[1024];

       int DEBUG = 0;

       /*
       .-----------------------------------------------------------.
       | Dichiara la struttura di OUTPUT                           |
       '-----------------------------------------------------------' */
       struct tUri2SingleFieldConnect  rUri2SingleFieldConnect; 
       struct tUri2SingleFieldConnect *pUri2SingleFieldConnect; 

       /*
       .--------------------------------------------------.
       | Alloca lo spazio per contenere la struttura dati |
       | che va a riempire la funzione. La                |
       | seconda istruzione serve per dedicare lo spazio  |
       | allocato alla struttura dati. Successivamente    |
       | inizializza l'intera struttura con NULL ed il    |
       | campo return_code a ZERO.                        |
       '--------------------------------------------------' */
       pUri2SingleFieldConnect = malloc(sizeof(struct tUri2SingleFieldConnect));
       pUri2SingleFieldConnect = &rUri2SingleFieldConnect;
       memset((void*)&rUri2SingleFieldConnect,0,sizeof(rUri2SingleFieldConnect));
       rUri2SingleFieldConnect.return_code = 0;

       /*
       .-----------------------------------------------------------------------.
       | PASSO 1: Estrae il PROTOCOLLO e verifica esistenza (len > 0)          |
       +-----------------------------------------------------------------------+
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       '-----------------------------------------------------------------------' */
       if (uripar == 0) 
       {
            rUri2SingleFieldConnect.return_code = -1 ;
            return rUri2SingleFieldConnect;
       };
       sprintf(uri,"%s",uripar); 

       /*
       .-----------------------------------------------------------------------.
       | PASSO 1: Estrae il PROTOCOLLO e verifica esistenza (len > 0)          |
       +-----------------------------------------------------------------------+
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       '-----------------------------------------------------------------------' */
       indice = 0;

       while ( uri[indice_uri] != 0 && uri[indice_uri] != ':' )
       {
               rUri2SingleFieldConnect.protocollo[indice] = uri[indice_uri];
               indice_uri++;
               indice++;
       }

       rUri2SingleFieldConnect.protocollo[indice] = 0;

       if ( strlen(rUri2SingleFieldConnect.protocollo) == 0 )
       {
            rUri2SingleFieldConnect.return_code = -2 ;
            return rUri2SingleFieldConnect;
       };

       if ( DEBUG == 1 )
	   printf("\n[Uri2SingleFieldConnect] -- uri/indiceUri/protocollo: [%s][%s]", uri, rUri2SingleFieldConnect.protocollo );

       /*
       .-----------------------------------------------------------------------.
       | PASSO 2: Verifica blocco di separazione protocollo|host               |
       +-----------------------------------------------------------------------+
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       '-----------------------------------------------------------------------' */
       if (( uri[indice_uri]     == ':' &&
             uri[indice_uri + 1] == '/' &&
             uri[indice_uri + 2] == '/'))
       {
            indice_uri = indice_uri + 3 ;
       }
       else
       {
            rUri2SingleFieldConnect.return_code = -3 ;
            return rUri2SingleFieldConnect;
       };

   	   if ( DEBUG == 1 )
		   printf("\n[Uri2SingleFieldConnect] -- separatore protocollo/host (://) : [%s]", uri );

       /*
       .-----------------------------------------------------------------------.
       | PASSO 2: Estrae l'HOSTNAME                                            |
       +-----------------------------------------------------------------------+ 
       | Legge fino ai due-punti oppure fine allo slash oppure se finisce il   |
       | buffer cioè se trova un zero-binario. Nel primo caso il carattere     |
       | riscontrato server per specificare la porta TCP/IP, nel secondo caso  |
       | un path per rintracciare una risorsa. L'hostname estratto poi         |
       | andrebbe verificato, una prima verifica è che non sia nullo, ad       |
       | esempio come nel caso seguente: http://:80, http://, http:///         |
       | andrebbero aggiunti altri controlli ad esempio che nel caso di nome   |
       | di dominio i caratteri che lo compongono siano validi.                |
       |                                                                       |
       | terminatori validi :                                                  |
       | * [0] : https://www.tim.it                                            |
       | * [:] : https://www.tim.it:443                                        |
       | * [/] : https://www.tim.it/index.html                                 |
       | * [\] : https://192.168.1.0\24             *network*                  | 
       | * [?] : https://www.tim.it?account=                                   |
       '-----------------------------------------------------------------------' */
       indice = 0;
       while ( uri[indice_uri] != 0 
            && uri[indice_uri] != ':' 
            && uri[indice_uri] != '/' 
            && uri[indice_uri] != '\\'
            && uri[indice_uri] != '?' )
       {
               rUri2SingleFieldConnect.host[indice] = uri[indice_uri];
               indice_uri++;
               indice++;
       }
       rUri2SingleFieldConnect.host[indice] = 0;

       if ( strlen(rUri2SingleFieldConnect.host) == 0 )
       {
          rUri2SingleFieldConnect.return_code = -4 ;
          return rUri2SingleFieldConnect;
       };

       if ( uri[indice_uri] == ':' || uri[indice_uri] == '\\' )
       {
            indice_uri = indice_uri + 1 ;
       }

   	   if ( DEBUG == 1 )
		   printf("\n[Uri2SingleFieldConnect] -- uri/indiceUri/host: [%s][%s]", uri, rUri2SingleFieldConnect.host );

       /*
       .-----------------------------------------------------------------------.
       | PASSO 3: Estrae la NETMASK                                            |
       +-----------------------------------------------------------------------+
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       '-----------------------------------------------------------------------' */
       rUri2SingleFieldConnect.netmask = 0 ;
       if ( uri[indice_uri-1] == '\\' )
       {
         /* ================ Estrazione [...] ================ */
            indice = 0;
            while ( uri[indice_uri] != 0 
			     && uri[indice_uri] != ':' 
				 && uri[indice_uri] != '/' 
				 && uri[indice_uri] != '?' )
            {
                    netmask[indice] = uri[indice_uri];

                    indice_uri++;
                    indice++;
            }
            netmask[indice] = 0;

         /* ============ Verifica Numericità [...] =========== */
            indice = 0;
            while ( netmask[indice] != 0 )
            {
                    if ( netmask[indice] < '0' || netmask[indice] > '9' )
                    {
                         rUri2SingleFieldConnect.return_code = -12 ;
                         return rUri2SingleFieldConnect;
                    }
                    indice++;
            }

         /* ======= Conversione string -> number [...] ======= */
            if (!(rUri2SingleFieldConnect.netmask=atoi(netmask)))
            {
                    rUri2SingleFieldConnect.return_code = -12 ;
                    return rUri2SingleFieldConnect;
            };

         /* ========== Verifica Range NETMASK 1-32 [...] ========= */
            if ( rUri2SingleFieldConnect.netmask < 1 || rUri2SingleFieldConnect.netmask > 32 )
            {
                    rUri2SingleFieldConnect.return_code = -13 ;
                    return rUri2SingleFieldConnect;
            };

	       if ( uri[indice_uri] == ':' || uri[indice_uri] == '\\' )
           {
                indice_uri = indice_uri + 1 ;
           }

           if (DEBUG==1)
             printf("\n[Uri2SingleFieldConnect] -- uri/netmask: [%s][%d]", uri, rUri2SingleFieldConnect.netmask );

       };

       /*
       .-----------------------------------------------------------------------.
       | PASSO 3: Estrae la PORTA                                              |
       +-----------------------------------------------------------------------+
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       |        	                                                       |
       |                                                                       |
       |                                                                       |
       |                                                                       |
       '-----------------------------------------------------------------------' */
       rUri2SingleFieldConnect.porta = 0 ; 
       if ( uri[indice_uri-1] == ':' ) 
       {
         /* ================ Estrazione [...] ================ */
            indice = 0;
            while ( uri[indice_uri] != 0 && uri[indice_uri] != '/' && uri[indice_uri] != '?' )
            {
                    porta[indice] = uri[indice_uri];
 
                    indice_uri++;
                    indice++;
            }
            porta[indice] = 0;

 	 /* ============ Verifica Numericità [...] =========== */
            indice = 0;
            while ( porta[indice] != 0 )
            {
                    if ( porta[indice] < '0' || porta[indice] > '9' )
                    { 
                         rUri2SingleFieldConnect.return_code = -5 ;
                         return rUri2SingleFieldConnect;
                    }
                    indice++;
            }

         /* ======= Conversione string -> number [...] ======= */
            if (!(rUri2SingleFieldConnect.porta=atoi(porta)))
            {
                    rUri2SingleFieldConnect.return_code = -5 ;
                    return rUri2SingleFieldConnect;
            };

         /* ========== Verifica Range Port TCP [...] ========= */
            if ( rUri2SingleFieldConnect.porta < 1 || rUri2SingleFieldConnect.porta > 65635 ) 
            {
                    rUri2SingleFieldConnect.return_code = -6 ;
                    return rUri2SingleFieldConnect;
            };
	
		    if (DEBUG==1)
			printf("\n[Uri2SingleFieldConnect] -- uri/porta: [%s][%d]", uri, rUri2SingleFieldConnect.porta );

       };

       /*
       .-----------------------------------------------------------------------.
       | PASSO 4: Estrae la PATH RISORSA + RISORSA                             |
       +-----------------------------------------------------------------------+
       | Legge e bufferizza fino a fine stringa o inizio query string, o       |
       | trova uno slash. In quel caso sbuferizza e considera path_risorsa     |
       | quanto letto fino a quel momento, anche path_risorsa viene            |
       | bufferizzata a sua volta perchè la path può essere a più livelli      |
       | (es. /dir1/dir2/...). Arrivati a fine stringa oppure al separatore    |
       | con query string il contenuto della bufferizzazione esterna viene     |
       | considerata risorsa.                                                  |
       |                                                                       |
       | esempio 1. /dir1/dir2/filename                                        |    
       |            ---------- --------                                        |
       |            path       risorsa                                         | 
       |                                                                       |
       '-----------------------------------------------------------------------' */
       char tmpbuf[1024];
       tmpbuf[0] = 0;  
       rUri2SingleFieldConnect.risorsa_path[0] = 0; 
       indice = 0;
       while ( uri[indice_uri] != 0 && uri[indice_uri] != '?' )
       {
               tmpbuf[indice] = uri[indice_uri];
               tmpbuf[indice+1] = 0; 
               indice++;

               if ( uri[indice_uri] == '/' )
               {
                    strcat (rUri2SingleFieldConnect.risorsa_path, tmpbuf);
                    indice = 0;
                    tmpbuf[indice] = 0;
               }

               indice_uri++;
       }
       strcpy(rUri2SingleFieldConnect.risorsa, tmpbuf) ; 

       /*
       .-----------------------------------------------------------------------.
       | PASSO 5: Estrae la QUERY STRING                                       |
       +-----------------------------------------------------------------------+
       | effettua il pharse della query string. Questa per essere valida :     |
       |                                                                       |
       | * non deve essere più lunga di 1Kb (1024 byte),                       | 
       | * deve essere nel formato variabile valore,                           |
       | * il separatore variabile valore è l'uguale (=)                       |
       | * il separatore di due coppie di variabile/valore è l'amperstand (&)  |
       | * il valore per una variabile può essere anche nullo                  |
       |                                                                       |
       | (per altre regole di conformità/validità è necessario approfondire,   |
       | Ad esempio non tutti i caratteri validi mi sembra, esistono delle     |
       | codifiche per rappresentare quelli non validi (tipo spazi, & etc...)  |
       | Non so ad esempio che succede se una variabile viene impostata più    |
       | di una volta...                                                       |
       |                                                                       |
       | Esempio di Query string valida : var1=100&var2=CASA&var3=&var4=1      |
       | Il blocco analizza la query string è popola una tabella variabile/    |
       | valore, quindi la quesry string di esempio andrà a popolare in questo |
       | modo la tabella di output :                                           | 
       |                                                                       |
       | Variabile     Valore                                                  |
       | ============= ======================                                  |
       | var1          100                                                     |
       | var2          CASA                                                    |
       | var3                                                                  |
       | var4          1                                                       |
       |                                                                       |
       | Il parse ha bisogno di una variabile di stato (parse_state) per       |
       | analizzare la query string, quello di seguito è il dominio di tutti i |
       | valori possibili :                                                    |
       |                                                                       |
       | 0 - Fine coppia o prima coppia. Attesa nuova variabile o fine         |
       |                                                                       |
       | 1 - Separatore Variabile/Valore (carattere di assegnazione: '=')      |
       |     Atteso nuova valore o separatore coppie nel caso il valore        |
       |     della variabile in analisi è nullo.                               |
       |                                                                       |
       | 2 - Separatore Coppie Variabile/Valore (carattere di separazione '&') | 
       |     Atteso nuova variabile.                                           |
       |                                                                       |
       | return_code :                                                         |
       |                                                                       |
       |  -6 errore: QueryString : Missing separatore URL/Query [?]            |                    
       |  -7 errore: QueryString : Missing QueryString (len 0)                 | 
       |  -8 errore: QueryString : Lunga più di __DIM_QUERY_STRING__ caratteri |
       |  -9 errore: QueryString : Missing separatore variabile                | 
       | -10 errore: QueryString : Missing variable (len 0)                    | 
       | -11 errore: QueryString : Missing carattere di assegnazione valore    | 
       |                                                                       |
       '-----------------------------------------------------------------------' */
       rUri2SingleFieldConnect.queryStringDimTabVariable = 0; 
       rUri2SingleFieldConnect.queryStringLen = 0;

       int parse_state = 0 ; 
       tmpbuf[0] = 0;
       indice = 0;

       /*
       .-----------------------------------------------------.
       | Se la URI non è terminata ma a fine URL non è stato |
       | trovato il carattere di separazione tra URL e Query |
       | String [?] allora è un errore. In caso contrario si |
       | incrementa il vettore di 1 per superare il          |
       | carattere di separazione.                           |
       '-----------------------------------------------------' */
       if ( uri[indice_uri]   != 0 && uri[indice_uri] != '?' )
       {
            rUri2SingleFieldConnect.return_code = -7 ;
            goto Uri2SingleFieldConnect_end ;
       };

       if ( uri[indice_uri+1] == 0 && uri[indice_uri] == '?' )
       {
            rUri2SingleFieldConnect.return_code = -8 ;
            goto Uri2SingleFieldConnect_end ;
       };

       indice_uri++;
 
       while ( indice_uri < strlen(uri) )
	   {
               /*
               .----------------------------------------------------------.
               | -= ERR =- La QueryString non può essere maggiore di 1024 |
               '----------------------------------------------------------' */
               rUri2SingleFieldConnect.queryStringLen++;
               if ( rUri2SingleFieldConnect.queryStringLen > __DIM_QUERY_STRING__ )
               {
                    rUri2SingleFieldConnect.return_code = -9 ;
                    goto Uri2SingleFieldConnect_end ;
               };

               /*
               .----------------------------------------.
               | Lettura in corso di variabile o valore |
               '----------------------------------------' */
               if ( uri[indice_uri] != '=' && uri[indice_uri] != '&' )
               {
                    tmpbuf[indice] = uri[indice_uri];
                    tmpbuf[indice+1] = 0;
                    indice++;
               }; 
 
               /*
               .-----------------------------.
               | carattere di assegnazione   |
               '-----------------------------' */ 
               if ( uri[indice_uri] == '=' )
               {
                    /*
                    .-----------------------------------------------------.
                    | -= ERR =- Missing & o doppio =                      |
                    +-----------------------------------------------------+
                    | Se è stato trovato il carattere di assegnazione ma  |
                    | è in corso la lettura di un valore (state=1)        |
                    | es. var1=asd=, var1==  significa che potrebbe       |
                    | mancare un carattere di separazione variabili (&)   |
                    | oppure è stato inserito 2 volte il carattere di     |
                    | assegnazione.                                       |
                    '-----------------------------------------------------' */ 
                    if ( parse_state == 1 ) 
                    {    
                         rUri2SingleFieldConnect.return_code = -10 ;
                         goto Uri2SingleFieldConnect_end ;
                    };

                    /*
                    .-----------------------------------------------------.
                    | -= ERR =- Missing Variable                          |   
                    +-----------------------------------------------------+
                    | Se è stato trovato il carattere di assegnazione ma  | 
                    | la variabile non è ancora stata definita            |
                    | e. "var1=100&=", "=" è un errore.                   | 
                    |                                                     |
                    |                                                     |
                    |                                                     |
                    '-----------------------------------------------------' */ 
                    if ( strlen(tmpbuf) == 0 )
                    {
                         rUri2SingleFieldConnect.return_code = -11 ;
                         goto Uri2SingleFieldConnect_end ;
                    };
                 
                    /*
                    .---------------------------------.
                    | found variable->name            |
                    '---------------------------------' */
                    rUri2SingleFieldConnect.queryStringTabVariable[rUri2SingleFieldConnect.queryStringDimTabVariable].variable = malloc(strlen(tmpbuf)+1) ;
                    strcpy(rUri2SingleFieldConnect.queryStringTabVariable[rUri2SingleFieldConnect.queryStringDimTabVariable].variable, tmpbuf) ; 
                    
                    indice         = 0 ;
                    tmpbuf[indice] = 0 ;
                    parse_state    = 1 ;
               }
     
               /*
               .--------------------------. 
               | delimitatore fine valore |
               '--------------------------' */ 
               if ( uri[indice_uri] == '&' )
               {
                    /*
                    .-----------------------------------------------------.
                    | -= ERR =- Missing = o doppio &                      |
                    +-----------------------------------------------------+
                    | Se è stato trovato il carattere di assegnazione ma  |
                    | la variabile non è completamente definita           |
                    | es. "var1&", "&", "&&" è un errore.                 |
                    | La variabile può essere nulla (es. var=&) quindi    |
                    | la casistica è da ritenersi valida.                 |
                    '-----------------------------------------------------' */
                    if ( parse_state == 0 ) 
                    {
                         rUri2SingleFieldConnect.return_code = -12 ;
                         goto Uri2SingleFieldConnect_end ;
                    };

                    /*
                    .---------------------------------.
                    | found variable->value           |
                    '---------------------------------' */ 
                    rUri2SingleFieldConnect.queryStringTabVariable[rUri2SingleFieldConnect.queryStringDimTabVariable].value = malloc(strlen(tmpbuf)+1) ;
                    strcpy(rUri2SingleFieldConnect.queryStringTabVariable[rUri2SingleFieldConnect.queryStringDimTabVariable].value, tmpbuf) ; 

                    indice         = 0 ;
                    tmpbuf[indice] = 0 ;
                    parse_state    = 0 ;

                    rUri2SingleFieldConnect.queryStringDimTabVariable++;
               }
               indice_uri++;
       }

       if ( rUri2SingleFieldConnect.queryStringLen > 0 )
       {
            if ( parse_state == 0 ) 
            {    
                 /*
                 .-----------------------------------------------------.
                 | -= ERR =- Missing & o doppio =                      |
                 +-----------------------------------------------------+
                 | Se è stato trovato il carattere di assegnazione ma  |
                 | è in corso la lettura di un valore (state=1)        |
                 | es. var1=asd=, var1==  significa che potrebbe       |
                 | mancare un carattere di separazione variabili (&)   |
                 | oppure è stato inserito 2 volte il carattere di     |
                 | assegnazione.                                       |
                 '-----------------------------------------------------' */
                 if ( strlen(tmpbuf) == 0 ) 
                 {
                      rUri2SingleFieldConnect.return_code = -10 ;
                 }

                 /*
                 .-----------------------------------------------------.
                 | -= ERR =- Missing Variable                          |
                 +-----------------------------------------------------+
                 | Se è stato trovato il carattere di assegnazione ma  |
                 | la variabile non è ancora stata definita            |
                 | e. "var1=100&=", "=" è un errore.                   |
                 |                                                     |
                 |                                                     |
                 |                                                     |
                 '-----------------------------------------------------' */
                 if ( strlen(tmpbuf) > 0 )
                 {  
                      rUri2SingleFieldConnect.return_code = -11 ;
                 };

                 goto Uri2SingleFieldConnect_end ; 
            };

            /*
            .---------------------------------.
            | found variable->value           |
            '---------------------------------' */
            rUri2SingleFieldConnect.queryStringTabVariable[rUri2SingleFieldConnect.queryStringDimTabVariable].value = malloc(strlen(tmpbuf)+1);
            strcpy(rUri2SingleFieldConnect.queryStringTabVariable[rUri2SingleFieldConnect.queryStringDimTabVariable].value, tmpbuf);

            rUri2SingleFieldConnect.queryStringDimTabVariable++;
       };
     
 
Uri2SingleFieldConnect_end: 

       /*
       .---------------------------------------------------------------.
       | Visualizza risultato del parse sull'URI letta.                |
       |                                                               |
       |                                                               |
       |                                                               |
       |                                                               |
       '---------------------------------------------------------------' */
       if ( DEBUG == 1 )
       {
            printf ("\n.-----------------------------------------------------------------------------.");
            printf ("\n|                Funzione Uri2SingleFieldConnect DUMP DEBUG                   |");
            printf ("\n'-----------------------------------------------------------------------------'\n\n");

            printf ("\nX------------------------- Valori di Input -----------------------------------X\n\n");

            printf ("\nuri                  : [%s]\n\n" , uri         );


            printf ("\nX------------------------- Valori di Output ----------------------------------X\n\n");

            printf("\nprotocollo            : [%s]", rUri2SingleFieldConnect.protocollo                ); 
            printf("\nhost                  : [%s]", rUri2SingleFieldConnect.host                      ); 
            printf("\nporta                 : [%d]", rUri2SingleFieldConnect.porta                     ); 
            printf("\nnetmask               : [%d]", rUri2SingleFieldConnect.netmask                   ); 
            printf("\npath                  : [%s]", rUri2SingleFieldConnect.risorsa_path              ); 
            printf("\nrisorsa               : [%s]", rUri2SingleFieldConnect.risorsa                   );
            printf("\nLunghezza QueryString : [%d]", rUri2SingleFieldConnect.queryStringLen            ); 
            printf("\nNumero Variabili      : [%d]", rUri2SingleFieldConnect.queryStringDimTabVariable ); 
            printf("\nreturn-code           : [%d]", rUri2SingleFieldConnect.return_code               );
            printf("\n");

            int indice = 0 ;
            while ( indice < rUri2SingleFieldConnect.queryStringDimTabVariable )
            {
                    printf ("\n[%3d.]  [%30s] = [%30s]", 
                            indice, 
                            rUri2SingleFieldConnect.queryStringTabVariable[indice].variable, 
                            rUri2SingleFieldConnect.queryStringTabVariable[indice].value ); 

                    indice++;
            }
            printf("\n");
       };
       return rUri2SingleFieldConnect ;
};


// __EOF__


