/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 

#define __NONE__    0
#define __INFO__    1
#define __WARN__    2
#define __ERR__     3

/*
.--------------------------------------------------------------------------------------------.
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                  CLIENT Messages Table                                     |
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                                                                            |
'--------------------------------------------------------------------------------------------' */
char *TABMSG2[][3] = {

                     /*
                     .---------------------------------.
                     |                                 |
                     |               Banner            |
                     |                                 |
                     '---------------------------------' */

                     { "MY_CLIENT_TCP__BANNER",
                       "__INFO__",
                       "%software_desc% version %software_version%\n"
                       "%developer_name% (c) %devel_aaaa_start%-%devel_aaaa_stop% Licence %licence_name%v%licence_version%\n\0" }, 

                     { "MY_CLIENT_TCP__FORMATO",
                       "__INFO__",
                       "\nformato: myClientTCP [-c |--connect] protocol://host:port/path/resource?queryString"
                       "\n                     [-U |--auth-user <user>] [-P|--auth-password]"
                       "\n                     [-k |--keystore <filename>] [-p|--password password]"
                       "\n                     [-t |--truststore <filename>"
                       "\n                     [-C |--ciphersuite {cipher1,cipher2,...,cipherN}]"
                       "\n                     [-s |--ssl-version <version>]"
                       "\n                     [-T |--type-operation <type>]"
                       "\n                     [-Ml|--message-language <language>]"
                       "\n                     [-Mt|--message-type <type>]"
                       "\n                     [-M |--message <text>]"
                       "\n                     [-S |--ssl-ciphersuite-server-scan {On|Off}]"
                       "\n                     [-Pc|--ssl-print-cert {On|Off}]"
                       "\n                     [-m |--max-request <max-value>]"
                       "\n                     [-Tt|--tcp-receive-timeout <seconds>]"
                       "\n                     [-o |--output-file <filename>]"
                       "\n                     [-d |--debug {+protocol1+protocol2...+protocolN}]"
                       "\n         myClientTCP [-f |--config <filename>]"
                       "\n         myClientTCP [-h |--help]"
                       "\n         myClientTCP [-v |--version]\n\n" },

                     { "MY_CLIENT_TCP__REMARK",
                       "__INFO__",
                       "\n%remark%\0" },

                     { "MY_CLIENT_TCP__RETURNCODE",
                       "__INFO__",
                       "\n\nExit to %software_return_ok% if everything is OK, %software_return_ko% if there is a problem.\0" },

                     { "MY_CLIENT_TCP__BUGREPORT",
                       "__INFO__",
                       "\nReport bugs to <%developer_email%>.\n\n\0" },

                     { "MY_CLIENT_TCP__VERSION",
                       "__INFO__",
                       "\nVersion %software_version%\n\n\0" },

                     /*
                     .---------------------------------.
                     |                                 |
                     |              Client             |
                     |                                 |
                     '---------------------------------' */
                     { "MY_CLIENT_TCP__CLIENT_CONNECT_PARAMETER_TESTA",
                       "__INFO__",
                       "\n\nX------------------------- Client Connect - Input Value -----------------------------XX"
                       "\nfrontend                    : [%frontend%]"
                       "\nfrontend version            : [%frontend_version%]"
                       "\nurl                         : [%url%]"
                       "\nprotocollo                  : [%protocollo%]"
                       "\nhost                        : [%host%]"
                       "\nnetmask                     : [%netmask%]" 
                       "\nporta                       : [%porta%]"
                       "\nrisorsa path                : [%risorsa_path%]"
                       "\nrisorsa                     : [%risorsa%]"
                       "\nLunghezza QueryString       : [%queryStringLen%]"
                       "\nNumero Variabili            : [%queryStringDimTabVariable%]\n" },

                     { "MY_CLIENT_TCP__CLIENT_CONNECT_PARAMETER_VARURL",
                       "__INFO__",
                       "[%queryStringTabVar_index%]  [%queryStringTabVar_variable%] = [%queryStringTabVar_value%]" },

                     { "MY_CLIENT_TCP__CLIENT_CONNECT_PARAMETER_CODA",
                       "__INFO__",
                       "\nauth user                   : [%auth_user%]"
                       "\nauth password               : [%auth_password%]"
                       "\nkeystore                    : [%keystore%]"
                       "\npassword                    : [%password%]"
                       "\ntruststore                  : [%truststore%]"
                       "\nciphersuite                 : [%ciphersuite%]"
                       "\nssl version                 : [%sslversion%]"
                       "\ntype operation              : [%type_operation%]"
                       "\nmessage_language            : [%message_language%]"
                       "\nmessage_type                : [%message_type%]"
                       "\nmessage                     : [%message%]"
                       "\nssl ciphersuite server scan : [%SSLCipherSuiteServerScan%]"
                       "\nmax Request                 : [%MaxRequest%]"
                       "\ntcp receive timeout         : [%TCPReceiveTimeout%]"
                       "\noutput file                 : [%OutputFile%]"
                       "\ndebug                       : [%debug%]" },

                     /*
                     .---------------------------------.
                     |                                 |
                     |       Controllo Parametri       |
                     |                                 |
                     '---------------------------------' */
                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_MISSING",
                       "__INFO__",
                       "Error. Missing Connection string.\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_PROTOCOLLO_NOSEP",
                       "__INFO__",
                       "Error. Connection string incorrect. Missing :// between protocol and hostname.\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_HOSTNAME",
                       "__INFO__",
                       "Error. Connection string incorrect. Host invalid (missing value).\n" },
					   
                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_NETMASK_NONUM",
                       "__INFO__",
                       "Error. Connection string incorrect. Netmask invalid (missing value or not numeric).\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_NETMASK_NORANGE",
                       "__INFO__",
                       "Error. Connection string incorrect. Netmask invalid (out of range 1..32).\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_PORTA_NONUM",
                       "__INFO__",
                       "Error. Connection string incorrect. Port invalid (missing value or not numeric).\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_PORTA_NORANGE",
                       "__INFO__",
                       "Error. Connection string incorrect. Port invalid (out of range 0..65535).\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_NOSEPURL",
                       "__INFO__",
                       "Error. Connection string incorrect. Missing separatore URL/Query.\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_LEN0",
                       "__INFO__",
                       "Error. Connection string incorrect. Query string invalid (missing).\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_NOTLENMAX",
                       "__INFO__",
                       "Error. Connection string incorrect. Query string invalid (len > 1024 byte).\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_NOSEPVAR",
                       "__INFO__",
                       "Error. Connection string incorrect. Query string missing variable separate character.\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_VARLEN0",
                       "__INFO__",
                       "Error. Connection string incorrect. Query string has variable not definited.\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_NOASSIGN",
                       "__INFO__",
                       "Error. Connection string incorrect. Query string missing a assign character.\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_UNKNOWN",
                       "__INFO__",
                       "Error. Connection string incorrect. Unknown error.\n" },


                     /*
                     .---------------------------------.
                     |                                 |
                     |       Controllo Parametri       |
                     |                                 |
                     '---------------------------------' */
                     { "MY_CLIENT_TCP__ERRORE_CONFIGURATION_FILE_NOTFOUND",
                       "__INFO__",
                       "Error. Configuration file not found or you have not permission.\n" },


                     /*
                     .----------------------------------------------------------------.
                     |                                                                |
                     |         Coppie Parametro|Valore in cui manca il valore         |
                     |                                                                |
                     '----------------------------------------------------------------' */

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_CONNECT",
                       "__INFO__",
                       "Error. Missing for -c|--connect parameter the connection string.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_AUTH_USER",
                       "__INFO__",
                       "Error. Missing for -U|--auth-user parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_AUTH_PASSWORD",
                       "__INFO__",
                       "Error. Missing for -P|--auth-password parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_HTTPPROXY_HOST",
                       "__INFO__",
                       "Error. Missing for -pH|--http-proxy-host parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_HTTPPROXY_PORT",
                       "__INFO__",
                       "Error. Missing for -pP|--http-proxy-port parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_HTTPPROXY_USER",
                       "__INFO__",
                       "Error. Missing for -pU|--http-proxy-user parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_HTTPPROXY_PASSWORD",
                       "__INFO__",
                       "Error. Missing for -pU|--http-proxy-password parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_KEYSTORE",
                       "__INFO__",
                       "Error. Missing for -k|--keystore parameter the keystore file.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_KEYSTORE_PASSWORD",
                       "__INFO__",
                       "Error. Missing for -p|--password parameter the secret string.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_TRUSTSTORE",
                       "__INFO__",
                       "Error. Missing for -t|--truststore parameter the truststore file.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_CIPHERSUITE",
                       "__INFO__",
                       "Error. Missing for -C|--chipersuite parameter ciphersuite valid value.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_VERSION_SSL",
                       "__INFO__",
                       "Error. Missing for -s|--ssl-version parameter version ssl valid value.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_TYPE_OPERATION",
                       "__INFO__",
                       "Error. Missing for -T|--type-operation parameter value.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_MESSAGE_LANGUAGE",
                       "__INFO__",
                       "Error. Missing for -Ml|--message-language parameter value.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_MESSAGE_TYPE",
                       "__INFO__",
                       "Error. Missing for -Mt|--message-type parameter value.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_MESSAGE",
                       "__INFO__",
                       "Error. Missing for -M|--message parameter value.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_SSL_CIPHERSUITE_SERVER_SCAN",
                       "__INFO__",
                       "Error. Missing for -S|--ssl-ciphersuite-server-scan parameter value.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_SSL_PRINT_CERT",
                       "__INFO__",
                       "Error. Missing for -Pc|--ssl-print-cert parameter value.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_MAX_REQUEST",
                       "__INFO__",
                       "Error. Missing for -m|--max-request parameter value.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_TCP_RECEIVE_TIMEOUT",
                       "__INFO__",
                       "Error. Missing for -Tt|--tcp-receive-timeout parameter value.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_OUTPUT_FILE",
                       "__INFO__",
                       "Error. Missing for -o|--output-file parameter value.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_CONFIGURATION_FILE",
                       "__INFO__",
                       "Error. Missing for -f|--config parameter configuration file.\n" },

                     { "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_DEBUG",
                       "__INFO__",
                       "Error. Missing for -d|--debug parameter.\n" },


                     /*
                     .------------------------------------------------------------------.
                     |                                                                  |
                     |       Coppie Parametro/Valore specificate più di una volta       |
                     |                                                                  |
                     '------------------------------------------------------------------' */
                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_CONNECT",
                       "__INFO__",
                       "Error. Duplicate -c|--connect parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_AUTH_USER",
                       "__INFO__",
                       "Error. Duplicate -U|--auth-user parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_AUTH_PASSWORD",
                       "__INFO__",
                       "Error. Duplicate -P|--auth-password parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_HTTPPROXY_HOST",
                       "__INFO__",
                       "Error. Duplicate -pH|--http-proxy-host parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_HTTPPROXY_PORT",
                       "__INFO__",
                       "Error. Duplicate -pP|--http-proxy-port parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_HTTPPROXY_USER",
                       "__INFO__",
                       "Error. Duplicate -pU|--http-proxy-user parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_HTTPPROXY_PASSWORD",
                       "__INFO__",
                       "Error. Duplicate -pU|--http-proxy-password parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_KEYSTORE",
                       "__INFO__",
                       "Error. Duplicate -k|--keystore parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_KEYSTORE_PASSWORD",
                       "__INFO__",
                       "Error. Duplicate -p|--password parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_TRUSTSTORE",
                       "__INFO__",
                       "Error. Duplicate -t|--truststore parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_CIPHERSUITE",
                       "__INFO__",
                       "Error. Duplicate -C|--chipersuite parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_SSLVERSION",
                       "__INFO__",
                       "Error. Duplicate -s|--ssl-version parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_TYPE_OPERATION",
                       "__INFO__",
                       "Error. Duplicate -T|--type-operation parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_MESSAGE_LANGUAGE",
                       "__INFO__",
                       "Error. Duplicate -Ml|--message-language parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_MESSAGE_TYPE",
                       "__INFO__",
                       "Error. Duplicate -Mt|--message-type parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_MESSAGE",
                       "__INFO__",
                       "Error. Duplicate -M|--message parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_SSL_CIPHERSUITE_SERVER_SCAN",
                       "__INFO__",
                       "Error. Duplicate -S|--ssl-ciphersuite-server-scan parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_SSL_PRINT_CERT",
                       "__INFO__",
                       "Error. Duplicate -S|--ssl-print-cert parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_MAX_REQUEST",
                       "__INFO__",
                       "Error. Duplicate -m|--max-request parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_TCP_RECEIVE_TIMEOUT",
                       "__INFO__",
                       "Error. Duplicate -Tt|--tcp-receive-timeout parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_OUTPUT_FILE",
                       "__INFO__",
                       "Error. Duplicate -s|--output-file parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_DEBUG",
                       "__INFO__",
                       "Error. Duplicate -d|--debug parameter.\n" },

                     { "MY_CLIENT_TCP__ERRORE_UNKNOWN_PARAMETRO",
                       "__INFO__",
                       "Error. Unknown Parameter.\n" },


                     /*
                     .------------------------------------------------------------------.
                     |                                                                  |
                     |           Stringa di connessione non valida. Motivazioni.        |
                     |                                                                  |
                     '------------------------------------------------------------------' */
                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_PROTOCOLLO",
                       "__INFO__",
                       "Error. Connection string incorrect. Protocol invalid (missing value).\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_PROTOCOLLO_NOSEP",
                       "__INFO__",
                       "Error. Connection string incorrect. Missing :// between protocol and hostname.\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_HOSTNAME",
                       "__INFO__",
                       "Error. Connection string incorrect. Host invalid (missing value).\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_PORTA_NONUM",
                       "__INFO__",
                       "Error. Connection string incorrect. Port invalid (missing value or not numeric).\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_PORTA_NORANGE",
                       "__INFO__",
                       "Error. Connection string incorrect. Port invalid (out of range 0..65535).\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_NOSEPURL",
                       "__INFO__",
                       "Error. Connection string incorrect. Missing separatore URL/Query.\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_LEN0",
                       "__INFO__",
                       "Error. Connection string incorrect. Query string invalid (missing).\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_NOTLENMAX",
                       "__INFO__",
                       "Error. Connection string incorrect. Query string invalid (len > 1024 byte).\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_NOSEPVAR",
                       "__INFO__",
                       "Error. Connection string incorrect. Query string missing variable separate character.\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_VARLEN0",
                       "__INFO__",
                       "Error. Connection string incorrect. Query string has variable not definited.\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_NOASSIGN",
                       "__INFO__",
                       "Error. Connection string incorrect. Query string missing a assign character.\n" },

                     { "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_UNKNOWN",
                       "__INFO__",
                       "Error. Connection string incorrect. Unknown error.\n" },


                     /*
                     .---------------------------------.
                     |                                 |
                     |       Controllo Parametri       |
                     |                                 |
                     '---------------------------------' */
                     { "MY_CLIENT_TCP__ERRORE_CONFIGURATION_FILE_NOTFOUND",
                       "__INFO__",
                       "Error. Configuration file not found or you have not permission.\n" },


                     /*
                     .------------------------------------.
                     |                                    |
                     |       Messaggi Variabili           |
                     |                                    |
                     '------------------------------------' */

                     /*
                     .---------------------------------------------------------------------------.
                     |                                                                           |
                     |  Messaggio   : clientConnectPrintHead                                     |
                     |                                                                           |
                     |  Descrizione : Print Head Row                                             |
                     |                                                                           |
                     |  Parameter.                                                               |
                     |                                                                           |
                     |     %A - Protocol                                                         |
                     |     %B - Host Target                                                      |
                     |     %C - Netmask Len                                                      |
                     |     %D - Port Target                                                      |
                     |     %E - Path Resouce Target                                              |
                     |     %F - Resource Target                                                  |
                     |     %G - Host Target Resolved (IP)                                        |
                     |     %H - Package Request Len                                              |
                     |                                                                           |
                     |  Mask.                                                                    |
                     |                                                                           |
                     |     PING %A://%B:%D%E%F (%G) %H(%H) bytes of data.                        |
                     |                                                                           |
                     |  Example.                                                                 |
                     |                                                                           |
                     |     PING http://www.domain.it:80/ (192.168.1.1) 77(77) bytes of data.     |
                     |                                                                           |
                     '---------------------------------------------------------------------------'*/
                     { "MY_CLIENT_TCP__MSG_HEAD",
                       "__INFO__",
                       "PING %protocollo%://%host%:%porta%%risorsa_path%%risorsa% (%TCPIp%) "
                                  "%ServerRequestBufferLen%(%ServerRequestBufferLen%) bytes of data.\n" }, 

                     /*
                     .---------------------------------------------------------------------------.
                     |                                                                           |
                     |  Messaggio   : clientConnectPrintHead_network                             |
                     |                                                                           |
                     |  Descrizione : Print Head Row (network)                                   |
                     |                                                                           |
                     |  Parameter.                                                               |
                     |                                                                           |
                     |     %A - Protocol                                                         |
                     |     %B - Host Target                                                      |
                     |     %C - Netmask Len                                                      |
                     |     %D - Port Target                                                      |
                     |     %E - Path Resouce Target                                              |
                     |     %F - Resource Target                                                  |
                     |     %G - Host Target Resolved (IP)                                        |
                     |     %H - Package Request Len                                              |
                     |                                                                           |
                     |  Mask.                                                                    |
                     |                                                                           |
                     |     PING %A://%B\%C:%D%E%F (%G) %H(%H) bytes of data.                     |
                     |                                                                           |
                     |  Example.                                                                 |
                     |                                                                           |
                     |     PING http://www.domain.it\24:80/ (192.168.1.1) 77(77) bytes of data.  |
                     |                                                                           |
                     '---------------------------------------------------------------------------'*/
                     { "MY_CLIENT_TCP__MSG_HEAD_NETWORK",
                       "__INFO__",
                       "PING %protocollo%://%host%\\%netmask%:%porta%%risorsa_path%%risorsa% (%TCPIp%) "
                                  "%ServerRequestBufferLen%(%ServerRequestBufferLen%) bytes of data.\n" }, 

                     /*
                     .---------------------------------------------------------------------------.
                     |                                                                           |
                     |  Messaggio   : clientConnectPrintDetails                                  |
                     |                                                                           |
                     |  Descrizione : Print Detail Row                                           |
                     |                                                                           |
                     |  Parameter   :                                                            |
                     |                                                                           |
                     |     %A - Server Response Buffer Len                                       |
                     |     %B - Host Target                                                      |
                     |     %C - Port Target                                                      |
                     |     %D - Protocollo                                                       |
                     |     %E - Sequenza                                                         |
                     |     %F - Tempo totale connessione                                         |
                     |     %G - SSL CipherSuite Algoritmo Simmetrico / Versione SSL utilizzata   |
                     |     %H - SSL CipherSuite Algoritmo Simmetrico                             |
                     |     %I - SSL CipherSuite Algoritmo Simmetrico / Lunghezza Chiave          |
                     |     %L - Server Message Response                                          |
                     |     %M - Server Message Response - Description                            |
                     |     %N - Esito Connessione                                                |
                     |                                                                           |
                     |  mask        :                                                            |
                     |                                                                           |
                     |     "%A byte from %B:%C %D_seq=%E time=%F s\t                             |
                     |                                            [ %G | %H | %I ]\t [%L] %M %N  |
                     |                                                                           |
                     |  example     :                                                            |
                     |                                                                           |
                     |     000128 byte from open.org:443 http_seq=001 time=27 s                  |
                     |             [ TLSv1/SSLv3 | RC4-MD5 | 128 ]                               |
                     |                   [405] HTTP.RECV.Method Not Allowed   fail               |
                     |                                                                           |
                     '---------------------------------------------------------------------------'*/
                     { "MY_CLIENT_TCP__MSG_DETAILS",
                       "__INFO__",
                       "%ServerResponseBufferLen% byte "
                           "from %host%:%porta% %protocollo%_seq=%sequence% "
                                "time=%tempoTotaleSingolaConnessione% s"
                                     "\t [ %SSLCipherSuite_AlgSim_SSLVersion% | %SSLCipherSuite% | %SSLCipherSuite_AlgSim_LenKey% ]"
                                     "\t [%msg%] %msgDesc% %returnClientConnect% \n" },

                     /*
                     .---------------------------------------------------------------------------.
                     |                                                                           |
                     |  Messaggio   : clientConnectPrintStatistic                                |
                     |                                                                           |
                     |  Descrizione : Print Statistic Session Terminated                         |
                     |                                                                           |
                     |  Parameter.                                                               |
                     |                                                                           |
                     |     %A - Protocol                                                         |
                     |     %B - Host Target                                                      |
                     |     %C - Port Target                                                      |
                     |     %D - Path Resouce Target                                              |
                     |     %E - Resource Target                                                  |
                     |     %F - Packet Trasmitted                                                |
                     |     %G - Packet Received                                                  |
                     |     %H - Durata Sessione                                                  |
                     |     %I - Min Single Connection                                            |
                     |     %L - Max Single Connection                                            |
                     |                                                                           |
                     |  Mask.                                                                    |
                     |                                                                           |
                     |     --- %A://%B:%C%D%E ping statistics ---                                |
                     |     %F packets transmitted, %G received, 0% packet loss, time %Hms        |
                     |     rtt min/avg/max/mdev = %I/0.000/%L/0.000 s                            |
                     |                                                                           |
                     |  Example.                                                                 |
                     |                                                                           |
                     |     --- ssl://www.domain.it:443 ping statistics ---                       |
                     |     14 packets transmitted, 14 received, 0% packet loss, time 1.97e+04s   |
                     |     rtt min/avg/max/mdev = 0/0.000/1.22e+03/0.000 s                       |
                     |                                                                           |
                     '---------------------------------------------------------------------------'*/
                     { "MY_CLIENT_TCP__MSG_STATISTIC",
                       "__INFO__",
                       "--- %protocollo%://%host%:%porta%%risorsa_path%%risorsa% ping statistics ---\n"
                       "%contaPacchettiInviati% packets transmitted, "
                            "%contaPacchettiRicevuti% received, 0% packet loss, time %SessionDurationTime%s\n"
                       "rtt min/avg/max/mdev = %MinConnectionTime%/0.000/%MaxConnectionTime%/0.000 s\n" },

                     /*
                     .----------------------------------------------------------------------------.
                     |                                                                            |
                     |  Messaggio   : clientListProposalsHandshakErr                              |
                     |                                                                            |
                     |  Descrizione : Print Message Error Handshake CipherSuite.                  |
                     |                                                                            |
                     |  Parameter.                                                                |
                     |                                                                            |
                     |     %ClientListProposalsNum% - Numero di Ciphers proposte dal client       |
                     |     %ClientListProposals%    - Lista delle Ciphers proposte dal client     |
                     |                                                                            |
                     |  Mask.                                                                     |
                     |                                                                            |
                     |     +++ ciphersuite handeshake error                                       |
                     |               [%ClientListProposalsNum%][%ClientListProposals%]            |
                     |                                                                            |
                     |  Example.                                                                  |
                     |                                                                            |
                     |  +++ ciphersuite handeshake error [79][ECDHE-ECDSA-AES256-GCM-SHA384:      |
                     |  ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:SRP-DSS-AES-256-CBC-SHA: |
                     |  SRP-RSA-AES-256-CBC-SHA:SRP-AES-256-CBC-SHA:DH-DSS-AES256-GCM-SHA384:     |
                     |  DHE-DSS-AES256-GCM-SHA384:DH-RSA-AES256-GCM-SHA384:DHE-DSS-AES256-SHA256: |
                     |  DH-RSA-AES256-SHA256:DH-DSS-AES256-SHA256:DHE-DSS-AES256-SHA:...          |
                     |                                                                            |
                     '----------------------------------------------------------------------------'*/
                     { "MY_CLIENT_SSL__CIPHERS_CLIENT_PROP",
                       "__INFO__",
                       "+++ ciphersuite handshake error [%ClientListProposalsNum%][%ClientListProposals%]\n" },

                     /*
                     .---------------------------------.
                     |                                 |
                     |              Tappo              |
                     |                                 |
                     '---------------------------------' */
                     { "END",
                       "NONE",
                       "End of Message Table. *Warning* Message not found.\n" }

};

