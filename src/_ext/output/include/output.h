/*
# Copyright (C) 2008-2011 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/


#define __NONE__    0
#define __INFO__    1
#define __WARN__    2
#define __ERR__     3

/*
.---------------------------------------------------.
|            Restituisce la Descrizione             |
|          di un messaggio dato il suo ID           |  
'---------------------------------------------------' */ 
extern int PrintMessage(int debug_level, char *TABMSG[][3], char *IDMsg, char *TabVariabili[][3], 
						     short int rowsBefore, short int rowsAfter, short int viewType); 

/*
.---------------------------------------------------.
|           Visualizza un buffer dati in            |
|             formato DUMP Esadecimale              |
'---------------------------------------------------' */
extern void dump(const unsigned char *data_buffer, const unsigned int length);


/*
.---------------------------------------------------.
|     Traduce la stringa di debug nel livello       |
|      di servizio per il singolo protocollo        |
'---------------------------------------------------' */
int GetDebugLevel ( char *strDebug, char *protocollo ); 


/*
.---------------------------------------------------.
|          Suddivide l'URL in singoli campi         |
'---------------------------------------------------' */
struct t_queryStringTabVariable
{
       char *variable ;
       char *value    ;
};

struct tUri2SingleFieldConnect
{
       char protocollo[1024]              ;
       char host[1024]                    ;
	   unsigned int netmask               ;
       unsigned int porta                 ;
       char risorsa_path[1024]            ;
       char risorsa[1024]                 ;
       int  queryStringDimTabVariable     ; 
       int  queryStringLen                ;
       struct t_queryStringTabVariable 
            queryStringTabVariable[10]    ; 
       int  return_code                   ;
};

extern struct tUri2SingleFieldConnect Uri2SingleFieldConnect(char *buffer); //buffer[1024]); 


///////////////////////////////////////////////////////////////////////////////////////////////
//+=============================================================================================
//| libreria string
//+=============================================================================================
extern char *replaceStr(const char *str, const char *strOLD, const char *strNEW);
extern char *mReplaceStr (const char *str, const char *TabVariabili[][3]);
extern char *my_itoa_OLD ( int val, int base );
extern char *my_itoa ( int val, char *mask );
extern char *my_dtoa ( double val, char *mask );
extern char *my_ltoa ( long val, char *mask ); 
extern char *my_stos ( char *strin, char *mask );
//+============================================================================================
//////////////////////////////////////////////////////////////////////////////////////////////
 


// __EOF__


