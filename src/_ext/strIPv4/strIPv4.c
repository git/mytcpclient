/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 

/*
.=====================================================================.
|                                                                     |
    Libreria         : _EXT/strIPv4

    Descrizione      : Funzioni legate ad IPv4 per la conversione 
                       da un formato ad un altro. 
                
                       Tabella delle conversioni implementate  
                       ---------------------------------------------
                                 | Str | Char4 | Long | StrBinary 
                       ----------|-----|-------|------|-------------
                       Str       | -   | Yes   |(Yes) |(Yes)         
                       Char4     | Yes | -     |(Yes) | Yes          
                       Long      |(Yes)|(Yes)  | -    | Yes          
                       StrBinary |(Yes)| Yes   | Yes  | -            
                                                                    
                       ---
                       (*) i Yes tra parentesi identificano le 
                           conversiono composte da 2 conversioni 
                           elementari.
 
    Elenco funzioni

       funzione                   descrizione
   -----------------------------------------------------------------
    1. convIPv4Str2Char4          conv. da stringa a char[4] 
    2. convIPv4Str2Long           conv. da stringa a Long 
    3. convIPv4Str2StringBinary   conv. da stringa a stringa binaria  
   -----------------------------------------------------------------
    4. convIPv4Char42Str          conv. da char[4] a stringa 
    5. convIPv4Char42Long         conv. da char[4] a Long 
    6. convIPv4Char42StringBinary conv. da char[4] a stringa binaria
   -----------------------------------------------------------------
    7. convIPv4Long2Str           conv. da long a stringa 
    8. convIPv4Long2Char4         conv. da long a char[4] 
    9. convIPv4Long2StringBinary  conv. da long a stringa binaria 
   -----------------------------------------------------------------
   10  convIPv4StringBinary2Str   conv. da stringa binaria a stringa 
   11. convIPv4StringBinary2Char4 conv. da stringa binaria a char[4]
   12. convIPv4StringBinary2Long  conv. da stringa binaria a Long 
   -----------------------------------------------------------------
   13. IPv4NetmaskValidation      test validazione netmask 
   14. IPv4LenNet2Netmask         converte lenNet in netmask
   15. getListHostNetworkIPv4     lista IP appartenenti ad una net 
   -----------------------------------------------------------------

|                                                                    |
'===================================================================='
*/

/*
.--------------.
| headers LIBC |
'--------------' */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "include/strIPv4.h"

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : convIPv4Str2Char4                                |
|                                                                 |
|  Descrizione : Converte un IPv4 da formato string a formato     | 
|                char[4], ossia in un array di 4 elementi char.   |
|                192.168.0.1->c[0]=192;c[1]=168;c[2]=0;c[3]=1     |
|                                                                 |
|  return-code : struttura complessa con IPv4[4] e return-code    |
|                                                                 |
|  dipendenza  : nessuna                                          |
|                                                                 |
|                                                                 |
|                                                                 |
|  TODO       :                                                   |
|                                                                 |
'-----------------------------------------------------------------'*/
struct tConvIPv4Str2Char4 convIPv4Str2Char4 (char IPstr[16])
{
    struct tConvIPv4Str2Char4 rConvIPv4Str2Char4 ;
    rConvIPv4Str2Char4.ret = 0 ;

    char strByte[4];
    int  iStrByte;
    int  countStrByte=0;

    int  iIPstr = 0;

    iStrByte=0;
    strByte[iStrByte]=0;

    while ( IPstr[iIPstr] != 0 )
    {
       /*
       .-------------------------.
       |      trovata cifra      |
       '-------------------------' */ 
       if (IPstr[iIPstr] >= '0' && IPstr[iIPstr] <= '9')
       {
           /*
           .-----------------------------------. 
           | Elimina lo zero non significativo |
           '-----------------------------------' */ 
           if (iStrByte==1 && 
                 strByte[0]=='0')  
                    iStrByte=0;

           /*
           .--------------------------------------------.
           | accodamento in strByte della cifra trovata |
           '--------------------------------------------' */ 
           strByte[iStrByte]=IPstr[iIPstr];                            
           strByte[iStrByte+1]='\0';
           iStrByte++;

           /*
           ,---------------------.
           | _ERROR_ cifra > 255 |
           '---------------------' */
           if ( atoi(strByte) > 255 ) { 
               rConvIPv4Str2Char4.ret = -1;
               return rConvIPv4Str2Char4 ;
           }   
       }

       /*
       .----------------------------------------------.
       |      trovato punto di separazione cifre      |  
       '----------------------------------------------' */ 
       if ( IPstr[iIPstr] == '.' )
       {
           /*
           ,---------------------.
           | _ERROR_ cifra nulla |
           '---------------------' */
           if ( strByte[0] == 0 ) { 
               rConvIPv4Str2Char4.ret = -2;
               return rConvIPv4Str2Char4 ;
           }

           /*
           .-----------------------------------.
           | _ERRORE_ trovate più di 4 StrByte |
           '-----------------------------------' */
           if ( countStrByte > 3 ) { 
               rConvIPv4Str2Char4.ret = -4; 
               return rConvIPv4Str2Char4 ; 
           } 

           //-------------------------------------------------------------------
           rConvIPv4Str2Char4.IPv4[countStrByte] = atoi(strByte); 
           //-------------------------------------------------------------------

           iStrByte=0;
           strByte[iStrByte]=0;
        
           countStrByte++;
       }

       /*
       .-------------------------------------------------------------.
       |   __ERRORE_ trovato un carattere non valido ( != 0-9,. )    |
       '-------------------------------------------------------------' */
       if ((IPstr[iIPstr] < '0' || IPstr[iIPstr] > '9') && IPstr[iIPstr] != '.' ) { 
           rConvIPv4Str2Char4.ret = -3;
           return rConvIPv4Str2Char4 ;
       };
    
       iIPstr++;
    };

    // =========== cattura l'ultima cifra ====================================== (inizio) 

    /*
    ,---------------------.
    | _ERROR_ cifra nulla |
    '---------------------' */
    if ( strByte[0] == 0 ) { 
       rConvIPv4Str2Char4.ret = -2;
       return rConvIPv4Str2Char4 ;
    }

    /*
    ,-----------------------------------.
    | _ERRORE_ trovate più di 4 StrByte |
    '-----------------------------------' */
    if ( countStrByte != 3 ) { 
          rConvIPv4Str2Char4.ret = -4;
          return rConvIPv4Str2Char4 ;
    }
 
    //-------------------------------------------------------------------
    rConvIPv4Str2Char4.IPv4[countStrByte] = atoi(strByte);
    //-------------------------------------------------------------------
    
    return rConvIPv4Str2Char4 ;
}; 

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : convIPv4Str2Long                                 |
|                                                                 |
|  Descrizione : Converte un IPv4 da formato string a formato     |
|                Long. (esempio: "192.168.0.1"->2345345234        |
|                                                                 |
|                                                                 |
|  return-code : struttura complessa con IPv4Long e return-code   |
|                                                                 |
|  dipendenza  : nessuna                                          |
|                                                                 |
|                                                                 |
|                                                                 |
|  TODO       :                                                   |
|                                                                 |
'-----------------------------------------------------------------'*/
struct tConvIPv4Str2Long convIPv4Str2Long (char IPstr[16])
{
    struct tConvIPv4Str2Long rConvIPv4Str2Long ;
    rConvIPv4Str2Long.ret = 0 ;

    /*
    .-------------------------------------.
    | converte la stringa IPv4 in Char[4] |
    '-------------------------------------' */
    struct tConvIPv4Str2Char4 rConvIPv4Str2Char4 ;
    rConvIPv4Str2Char4 = convIPv4Str2Char4(IPstr);
    if (rConvIPv4Str2Char4.ret < 0) {
       rConvIPv4Str2Long.ret = rConvIPv4Str2Char4.ret;
       return rConvIPv4Str2Long ;
    }

    /*
    .---------------------------------.
    | converte l'IPv4 Char[4] in Long |
    '---------------------------------' */
    struct tConvIPv4Char42Long rConvIPv4Char42Long ;
    rConvIPv4Char42Long = convIPv4Char42Long (rConvIPv4Str2Char4.IPv4);
    if (rConvIPv4Char42Long.ret < 0) {
       rConvIPv4Str2Long.ret = rConvIPv4Char42Long.ret;
       return rConvIPv4Str2Long ;
    }

    rConvIPv4Str2Long.IPv4Long = rConvIPv4Char42Long.IPv4Long ;

    return rConvIPv4Str2Long ;
};

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : convIPv4Str2StringBinary                         |
|                                                                 |
|  Descrizione : Converte un IPv4 da formato string a formato     |
|                StringBinary (esempio: "192.168.0.1"->1011111100 |
|                100000001111110101010"                           |
|                                                                 |
|  return-code : struttura complessa con IPv4Long e return-code   |
|                                                                 |
|  dipendenza  : nessuna                                          |
|                                                                 |
|                                                                 |
|                                                                 |
|  TODO       :                                                   |
|                                                                 |
'-----------------------------------------------------------------'*/
struct tConvIPv4Str2StringBinary convIPv4Str2StringBinary (char IPstr[16])
{
    struct tConvIPv4Str2StringBinary rConvIPv4Str2StringBinary ;
    rConvIPv4Str2StringBinary.ret = 0 ;

    /*
    .-------------------------------------.
    | converte la stringa IPv4 in Char[4] |
    '-------------------------------------' */
    struct tConvIPv4Str2Char4 rConvIPv4Str2Char4 ;
    rConvIPv4Str2Char4 = convIPv4Str2Char4(IPstr);
    if (rConvIPv4Str2Char4.ret < 0) {
       rConvIPv4Str2StringBinary.ret = rConvIPv4Str2Char4.ret;
       return rConvIPv4Str2StringBinary ;
    }

    /*
    .-----------------------------------------.
    | converte l'IPv4 Char[4] in stringBinary |
    '-----------------------------------------' */
    struct tConvIPv4Char42StringBinary rConvIPv4Char42StringBinary ;
    rConvIPv4Char42StringBinary = convIPv4Char42StringBinary (rConvIPv4Str2Char4.IPv4);
    if (rConvIPv4Char42StringBinary.ret < 0) {
       rConvIPv4Str2StringBinary.ret = rConvIPv4Char42StringBinary.ret;
       return rConvIPv4Str2StringBinary ;
    }

    strcpy(rConvIPv4Str2StringBinary.IPv4binaryString,rConvIPv4Char42StringBinary.IPv4binaryString) ;

    return rConvIPv4Str2StringBinary ;

};

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : convIPv4Char42Str                                |
|                                                                 |
|  Descrizione : Converte un IPv4 da formato char[4] a formato    |
|                string, ossia da un array di 4 elementi char.    |
|                c[0]=192;c[1]=168;c[2]=0;c[3]=1->"192.168.0.1"   |
|                                                                 |
|  return-code : struttura complessa con IPv4Str e return-code    |
|                                                                 |
|  dipendenza  : nessuna                                          |
|                                                                 |
|                                                                 |
|                                                                 |
|  TODO       :                                                   |
|                                                                 |
'-----------------------------------------------------------------' */
struct tConvIPv4Char42Str convIPv4Char42Str(unsigned char IPv4Char4[4])
{
    struct tConvIPv4Char42Str rConvIPv4Char42Str ;

    rConvIPv4Char42Str.ret = 0 ;

    sprintf(rConvIPv4Char42Str.IPv4Str, "%d.%d.%d.%d", IPv4Char4[0], IPv4Char4[1], IPv4Char4[2], IPv4Char4[3] ); 

    return rConvIPv4Char42Str ;
};

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : convIPv4Char42Long                               |
|                                                                 |
|  Descrizione : Converte un IPv4 da formato char[4] a formato    |
|                long, ossia da un array di 4 elementi char.      |
|                c[0]=192;c[1]=168;c[2]=0;c[3]=1->3234345         |
|                                                                 |
|  return-code : struttura complessa con IPv4Long e return-code   |
|                                                                 |
|  dipendenza  : nessuna                                          |
|                                                                 |
|                                                                 |
|                                                                 |
|  TODO       :                                                   |
|                                                                 |
'-----------------------------------------------------------------'*/
struct tConvIPv4Char42Long convIPv4Char42Long(unsigned char IPv4Char4[4])
{
    struct tConvIPv4Char42Long rConvIPv4Char42Long ;
    rConvIPv4Char42Long.ret = 0 ;

    rConvIPv4Char42Long.IPv4Long = (IPv4Char4[0] * 256 * 256 * 256) + \
                                   (IPv4Char4[1] * 256 * 256) + \
                                   (IPv4Char4[2] * 256) + \
                                    IPv4Char4[3] ;
    return rConvIPv4Char42Long ;
};     

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : convIPv4Char42StringBinary                       |
|                                                                 |
|  Descrizione : Converte un IPv4 da formato char[4] a formato    |
|                stringBinary. (esempio :                         |
|                c[0]=192;c[1]=168;c[2]=0;c[3]->                  |
|                1111110000001111010101010111111001               |
|                                                                 |
|  return-code : struttura complessa con IPv4StringBinary e ret   |
|                                                                 |
|  dipendenza  : ConvIPv4Char42Long                               |
|                ConvIPv4Long2StringBinary                        |
|                                                                 |
|                                                                 |
|  TODO       :                                                   |
|                                                                 |
'-----------------------------------------------------------------'*/
struct tConvIPv4Char42StringBinary convIPv4Char42StringBinary(unsigned char IPv4char4[4])
{

    struct tConvIPv4Char42StringBinary rConvIPv4Char42StringBinary ;

    struct tConvIPv4Char42Long rConvIPv4Char42Long ;
    rConvIPv4Char42Long = convIPv4Char42Long (IPv4char4);
    if (rConvIPv4Char42Long.ret < 0) {
       rConvIPv4Char42StringBinary.IPv4binaryString[0] = 0;
       rConvIPv4Char42StringBinary.ret = -1 ;
       return rConvIPv4Char42StringBinary ;
    }

    struct tConvIPv4Long2StringBinary rConvIPv4Long2StringBinary;
    rConvIPv4Long2StringBinary = convIPv4Long2StringBinary (rConvIPv4Char42Long.IPv4Long);
    if (rConvIPv4Long2StringBinary.ret < 0) {
       rConvIPv4Char42StringBinary.IPv4binaryString[0] = 0;
       rConvIPv4Char42StringBinary.ret = -2 ;
       return rConvIPv4Char42StringBinary ;
    }

    rConvIPv4Char42StringBinary.ret = 0 ;
    strcpy(rConvIPv4Char42StringBinary.IPv4binaryString,rConvIPv4Long2StringBinary.IPv4binaryString);
    return rConvIPv4Char42StringBinary ;
}

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : ConvIPv4Long2Str                                 |
|                                                                 |
|  Descrizione : Converte un IPv4 da Long a stringa               |
|                4--------- -> 255.0.255.0                        |
|                                                                 |
|  return-code :                                                  |
|                                                                 |
|  dipendenza  : ConvIPv4Long2Char4                               |
|                ConvIPv4Char42Str                                |
|                                                                 |
|                                                                 |
|  TODO        :                                                  |
|                                                                 |
'-----------------------------------------------------------------'*/
struct tConvIPv4Long2Str convIPv4Long2Str (unsigned long int  IPv4Long)
{
   struct tConvIPv4Long2Str rConvIPv4Long2Str ;

   struct tConvIPv4Long2Char4 rConvIPv4Long2Char4 ;
   rConvIPv4Long2Char4 = convIPv4Long2Char4 (IPv4Long);
   if (rConvIPv4Long2Char4.ret < 0) {
      rConvIPv4Long2Str.IPv4Str[0] = 0 ;
      rConvIPv4Long2Str.ret = -1 ;
      return rConvIPv4Long2Str ;
   }

   struct tConvIPv4Char42Str rConvIPv4Char42Str ;
   rConvIPv4Char42Str = convIPv4Char42Str(rConvIPv4Long2Char4.IPv4);
   if (rConvIPv4Char42Str.ret < 0) {
      rConvIPv4Long2Str.IPv4Str[0] = 0 ;
      rConvIPv4Long2Str.ret = -2 ;
      return rConvIPv4Long2Str ;
   }

   strcpy(rConvIPv4Long2Str.IPv4Str,rConvIPv4Char42Str.IPv4Str);
   rConvIPv4Long2Str.ret = 0 ;
   return rConvIPv4Long2Str ;
}

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : convIPv4Long2Char4                               |
|                                                                 |
|  Descrizione : Converte un IPv4 da formato numerico Long a      |
|                formato array Char4 . (esempio :                 |
|                ................................                 |
|                ....................................             |
|                                                                 |
|  return-code : struttura complessa con IPv4Char4 e ret          |
|                                                                 |
|  dipendenza  : ConvIPv4Long2StringBinary                        |
|                ConvIPv4StringBinary2Char4                       |
|                                                                 |
|                                                                 |
|  TODO       :                                                   |
|                                                                 |
'-----------------------------------------------------------------'*/
struct tConvIPv4Long2Char4 convIPv4Long2Char4(unsigned long int  IPv4Long)
{

    struct tConvIPv4Long2Char4 rConvIPv4Long2Char4 ; 

    struct tConvIPv4Long2StringBinary rConvIPv4Long2StringBinary ;
    rConvIPv4Long2StringBinary = convIPv4Long2StringBinary (IPv4Long);
    if (rConvIPv4Long2StringBinary.ret < 0) {
       rConvIPv4Long2Char4.IPv4[0] = 0;
       rConvIPv4Long2Char4.ret = -1 ;
       return rConvIPv4Long2Char4 ;
    }

    struct tConvIPv4StringBinary2Char4 rConvIPv4StringBinary2Char4;
    rConvIPv4StringBinary2Char4 = convIPv4StringBinary2Char4 (rConvIPv4Long2StringBinary.IPv4binaryString);
    if (rConvIPv4StringBinary2Char4.ret < 0) {
       rConvIPv4Long2Char4.IPv4[0] = 0;
       rConvIPv4Long2Char4.ret = -2 ;
       return rConvIPv4Long2Char4 ;
    }
//---------------
    rConvIPv4Long2Char4.ret = 0 ;
    rConvIPv4Long2Char4.IPv4[0] = rConvIPv4StringBinary2Char4.IPv4[0] ;
    rConvIPv4Long2Char4.IPv4[1] = rConvIPv4StringBinary2Char4.IPv4[1] ;
    rConvIPv4Long2Char4.IPv4[2] = rConvIPv4StringBinary2Char4.IPv4[2] ;
    rConvIPv4Long2Char4.IPv4[3] = rConvIPv4StringBinary2Char4.IPv4[3] ;
//---------------------------------
    return rConvIPv4Long2Char4 ;
}

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : convIPv4Long2StringBinary                        |
|                                                                 |
|  Descrizione : Converte un IPv4 da formato Long a formato       |
|                stringBinary. (esempio : 3234544 ->              |
|                1111110000001111010101010111111001               |
|                                                                 |
|  return-code : struttura complessa con IPv4StringBinary e ret   |
|                                                                 |
|  dipendenza  : nessuna                                          |
|                                                                 |
|                                                                 |
|                                                                 |
|  TODO       :                                                   |
|                                                                 |
'-----------------------------------------------------------------'*/
struct tConvIPv4Long2StringBinary convIPv4Long2StringBinary(unsigned long int  IPv4Long)
{
       register short int bit, binStrbit ;
       char IPv4binaryString[32];
       unsigned long int IPv4LongAppo = IPv4Long;
       struct tConvIPv4Long2StringBinary rConvIPv4Long2StringBinary ;

       /*
       .-------------------------------------------------.
       '-------------------------------------------------'*/
       binStrbit=0;
       bit=32;
       while ( bit > 0 ) { 
          if ( IPv4LongAppo < pow(2,bit-1) ) 
             rConvIPv4Long2StringBinary.IPv4binaryString[binStrbit] = '0';

          if ( IPv4LongAppo >= pow(2,bit-1) ) {
             IPv4LongAppo = IPv4LongAppo - pow(2,bit-1) ;
             rConvIPv4Long2StringBinary.IPv4binaryString[binStrbit] = '1';
          }
          binStrbit++;
          rConvIPv4Long2StringBinary.IPv4binaryString[binStrbit]=0;
          bit--;
       }
       rConvIPv4Long2StringBinary.ret = 0 ;
       return rConvIPv4Long2StringBinary;
};

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : ConvIPv4StringBinary2Str                         |
|                                                                 |
|  Descrizione : Converte un IPv4 in stringa binaria in stringa   |
|                nel formato classico della 4 cifre col punto di  |
|                separazione. Di seguito un esempio :             |
|                11111111000000001111111100000000 -> 255.0.255.0  |
|                                                                 |
|  return-code :                                                  |
|                                                                 |
|  dipendenza  : ConvStringBinary2Char4                           |
|                ConvChar42Str                                    |
|                                                                 |
|                                                                 |
|  TODO        :                                                  |
|                                                                 |
'-----------------------------------------------------------------'*/
struct tConvIPv4StringBinary2Str convIPv4StringBinary2Str (char IPv4binaryString[32])
{

   struct tConvIPv4StringBinary2Str rConvIPv4StringBinary2Str ; 

   struct tConvIPv4StringBinary2Char4 rConvIPv4StringBinary2Char4 ;
   rConvIPv4StringBinary2Char4 = convIPv4StringBinary2Char4 (IPv4binaryString);
   if (rConvIPv4StringBinary2Char4.ret < 0) {
      rConvIPv4StringBinary2Str.IPv4Str[0] = 0 ; 
      rConvIPv4StringBinary2Str.ret = -1 ; 
      return rConvIPv4StringBinary2Str ;
   }

   struct tConvIPv4Char42Str rConvIPv4Char42Str ;
   rConvIPv4Char42Str = convIPv4Char42Str(rConvIPv4StringBinary2Char4.IPv4);
   if (rConvIPv4Char42Str.ret < 0) {
      rConvIPv4StringBinary2Str.IPv4Str[0] = 0 ;
      rConvIPv4StringBinary2Str.ret = -2 ;
      return rConvIPv4StringBinary2Str ;
   }

   strcpy(rConvIPv4StringBinary2Str.IPv4Str,rConvIPv4Char42Str.IPv4Str);
   rConvIPv4StringBinary2Str.ret = 0 ;
   return rConvIPv4StringBinary2Str ;
}

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : ConvIPv4StringBinary2Char4                       |
|                                                                 |
|  Descrizione :                                                  |
|                                                                 |
|                                                                 |
|                                                                 |
|  return-code :                                                  |
|                                                                 |
|  dipendenza  : nessuna                                          |
|                                                                 |
|                                                                 |
|                                                                 |
|  TODO       :                                                   |
|                                                                 |
'-----------------------------------------------------------------'*/
struct tConvIPv4StringBinary2Char4 convIPv4StringBinary2Char4 (char IPv4binaryString[32])
{
   #define _SIZE_ 32 
   #define _BYTE_  4

   struct tConvIPv4StringBinary2Char4 rConvIPv4StringBinary2Char4 ;

   register unsigned int byte, bit, potenza ;     

   byte = _BYTE_ ;
   bit  = _SIZE_ ;
   while ( byte > 0 ) 
   {
       potenza = 0 ;
       rConvIPv4StringBinary2Char4.IPv4[byte-1] = 0; 
       while ( potenza < 8 ) 
       {
           if (IPv4binaryString[bit-1]=='1') 
                  rConvIPv4StringBinary2Char4.IPv4[byte-1] = rConvIPv4StringBinary2Char4.IPv4[byte-1] + pow(2,potenza) ;
           potenza++;
           bit--;
       };
       byte-- ; 
   }
   rConvIPv4StringBinary2Char4.ret = 0 ; 
   return rConvIPv4StringBinary2Char4 ; 
};

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : convIPv4StringBinary2Long                        |
|                                                                 |
|  Descrizione : Converte un IPv4 da formato StringBinary al      |
|                formato long. (esempio 111111111100001100110011  |
|                00110011->3234345)                               |
|                                                                 |
|  return-code : struttura complessa con IPv4Long e return-code   |
|                                                                 |
|  dipendenza  : nessuna                                          |
|                                                                 |
|                                                                 |
|                                                                 |
|  TODO       :                                                   |
|                                                                 |
'-----------------------------------------------------------------'*/
struct tConvIPv4StringBinary2Long convIPv4StringBinary2Long(char IPv4binaryString[32])
{
      #define _SIZE_ 32

      register short int bit ;
      struct tConvIPv4StringBinary2Long rConvIPv4StringBinary2Long ;

      bit=0;
      rConvIPv4StringBinary2Long.IPv4Long = 0 ;
      while ( bit < _SIZE_ ) {
          if (IPv4binaryString[bit] == '1') {
                rConvIPv4StringBinary2Long.IPv4Long = rConvIPv4StringBinary2Long.IPv4Long + pow(2,(31-bit)) ;
          }
          bit++;
      }

      rConvIPv4StringBinary2Long.ret = 0 ;
      return rConvIPv4StringBinary2Long;
};

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : IPv4NetmaskValidation                            |
|                                                                 |
|  Descrizione : Converte il formato netmask in lunghezza rete    |
|                11111111111111111111111100000000->4123123123     |
|                se trova nella stringa binaria uno "0" in mezzo  |
|                a 2 "1" (es. "11011") non valida il netmask.     |
|                                                                 |
|                                                                 |
|  return-code :                                                  |
|                                                                 |
|  dipendenza  : nessuna                                          |
|                                                                 |
|                                                                 |
|                                                                 |
|  TODO       :                                                   |
|                                                                 |
'-----------------------------------------------------------------'*/
signed char IPv4NetmaskValidation (char IPv4binaryString[32])
{
    #define _SIZE_ 32

    signed char networkLen = 0 ;
    signed char rIPv4NetmaskValidation = 0 ;

    register short int bit = _SIZE_  ;
    while ( bit > 0 )
    {
        if ( IPv4binaryString[bit-1] == '1' )
              networkLen++;

        if ( bit < _SIZE_ - 1 &&
              IPv4binaryString[bit]   == '0' &&
              IPv4binaryString[bit+1] == '1' )
                return -1;

        bit--;
    }
    return networkLen;
};

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : IPv4LenNet2Netmask                               |
|                                                                 |
|  Descrizione : Converte il formato netmask len in netmask       |
|                24->11111111111111111111111100000000             |
|                La len deve essere compresa tra 1 e 32           |
|                                                                 |
|                                                                 |
|  return-code :                                                  |
|                                                                 |
|  dipendenza  : nessuna                                          |
|                                                                 |
|                                                                 |
|                                                                 |
|  TODO       :                                                   |
|                                                                 |
'-----------------------------------------------------------------' */
struct tIPv4LenNet2Netmask IPv4LenNet2Netmask (signed char networkLen)
{
    #define _SIZE_ 32

	register short int bit;
    struct tIPv4LenNet2Netmask rIPv4LenNet2Netmask ;
   
	if ( networkLen < 1 || networkLen > 32 )
    {
	     rIPv4LenNet2Netmask.ret = -1 ;
	     return rIPv4LenNet2Netmask ;
    }

	bit = 0 ;
    while ( bit < _SIZE_ )
    {
        rIPv4LenNet2Netmask.IPv4binaryString[bit] = '1' ;
        if ( bit >= networkLen )  
             rIPv4LenNet2Netmask.IPv4binaryString[bit] = '0' ;
        bit++;
    }
	
    rIPv4LenNet2Netmask.ret = 0 ;
    return rIPv4LenNet2Netmask ;
};

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzion     : getListHostNetworkIPv4                           |
|                                                                 |
|  Descrizione :                                                  |
|                                                                 |
|                                                                 |
|                                                                 |
|  return-code :                                                  |
|                                                                 |
|  dipendenza  :                                                  |
|                                                                 |
|                                                                 |
|                                                                 |
|  TODO       :                                                   |
|                                                                 |
|   1) funzione getListHostNetwork                                | 
|         parametri: IP long / Netmask long -->                   | 
|         OUTPUT numero di host nella rete                        |
|            lista IP della rete (compreso gateway e broadcast)   | 
|            possibile gateway (primo della rete)                 | 
|            possibile broadcast (ultimo della rete)              | 
|            classe (A,B,C)                                       | 
|            tipo (pubblico, privato)                             | 
|            host tipo (network, host, gateway, broadcast         | 
|            rete, segmento/sottorete                             | 
|            dimensione sottorete X di rete Y                     | 
|                                                                 |
|   2) funzione verifica se IP è nella rete                       |  
|         parametri: IP long/Netmask long/IP da verificare -->    | 
|         OUTPUT boolean                                          | 
|                                                                 |
|                                                                 |
|                                                                 |
'-----------------------------------------------------------------'*/
struct tGetListHostNetworkIPv4 getListHostNetworkIPv4 (char IPv4Host[32], char IPv4Netmask[32])
{
    struct tGetListHostNetworkIPv4 rGetListHostNetworkIPv4; 

    /*
    .----------------------------------------------------.
    |                                                    | 
    | converte l'HOST IPv4 stringBinary in Long          |
    |                                                    |
    '----------------------------------------------------' */
    struct tConvIPv4StringBinary2Long rConvIPv4StringBinary2Long_HOST;
    rConvIPv4StringBinary2Long_HOST = convIPv4StringBinary2Long (IPv4Host);
    if (rConvIPv4StringBinary2Long_HOST.ret < 0) {
       printf("\nError [%d]", rConvIPv4StringBinary2Long_HOST.ret);
       rGetListHostNetworkIPv4.ret = -1;
       return rGetListHostNetworkIPv4;
    }

    /*
    .----------------------------------------------------.
    |                                                    |
    | verifica il NETMASK                                |  
    |                                                    |
    '----------------------------------------------------' */
    signed char rIPv4NetmaskValidation = 0;
    rIPv4NetmaskValidation = IPv4NetmaskValidation (IPv4Netmask);
    if (rIPv4NetmaskValidation < 0) {
       rGetListHostNetworkIPv4.ret = -2;  
       return rGetListHostNetworkIPv4; 
    }

    /*
    .--------------------------------------------------.
    |                                                  |
    | Individua la NETWORK                             |
    |                                                  |
    '--------------------------------------------------' */
    char network[32];
    register int bit = 0 ;
    while (bit < rIPv4NetmaskValidation) {
         network[bit] = IPv4Host[bit] ;
         bit++;
    }
    while (bit < 32) {
         network[bit] = '0' ;
         bit++;
    }

    /*
    .--------------------------------------------------.
    | converte l'IPv4 NETWORK da stringBinary a Long   |
    '--------------------------------------------------' */
    struct tConvIPv4StringBinary2Long rConvIPv4StringBinary2Long_NETWORK;
    rConvIPv4StringBinary2Long_NETWORK = convIPv4StringBinary2Long (network);
    if (rConvIPv4StringBinary2Long_NETWORK.ret < 0) {
       printf("\nError [%d]", rConvIPv4StringBinary2Long_NETWORK.ret);
       rGetListHostNetworkIPv4.ret = -3;
    } 
    
    /*
    .--------------------------------------------------.
    |                                                  |
    | Individua il BROADCAST (ultimo IP della network) |
    |                                                  |
    '--------------------------------------------------' */
    char hostFinNetwork[32]; 
    register int bit2 = 0 ;   
    while (bit2 < rIPv4NetmaskValidation) {
         hostFinNetwork[bit2] = IPv4Host[bit2] ; 
         bit2++;
    }
    while (bit2 < 32) {
         hostFinNetwork[bit2] = '1' ;
         bit2++;
    }
    /*
    .--------------------------------------------------.
    | converte l'IPv4 BROADCAST da stringBinary a Long |
    '--------------------------------------------------' */
    struct tConvIPv4StringBinary2Long rConvIPv4StringBinary2Long_BROADCAST;
    rConvIPv4StringBinary2Long_BROADCAST = convIPv4StringBinary2Long (hostFinNetwork);
    if (rConvIPv4StringBinary2Long_BROADCAST.ret < 0) {
       printf("\nError [%d]", rConvIPv4StringBinary2Long_BROADCAST.ret);
       rGetListHostNetworkIPv4.ret = -3;
       return rGetListHostNetworkIPv4;
    }

    /*
    .---------------------------------------.
    | Calcola il numero di IP della Network |
    '---------------------------------------' */ 
    rGetListHostNetworkIPv4.networkLen        = rIPv4NetmaskValidation ;
    rGetListHostNetworkIPv4.IPv4LongNetwork   = rConvIPv4StringBinary2Long_NETWORK.IPv4Long ; 
    rGetListHostNetworkIPv4.IPv4LongGateway   = rConvIPv4StringBinary2Long_NETWORK.IPv4Long + 1 ;
    rGetListHostNetworkIPv4.IPv4LongBroadcast = rConvIPv4StringBinary2Long_BROADCAST.IPv4Long ;  
    rGetListHostNetworkIPv4.numHostNetwork    = rConvIPv4StringBinary2Long_BROADCAST.IPv4Long - rConvIPv4StringBinary2Long_NETWORK.IPv4Long ;
    rGetListHostNetworkIPv4.numHost           = rConvIPv4StringBinary2Long_HOST.IPv4Long - rConvIPv4StringBinary2Long_NETWORK.IPv4Long ;
    rGetListHostNetworkIPv4.ret               = 0;
		
    /*
    .---------------------------------------.
    | Visualizza Statistiche                |
    '---------------------------------------' */ 
    unsigned int debug = 0 ;
	if ( debug == 1 ) {
       printf(
	     "\nIPv4 getListHostNetwork * "
         "\n - HostStringBinary[%s]"
         "\n - NetmaskStringBinary[%s] : "
         "\n - networkLen[%d]"
         "\n - NetworkLong[%lu]"
         "\n - GatewayLong[%lu]"
         "\n - BroadcastLong[%lu]"
         "\n - numHostNetwork[%lu]"
         "\n - numHost[%lu]"
         "\n - ret[%d]",
         IPv4Host,
	     IPv4Netmask,
         rGetListHostNetworkIPv4.networkLen,
         rGetListHostNetworkIPv4.IPv4LongNetwork,
         rGetListHostNetworkIPv4.IPv4LongGateway,
         rGetListHostNetworkIPv4.IPv4LongBroadcast,
         rGetListHostNetworkIPv4.numHostNetwork,
         rGetListHostNetworkIPv4.numHost,
         rGetListHostNetworkIPv4.ret );
   }

   return rGetListHostNetworkIPv4;
};

// ===---- EOF

