/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 


/*
.-----|Str|---------------------------------------------------------------------.
'-------------------------------------------------------------------------------' */

struct tConvIPv4Str2Char4 
{
   unsigned char      IPv4[4] ;
   short int          ret ;
} ;

struct tConvIPv4Str2Long 
{
   unsigned long int  IPv4Long ;
   short int          ret ;
} ;

struct tConvIPv4Str2StringBinary 
{
   char               IPv4binaryString[32] ;
   short int          ret ;
} ;

struct tConvIPv4Str2Char4          convIPv4Str2Char4          (char IPv4str[16]);
struct tConvIPv4Str2Long           convIPv4Str2Long           (char IPv4str[16]);
struct tConvIPv4Str2StringBinary   convIPv4Str2StringBinary   (char IPv4str[16]);


/*
.---|Char4|---------------------------------------------------------------------.
'-------------------------------------------------------------------------------' */

struct tConvIPv4Char42Str 
{
   char               IPv4Str[16] ;
   short int          ret ;
} ;

struct tConvIPv4Char42Long 
{
   unsigned long int  IPv4Long ;
   short int          ret ;
} ;

struct tConvIPv4Char42StringBinary
{
   char               IPv4binaryString[32] ;
   short int          ret ;
};

struct tConvIPv4Char42Str          convIPv4Char42Str          (unsigned char IPv4char4[4]);
struct tConvIPv4Char42Long         convIPv4Char42Long         (unsigned char IPv4char4[4]);
struct tConvIPv4Char42StringBinary convIPv4Char42StringBinary (unsigned char IPv4Char4[4]);


/*
.---|Long|---------------------------------------------------------------------.
'------------------------------------------------------------------------------' */

struct tConvIPv4Long2Str 
{
   char               IPv4Str[16] ;
   short int          ret ;
};

struct tConvIPv4Long2Char4 
{
   unsigned char      IPv4[4] ;
   short int          ret ;
};


struct tConvIPv4Long2StringBinary
{
   char               IPv4binaryString[32] ;
   short int          ret ;
};

struct tConvIPv4Long2Str           convIPv4Long2Str           (unsigned long int  IPv4Long);
struct tConvIPv4Long2Char4         convIPv4Long2Char4         (unsigned long int  IPv4Long);
struct tConvIPv4Long2StringBinary  convIPv4Long2StringBinary  (unsigned long int  IPv4Long);


/*
.---|StringBinary|-------------------------------------------------------------.
'------------------------------------------------------------------------------' */
 
struct tConvIPv4StringBinary2Str
{
   char               IPv4Str[16] ;
   short int          ret ;
} ;

struct tConvIPv4StringBinary2Long
{
   unsigned long int  IPv4Long ;
   short int          ret ;
} ;

struct tConvIPv4StringBinary2Char4 
{
   unsigned char      IPv4[4] ;
   short int          ret ;
};

struct tConvIPv4StringBinary2Str   convIPv4StringBinary2Str   (char IPv4binaryString[32]);
struct tConvIPv4StringBinary2Char4 convIPv4StringBinary2Char4 (char IPv4binaryString[32]);
struct tConvIPv4StringBinary2Long  convIPv4StringBinary2Long  (char IPv4binaryString[32]);


/*
.-----------------------------------------------------------------------------.
'-----------------------------------------------------------------------------' */

signed char                        IPv4NetmaskValidation      (char IPv4binaryString[32]);

struct tIPv4LenNet2Netmask
{
   char               IPv4binaryString[32] ;
   short int          ret ;
};

struct tIPv4LenNet2Netmask         IPv4LenNet2Netmask         (signed char networkLen);

/*
.-----------------------------------------------------------------------------.
'-----------------------------------------------------------------------------' */

struct tGetListHostNetworkIPv4 
{
   unsigned long int  networkLen;
   unsigned long int  IPv4LongNetwork;
   unsigned long int  IPv4LongGateway;
   unsigned long int  IPv4LongBroadcast; 
   unsigned long int  numHostNetwork;
   unsigned long int  numHost;
   short int          ret ;
};

struct tGetListHostNetworkIPv4     getListHostNetworkIPv4     (char IPv4Host[32], char IPv4Netmask[32]); 


