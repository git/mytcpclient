/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 

 
/*
.--------------.
| headers LIBC |
'--------------' */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>


#include "include/strIPv4.h" 


int main(int argc, char **argv)
{
    char *host;
    char *netmask;

    if (argc < 3) { 
          printf ("usage: %s <host> <netmask>");
          exit(1);
    };
    
    host    = malloc(strlen(argv[1])+1);
    host    = argv[1];  

    netmask = malloc(strlen(argv[2])+1);  
    netmask = argv[2]; 

    /*
    .-----------------------------------------------------------.
    |                                                           | 
    |                          H o s t                          |
    |                                                           |
    '-----------------------------------------------------------' */

    /*
    .-----------------------------------------------------------------------------------------------------.
    '----------Str----------------------------------------------------------------------------------------' */

    printf("\n--- conversione IPv4 da stringa -------------------------------------------------------------");

    /* 
    .-------------------------------------.
    | converte la stringa IPv4 in Char[4] |
    '-------------------------------------' */  
    struct tConvIPv4Str2Char4 rConvIPv4Str2Char4__HOST ;
    rConvIPv4Str2Char4__HOST = convIPv4Str2Char4(host);
    if (rConvIPv4Str2Char4__HOST.ret < 0) { 
       printf("\nError [%d]", rConvIPv4Str2Char4__HOST.ret);
       exit(1);
    }
    printf("\nc0nv HOST*IPv4String[%s]->IPv4Char4[%8hhu].[%8hhu].[%8hhu].[%8hhu]", host,  
               rConvIPv4Str2Char4__HOST.IPv4[0], 
                 rConvIPv4Str2Char4__HOST.IPv4[1], 
                   rConvIPv4Str2Char4__HOST.IPv4[2], 
                      rConvIPv4Str2Char4__HOST.IPv4[3]); 

    /*
    .-----------------------------------.
    | converte la stringa IPv4 in Long  |
    '-----------------------------------' */
    struct tConvIPv4Str2Long rConvIPv4Str2Long__HOST ;
    rConvIPv4Str2Long__HOST = convIPv4Str2Long (host);
    if (rConvIPv4Str2Long__HOST.ret < 0) {
       printf("\nError [%d]", rConvIPv4Str2Long__HOST.ret);
       exit(1);
    }
    printf("\nc0nv HOST*IPv4String[%s]->IPv4Long[%lu]", host, rConvIPv4Str2Long__HOST.IPv4Long ); 

    /*
    .-------------------------------------------.
    | converte la stringa IPv4 in stringBinary  |
    '-------------------------------------------' */
    struct tConvIPv4Str2StringBinary rConvIPv4Str2StringBinary__HOST;
    rConvIPv4Str2StringBinary__HOST = convIPv4Str2StringBinary (host);
    if (rConvIPv4Str2StringBinary__HOST.ret < 0) {
       printf("\nError [%d]", rConvIPv4Str2StringBinary__HOST.ret);
       exit(1);
    }
    printf("\nc0nv HOST*IPv4String[%s]->IPv4StringBinary[%s]",
                       host, rConvIPv4Str2StringBinary__HOST.IPv4binaryString);

    /*
    .-----------------------------------------------------------------------------------------------------.
    '----------Char4--------------------------------------------------------------------------------------' */

    printf("\n\n--- conversione IPv4 da char4 ---------------------------------------------------------------");

    /*
    .-------------------------------------.
    | converte IPv4 Char[4] in stringa    |
    '-------------------------------------' */
    struct tConvIPv4Char42Str rConvIPv4Char42Str__HOST ;
    rConvIPv4Char42Str__HOST = convIPv4Char42Str(rConvIPv4Str2Char4__HOST.IPv4);
    if (rConvIPv4Char42Str__HOST.ret < 0) {
       printf("\nError [%d]", rConvIPv4Char42Str__HOST.ret);
       exit(1);
    }
    printf("\nc0nv HOST*IPv4Char4[%8hhu].[%8hhu].[%8hhu].[%8hhu]->String[%s]", 
               rConvIPv4Str2Char4__HOST.IPv4[0],
                 rConvIPv4Str2Char4__HOST.IPv4[1],
                   rConvIPv4Str2Char4__HOST.IPv4[2],
                     rConvIPv4Str2Char4__HOST.IPv4[3],
                       rConvIPv4Char42Str__HOST.IPv4Str );

    /*
    .---------------------------------.
    | converte l'IPv4 Char[4] in Long |
    '---------------------------------' */ 
    struct tConvIPv4Char42Long rConvIPv4Char42Long__HOST ;
    rConvIPv4Char42Long__HOST = convIPv4Char42Long (rConvIPv4Str2Char4__HOST.IPv4);
    if (rConvIPv4Char42Long__HOST.ret < 0) {
       printf("\nError [%d]", rConvIPv4Char42Long__HOST.ret);
       exit(1);
    }
    printf("\nc0nv HOST*IPv4Char4[%8hhu].[%8hhu].[%8hhu].[%8hhu]->IPv4Long[%lu]", 
               rConvIPv4Str2Char4__HOST.IPv4[0], 
                 rConvIPv4Str2Char4__HOST.IPv4[1], 
                   rConvIPv4Str2Char4__HOST.IPv4[2], 
                     rConvIPv4Str2Char4__HOST.IPv4[3], 
                       rConvIPv4Char42Long__HOST.IPv4Long);  

    /*
    .-----------------------------------------.
    | converte l'IPv4 Char[4] in stringBinary |
    '-----------------------------------------' */ 
    struct tConvIPv4Char42StringBinary rConvIPv4Char42StringBinary__HOST; 
    rConvIPv4Char42StringBinary__HOST = convIPv4Char42StringBinary (rConvIPv4Str2Char4__HOST.IPv4);
    if (rConvIPv4Char42StringBinary__HOST.ret < 0) {
       printf("\nError [%d]", rConvIPv4Char42StringBinary__HOST.ret);
       exit(1);
    }
    printf("\nc0nv HOST*IPv4Char4[%8hhu].[%8hhu].[%8hhu].[%8hhu]->IPv4StringBinary[%s]",
               rConvIPv4Str2Char4__HOST.IPv4[0],
                 rConvIPv4Str2Char4__HOST.IPv4[1],
                   rConvIPv4Str2Char4__HOST.IPv4[2],
                     rConvIPv4Str2Char4__HOST.IPv4[3],
                       rConvIPv4Char42StringBinary__HOST.IPv4binaryString);

    /*
    .-----------------------------------------------------------------------------------------------------.
    '-----------long--------------------------------------------------------------------------------------' */

    printf("\n\n--- conversione IPv4 da long -----------------------------------------------------------------");

    /*
    .--------------------------------------.
    | converte l'IPv4 Long in string       |
    '--------------------------------------' */
    struct tConvIPv4Long2Str rConvIPv4Long2Str__HOST;
    rConvIPv4Long2Str__HOST = convIPv4Long2Str (rConvIPv4Char42Long__HOST.IPv4Long);
    if (rConvIPv4Long2Str__HOST.ret < 0) {
       printf("\nError [%d]", rConvIPv4Long2Str__HOST.ret);
       exit(1);
    }
    printf("\nc0nv HOST*IPv4Long[%lu]->IPv4Str[%s]",
                 rConvIPv4Char42Long__HOST.IPv4Long,
                       rConvIPv4Long2Str__HOST.IPv4Str );

    /*
    .--------------------------------------.
    | converte l'IPv4 Long in Char4        |
    '--------------------------------------' */
    struct tConvIPv4Long2Char4 rConvIPv4Long2Char4__HOST;
    rConvIPv4Long2Char4__HOST = convIPv4Long2Char4 (rConvIPv4Char42Long__HOST.IPv4Long);
    if (rConvIPv4Long2Char4__HOST.ret < 0) {
       printf("\nError [%d]", rConvIPv4Long2Char4__HOST.ret);
       exit(1);
    }
    printf("\nc0nv HOST*IPv4Long[%lu]->IPv4Char4[%8hhu].[%8hhu].[%8hhu].[%8hhu]",
                 rConvIPv4Char42Long__HOST.IPv4Long,
                   rConvIPv4Long2Char4__HOST.IPv4[0],
                    rConvIPv4Long2Char4__HOST.IPv4[1],
                     rConvIPv4Long2Char4__HOST.IPv4[2],
                      rConvIPv4Long2Char4__HOST.IPv4[3]); 

    /*
    .--------------------------------------.
    | converte l'IPv4 Long in stringBinary |
    '--------------------------------------' */
    struct tConvIPv4Long2StringBinary rConvIPv4Long2StringBinary__HOST;
    rConvIPv4Long2StringBinary__HOST = convIPv4Long2StringBinary (rConvIPv4Char42Long__HOST.IPv4Long);
    if (rConvIPv4Long2StringBinary__HOST.ret < 0) {
       printf("\nError [%d]", rConvIPv4Long2StringBinary__HOST.ret);
       exit(1);
    }
    printf("\nc0nv HOST*IPv4Long[%lu]->IPv4StringBinary[%s]",
                 rConvIPv4Char42Long__HOST.IPv4Long,
                       rConvIPv4Long2StringBinary__HOST.IPv4binaryString );

    /*
    .-----------------------------------------------------------------------------------------------------.
    '-----------stringBinary------------------------------------------------------------------------------' */

    printf("\n\n--- conversione da IPv4 stringBinary --------------------------------------------------------");

    /*
    .-----------------------------------------.
    | converte l'IPv4 stringBinary in Stringa |
    '-----------------------------------------' */
    struct tConvIPv4StringBinary2Str rConvIPv4StringBinary2Str__HOST;
    rConvIPv4StringBinary2Str__HOST = convIPv4StringBinary2Str (rConvIPv4Char42StringBinary__HOST.IPv4binaryString);
    if (rConvIPv4StringBinary2Str__HOST.ret < 0) {
       printf("\nError [%d]", rConvIPv4StringBinary2Str__HOST.ret);
       exit(1);
    }
    printf("\nc0nv HOST*IPv4StringBinary[%s]->str[%s]",
            rConvIPv4Char42StringBinary__HOST.IPv4binaryString,
                     rConvIPv4StringBinary2Str__HOST.IPv4Str);
					
    /*
    .-----------------------------------------.
    | converte l'IPv4 stringBinary in Char[4] |
    '-----------------------------------------' */
    struct tConvIPv4StringBinary2Char4 rConvIPv4StringBinary2Char4__HOST;
    rConvIPv4StringBinary2Char4__HOST = convIPv4StringBinary2Char4 (rConvIPv4Char42StringBinary__HOST.IPv4binaryString);
    if (rConvIPv4StringBinary2Char4__HOST.ret < 0) {
       printf("\nError [%d]", rConvIPv4StringBinary2Char4__HOST.ret);
       exit(1);
    }
    printf("\nc0nv HOST*IPv4StringBinary[%s]->Char4[%8hhu].[%8hhu].[%8hhu].[%8hhu]",
            rConvIPv4Char42StringBinary__HOST.IPv4binaryString, 
               rConvIPv4StringBinary2Char4__HOST.IPv4[0],
                 rConvIPv4StringBinary2Char4__HOST.IPv4[1],
                   rConvIPv4StringBinary2Char4__HOST.IPv4[2],
                     rConvIPv4StringBinary2Char4__HOST.IPv4[3]); 
    /*
    .--------------------------------------.
    | converte l'IPv4 stringBinary in Long |
    '--------------------------------------' */
    struct tConvIPv4StringBinary2Long rConvIPv4StringBinary2Long__HOST;
    rConvIPv4StringBinary2Long__HOST = convIPv4StringBinary2Long (rConvIPv4Long2StringBinary__HOST.IPv4binaryString);
    if (rConvIPv4StringBinary2Long__HOST.ret < 0) {
       printf("\nError [%d]", rConvIPv4StringBinary2Long__HOST.ret);
       exit(1);
    }
    printf("\nc0nv HOST*IPv4StringBinary[%s]->IPv4Long[%lu]",
                 rConvIPv4Long2StringBinary__HOST.IPv4binaryString,
                       rConvIPv4StringBinary2Long__HOST.IPv4Long );



    /*
    .-----------------------------------------------------------.
    |                                                           |
    |                      N e t m a s k                        |
    |                                                           |
    '-----------------------------------------------------------' */

    printf("\n\n--- verifica netmask ------------------------------------------------------------------------");

    /*
    .-------------------------------------------.
    | converte la stringa IPv4 in stringBinary  |
    '-------------------------------------------' */
    struct tConvIPv4Str2StringBinary rConvIPv4Str2StringBinary__NETMASK;
    rConvIPv4Str2StringBinary__NETMASK = convIPv4Str2StringBinary (netmask);
    if (rConvIPv4Str2StringBinary__NETMASK.ret < 0) {
       printf("\nError [%d]", rConvIPv4Str2StringBinary__NETMASK.ret);
       exit(1);
    }
    printf("\nc0nv NETMASK*IPv4String[%s]->IPv4StringBinary[%s]",
                       netmask, rConvIPv4Str2StringBinary__NETMASK.IPv4binaryString);

    /*
    .-------------------------------------------------.
    | converte Netmask in LenNet (netmask Validation  |
    '-------------------------------------------------' */
    signed char rIPv4NetmaskValidation = 0;
    rIPv4NetmaskValidation = IPv4NetmaskValidation (rConvIPv4Str2StringBinary__NETMASK.IPv4binaryString);
    if (rIPv4NetmaskValidation < 0) {
       printf("\nError netmask");
       exit(1);
    }
    printf("\nIPv4 Netmask validation*IPv4StringBinary[%s] - NetworkLen : [%d]",
               rConvIPv4Str2StringBinary__NETMASK.IPv4binaryString, 
                     rIPv4NetmaskValidation );

    /*
    .-----------------------------------------------------------.
    | converte LenNet in Netmask                                |
    '-----------------------------------------------------------' */					 

    printf("\n\n--- IPv4LenNet2Netmask ------------------------------------------------------------------");

    struct tIPv4LenNet2Netmask rIPv4LenNet2Netmask ;

	unsigned int lenNet = rIPv4NetmaskValidation ;
    rIPv4LenNet2Netmask = IPv4LenNet2Netmask (lenNet) ;
    if (rIPv4LenNet2Netmask.ret < 0) {
       printf("\nError lenNet2netmask");
       exit(1);
    }
    printf("\nIPv4 LetNet2*IPv4StringBinary -- NetworkLen : [%d] - IPv4StringBinary[%s]",
               lenNet, rIPv4LenNet2Netmask.IPv4binaryString );

	strcpy(rConvIPv4Str2StringBinary__NETMASK.IPv4binaryString, rIPv4LenNet2Netmask.IPv4binaryString);

    /*
    .-----------------------------------------------------------.
    |                                                           |
    |                  g e t L i s t H o s t                    |
    |                                                           |
    '-----------------------------------------------------------' */

    printf("\n\n--- getListHostNetworkIPv4 ------------------------------------------------------------------");

    /*
    .------------------------.
    | getListHostNetworkIPv4 |
    '------------------------' */
    struct tGetListHostNetworkIPv4 rGetListHostNetworkIPv4; 
    rGetListHostNetworkIPv4 = getListHostNetworkIPv4 
        ( rConvIPv4Long2StringBinary__HOST.IPv4binaryString, 
             rConvIPv4Str2StringBinary__NETMASK.IPv4binaryString); 
    if (rGetListHostNetworkIPv4.ret < 0) {
       printf("\nError [%d]", rGetListHostNetworkIPv4.ret);
       exit(1);
    }
    printf("\nIPv4 getListHostNetwork * "
           "\n - HostStringBinary[%s]"
           "\n - NetmaskStringBinary[%s] : "
           "\n - networkLen[%d]"
           "\n - NetworkLong[%lu]"
           "\n - GatewayLong[%lu]"
           "\n - BroadcastLong[%lu]"
           "\n - numHostNetwork[%lu]" 
           "\n - numHost[%lu]"
           "\n - ret[%d]",
           rConvIPv4Long2StringBinary__HOST.IPv4binaryString,
           rConvIPv4Str2StringBinary__NETMASK.IPv4binaryString, 
           rGetListHostNetworkIPv4.networkLen, 
           rGetListHostNetworkIPv4.IPv4LongNetwork,
           rGetListHostNetworkIPv4.IPv4LongGateway,
           rGetListHostNetworkIPv4.IPv4LongBroadcast, 
           rGetListHostNetworkIPv4.numHostNetwork,
           rGetListHostNetworkIPv4.numHost,
           rGetListHostNetworkIPv4.ret ); 

    exit(0);
};

