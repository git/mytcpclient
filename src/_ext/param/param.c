/*
# Copyright (C) 2008-2011  
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 

/*
.=====================================================================.
|                                                                     |
    Libreria         : _EXT/param

    Descrizione      : Funzioni di verifiche dei parametri 
                       passati in input alla componente di frontnd. 

    Elenco funzioni

       funzione                 descrizione
    ------------------------------------------------------------
    1. ParametriCheck           Verifica e trasformazione  
                                parametri passati alla componente
                                di frontend. 

    2. printFormato             Visualizza il formato del comando.  

    3. printListaParametri      Visualizza la lista parametri con
                                descrizione
|                                                                    |
'===================================================================='
*/
 
/*
.--------------.
| headers LIBC |
'--------------' */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/*
.----------------------------.
| header Output Class        |
'----------------------------' */
#include "output.h"                 /* per la PrintMessage() */ 
#include "output_message.h"
 
/*
.--------------------------.
| header Conf Class        |
'--------------------------' */
#include "conf.h"                   /* per LoadConf() */

/*
.--------------------.
| header Locale      |
'--------------------' */
#include "param.h" 
#include "param.mytcpclient.conf.h"

/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione    : ParametriCheck                                   |
|                                                                 |
|  Descrizione : Converte i parametri passati dal formato ARRAY   |
|                [**argv[]] al formato così come ci si aspetta    |
|                che vengano formattati in output.                |
|                La funzione non effettua controlli sulla         |
|                qualità dei parametri passati o manipolazioni di |
|                alcun tipo [ad esempio applicazione default] se  |
|                il parametro obbligatorio non risulta inserito o |
|                se non è corretto. La funzione si limita ad      |
|                identificare i parametri correttamente per       | 
|                svolgere il suo lavoro di conversione. La logica |
|                deve stare fuori, in cui usufruirà  della        | 
|                struttura di output.                             | 
|                                                                 |
|  return-code : struttura complessa con return-code              |
|                                                                 |
|  dipendenza  : _ext.conf.getVar()                               |
|                _ext.conf.LoadConf()                             |
|                _ext.output.PrintMessage()                       |
|                _ext.output.Uri2SingleFieldConnect()             |
|                                                                 |
|                                                                 |
|  TODO       : * spostare la logica fuori eliminarla da qui.     |
|                                                                 |
|               * spostare i messaggi in un file XML quindi non   |
|                 più tramite la tabella di memoria memorizzata   |
|                 nel file output.c. Prevedere il multilingua.    | 
|                                                                 |
|               * la lista dei parametri validi dovrebbe essere   |
|                 non cablata nel codice.                         |
|                                                                 |
|               * i parametri dovrebbero essere letti anche da    |
|                 file, se un parametro è presente sia nel file   |
|                 che sulla linea di comando prevale la linea di  |
|                 comando.                                        |
|                                                                 |
|               * La gestione sicuramente è migliorabile :        |
|                 1) definire il numero di valori massimi (0, n)  |
|                 2) definire il numero di valori minimo (0, n)   |
|                 Se ad esempio il parametro non prevede valori   |
|                 si impostano entrambi MINVALUES, MAXVALUES a 0. |
|                                                                 | 
'-----------------------------------------------------------------'*/
extern struct tParametriCheck ParametriCheck (int nParametri, char **lParametri, char *fileconf)
{

    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliBanner[][3] = {
            { "%software_desc%"        , INFOTAB[INFOTAB_SOFTWARE_DESC]      , "string" },
            { "%software_version%"     , INFOTAB[INFOTAB_SOFTWARE_VERSION]   , "string" },
            { "%developer_name%"       , INFOTAB[INFOTAB_DEVELOPER_NAME]     , "string" },
            { "%devel_aaaa_start%"     , INFOTAB[INFOTAB_DEVEL_AAAA_START]   , "string" },
            { "%devel_aaaa_stop%"      , INFOTAB[INFOTAB_DEVEL_AAAA_STOP]    , "string" },
            { "%licence_name%"         , INFOTAB[INFOTAB_LICENCE_NAME]       , "string" },
            { "%licence_version%"      , INFOTAB[INFOTAB_LICENCE_VERSION]    , "string" },
            { "EOT"                    , ""                                  , ""       }
    };

    char *TabVariabiliRemark[][3] = {
            { "%remark%"               , INFOTAB[INFOTAB_SOFTWARE_DESC]      , "string" },
            { "EOT"                    , ""                                  , ""       }
    };

    char *TabVariabiliReturnCode[][3] = {
            { "%software_return_ok%"   , INFOTAB[INFOTAB_SOFTWARE_RETURN_OK] , "string" },
            { "%software_return_ko%"   , INFOTAB[INFOTAB_SOFTWARE_RETURN_KO] , "string" },
            { "EOT"                    , ""                                  , ""       }
    };

    char *TabVariabiliBugReport[][3] =  {
            { "%developer_email%"      , INFOTAB[INFOTAB_DEVELOPER_EMAIL]    , "string" },
            { "EOT"                    , ""                                  , ""       }
    };

    char *TabVariabiliVersion[][3] =  {
            { "%software_version%"     , INFOTAB[INFOTAB_SOFTWARE_VERSION]   , "string" },
            { "EOT"                    , ""                                  , ""       }
    };

    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , ""                                  , "" }
    };
      
  
    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tParametriCheck  rParametriCheck;
    struct tParametriCheck *pParametriCheck;

    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE * LIB Conf |
    '------------------------------------------' */
    tLoadConf  rLoadConf;
    tLoadConf *pLoadConf;

    char *buffer;
	
    int DEBUG = 0 ;

    /*
    .--------------------------------------------------.
    | Alloca lo spazio per contenere la struttura dati |
    | che va a riempire la funzione ClientConnect. La  |
    | seconda istruzione serve per dedicare lo spazio  |
    | allocato alla struttura dati. Successivamente    |
    | inizializza l'intera struttura con NULL ed il    |
    | campo return_code a ZERO.                        |
    '--------------------------------------------------' */
    pParametriCheck = malloc(sizeof(struct tParametriCheck));
    pParametriCheck = &rParametriCheck;
    memset((void*)&rParametriCheck,0,sizeof(rParametriCheck));
    rParametriCheck.return_code = 0;

    pLoadConf = malloc(sizeof(struct tLoadConf));
    pLoadConf = &rLoadConf;
    memset((void*)&rLoadConf,0,sizeof(rLoadConf));
    rLoadConf.return_code = 0;


    /*
    .---------------------------------------------------------------------------------------------.
    |                                                                                             |
    | STEP01: Inizializza OUTPUT.                                                                 |
    |                                                                                             |
    | Inizializza e carica struttura di OUTPUT con i dati presenti nel file nell'header           |
    | include/param_message.h. In futo verrà rivisto questo punto.                                |
    |                                                                                             | 
    '---------------------------------------------------------------------------------------------' */

    /* frontend utilizzato */
    rParametriCheck.frontend = malloc(strlen(INFOTAB[INFOTAB_SOFTWARE_NAME])+1) ;
    strcpy(rParametriCheck.frontend, INFOTAB[INFOTAB_SOFTWARE_NAME]);

    /* frontend utilizzato (version) */
    rParametriCheck.frontend_version = malloc(strlen(INFOTAB[INFOTAB_SOFTWARE_VERSION])+1);
    strcpy(rParametriCheck.frontend_version, INFOTAB[INFOTAB_SOFTWARE_VERSION]);

    /* frontend utilizzato (remark) */
    rParametriCheck.frontend_remark = malloc(strlen(INFOTAB[INFOTAB_SOFTWARE_REMARK])+1);
    strcpy(rParametriCheck.frontend_remark, INFOTAB[INFOTAB_SOFTWARE_REMARK]);

    /* tabella parametri */
    register int indice = 0;
    register int finetabella = 0;
    while ( finetabella != 1 )
    {
	rParametriCheck.PARAM_TAB[indice].T_ID = malloc(strlen(PARAM_TAB[indice][PARAM_T_ID])+1) ;
	strcpy(rParametriCheck.PARAM_TAB[indice].T_ID,PARAM_TAB[indice][PARAM_T_ID]);

	rParametriCheck.PARAM_TAB[indice].T_PARAM = malloc(strlen(PARAM_TAB[indice][PARAM_T_PARAM])+1) ;
	strcpy(rParametriCheck.PARAM_TAB[indice].T_PARAM, PARAM_TAB[indice][PARAM_T_PARAM]) ;

	rParametriCheck.PARAM_TAB[indice].T_PARAM_EXT = malloc(strlen(PARAM_TAB[indice][PARAM_T_PARAM_EXT])+1) ;
	strcpy(rParametriCheck.PARAM_TAB[indice].T_PARAM_EXT, PARAM_TAB[indice][PARAM_T_PARAM_EXT]) ;

        rParametriCheck.PARAM_TAB[indice].T_PARAM_VALUE_NOTE = malloc(strlen(PARAM_TAB[indice][PARAM_T_PARAM_VALUE_NOTE])+1) ;
        strcpy(rParametriCheck.PARAM_TAB[indice].T_PARAM_VALUE_NOTE, PARAM_TAB[indice][PARAM_T_PARAM_VALUE_NOTE]) ;

        rParametriCheck.PARAM_TAB[indice].T_PARAM_FORMAT_NOTE = malloc(strlen(PARAM_TAB[indice][PARAM_T_PARAM_FORMAT_NOTE])+1) ;
        strcpy(rParametriCheck.PARAM_TAB[indice].T_PARAM_FORMAT_NOTE, PARAM_TAB[indice][PARAM_T_PARAM_FORMAT_NOTE]) ;

	rParametriCheck.PARAM_TAB[indice].T_PARAM_REMARK = malloc(strlen(PARAM_TAB[indice][PARAM_T_PARAM_REMARK])+1) ;
	strcpy(rParametriCheck.PARAM_TAB[indice].T_PARAM_REMARK, PARAM_TAB[indice][PARAM_T_PARAM_REMARK]) ;

	rParametriCheck.PARAM_TAB[indice].T_PARAM_ERRMSG_INCOMPLETE_MISSING_VALUE = 
                     malloc(strlen(PARAM_TAB[indice][PARAM_T_PARAM_ERRMSG_INCOMPLETE_MISSING_VALUE])+1) ;
	strcpy(rParametriCheck.PARAM_TAB[indice].T_PARAM_ERRMSG_INCOMPLETE_MISSING_VALUE, 
                                  PARAM_TAB[indice][PARAM_T_PARAM_ERRMSG_INCOMPLETE_MISSING_VALUE]) ;

	rParametriCheck.PARAM_TAB[indice].T_PARAM_ERRMSG_DUPLICATE = malloc(strlen(PARAM_TAB[indice][PARAM_T_PARAM_ERRMSG_DUPLICATE])+1) ;
	strcpy(rParametriCheck.PARAM_TAB[indice].T_PARAM_ERRMSG_DUPLICATE, PARAM_TAB[indice][PARAM_T_PARAM_ERRMSG_DUPLICATE]) ;

        rParametriCheck.PARAM_TAB[indice].T_PARAM_SEQUENCE = malloc(strlen(PARAM_TAB[indice][PARAM_T_PARAM_SEQUENCE])+1) ;
        strcpy(rParametriCheck.PARAM_TAB[indice].T_PARAM_SEQUENCE, PARAM_TAB[indice][PARAM_T_PARAM_SEQUENCE]) ;

	rParametriCheck.PARAM_TAB[indice].T_FLUTILIZZO = malloc(strlen(PARAM_TAB[indice][PARAM_T_FLUTILIZZO])+1) ;
	strcpy(rParametriCheck.PARAM_TAB[indice].T_FLUTILIZZO, PARAM_TAB[indice][PARAM_T_FLUTILIZZO]) ;

	rParametriCheck.PARAM_TAB[indice].T_VALUE = malloc(strlen(PARAM_TAB[indice][PARAM_T_VALUE])+1) ;
	strcpy(rParametriCheck.PARAM_TAB[indice].T_VALUE, PARAM_TAB[indice][PARAM_T_VALUE]) ;

        rParametriCheck.PARAM_TAB[indice].T_REQUIRED_FIELD = atoi(PARAM_TAB[indice][PARAM_T_REQUIRED_FIELD]) ;
//=============		
	if (DEBUG==1) printf ("\n[%s][%s|%s|%s|%s|%s] : [%s|%s] : [%s] : [%s|%s] : [%d]" , 
			rParametriCheck.PARAM_TAB[indice].T_ID, 
                        rParametriCheck.PARAM_TAB[indice].T_PARAM, 
			rParametriCheck.PARAM_TAB[indice].T_PARAM_EXT,
                        rParametriCheck.PARAM_TAB[indice].T_PARAM_VALUE_NOTE,
                        rParametriCheck.PARAM_TAB[indice].T_PARAM_FORMAT_NOTE,
			rParametriCheck.PARAM_TAB[indice].T_PARAM_REMARK,
			rParametriCheck.PARAM_TAB[indice].T_PARAM_ERRMSG_INCOMPLETE_MISSING_VALUE,
			rParametriCheck.PARAM_TAB[indice].T_PARAM_ERRMSG_DUPLICATE,
                        rParametriCheck.PARAM_TAB[indice].T_PARAM_SEQUENCE,
			rParametriCheck.PARAM_TAB[indice].T_FLUTILIZZO, 
			rParametriCheck.PARAM_TAB[indice].T_VALUE, 
                        rParametriCheck.PARAM_TAB[indice].T_REQUIRED_FIELD );
//=============		
	if ( strcmp(PARAM_TAB[indice][PARAM_T_ID], __PARAM_T_ID_VALUETAPPO__ ) == 0 )
			finetabella = 1;
			
	indice++;              
    };
		
    /* esito */
    rParametriCheck.return_code = 0 ;
	
    /*
    .------------------------------------------.
    |  Determina il livello di DEBUG           |
    '------------------------------------------' */
    int debug_level = GetDebugLevel ( "+++param", "param" ) ; 


    /*
    .==================================================================.
    |
    |
    | STEP 2: Estrapolazione parametri da file di configurazione
    |
    |
    '==================================================================' */

    //register int i=0;
    //while (( strcmp(rParametriCheck.PARAM_TAB[i].T_ID, __PARAM_T_ID_VALUETAPPO__ ) != 0 ) &&
    //         ( strcmp(rParametriCheck.PARAM_TAB[i].T_ID, "-f" ) != 0 ))
    //                            i++;

    // Parametro "impostazione file di configurazione" trovato 
    //if ( strcmp(rParametriCheck.PARAM_TAB[i].T_ID, "-f" ) == 0 )
    //{
       /*
       .--------------------------.
       | Load configuration file  |
       '--------------------------' *//*
       rLoadConf = LoadConf (PARAM_TAB[16][PARAM_T_VALUE]);
       if (rLoadConf.return_code == -1) {
           PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__ERRORE_CONFIGURATION_FILE_NOTFOUND", TabVariabiliEMPTY, 0, 0, 0);
           PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__BANNER", TabVariabiliEMPTY, 0, 0, 0                            );
           PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__FORMATO", TabVariabiliEMPTY, 0, 0, 0                           );
           rParametriCheck.return_code = -1;
           return rParametriCheck; };
       */

       /*
       .---------------------------------------.
       | Get parameter from configuration file |
       '---------------------------------------' *//*
       PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__BANNER", TabVariabiliEMPTY);

       printf ("\nEnvironment configuration file [%s]\n", buffer);
       printf ("\n.--------------.---------------------------------------------------------.");
       printf ("\n| parameter    | value                                                   |");
       printf ("\n.--------------.---------------------------------------------------------.");
       printf ("\n| -connect     | [%70s] |", getVar(rLoadConf,"default","connect")          );
       printf ("\n| -keystore    | [%70s] |", getVar(rLoadConf,"default","keystore")         );
       printf ("\n| -password    | [%70s] |", getVar(rLoadConf,"default","password")         );
       printf ("\n| -truststore  | [%70s] |", getVar(rLoadConf,"default","truststore")       );
       printf ("\n| -ciphersuite | [%70s] |", getVar(rLoadConf,"default","ciphersuite")      );
       printf ("\n'--------------'---------------------------------------------------------'");
       printf ("\n");
       printf ("\nThe parameter Configuration File (-config) is not implemented.");
       printf ("\n\n");
       selected_parameter = 0;

       rParametriCheck.return_code = -1;
       return rParametriCheck;
       */
    //};


    /*
    .==========================================================================.                                       
    | 
    |
    | STEP 3: Estrapolazione parametri dalla riga di comando, 
    |
    |
    '==========================================================================' */ 

    /*
    .--------------------------------------------------.
    | Nessun parametro: visualizza banner ed esce.     |
    '--------------------------------------------------' */
    if (nParametri == 1)
    {
        PrintMessage (debug_level, TABMSG, "MY_CLIENT_TCP__BANNER", TabVariabiliBanner, 0, 0, 0); 
        printFormato();
        rParametriCheck.return_code = +1;                   
        return rParametriCheck;
    };

    /*
    .------------------------------------------.
    |  Son presenti parametri da analizzare.   |
    '------------------------------------------' */
    indice = 1;
    int selected_parameter = 0;
    while ( indice < nParametri )
    {
	/*
        .--------------------------------------------------.
        |  Legge il parametro successivo                   |
        '--------------------------------------------------' */
        buffer = malloc(strlen(lParametri[indice])+1) ;
	strcpy(buffer, lParametri[indice]) ;
				
        /*
        .---------------------------------------------------.
        |  *HELP* Se il parametro è -h|--help i precedenti  |
        |  parametri ed anche i successivi ancora da        |
        |  leggere *non* vengono considerati.               |
        '---------------------------------------------------' */
        if ( strcmp(buffer,"-h") == 0 || strcmp(buffer,"--help") == 0 ) 
        { 
             PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__BANNER", TabVariabiliBanner, 0, 0, 0);
             printFormato();
             PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__REMARK", TabVariabiliRemark, 0, 0, 0);
             printListaParametri(); 
             PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__RETURNCODE", TabVariabiliReturnCode, 0, 0, 0);
             PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__BUGREPORT", TabVariabiliBugReport, 0, 0, 0);
             rParametriCheck.return_code = +1;                   
             return rParametriCheck;
        };

        /*
        .--------------------------------------------------------.
        |  *VERSION* Se il parametro è -v|--version i precedenti |
        |  parametri ed anche i successivi ancora da             |
        |  leggere *non* vengono considerati.                    |
        '--------------------------------------------------------' */
        if ( strcmp(buffer,"-v") == 0 || strcmp(buffer,"--version")     == 0 )
        { 
             PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__BANNER", TabVariabiliBanner, 0, 0, 0);
             printFormato();
             PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__REMARK", TabVariabiliRemark, 0, 0, 0);
             PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__VERSION", TabVariabiliVersion, 0, 0, 0);
             rParametriCheck.return_code = +1;               
             return rParametriCheck;
        };

        /*
        .---------------------------------------------------------------------------.
	|                                                                           |
        |  Verifica di esistenza e sintattica del parametro.                        |
        |  Cerca il parametro nel catalogo parametri (PARAM_TAB)                    |
        |                                                                           |
        |  Ogni parametro prevedere sempre un valore, quindi il comando             |
	|  dovrà avere sempre il seguente formato:                                  |
	|                                                                           |
	|      parametro-1 valore-1                                                 |
	|      parametro-2 valore-2                                                 |
	|      ...                                                                  |
	|      parametro-n valore-n                                                 |
	|                                                                           |
	|  Il valore è un informazione obbligatoria, gli esempi successivi          |
	|  comandi risultano errati :                                               |
	|  * comando par-id-1  *Err*                                                |
	|  * comando par-id-1  *Err*   par-id-2 par-value-2                         |
	|  * comando par-id-1 par-id-1 par-id-2    *Err*                            |
	|                                                                           |
        |                                                                           |		
        '---------------------------------------------------------------------------' */ 
	if (DEBUG==1) printf ("[#debug] parametriCheck :: Nuovo PAROLA letta: [%s]\n", buffer);
		
        /* search parameter */
        register int i = 0;  
		
	while (( strcmp(rParametriCheck.PARAM_TAB[i].T_ID, __PARAM_T_ID_VALUETAPPO__ ) != 0 ) &&
	          ( strcmp(buffer,rParametriCheck.PARAM_TAB[i].T_PARAM) != 0 && 
			    strcmp(buffer,rParametriCheck.PARAM_TAB[i].T_PARAM_EXT) != 0 ))
        { 
	 	  i++;
 
		  if (DEBUG==1) printf ("\n* (%s==%s) %s == [%s|%s]",
					rParametriCheck.PARAM_TAB[i].T_ID, 
					__PARAM_T_ID_VALUETAPPO__, 
   				        buffer, 
					rParametriCheck.PARAM_TAB[i].T_PARAM, 
					rParametriCheck.PARAM_TAB[i].T_PARAM_EXT ); 
	};
					
        register int fParameterFound = 0;
	if ( strcmp(rParametriCheck.PARAM_TAB[i].T_ID, __PARAM_T_ID_VALUETAPPO__ ) != 0 )  /* valore tappo */
	     fParameterFound = 1;
			
        /* -+-+-+-+ Found Parameter -+-+-+-+ */
    	if ( fParameterFound == 1 )   
        {
	     	if (DEBUG==1) printf ("[#debug] parametriCheck :: La PAROLA letta [%s] è un Parametro censito\n", buffer);
			
	    	/*
        	.-------------------------------------------------------------------------.
	    	| Error-TYPE-1 - Incomplete Previous Parameter Found                      |
		| Se il parse ha identificato un nuovo parametro e la variabile           |
		| selected_parameter *NON* è uguale a ZERO bensì contiene un valore       |
		| maggiore di ZERO significa che il parse stava analizzando un parametro  |
		| di quelli supportati e si aspettava di trovare un valore ancora per     |
		| poterlo completamente definire. Quindi il parse si ferma con un errore  |                                      
		'-------------------------------------------------------------------------' */
		if ( selected_parameter > 0 )
		{				
			/* finding index of incomplete parameter in PARAM for a more just error message */ 			
			register int i2 = 0;
			while (( strcmp(rParametriCheck.PARAM_TAB[i2].T_ID, __PARAM_T_ID_VALUETAPPO__ ) != 0 ) &&
				      ( selected_parameter != atoi(rParametriCheck.PARAM_TAB[i2].T_ID)) ) 
							i2++;
			
			if ( strcmp(rParametriCheck.PARAM_TAB[i2].T_ID, __PARAM_T_ID_VALUETAPPO__ ) != 0 )  /* MSG for PARAM found */
				PrintMessage(debug_level, TABMSG, rParametriCheck.PARAM_TAB[i2].T_PARAM_ERRMSG_INCOMPLETE_MISSING_VALUE, 
                                                                TabVariabiliEMPTY, 0, 0, 0);

			if ( strcmp(rParametriCheck.PARAM_TAB[i2].T_ID, __PARAM_T_ID_VALUETAPPO__ ) == 0 )  /* MSG fot PARAM not found */
				PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__ERRORE_UNKNOWN_PARAMETRO", TabVariabiliEMPTY, 0, 0, 0 );

                        PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__BANNER", TabVariabiliBanner, 0, 0, 0);
                        printFormato();
			rParametriCheck.return_code = -1;                    
			return rParametriCheck;
		};
			
		/* Error-TYPE-2 - parameter used duplicate */ 
		if ( strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO,"1") == 0 ) 
		{
			PrintMessage(debug_level, TABMSG, rParametriCheck.PARAM_TAB[i].T_PARAM_ERRMSG_DUPLICATE, TabVariabiliEMPTY, 0, 0, 0);
                        PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__BANNER", TabVariabiliBanner, 0, 0, 0);
                        printFormato();
			rParametriCheck.return_code = -1;                    
			return rParametriCheck; 
		}; 
	    
		/* setting On to "flag Parameter Used" */
		strcpy(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO,"1") ;
					
		/* store ID Parameter Opening */
		selected_parameter = atoi(rParametriCheck.PARAM_TAB[i].T_ID) ;
        };
	

	/* -+-+-+-+ Value -+-+-+-+ */
	if ( fParameterFound == 0 )    
        {		
     	     // Err - missing parameter open 
	     if ( selected_parameter == 0 )
	     { 
		  PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__ERRORE_UNKNOWN_PARAMETRO", TabVariabiliEMPTY, 0, 0, 0 ); 
                  PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__BANNER", TabVariabiliBanner, 0, 0, 0);
                  printFormato();
		  rParametriCheck.return_code = -1;                    
		  return rParametriCheck;  
	     };
			
	     // cerca il parametro corrisponde al valore letto per completarlo
	     if ( selected_parameter > 0 )
	     { 
	       	  register int i2 = 0;
	    	  while (( strcmp(rParametriCheck.PARAM_TAB[i2].T_ID, __PARAM_T_ID_VALUETAPPO__ ) != 0 ) &&
		 	 ( selected_parameter != atoi(rParametriCheck.PARAM_TAB[i2].T_ID)) ) 
				i2++;

	          // Parametro aperto non trovato -- molto strano 
	       	  if ( strcmp(rParametriCheck.PARAM_TAB[i2].T_ID, __PARAM_T_ID_VALUETAPPO__ ) == 0 )
	     	  {
			PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__ERRORE_UNKNOWN_PARAMETRO", TabVariabiliEMPTY, 0, 0, 0 );
                        PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__BANNER", TabVariabiliBanner, 0, 0, 0);
                        printFormato();
			rParametriCheck.return_code = -1;                    
			return rParametriCheck;
   	     	  };

	       	  if ( strcmp(rParametriCheck.PARAM_TAB[i2].T_ID, __PARAM_T_ID_VALUETAPPO__ ) != 0 )
			if (DEBUG==1) printf ("[#debug] parametriCheck :: Ricerca del parametro aperto per completamento [%d][%s|%s]\n", 
					selected_parameter, rParametriCheck.PARAM_TAB[i2].T_PARAM, rParametriCheck.PARAM_TAB[i2].T_PARAM_EXT);
			
   	       	  // store value   
	     	  rParametriCheck.PARAM_TAB[i2].T_VALUE = malloc (strlen(buffer)+1);
	     	  memset(rParametriCheck.PARAM_TAB[i2].T_VALUE,0,strlen(buffer));
	     	  strcpy(rParametriCheck.PARAM_TAB[i2].T_VALUE, buffer);
					
	     	  //----------------------------------------------------------------
	     	  if (DEBUG==1) printf("[#debug] parametriCheck :: nuovo paramentro completato: [%d][%s|%s][%s]\n", 
		  	i2, rParametriCheck.PARAM_TAB[i2].T_PARAM, rParametriCheck.PARAM_TAB[i2].T_PARAM_EXT, 
                               rParametriCheck.PARAM_TAB[i2].T_VALUE);
	     	  //----------------------------------------------------------------

	     	  // close update parameter 
	     	  selected_parameter = 0 ;
	     }
      	}

        indice++;			
    };


    //----------------------------------------------------------------
    if (DEBUG==1) printf("[#debug] parametriCheck :: terminate PAROLE\n");
    //----------------------------------------------------------------
	
    /*
    .----------------------------------------------------------------------------.
    | Error-TYPE-1 - Incomplete Previous Parameter Found                         |
    | Se il parse ha concluso l'analisi del comando e la variabile               |
    | selected_parameter *NON* è uguale a ZERO bensì contiene un valore          |
    | maggiore di ZERO significa che il parse stava analizzando un parametro     |
    | di quelli supportati e si aspettava di trovare qualcosa ancora per         |
    | poterlo completamente definire. Quindi il parse si ferma con un errore     |                                      
    '----------------------------------------------------------------------------' */
    if ( selected_parameter > 0 )
    {				
   	 /* finding index of incomplete parameter in PARAM for a more just error message */ 			
	register int i2 = 0;
	while ( (strcmp(rParametriCheck.PARAM_TAB[i2].T_ID, __PARAM_T_ID_VALUETAPPO__ ) != 0 ) &&
		       (selected_parameter != atoi(rParametriCheck.PARAM_TAB[i2].T_ID)) ) 
					i2++;

	if ( strcmp(rParametriCheck.PARAM_TAB[i2].T_ID, __PARAM_T_ID_VALUETAPPO__ ) != 0 )  /* MSG for PARAM found */
			PrintMessage(debug_level, TABMSG, rParametriCheck.PARAM_TAB[i2].T_PARAM_ERRMSG_INCOMPLETE_MISSING_VALUE, 
                                           TabVariabiliEMPTY, 0, 0, 0);

	if ( strcmp(rParametriCheck.PARAM_TAB[i2].T_ID, __PARAM_T_ID_VALUETAPPO__ ) == 0 )  /* MSG fot PARAM not found */
			PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__ERRORE_UNKNOWN_PARAMETRO", TabVariabiliEMPTY, 0, 0, 0 );
	    
        PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__BANNER", TabVariabiliBanner, 0, 0, 0);
        printFormato();
	rParametriCheck.return_code = -1;                    
	return rParametriCheck;
    };

    /*
    .------------------------------------------------------------.
    | Rilegge la PARAM_TAB per verificare la presenza parametri  | 
    | definiti come Obbligatori.                                 |
    |                                                            |
    '------------------------------------------------------------' */

    //----------------------------------------------------------------
    if (DEBUG==1) printf("[#debug] parametriCheck :: verifica REQUIRED FIELD\n");
    //----------------------------------------------------------------

    indice = 0;
    while ( strcmp(PARAM_TAB[indice][PARAM_T_ID], __PARAM_T_ID_VALUETAPPO__ ) != 0 )
    {
        if ( rParametriCheck.PARAM_TAB[indice].T_REQUIRED_FIELD == 1 &&
             strcmp(rParametriCheck.PARAM_TAB[indice].T_FLUTILIZZO,"1") != 0 ) 
        {
             PrintMessage(debug_level, TABMSG, rParametriCheck.PARAM_TAB[indice].T_PARAM_ERRMSG_INCOMPLETE_MISSING_VALUE, 
                                    TabVariabiliEMPTY, 0, 0, 0);
             PrintMessage(debug_level, TABMSG, "MY_CLIENT_TCP__BANNER", TabVariabiliBanner, 0, 0, 0);
             printFormato();
             rParametriCheck.return_code = -1;
             return rParametriCheck;
        };
        indice++;
    };

    /*
    .--------------------------------------------------------.
    | Parametri sintatticamenbte corretti. Visualizza Banner |
    '--------------------------------------------------------' */
    PrintMessage (debug_level, TABMSG, "MY_CLIENT_TCP__BANNER", TabVariabiliBanner, 0, 0, 0);


    /*
    .------------------------------------------------------------------.
    | Visualizza i parametri impostati analizzati dalla routine per    |
    | attività di DEBUG. (DEBUG = 1|0 * Attiva|Non Attiva).            |
    '------------------------------------------------------------------' */ 
    if ( DEBUG == 1 )
    {
         register int indice;    
          
         printf ("\n.-----------------------------------------------------------------------------.");
         printf ("\n|                 Funzione ParametriCheck DUMP DEBUG                          |");
         printf ("\n'-----------------------------------------------------------------------------'\n\n");
       
         printf ("\nX------------------------- Valori di Input -----------------------------------X\n\n");

         printf ("\nfrontend              : [%s]" , INFOTAB[INFOTAB_SOFTWARE_NAME]    );
         printf ("\nfrontend version      : [%s]" , INFOTAB[INFOTAB_SOFTWARE_VERSION] );
         printf ("\nfrontend remark       : [%s]" , INFOTAB[INFOTAB_SOFTWARE_REMARK]  );

         indice = 0;
         while ( indice < nParametri )
         {
                 printf ("\nParametro [%d]         : [%s]" , indice, lParametri[indice]);
                 indice++;
         }; 
         printf ("\nnumero parametri      : [%d]\n\n" , nParametri );


         printf ("\nX------------------------- Valori di Output ----------------------------------X\n\n");

         printf ("\nfrontend              : [%s]" , rParametriCheck.frontend         );
         printf ("\nfrontend version      : [%s]" , rParametriCheck.frontend_version );
         printf ("\nfrontend remark       : [%s]" , rParametriCheck.frontend_remark  );

		 //---------
		 //---------
		 
         indice = 0;		 
         while ( strcmp(rParametriCheck.PARAM_TAB[indice].T_ID, __PARAM_T_ID_VALUETAPPO__ ) != 0 )
         {
	    	printf ("\n[%s] : [%s]" , rParametriCheck.PARAM_TAB[indice].T_PARAM_EXT, rParametriCheck.PARAM_TAB[indice].T_VALUE	);
		indice++;                                                       
         };

	 printf ("\nreturn_code                 : [%d]" , rParametriCheck.return_code                                         );
         printf ("\n\n");
    };

    return rParametriCheck;

};


/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione    : printFormato                                     |
|                                                                 |
|  Descrizione : Visualizza il formato del comando.               |
|                                                                 |
|       Esempio                                                   |
|                                                                 |
|           formato: myClientTCP [ -c|--connect ] protocol://     | 
|                                [ -U|--auth-user <user>] [ -     | 
|                                [-pH|--http-proxy-host <host     |
|                                [-pU|--http-proxy-user <user     |
|                                [ -k|--keystore <filename>]      |
|                                [ -t|--truststore <filename>     | 
|                                [ -c|--ciphersuite {cipher1,     |
|                                [ -s|--ssl-version <version>     |
|                                [ -T|--type-operation <type>     |
|                                [-Ml|--message-language <lan     |
|                                [-Mt|--message-type <type>]      | 
|                                [ -M|--message <text>]           |
|                                [ -S|--ssl-ciphersuite-serve     |
|                                [-Pc|--ssl-print-cert {On|Of     |
|                                [ -m|--max-request <max-valu     |
|                                [-Tt|--tcp-receive-timeout <     | 
|                                [ -o|--output-file <filename     | 
|                                [ -d|--debug {+protocol1+pro     | 
|                    myClientTCP [ -f|--config <filename>]        |
|                    myClientTCP [ -h|--help ]                    | 
|                    myClientTCP [ -v|--version ]                 | 
|                                                                 | 
|  return-code : -                                                |
|                                                                 |
|  dipendenza  : -                                                |
|                                                                 |
|  TODO        : -                                                |
|                                                                 |
|                                                                 |
'-----------------------------------------------------------------' */
extern void printFormato()
{
    /* tabella parametri */
    register int indice = 0;
    while ( strcmp(PARAM_TAB[indice][PARAM_T_ID], __PARAM_T_ID_VALUETAPPO__ ) != 0 )
    {
       
        if ( strcmp(PARAM_TAB[indice][PARAM_T_PARAM_SEQUENCE],"1") == 0 )  
        {
             printf("\n");
        }
 
        if ( indice == 0 )  
        { 
               printf("formato: %s ", INFOTAB[INFOTAB_SOFTWARE_NAME] ); 
        }

        if ( strcmp(PARAM_TAB[indice][PARAM_T_PARAM],"-f" ) == 0 ||
             strcmp(PARAM_TAB[indice][PARAM_T_PARAM],"-h" ) == 0 ||
             strcmp(PARAM_TAB[indice][PARAM_T_PARAM],"-v" ) == 0 )
        {
               printf("         %s ", INFOTAB[INFOTAB_SOFTWARE_NAME] );
        }

        if ( indice > 0 && 
               strcmp(PARAM_TAB[indice][PARAM_T_PARAM],"-f" ) != 0 && 
               strcmp(PARAM_TAB[indice][PARAM_T_PARAM],"-h" ) != 0 && 
               strcmp(PARAM_TAB[indice][PARAM_T_PARAM],"-v" ) != 0 )
        {
               if ( strcmp(PARAM_TAB[indice][PARAM_T_PARAM_SEQUENCE],"1") == 0 )
                      printf("                     ");
        };

        printf("[%3s|%s %s] %s", PARAM_TAB[indice][PARAM_T_PARAM],
           PARAM_TAB[indice][PARAM_T_PARAM_EXT], PARAM_TAB[indice][PARAM_T_PARAM_VALUE_NOTE],
              PARAM_TAB[indice][PARAM_T_PARAM_FORMAT_NOTE] );

        indice++;
    };
    printf("\n");
};


/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione    : printListaParametri                              |
|                                                                 |
|  Descrizione : Visualizza la lista parametri con descrizione    |
|                                                                 |
|  return-code : -                                                |
|                                                                 |
|  dipendenza  : -                                                |
|                                                                 |
|  TODO        : -                                                |
|                                                                 |
|                                                                 |
'-----------------------------------------------------------------' */
extern void printListaParametri()
{
    /* tabella parametri */
    register int indice = 0;
    printf("\nList of parameters with a small description:\n"); 
    while ( strcmp(PARAM_TAB[indice][PARAM_T_ID], __PARAM_T_ID_VALUETAPPO__ ) != 0 )
    {

        /* se inizia nuovo parametro / gruppo e non si tratta di primo chiude il parametro / gruppo
           precedente con descrizione comand */
        if ( strcmp(PARAM_TAB[indice][PARAM_T_PARAM_SEQUENCE],"1") == 0 && indice > 0 )
        {
             printf("%\n                       %s", PARAM_TAB[indice-1][PARAM_T_PARAM_REMARK]); 
        }
 
        /* se inizia nuovo parametro / gruppo allora salto pagina */
        if ( strcmp(PARAM_TAB[indice][PARAM_T_PARAM_SEQUENCE],"1") == 0 )
        {
             printf("\n");
        }

        printf("  %3s|%s %s %s", PARAM_TAB[indice][PARAM_T_PARAM],
           PARAM_TAB[indice][PARAM_T_PARAM_EXT], PARAM_TAB[indice][PARAM_T_PARAM_VALUE_NOTE],
              PARAM_TAB[indice][PARAM_T_PARAM_FORMAT_NOTE], PARAM_TAB[indice][PARAM_T_PARAM_REMARK] );

        indice++;
    };
};


