/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 

 
#define __NONE__    0
#define __INFO__    1
#define __WARN__    2
#define __ERR__     3
#define __DEBUG__   5 

/*
.--------------------------------------------------------------------------------------------.
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                   Informazoni Software                                     |
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                                                                            |
'--------------------------------------------------------------------------------------------' */

/*
   --------- INFOTAB: position and value fields 
*/
#define INFOTAB_SOFTWARE_NAME        0
#define INFOTAB_SOFTWARE_DESC        1
#define INFOTAB_SOFTWARE_REMARK      2
#define INFOTAB_SOFTWARE_VERSION     3
#define INFOTAB_SOFTWARE_RETURN_OK   4
#define INFOTAB_SOFTWARE_RETURN_KO   5
#define INFOTAB_DEVELOPER_NAME       6 
#define INFOTAB_DEVELOPER_EMAIL      7 
#define INFOTAB_DEVEL_AAAA_START     8 
#define INFOTAB_DEVEL_AAAA_STOP      9 
#define INFOTAB_LICENCE_NAME        10 
#define INFOTAB_LICENCE_VERSION     11 

char *INFOTAB[12] = {

     /* software name */       "myClientTCP",
     /* software desc */       "Universal Client TCP",
     /* software remarks */    "TCP client to measure the degree of security of a connection.",
     /* software version */    "0.0.5r6",
     /* software return Ok */  "0",
     /* software return Err */ "1", 
     /* developer_name */      "Angelo Raffaele Granito",
     /* developer_email */     "raffaele.granito@tiscali.it",
     /* start_year_develop */  "2008",
     /* stop_year_develop */   "2019",
     /* licence */             "GPL",
     /* licence version */     "3"
};


	
/*
.--------------------------------------------------------------------------------------------.
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                        PARAM Table                                         |
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                                                                            |
'--------------------------------------------------------------------------------------------' */

/* 
------ position field in PARAM_TAB 
*/
#define PARAM_T_ID                         	 	        0
#define PARAM_T_PARAM                       			1
#define PARAM_T_PARAM_EXT                   			2
#define PARAM_T_PARAM_VALUE_NOTE                                3 
#define PARAM_T_PARAM_FORMAT_NOTE                               4 
#define PARAM_T_PARAM_REMARK                		        5	
#define PARAM_T_PARAM_ERRMSG_INCOMPLETE_MISSING_VALUE 	        6 
#define PARAM_T_PARAM_ERRMSG_DUPLICATE      	        	7
#define PARAM_T_PARAM_SEQUENCE                                  8
#define PARAM_T_FLUTILIZZO                  		        9	
#define PARAM_T_VALUE                       		       10	
#define PARAM_T_REQUIRED_FIELD                                 11


/*
------ TAPPO ID Value
*/
#define __PARAM_T_ID_VALUETAPPO__                           "9999"



/*
------ Parameter Table 
*/
char *PARAM_TAB[][12] = { 

              /* ID     
                 Param 
                 Param Esteso                      
                 Param Value Note
                 Param Format Note
                 Param Remark                                                    
                 Param Incomplete Err                                                        
                 Parameter Duplicate                                                       
                 Sequence
                 FlUtilizzo             
                 Value
                 required field */ 

 	    {    "1",   
                 "-c", 
                 "--connect", 
                 "\0",
                 "protocol://host:port/path/resource?queryString",  
                 "Connection string. Protocol supported: tcp, ssl, https, ems, emss. Hostname can be set by specifying the IP or FQDN.", 
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_CONNECT", 
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_CONNECT", 
                 "1", 
                 "0", 
                 "\0",
                 "0" }, 

            {    "2",   
                 "-U", 
                 "--auth-user", 
                 "<user>",  
                 "\0",
                 "Some server implementations require user name and password to authenticate the user.", 
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_AUTH_USER", 
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_AUTH_USER", 
                 "1",
                 "0", 
                 "\0",
                 "0" }, 

            {    "3",   
                 "-P", 
                 "--auth-password",  
                 "<password>",
                 "\0",
                 "Some server implementations require user name and password to authenticate the user.", 
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_AUTH_PASSWORD", 
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_AUTH_PASSWORD", 
                 "2",
                 "0", 
                 "\0",
                 "0" }, 

            {    "4",
                 "-pH",
                 "--http-proxy-host",
                 "<host>",
                 "\0",
                 "IP e Porta sul quale risponde il servizio di proxy http.",
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_HTTPPROXY_HOST",
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_HTTPPROXY_HOST",
                 "1",
                 "0",
                 "\0",
                 "0" },

            {    "5",
                 "-pP",
                 "--http-proxy-port",
                 "<port>",
                 "\0",
                 "IP e Porta sul quale risponde il servizio di proxy http.",
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_HTTPPROXY_PORT",
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_HTTPPROXY_PORT",
                 "2",
                 "0",
                 "\0",
                 "0" },

            {    "6",
                 "-pU",
                 "--http-proxy-user",
                 "<user>",
                 "\0",
                 "Utente e Password per l’autenticazione al server proxy, se prevista.",
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_HTTPPROXY_USER",
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_HTTPPROXY_USER",
                 "1",
                 "0",
                 "\0",
                 "0" },

            {    "7",
                 "-pS",
                 "--http-proxy-password",
                 "<password>",
                 "\0",
                 "Utente e Password per l’autenticazione al server proxy, se prevista.",
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_HTTPPROXY_PASSWORD",
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_HTTPPROXY_PASSWORD",
                 "2",
                 "0",
                 "\0",
                 "0" },

            {    "4",   
                 "-k", 
                 "--keystore", 
                 "<filename>", 
                 "\0",
                 "PEM file that is contained identity of the client (private, certificate). The private key must be encrypted form.", 
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_KEYSTORE", 
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_KEYSTORE", 
                 "1",
                 "0", 
                 "\0",
                 "0" },

            {    "5",   
                 "-p", 
                 "--password", 
                 "<password>",  
                 "\0",
                 "Used to decrypt the private key in keystore.", 
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_KEYSTORE_PASSWORD", 
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_KEYSTORE_PASSWORD", 
                 "2",
                 "0", 
                 "\0",
                 "0" },

            {    "6",   
                 "-t", 
                 "--truststore", 
                 "<filename>",  
                 "\0",
                 "PEM file that contains the trusted CA, the only ones who can vouch for the identity of the server.", 
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_TRUSTSTORE", 
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_TRUSTSTORE", 
                 "1",
                 "0", 
                 "\0",
                 "0" },

            {    "7",   
                 "-c", 
                 "--ciphersuite", 
                 "{cipher1,cipher2,...,cipherN}",  
                 "\0",
                 "Ciphersuite list of proposals to the server, in order of preference. The values are separated with no spaces. Complete List of supported cipher: "
                 "NULL-{MD5|SHA},RC2-CBC-MD5, RC4-{MD5|SHA}, EXP-RC2-CBC-MD5, EXP-RC4-MD5, EXP-DES-CBC-SHA, DES-CBC-{MD5|SHA},DES-CBC3-{MD5|SHA}, AES128-SHA, AES256-SHA.",
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_CIPHERSUITE", 
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_CIPHERSUITE", 
                 "1",
                 "0", 
                 "\0",
                 "0" },

            {    "8",   
                 "-s", 
                 "--ssl-version", 
                 "<version>",  
                 "\0",
                 "Complete List of version supported: SSLv2|SSLv3|TLSv1", 
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_VERSION_SSL", 
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_SSLVERSION", 
                 "1",
                 "0", 
                 "\0",
                 "0" },

           {     "9",
                 "-T",
                 "--type-operation",
                 "<type>",  
                 "\0",
                 "...",
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_TYPE_OPERATION",
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_TYPE_OPERATION",
                 "1",
                 "0",
                 "\0",
                 "0" },

           {     "10",
                 "-Ml",
                 "--message-language",
                 "<language>",  
                 "\0",
                 "...",
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_MESSAGE_LANGUAGE",
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_MESSAGE_LANGUAGE",
                 "1",
                 "0",
                 "\0",
                 "0" },

           {     "11",
                 "-Mt",
                 "--message-type",
                 "<type>",  
                 "\0",
                 "...", 
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_MESSAGE_TYPE",
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_MESSAGE_TYPE",
                 "1",
                 "0",
                 "\0",
                 "0" },

           {     "12",
                 "-M",
                 "--message", 
                 "<text>", 
                 "\0",
                 "...",
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_MESSAGE",
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_MESSAGE",
                 "1",
                 "0",
                 "\0",
                 "0" },

           {     "13",
                 "-S",
                 "--ssl-ciphersuite-server-scan",
                 "{On|Off}",  
                 "\0",
                 "...",
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_SSL_CIPHERSUITE_SERVER_SCAN",
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_SSL_CIPHERSUITE_SERVER_SCAN",
                 "1",
                 "0", 
                 "\0",
                 "0" },

           {     "14",
                 "-Pc",
                 "--ssl-print-cert",
                 "{On|Off}",
                 "\0",
                 "...",
                 "\0",
                 "\0",
                 "1",
                 "0",
                 "\0",
                 "0" },

           {     "15",
                 "-m", 
                 "--max-request", 
                 "<max-value>",  
                 "\0",
                 "...", 
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_MAX_REQUEST", 
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_MAX_REQUEST", 
                 "1",
                 "0", 
                 "\0",
                 "0" },

           {     "16",
                 "-Tt", 
                 "--tcp-receive-timeout", 
                 "<seconds>",  
                 "\0",
                 "...", 
                 "", 
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_OUTPUT_FILE", 
                 "1",
                 "0", 
                 "\0",
                 "0" },

           {     "17",
                 "-o",
                 "--output-file",
                 "<filename>",
                 "\0",
                 "...",
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_OUTPUT_FILE",
                 "MY_CLIENT_TCP__ERRORE_DUPLICATE_PARAMETRO_DEBUG",
                 "1",
                 "0",
                 "\0",
                 "0" },

           {     "18",
                 "-d",
                 "--debug",
                 "{+protocol1+protocol2...+protocolN}",
                 "\0",
                 "...",
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_DEBUG",
                 "\0",
                 "1",
                 "0",
                 "\0",
                 "0" },

           {     "19",
                 "-f",
                 "--config",
                 "<filename>",  
                 "\0",
                 "Configuration file, as an alternative to the parameters on the command line. [function not yet implemented]",
                 "MY_CLIENT_TCP__ERRORE_ATTESO_PARAMETRO_CONFIGURATION_FILE",
                 "",
                 "1",
                 "0",
                 "\0",
                 "0" },

           {     "20",
                 "-h", 
                 "--help", 
                 "\0",
                 "\0", 
                 "Print this help and quit.", 
                 "\0", 
                 "\0", 
                 "1", 
                 "0", 
                 "\0",
                 "0" },

           {     "21",   
                 "-v", 
                 "--version", 
                 "\0",
                 "\0", 
                 "Print the version and quit.", 
                 "\0", 
                 "\0", 
                 "1", 
                 "0", 
                 "\0",
                 "0" },

          {      "9999",   
                 "\0", 
                 "\0", 
                 "\0",
                 "\0",
                 "\0", 
                 "MY_CLIENT_TCP__ERRORE_UNKNOWN_PARAMETRO", 
                 "\0", 
                 "1", 
                 "0", 
                 "\0",
                 "0" }
    };
	
		
