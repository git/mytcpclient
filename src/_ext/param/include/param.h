/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#define __DIM_MAX_COMANDO__ 2048

/*
.-------------------------------------------------------.
|                                                       |
|              Converte l'ARRAY ARGV nella              |
|            Struttura di input di libClient            |
|                                                       |
'-------------------------------------------------------' */
typedef struct tParametriTab
{
   char *T_ID ;
   char *T_PARAM ;
   char *T_PARAM_EXT ;
   char *T_PARAM_VALUE_NOTE ;
   char *T_PARAM_FORMAT_NOTE ; 
   char *T_PARAM_REMARK ;
   char *T_PARAM_ERRMSG_INCOMPLETE_MISSING_VALUE ;
   char *T_PARAM_ERRMSG_DUPLICATE ;
   char *T_PARAM_SEQUENCE ;
   char *T_FLUTILIZZO ;
   char *T_VALUE ;
   int  T_REQUIRED_FIELD ;
} tParametriTab ;

struct tParametriCheck
{
   char         *frontend             ;
   char         *frontend_version     ;
   char         *frontend_remark      ; 
   tParametriTab PARAM_TAB[100]       ;
   int           return_code          ; 
};

extern struct tParametriCheck ParametriCheck ( int nParametri, char **lParametri, char *fileconf );

extern void printListaParametri(); 
extern void printBugReport();
extern void printBanner();
extern void printFormato(); 
extern void printBugReport(); 
extern void printVersion(); 
extern void printRemark(char *remark); 
extern void printReturnCode(); 


// __EOF__


