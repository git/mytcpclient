/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 

/*
.--------------.
| headers LIBC |
'--------------' */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/*
.----------------------------.
| header Client              |
'----------------------------' */
#include "client.h"

/*
.-------------------.
| header _ext/param |
'-------------------' */
#include "param.h"

/*
.----------------------------.
| header _ext/Output Class   |                 
'----------------------------' */
#include "output.h"                     // ---------- per utilizzo di PrintMessage 

/*
.--------------------.
| header myTCPClient |
'--------------------' */
#include "myTCPClient.h" 
 

int main(int argc, char **argv)
{
    int DEBUG = 0 ;
	
    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tParametriCheck  rParametriCheck;
    struct tParametriCheck *pParametriCheck;

    struct tClientConnect   rClientConnect; 
    struct tClientConnect  *pClientConnect;

    struct tClientConnectParameter   rClientConnectParameter; 
    struct tClientConnectParameter  *pClientConnectParameter;

    /*
    .--------------------------------------------------.
    | Alloca lo spazio per contenere la struttura dati |
    | che va a riempire la funzione ClientConnect. La  |
    | seconda istruzione serve per dedicare lo spazio  |
    | allocato alla struttura dati. Successivamente    |
    | inizializza l'intera struttura con NULL ed il    |
    | campo return_code a ZERO.                        |
    '--------------------------------------------------' */
    pParametriCheck = malloc(sizeof(struct tParametriCheck));
    pParametriCheck = &rParametriCheck;
    memset((void*)&rParametriCheck,0,sizeof(rParametriCheck));
    rParametriCheck.return_code = 0;

    pClientConnectParameter = malloc(sizeof(struct tClientConnectParameter));
    pClientConnectParameter = &rClientConnectParameter;
    memset((void*)&rClientConnectParameter,0,sizeof(rClientConnectParameter));
  //rClientConnectParameter.return_code = 0;

    pClientConnect = malloc(sizeof(struct tClientConnect));
    pClientConnect = &rClientConnect;
    memset((void*)&rClientConnect,0,sizeof(rClientConnect));
    rClientConnect.return_code = 0;

		
    /*
    .--------------------------------------------------.
    | Verifica i parametri passati                     |
    '--------------------------------------------------' */
    char *ParametriCheck_fileconf = "<file-da-inserire>\0" ; /* file di configurazione della ParametriCheck */  
    rParametriCheck =
       ParametriCheck ( argc, argv, ParametriCheck_fileconf );
    if (rParametriCheck.return_code != 0)
        exit(0);

    /*
    .--------------------------------------------------.
    | Converte i parametri passati nel formato client  |
    '--------------------------------------------------' */
    rClientConnectParameter = ParametriCheck2ClientConnect (rParametriCheck);
	
    /*
    .---------------------------.
    | invoca il client          |
    '---------------------------' */	
    rClientConnect = 
        ClientConnect ( rClientConnectParameter ); 

    exit (0);
	
};     


/*
.-----------------------------------------------------------------.
|                                                                 |
|  Funzione    : ParametriCheck2ClientConnect                     |
|                                                                 |
|  Descrizione : Converte la struttura di output della            |
|                ParametriCheck nella struttura di input della    |
|                ClientConnect.                                   |
|                                                                 |
|  return-code : struttura complessa con return-code              |
|                                                                 |
|  dipendenza  : frontend.conf.getVar()                           |
|                frontend.conf.LoadConf()                         |
|                client.common.output.PrintMessage()              |
|                                                                 |
|  TODO       :                                                   |
|                                                                 | 
'-----------------------------------------------------------------'*/
struct tClientConnectParameter ParametriCheck2ClientConnect (struct tParametriCheck rParametriCheck)
{
    int DEBUG = 1 ;

    /*
    .------------------------------------------.
    |  Determina il livello di DEBUG           |
    '------------------------------------------' */
    int debug_level = GetDebugLevel ( "+++param", "param" ) ; 
	
    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tClientConnectParameter   rClientConnectParameter; 
    struct tClientConnectParameter  *pClientConnectParameter;

    /*
    .--------------------------------------------------.
    | Alloca lo spazio per contenere la struttura dati |
    | che va a riempire la funzione ClientConnect. La  |
    | seconda istruzione serve per dedicare lo spazio  |
    | allocato alla struttura dati. Successivamente    |
    | inizializza l'intera struttura con NULL ed il    |
    | campo return_code a ZERO.                        |
    '--------------------------------------------------' */
    pClientConnectParameter = malloc(sizeof(struct tClientConnectParameter));
    pClientConnectParameter = &rClientConnectParameter;
    memset((void*)&rClientConnectParameter,0,sizeof(rClientConnectParameter));
	
    /*
    .---------------------------------------------------------.
    | Inizializza struttura di OUTPUT della funzione          |
    '---------------------------------------------------------' */
    rClientConnectParameter.frontend                  = 0 ;
    rClientConnectParameter.frontend_version          = 0 ;
    rClientConnectParameter.url                       = 0 ;
    rClientConnectParameter.protocollo                = 0 ;
    rClientConnectParameter.host                      = 0 ;
    rClientConnectParameter.porta                     = 0 ;
    rClientConnectParameter.risorsa_path              = 0 ; 
    rClientConnectParameter.risorsa                   = 0 ;
    rClientConnectParameter.queryStringDimTabVariable = 0 ; 
    rClientConnectParameter.queryStringLen            = 0 ; 
  //rClientConnectParameter.queryStringTabVar[10]     =   ; 
    rClientConnectParameter.httpProxyHost             = 0 ;
    rClientConnectParameter.httpProxyPort             = 0 ;
    rClientConnectParameter.httpProxyUser             = 0 ;
    rClientConnectParameter.httpProxyPassword         = 0 ; 
    rClientConnectParameter.auth_user                 = 0 ;
    rClientConnectParameter.auth_password             = 0 ;
    rClientConnectParameter.keystore                  = 0 ;
    rClientConnectParameter.password                  = 0 ;
    rClientConnectParameter.truststore                = 0 ;
    rClientConnectParameter.ciphersuite               = 0 ;
    rClientConnectParameter.sslversion                = 0 ;
    rClientConnectParameter.type_operation            = 0 ; 
    rClientConnectParameter.message_language          = 0 ;
    rClientConnectParameter.message_type              = 0 ;
    rClientConnectParameter.message                   = 0 ; 
    rClientConnectParameter.SSLCipherSuiteServerScan  = 0 ; 
    rClientConnectParameter.MaxRequest                = 0 ;
    rClientConnectParameter.TCPReceiveTimeout         = 0 ; 
    rClientConnectParameter.OutputFile                = 0 ;
    rClientConnectParameter.debug                     = 0 ;

    /*
    .------------------------------------------------------------------.
    | Valorizzazione delle struttura di OUTPUT della funzione          |
    '------------------------------------------------------------------' */
    rClientConnectParameter.frontend = malloc(strlen(rParametriCheck.frontend)+1); 
    memset(rClientConnectParameter.frontend, 0, strlen(rParametriCheck.frontend)+1); 
    strcpy ( rClientConnectParameter.frontend, rParametriCheck.frontend );

    rClientConnectParameter.frontend_version = malloc(strlen(rParametriCheck.frontend_version)+1); 
    memset(rClientConnectParameter.frontend_version, 0, strlen(rClientConnectParameter.frontend_version)+1); 
    strcpy ( rClientConnectParameter.frontend_version, rParametriCheck.frontend_version );

    register short int i = 0 ;
    while ( strcmp(rParametriCheck.PARAM_TAB[i].T_ID,"9999") != 0 )
    {
            /* ---=== -c | --connect ===--- */
            if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-c")==0 &&
                        strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
                    rClientConnectParameter.url = malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
                    strcpy(rClientConnectParameter.url, rParametriCheck.PARAM_TAB[i].T_VALUE);
            }

            /* ---=== -pH | --http-proxy-host ===--- */
            if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-pH")==0 &&
                        strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
                    rClientConnectParameter.httpProxyHost = malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
                    strcpy(rClientConnectParameter.httpProxyHost, rParametriCheck.PARAM_TAB[i].T_VALUE);
            }

            /* ---=== -pP | --http-proxy-port ===--- */
            if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-pP")==0 &&
                        strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
                    rClientConnectParameter.httpProxyPort = malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
                    strcpy(rClientConnectParameter.httpProxyPort, rParametriCheck.PARAM_TAB[i].T_VALUE);
            }

            /* ---=== -pU | --http-proxy-user ===--- */
            if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-pU")==0 &&
                        strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
                    rClientConnectParameter.httpProxyUser = malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
                    strcpy(rClientConnectParameter.httpProxyUser, rParametriCheck.PARAM_TAB[i].T_VALUE);
            }

            /* ---=== -pS | --http-proxy-password ===--- */
            if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-pS")==0 &&
                        strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
                    rClientConnectParameter.httpProxyPassword = malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
                    strcpy(rClientConnectParameter.httpProxyPassword, rParametriCheck.PARAM_TAB[i].T_VALUE);
            }

            /* ---=== -U | --auth-user ===--- */							
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-U")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		     rClientConnectParameter.auth_user = malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
		     strcpy(rClientConnectParameter.auth_user, rParametriCheck.PARAM_TAB[i].T_VALUE);
	    }
				
	    /* ---=== -P | --auth-password ===--- */							
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-P")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		     rClientConnectParameter.auth_password = malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
		     strcpy(rClientConnectParameter.auth_password, rParametriCheck.PARAM_TAB[i].T_VALUE);  
	    }
							
	    /* ---=== -k | --keystore ===--- */
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-k")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		     rClientConnectParameter.keystore = malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
		     strcpy(rClientConnectParameter.keystore, rParametriCheck.PARAM_TAB[i].T_VALUE);
	    }
							
   	    /* ---=== -p | --password ===--- */				
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-p")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		     rClientConnectParameter.password = malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
		     strcpy(rClientConnectParameter.password, rParametriCheck.PARAM_TAB[i].T_VALUE); 
	    }
					
	    /* -t | --truststore */							
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-t")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		     rClientConnectParameter.truststore=malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
		     strcpy(rClientConnectParameter.truststore, rParametriCheck.PARAM_TAB[i].T_VALUE);
	    }
							
	    /* -C | --ciphersuite */							
  	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-C")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		     rClientConnectParameter.ciphersuite=malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
	 	     strcpy(rClientConnectParameter.ciphersuite, rParametriCheck.PARAM_TAB[i].T_VALUE);
	    }
				
	    /* -s | --ssl-version */							
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-s")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		    rClientConnectParameter.sslversion=malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
		    strcpy(rClientConnectParameter.sslversion, rParametriCheck.PARAM_TAB[i].T_VALUE);
	    }
							
	    /* -T | --type-operation */							
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-T")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		    rClientConnectParameter.type_operation=malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
		    strcpy(rClientConnectParameter.type_operation, rParametriCheck.PARAM_TAB[i].T_VALUE);
	    }
							
	    /* -Ml | --message-language */							
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-Ml")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		    rClientConnectParameter.message_language=malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
		    strcpy(rClientConnectParameter.message_language, rParametriCheck.PARAM_TAB[i].T_VALUE);
	    }
							
	    /* -Mt | --message-type */							
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-Mt")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		    rClientConnectParameter.message_type=malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
                    strcpy(rClientConnectParameter.message_type, rParametriCheck.PARAM_TAB[i].T_VALUE);
	    }
							
	    /* -M | --message */	
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-M")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		    rClientConnectParameter.message=malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
		    strcpy(rClientConnectParameter.message, rParametriCheck.PARAM_TAB[i].T_VALUE);
	    }
							
	    /* -S | --ssl-ciphersuite-server-scan */	
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-S")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		    rClientConnectParameter.SSLCipherSuiteServerScan=malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1); 
		    strcpy(rClientConnectParameter.SSLCipherSuiteServerScan, rParametriCheck.PARAM_TAB[i].T_VALUE);
	    }
							
	    /* -Pc | --ssl-print-cert */							
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-Pc")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		 // .....
	    }
							
	    /* -m | -max-request */	
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-m")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		    rClientConnectParameter.MaxRequest = atol(rParametriCheck.PARAM_TAB[i].T_VALUE);
	    }
							
	    /* -Tt | --tcp-receive-timeout */
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-Tt")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
 		    rClientConnectParameter.TCPReceiveTimeout = atol(rParametriCheck.PARAM_TAB[i].T_VALUE); 
	    }
							
	    /* -o | --output-file */	
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-o")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		    rClientConnectParameter.OutputFile=malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
		    strcpy(rClientConnectParameter.OutputFile, rParametriCheck.PARAM_TAB[i].T_VALUE);
	    }
							
	    /* -d | --debug */
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-d")==0 && 
			strcmp(rParametriCheck.PARAM_TAB[i].T_FLUTILIZZO, "1")==0 ) {
		    rClientConnectParameter.debug=malloc(strlen(rParametriCheck.PARAM_TAB[i].T_VALUE)+1);
		    strcpy(rClientConnectParameter.debug, rParametriCheck.PARAM_TAB[i].T_VALUE);
	    }
							
	    /* -f | --config *//*	
	    if ( strcmp(rParametriCheck.PARAM_TAB[i].T_PARAM, "-f")==0 && 
		    strcmp(rParametriCheck.PARAM_TAB_EXT[i].T_FLUTILIZZO, "1")==0 ) {
	    } */
							
	    /* --help è nel parametri *//*							
	    if ( strcmp(rParametriCheck.PARAM_TAB_EXT[i].T_PARAM, "--help")==0 && 
		    strcmp(rParametriCheck.PARAM_TAB_EXT[i].T_FLUTILIZZO, "1")==0 ) {
	    } */
	  
	    /* --version è nei parametri *//*							
            if ( strcmp(rParametriCheck.PARAM_TAB_EXT[i].T_PARAM, "--version")==0 && 
    		    strcmp(rParametriCheck.PARAM_TAB_EXT[i].T_FLUTILIZZO, "1")==0 ) {
	    } */
		
	    i++;
    }		
	
	
    /*
    .------------------------------------------------------------------.
    | Visualizza i parametri impostati analizzati dalla routine per    |
    | attività di DEBUG. (DEBUG = 1|0 * Attiva|Non Attiva).            |
    '------------------------------------------------------------------' */ 
    if ( DEBUG == 0 )
           clientConnectParameterPrint ( rClientConnectParameter );

    return rClientConnectParameter ;
};  



   
