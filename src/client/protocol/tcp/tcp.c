/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/*
.=====================================================================.
|                                                                     |
    Libreria         : client.protocol.TCP  
                                      
    Description      :                 


    Elenco funzioni

       funzione                 descrizione
    ------------------------------------------------------------
    1. TCPConnect               Open TCP Sessione
    2. TCPSocketSetBlocking     Set Socket Blocking
    3. TCPSocketSetNotBlocking  Set Socket Not Blocking
    4. TCPGetHostByName         Resolve hostname|fqdn
    5. TCPSend                  Send string to server
    6. TCPReceive               Receive string from server
    7. TCPReceiveSetTimeout     Set Timeout for Receive string from server 
    8. TCPDisconnect            Close TCP Session
    9. TCPClientConnect         Minimal TCP Client
|                                                                     |
'=====================================================================' */ 

/*
.--------------.
| headers LIBC |
'--------------' */
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <unistd.h> 

/*
.-------------------------.
| header common           |
'-------------------------' */
#include "client.h"
#include "output.h" 

/*
.-----------------.
| header locale   |
'-----------------' */
#include "tcp.h"  
#include "tcp_messages.h"


/*
.-------------------------------------------------------------------------------------.
|                                                                                     |
|  Function    : TCPConnect                                                           |
|                                                                                     |
|  Description : Open TCP Session                                                     |
|                                                                                     |
|                Verify socket address (IP:port) and Open TCP Session                 | 
|                The messages 1,2,3 are the OPEN SESSION                              |  
|                TCP three way handshake ;)                                           | 
|                I use socket() function.                                             | 
|                                                                                     |
|                .--------.                                 .--------.                | 
|                |        |       [1] SYN Message           |        |                |
|                | Client | ------------------------------> | Server |                |
|                |        |                                 |        |                |
|                |        |       [2] SYN ACK Message       |        |                |
|                |        | <------------------------------ |        |                |
|                |        |                                 |        |                |
|                |        |       [3] ACK Message           |        |                |
|                |        | ------------------------------> |        |                |
|                |        |                                 |        |                |
|                '--------'                                 '--------'                |
|                                                                                     |
|  Parameter   :  1. host          string                                             |
|                 2. porta         int                                                |
|                 3. debug_level   {__INFO__|__WARNING__|__ERROR__}                   | 
|                                                                                     |
|  Return-code :  0 Ok                                                                |
|                -1 Hostname Resolv Error                                             |
|                -2 TCP Port Error                                                    |
|                -3 Create TCP Socket Error                                           |
|                -4 Connect TCP Socket Server Error                                   |
|                                                                                     |
|  Dipendenze  : this->TCPGetHostByName                                               |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
'-------------------------------------------------------------------------------------'*/
extern struct tTCPConnect TCPConnect( char *host, int porta, long int timeout, int debug_level )
{
    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , "", ""                                  }
    };

    /*
    .------------------------------------------.
    | Dichiarazione delle strutture TCP/IP     |
    '------------------------------------------' */
    struct sockaddr_in addr ;

    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tTCPConnect  rTCPConnect;
    struct tTCPConnect *pTCPConnect;

    /*
    .------------------------------------------.
    | Allocazione SPAZIO                       |
    '------------------------------------------' */
    pTCPConnect = malloc(sizeof(struct tTCPConnect));

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pTCPConnect = &rTCPConnect;

    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rTCPConnect,0,sizeof(rTCPConnect)); 
    rTCPConnect.return_code = 0;


    /*
    .------------------------------------------------------------------------.
    | PASSO 1. Risoluzione Hostname                                          |
    +------------------------------------------------------------------------+
    |                                                                        |
    |                                                                        |
    |                                                                        |
    |                                                                        |
    '------------------------------------------------------------------------' */

    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tTCPGetHostByName  rTCPGetHostByName;
    struct tTCPGetHostByName *pTCPGetHostByName;

    /*
    .------------------------------------------.
    | Allocazione SPAZIO                       |
    '------------------------------------------' */
    pTCPGetHostByName = malloc(sizeof(struct tTCPGetHostByName));

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pTCPGetHostByName = &rTCPGetHostByName;

    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rTCPGetHostByName, 0, sizeof(rTCPGetHostByName));

    /*
    .-------------.
    | Risoluzione |
    '-------------' */ 
    PrintMessage(debug_level, TCP_TABMSG, "TCP_RESOLVHOST_RESOLVHOST", TabVariabiliEMPTY, 0, 0, 0);
    rTCPGetHostByName = TCPGetHostByName ( host, debug_level );
    if ( rTCPGetHostByName.return_code < 0 )
    {
         /*
         .----------------------.
         | Messaggio di errore  |
         '----------------------' */ 
         PrintMessage(debug_level, TCP_TABMSG, "TCP_RESOLVHOST_KO", TabVariabiliEMPTY, 0, 0, 0);
         PrintMessage(debug_level, TCP_TABMSG, "TCP_RESOLVHOST_RESOLVHOST_KO", TabVariabiliEMPTY, 0, 0, 0);

         /*
         .----------------------.
         | Struttura di ritorno |
         '----------------------' */
         rTCPConnect.idmsg       = 0                                               ;
         rTCPConnect.msgDesc     = malloc(strlen("TCP.CONNECT.RESOLV.ERROR"))      ;
         rTCPConnect.msgDesc     = "TCP.CONNECT.RESOLV.ERROR"                      ;
         rTCPConnect.return_code = -1                                              ;
         return rTCPConnect;
    }
    PrintMessage(debug_level, TCP_TABMSG, "TCP_RESOLVHOST_OK", TabVariabiliEMPTY, 0, 0, 0);

    /*
    .--------------------------------------------------------------.
    | Muove l'IP risolto [solo il primo] nella struttura di OUTPUT |           
    '--------------------------------------------------------------' */
    rTCPConnect.ip = rTCPGetHostByName.tabAddress[0]; 

    /*
    .------------------------------------------------.
    | Visualizza in modalità di debug gli IP risolti |
    '------------------------------------------------' */
    if ( debug_level == __INFO__ )
    {
         int indice = 0;
         while ( indice < rTCPGetHostByName.dimTabAddress )
         { 
                 printf("+Host [%s] => IP [%s]\n", host, rTCPGetHostByName.tabAddress[indice] );
                 indice++;
         };
    }


    /*
    .------------------------------------------------------------------------.
    | PASSO 2. Verifica la PORTA TCP/IP                                      |
    +------------------------------------------------------------------------+
    |                                                                        |
    |                                                                        |
    |                                                                        |
    |                                                                        |
    '------------------------------------------------------------------------' */

    /*
          funzionalità controllo della port ritondata
          già implementata nella funzionalità sotto specificata
          client/common/output/output.c (function U r i 2SingleFieldConnect)
          pertanto viene commentata.
                                                                            */

//  *----- Nuovo -----*    
//  rTCPConnect.port = porta ;

//  *---- Vecchio ----* 
    PrintMessage(debug_level, TCP_TABMSG, "TCP_CONNECT_VERIFYPORTRANGE", TabVariabiliEMPTY, 0, 0, 0);
    if ( porta < 1 || porta > 65635 )
    {
       /*
       .----------------------.
       | Messaggio di errore  |
       '----------------------' */
       PrintMessage(debug_level, TCP_TABMSG, "TCP_CONNECT_KO", TabVariabiliEMPTY, 0, 0, 0);
 
       /*
       .----------------------.
       | Struttura di ritorno |
       '----------------------' */
       rTCPConnect.idmsg       = 0                                                   ;
       rTCPConnect.msgDesc     = malloc(strlen("TCP.CONNECT.VERIFYPORTRANGE.ERROR")) ;
       rTCPConnect.msgDesc     = "TCP.CONNECT.VERIFYPORTRANGE.ERROR"                 ;
       rTCPConnect.return_code = -2                                                  ;
       return rTCPConnect;
    }

    rTCPConnect.port = porta ;
    PrintMessage(debug_level, TCP_TABMSG, "TCP_CONNECT_OK", TabVariabiliEMPTY, 0, 0, 0);
 

    /*
    .------------------------------------------------------------------------.
    | PASSO 3. Creazione SOCKET                                              |
    +------------------------------------------------------------------------+
    |                                                                        | 
    | The socket() function shall create an unbound socket in a              |
    | communications domain, and return a file descriptor that can be used   | 
    | in later function calls that operate on sockets.                       | 
    |                                                                        |
    | The socket() function takes the following arguments:                   | 
    |                                                                        |
    | domain         | Specifies the communications domain in which a        |
    |                | socket is to be created.                              | 
    |                |                                                       |
    | type           | Specifies the type of socket to be created.           | 
    |                |                                                       |
    | protocol       | Specifies a particular protocol to be used with the   |
    |                | socket. Specifying a protocol of 0 causes socket()    | 
    |                | to use an unspecified default protocol appropriate    |
    |                | for the requested socket type.                        | 
    |                                                                        |
    | The domain argument specifies the address family used in the           |
    | communications domain. The address families supported by the system    | 
    | are implementation-defined.                                            | 
    |                                                                        |
    | Symbolic constants that can be used for the domain argument are        |
    | defined in the <sys/socket.h> header.                                  | 
    |                                                                        |
    | The type argument specifies the socket type, which determines the      |
    | semantics of communication over the socket. The following socket types |  
    | are defined; implementations may specify additional socket types :     | 
    |                                                                        |
    | SOCK_STREAM    | Provides sequenced, reliable, bidirectional,          |
    |                | connection-mode byte streams, and may provide a       | 
    |                | transmission mechanism for out-of-band data.          | 
    |                |                                                       |
    | SOCK_DGRAM     | Provides datagrams, which are connectionless-mode,    |
    |                | unreliable messages of fixed maximum length.          | 
    |                |                                                       |
    | SOCK_SEQPACKET | Provides sequenced, reliable, bidirectional,          |
    |                | connection-mode transmission paths for records.       | 
    |                | A record can be sent using one or more output         |
    |                | operations and received using one or more input       | 
    |                | operations, but a single operation never transfers    |
    |                | part of more than one record.                         | 
    |                | Record boundaries are visible to the receiver via     |
    |                | the MSG_EOR flag.                                     | 
    |                                                                        |
    | If the protocol argument is non-zero, it shall specify a protocol      |
    | that is supported by the address family.                               | 
    | If the protocol argument is zero, the default protocol for this        |
    | address family and type shall be used.                                 | 
    | The protocols supported by the system are implementation-defined.      |  
    |                                                                        |
    | The process may need to have appropriate privileges to use the         |
    | socket() function or to create some sockets.                           | 
    |                                                                        |
    |                                                                        |
    | >> RETURN VALUE                                                        | 
    |                                                                        |
    | Upon successful completion, socket() shall return a non-negative       |
    | integer, the socket file descriptor. Otherwise, a value of -1 shall    | 
    | be returned and errno set to indicate the error.                       |
    |                                                                        |
    |                                                                        |
    | >> ERRORS                                                              | 
    |                                                                        |
    | The socket() function shall fail if:                                   |  
    |                                                                        |
    | [EAFNOSUPPORT]    | The implementation does not support the specified  |
    |                   | address family.                                    | 
    | [EMFILE]          | No more file descriptors are available for         |
    |                   | this process.                                      |  
    | [ENFILE]          | No more file descriptors are available for         |
    |                   | the system.                                        | 
    | [EPROTONOSUPPORT] | The protocol is not supported by the address       |
    |                   | family, or the protocol                            | 
    |                   | is not supported by the implementation.            | 
    | [EPROTOTYPE]      | The socket type is not supported by the protocol.  | 
    |                                                                        |
    | The socket() function may fail if:                                     | 
    |                                                                        |
    | [EACCES]          | The process does not have appropriate privileges.  |
    | [ENOBUFS]         | Insufficient resources were available in the       |
    |                   | system to perform the operation.                   |
    | [ENOMEM]          | Insufficient memory was available to fulfill       |
    |                   | the request.                                       |  
    |                                                                        |
    '------------------------------------------------------------------------' */	
    PrintMessage(debug_level, TCP_TABMSG, "TCP_CONNECT_SOCKETOPEN", TabVariabiliEMPTY, 0, 0, 0);
    if((rTCPConnect.sock=socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))<0)   
    {
         rTCPConnect.idmsg = errno ;
         rTCPConnect.return_code = -3 ;

         PrintMessage(debug_level, TCP_TABMSG, "TCP_CONNECT_KO", TabVariabiliEMPTY, 0, 0, 0);
         PrintMessage(debug_level, TCP_TABMSG, "TCP_CONNECT_SOCKETOPEN_KO", TabVariabiliEMPTY, 0, 0, 0);
         switch ( rTCPConnect.idmsg )
         {
                  case EAFNOSUPPORT    : PrintMessage(debug_level, TCP_TABMSG, "EAFNOSUPPORT", TabVariabiliEMPTY, 0, 0, 0);  
                                         rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.SOCKET.EAFNOSUPPORT"));
                                         rTCPConnect.msgDesc = "TCP.CONNECT.SOCKET.EAFNOSUPPORT";
                                         break;

                  case EMFILE          : PrintMessage(debug_level, TCP_TABMSG, "EMFILE", TabVariabiliEMPTY, 0, 0, 0); 
                                         rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.SOCKET.EMFILE"));
                                         rTCPConnect.msgDesc = "TCP.CONNECT.SOCKET.EMFILE";
                                         break;

                  case ENFILE          : PrintMessage(debug_level, TCP_TABMSG, "ENFILE", TabVariabiliEMPTY, 0, 0, 0); 
                                         rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.SOCKET.ENFILE"));
                                         rTCPConnect.msgDesc = "TCP.CONNECT.SOCKET.ENFILE";
                                         break;

                  case EPROTONOSUPPORT : PrintMessage(debug_level, TCP_TABMSG, "EPROTONOSUPPORT", TabVariabiliEMPTY, 0, 0, 0); 
                                         rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.SOCKET.EPROTONOSUPPORT"));
                                         rTCPConnect.msgDesc = "TCP.CONNECT.SOCKET.EPROTONOSUPPORT";
                                         break;

                  case EPROTOTYPE      : PrintMessage(debug_level, TCP_TABMSG, "EPROTOTYPE", TabVariabiliEMPTY, 0, 0, 0); 
                                         rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.SOCKET.EPROTOTYPE"));
                                         rTCPConnect.msgDesc = "TCP.CONNECT.SOCKET.EPROTOTYPE";
                                         break;

                  case EACCES          : PrintMessage(debug_level, TCP_TABMSG, "EACCES", TabVariabiliEMPTY, 0, 0, 0); 
                                         rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.SOCKET.EACCES"));
                                         rTCPConnect.msgDesc = "TCP.CONNECT.SOCKET.EACCES";
                                         break;

                  case ENOBUFS         : PrintMessage(debug_level, TCP_TABMSG, "ENOBUFS", TabVariabiliEMPTY, 0, 0, 0);  
                                         rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.SOCKET.ENOBUFS"));
                                         rTCPConnect.msgDesc = "TCP.CONNECT.SOCKET.ENOBUFS";
                                         break;

                  case ENOMEM          : PrintMessage(debug_level, TCP_TABMSG, "ENOMEM", TabVariabiliEMPTY, 0, 0, 0);  
                                         rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.SOCKET.ENOMEM"));
                                         rTCPConnect.msgDesc = "TCP.CONNECT.SOCKET.ENOMEM";
                                         break;

                  default              : PrintMessage(debug_level, TCP_TABMSG, "CREATE_SOCKET_UNKNOWN_ERROR", TabVariabiliEMPTY, 0, 0, 0); 
                                         rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.SOCKET.CREATE_SOCKET_UNKNOWN_ERROR"));
                                         rTCPConnect.msgDesc = "TCP.CONNECT.SOCKET.CREATE_SOCKET_UNKNOWN_ERROR";
                                         break;
         };

         return rTCPConnect;

    }
    PrintMessage(debug_level, TCP_TABMSG, "TCP_CONNECT_OK", TabVariabiliEMPTY, 0, 0, 0);


    /*
    .------------------------------------------------------------------------.
    | PASSO 4. Connessione SOCKET                                            |
    +------------------------------------------------------------------------+
    |                                                                        |
    | The connect() function shall attempt to make a connection on a socket. |
    | The function takes the following arguments :                           |
    |                                                                        |
    | socket           |  Specifies the file descriptor associated with      |
    |                  |  the socket.                                        | 
    |                  |                                                     |
    | address          |  Points to a sockaddr structure containing the      |
    |                  |  peer address. The length and format of the address |
    |                  |  depend on the address family of the socket.        |  
    |                  |                                                     | 
    | address_len      |  Specifies the length of the sockaddr structure     |
    |                  |  pointed to by the address argument.                | 
    |                                                                        |
    |                                                                        |
    | If the socket has not already been bound to a local address, connect() |
    | shall bind it to an address which, unless the socket's address family  |  
    | is AF_UNIX, is an unused local address.                                | 
    |                                                                        |
    | If the initiating socket is not connection-mode, then connect() shall  |
    | set the socket's peer address, and no connection is made. For          |
    | SOCK_DGRAM sockets, the peer address identifies where all datagrams    | 
    | are sent on subsequent send() functions, and limits the remote sender  |
    | for subsequent recv() functions. If address is a null address for the  |
    | protocol, the socket's peer address shall be reset.                    | 
    |                                                                        |
    | If the initiating socket is connection-mode, then connect() shall      |
    | attempt to establish a connection to the address specified by the      |
    | address argument. If the connection cannot be established immediately  | 
    | and O_NONBLOCK is not set for the file descriptor for the socket,      |
    | connect() shall block for up to an unspecified timeout interval until  |
    | the connection is established. If the timeout interval expires before  |
    | the connection is established, connect() shall fail and the connection |
    | attempt shall be aborted. If connect() is interrupted by a signal      | 
    | that is caught while blocked waiting to establish a connection,        |
    | connect() shall fail and set errno to [EINTR], but the connection      |
    | request shall not be aborted, and the connection shall be              | 
    | established asynchronously.                                            | 
    |                                                                        |
    | If the connection cannot be established immediately and O_NONBLOCK is  |
    | set for the file descriptor for the socket, connect() shall fail and   | 
    | set errno to [EINPROGRESS], but the connection request shall not be    |
    | aborted, and the connection shall be established asynchronously.       |
    | Subsequent calls to connect() for the same socket, before the          | 
    | connection is established, shall fail and set errno to [EALREADY].     |  
    |                                                                        |
    | When the connection has been established asynchronously, select() and  |
    | poll() shall indicate that the file descriptor for the socket is ready |
    | for writing.                                                           | 
    |                                                                        |
    | The socket in use may require the process to have appropriate          |
    | privileges to use the connect() function.                              | 
    |                                                                        |
    | RETURN VALUE                                                           |
    |                                                                        |
    | Upon successful completion, connect() shall return 0; otherwise, -1    |
    | shall be returned and errno set to indicate the error.                 | 
    |                                                                        |
    | ERRORS                                                                 | 
    |                                                                        |
    | The connect() function shall fail if :                                 | 
    |                                                                        |
    | [EADDRNOTAVAIL]  |  The specified address is not available from the    |
    |                  |  local machine.                                     | 
    | [EAFNOSUPPORT]   |  The specified address is not a valid address for   |
    |                  |  the address family of the specified socket.        | 
    | [EALREADY]       |  A connection request is already in progress for    |
    |                  |  the specified socket.                              | 
    | [EBADF]          |  The socket argument is not a valid file            |
    |                  |  descriptor.                                        | 
    | [ECONNREFUSED]   |  The target address was not listening for           |
    |                  |  connections or refused the connection request.     | 
    | [EINPROGRESS]    |  O_NONBLOCK is set for the file descriptor for the  |
    |                  |  socket and the connection cannot be immediately    |
    |                  |  established; the connection shall be established   | 
    |                  |  asynchronously.                                    | 
    |                  |                                                     |
    | [EINTR]          |  The attempt to establish a connection was          |
    |                  |  interrupted by delivery of a signal that was       |  
    |                  |  caught; the connection shall be established        |
    |                  |  asynchronously.                                    | 
    |                  |                                                     |
    | [EISCONN]        |  The specified socket is connection-mode and is     |  
    |                  |  already connected.                                 | 
    | [ENETUNREACH]    |  No route to the network is present.                | 
    | [ENOTSOCK]       |  The socket argument does not refer to a socket.    | 
    | [EPROTOTYPE]     |  The specified address has a different type than    | 
    |                  |  the socket bound to the specified peer address.    | 
    | [ETIMEDOUT]      |  The attempt to connect timed out before a          |
    |                  |  connection was made.                               | 
    |                                                                        |
    |                                                                        |
    | If the address family of the socket is AF_UNIX, then connect()         |
    | shall fail if :                                                        | 
    |                                                                        |
    | [EIO]            | An I/O error occurred while reading from or writing |
    |                  | to the file system.                                 | 
    | [ELOOP]          | A loop exists in symbolic links encountered during  |
    |                  | resolution of the pathname in address.              | 
    | [ENAMETOOLONG]   | A component of a pathname exceeded {NAME_MAX}       |
    |                  | characters, or an entire pathname exceeded          |
    |                  | {PATH_MAX} characters.                              | 
    | [ENOENT]         | A component of the pathname does not name an        |
    |                  | existing file or the pathname is an empty string.   | 
    | [ENOTDIR]        | A component of the path prefix of the pathname in   |
    |                  | address is not a directory.                         | 
    |                                                                        |
    | The connect() function may fail if :                                   | 
    |                                                                        |
    | [EACCES]         | Search permission is denied for a component of the  |
    |                  | path prefix; or write access to the named socket    |
    |                  | is denied.                                          | 
    | [EADDRINUSE]     | Attempt to establish a connection that uses         |
    |                  | addresses that are already in use.                  | 
    | [ECONNRESET]     | Remote host reset the connection request.           | 
    | [EHOSTUNREACH]   | The destination host cannot be reached (probably    | 
    |                  | because the host is down or a remote router cannot  | 
    |                  | reach it).                                          | 
    | [EINVAL]         | The address_len argument is not a valid length for  | 
    |                  | the address family; or invalid address family in    |
    |                  | the sockaddr structure.                             | 
    | [ELOOP]          | More than {SYMLOOP_MAX} symbolic links were         |
    |                  | encountered during resolution of the pathname in    |
    |                  | address.                                            | 
    | [ENAMETOOLONG]   | Pathname resolution of a symbolic link produced an  |
    |                  | intermediate result whose length exceeds            |
    |                  | {PATH_MAX}.                                         | 
    | [ENETDOWN]       | The local network interface used to reach the       |
    |                  | destination is down.                                | 
    | [ENOBUFS]        | No buffer space is available.                       | 
    | [EOPNOTSUPP]     | The socket is listening and cannot be connected.    | 
    |                                                                        |
    |                                                                        |
    '------------------------------------------------------------------------' */
	fd_set fdset;
        
	int so_error;
        
    socklen_t len;
		
    int seconds;                                                                        

    struct hostent *hp ;

    /* struttura tempo impostato con il timeout passato */
	struct timeval Timeout;
    Timeout.tv_sec = timeout;
    Timeout.tv_usec = 0;

	/* struttura address da passare alla connect */
    struct sockaddr_in address;
    memset(&addr,0,sizeof(addr));
    addr.sin_addr.s_addr = inet_addr ( rTCPGetHostByName.tabAddress[0] ) ;
    addr.sin_family = AF_INET;
    addr.sin_port   = htons(porta);

	
    PrintMessage(debug_level, TCP_TABMSG, "TCP_CONNECT_SOCKET", TabVariabiliEMPTY, 0, 0, 0);
	                                                                                       
    /*
    .-------------------------------------------------------.
    | Setup "non blocking" socket, se impostato un timeout  |
	| In questa modalità la connect non aspetta la risposta |
	| (ACK) dal target e prosegue. La risposta la riceve    |
	| in modalità asincrona.                                |
    '-------------------------------------------------------' */
    if ( timeout > 0 )
           fcntl(rTCPConnect.sock, F_SETFL, O_NONBLOCK); 
                                                                                           																						 																						   
    /*
    .-------------------.
    | Connect           |
    '-------------------' */
    int rc = connect ( rTCPConnect.sock, (struct sockaddr *) &addr,sizeof(addr) ) ;

	/* CASO 1: Esito Positivo, --- Nessun Timeout --- */
    if ( rc == 0 )
    {
         PrintMessage(debug_level, TCP_TABMSG, "TCP_CONNECT_OK", TabVariabiliEMPTY, 0, 0, 0);

         /*
         .----------------------.
         | Struttura di ritorno |
         '----------------------' */
         rTCPConnect.idmsg       = 0                                               ;
         rTCPConnect.msgDesc     = malloc(strlen("TCP.CONNECT.OK"))                ;
         rTCPConnect.msgDesc     = "TCP.CONNECT.OK"                                ;
         rTCPConnect.return_code = 0                                               ;
         return rTCPConnect;
    }
	
	/* CASO 2: Esito Negativo, escludendo l'EINPROGRESS nel caso di TIMEOUT (socket non blocking) */
    if ( rc < 0 && errno != EINPROGRESS )                                                  	
    {   
         rTCPConnect.idmsg       = errno ;
         rTCPConnect.return_code = -4    ;

         PrintMessage(debug_level, TCP_TABMSG, "TCP_CONNECT_KO", TabVariabiliEMPTY, 0, 0, 0);
         PrintMessage(debug_level, TCP_TABMSG, "TCP_CONNECT_SOCKET_KO", TabVariabiliEMPTY, 0, 0, 0);
         switch ( rTCPConnect.idmsg )
         {
                  case EADDRNOTAVAIL : PrintMessage(debug_level, TCP_TABMSG, "EADDRNOTAVAIL", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EADDRNOTAVAIL"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.EADDRNOTAVAIL";
                                       break;

                  case EAFNOSUPPORT  : PrintMessage(debug_level, TCP_TABMSG, "EAFNOSUPPORT", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EAFNOSUPPORT"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.EAFNOSUPPORT";
                                       break;

                  case EALREADY      : PrintMessage(debug_level, TCP_TABMSG, "EALREADY", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EALREADY"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.EALREADY";
                                       break;

                  case EBADF         : PrintMessage(debug_level, TCP_TABMSG, "EBADF", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EBADF"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.EBADF";
                                       break;

                  case ECONNREFUSED  : PrintMessage(debug_level, TCP_TABMSG, "ECONNREFUSED", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.ECONNREFUSED"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.ECONNREFUSED";
                                       break;

                  case EINPROGRESS   : PrintMessage(debug_level, TCP_TABMSG, "EINPROGRESS", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EINPROGRESS"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.EINPROGRESS";
                                       break;

                  case EINTR         : PrintMessage(debug_level, TCP_TABMSG, "EINTR", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EINTR"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.EINTR";
                                       break;

                  case EISCONN       : PrintMessage(debug_level, TCP_TABMSG, "EISCONN", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EISCONN"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.EISCONN";
                                       break;

                  case ENETUNREACH   : PrintMessage(debug_level, TCP_TABMSG, "ENETUNREACH", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.ENETUNREACH"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.ENETUNREACH";
                                       break;

                  case ENOTSOCK      : PrintMessage(debug_level, TCP_TABMSG, "ENOTSOCK", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.ENOTSOCK"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.ENOTSOCK";
                                       break;

                  case EPROTOTYPE    : PrintMessage(debug_level, TCP_TABMSG, "EPROTOTYPE", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EPROTOTYPE"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.EPROTOTYPE";
                                       break;

                  case ETIMEDOUT     : PrintMessage(debug_level, TCP_TABMSG, "ETIMEDOUT", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.ETIMEDOUT"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.ETIMEDOUT";
                                       break;

                  case EIO           : PrintMessage(debug_level, TCP_TABMSG, "EIO", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EIO"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.EIO";
                                       break;

                  case ELOOP         : PrintMessage(debug_level, TCP_TABMSG, "ELOOP", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.ELOOP"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.ELOOP";
                                       break;

                  case ENAMETOOLONG  : PrintMessage(debug_level, TCP_TABMSG, "ENAMETOOLONG", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.ENAMETOOLONG"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.ENAMETOOLONG";
                                       break;

                  case ENOENT        : PrintMessage(debug_level, TCP_TABMSG, "ENOENT", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.ENOENT"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.ENOENT";
                                       break;

                  case ENOTDIR       : PrintMessage(debug_level, TCP_TABMSG, "ENOTDIR", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.ENOTDIR"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.ENOTDIR";
                                       break;

                  case EACCES        : PrintMessage(debug_level, TCP_TABMSG, "EACCES", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EACCES"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.EACCES";
                                       break;

                  case EADDRINUSE    : PrintMessage(debug_level, TCP_TABMSG, "EADDRINUSE", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EADDRINUSE"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.EADDRINUSE";
                                       break;

                  case ECONNRESET    : PrintMessage(debug_level, TCP_TABMSG, "ECONNRESET", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.ECONNRESET"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.ECONNRESET";
                                       break;

                  case EHOSTUNREACH  : PrintMessage(debug_level, TCP_TABMSG, "EHOSTUNREACH", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EHOSTUNREACH"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.EHOSTUNREACH";
                                       break;

                  case EINVAL        : PrintMessage(debug_level, TCP_TABMSG, "EINVAL", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EINVAL"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.EINVAL";
                                       break;

               // case ELOOP         : PrintMessage(debug_level, TCP_TABMSG, "ELOOP", TabVariabiliEMPTY, 0, 0, 0);
               //                      rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.ELOOP"));
               //                      rTCPConnect.msgDesc = "TCP.CONNECT.ELOOP";
               //                      break;

               // case ENAMETOOLONG  : PrintMessage(debug_level, TCP_TABMSG, "ENAMETOOLONG", TabVariabiliEMPTY, 0, 0, 0);
               //                      rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.ENAMETOOLONG"));
               //                      rTCPConnect.msgDesc = "TCP.CONNECT.ENAMETOOLONG";
               //                      break;

                  case ENETDOWN      : PrintMessage(debug_level, TCP_TABMSG, "ENETDOWN", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.ENETDOWN"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.ENETDOWN";
                                       break;

                  case ENOBUFS       : PrintMessage(debug_level, TCP_TABMSG, "ENOBUFS", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.ENOBUFS"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.ENOBUFS";
                                       break;

                  case EOPNOTSUPP    : PrintMessage(debug_level, TCP_TABMSG, "EOPNOTSUPP", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EOPNOTSUPP"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.EOPNOTSUPP";
                                       break;

                  default            : PrintMessage(debug_level, TCP_TABMSG, "CONNECT_SOCKET_UNKNOWN_ERROR", TabVariabiliEMPTY, 0, 0, 0);
                                       rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.CONNECT_SOCKET_UNKNOWN_ERROR"));
                                       rTCPConnect.msgDesc = "TCP.CONNECT.CONNECT_SOCKET_UNKNOWN_ERROR";
                                       break;
         };

         return rTCPConnect;
    }
	
	/* CASO 3: Esito Negativo per EINPROGRESS nel caso di TIMEOUT (socket non blocking) */
    if ( rc < 0 && errno == EINPROGRESS )                                                  	
    {
        /* Setting attributi socket (??????) */
        FD_ZERO(&fdset);
        FD_SET(rTCPConnect.sock, &fdset);

        // Verifica se il socket è ready */
        rc = select(rTCPConnect.sock + 1, NULL, &fdset, NULL, &Timeout);

        switch(rc) {

		  case 1: // data to read
		   
              len = sizeof(so_error);

              getsockopt(rTCPConnect.sock, SOL_SOCKET, SO_ERROR, &so_error, &len);

              if (so_error == 0) 
			  {
                  PrintMessage(debug_level, TCP_TABMSG, "TCP_CONNECT_OK", TabVariabiliEMPTY, 0, 0, 0);

                  /*
				  .----------------------.
				  | Struttura di ritorno |
				  '----------------------' */
				  rTCPConnect.idmsg       = 0                                               ;
				  rTCPConnect.msgDesc     = malloc(strlen("TCP.CONNECT.OK"))                ;
				  rTCPConnect.msgDesc     = "TCP.CONNECT.OK"                                ;
				  rTCPConnect.return_code = 0                                               ;
				  return rTCPConnect;
			  } 
			  else { // error
                  // printf("socket %s:%d NOT connected: %s\n", host, porta, strerror(so_error));
				  				  /* struttura di ritorno */
				  rTCPConnect.idmsg       = errno ;
				  PrintMessage(debug_level, TCP_TABMSG, "EINPROGRESS", TabVariabiliEMPTY, 0, 0, 0);
				  rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EINPROGRESS"));
				  rTCPConnect.msgDesc = "TCP.CONNECT.EINPROGRESS";
				  rTCPConnect.return_code = -4 ;
              }
              break;
          case 0: //timeout
                  // fprintf(stderr, "connection timeout trying to connect to %s:%d\n", host, porta);

				  /* struttura di ritorno */
				  rTCPConnect.idmsg       = errno ;
				  PrintMessage(debug_level, TCP_TABMSG, "EINPROGRESS", TabVariabiliEMPTY, 0, 0, 0);
				  rTCPConnect.msgDesc = malloc(strlen("TCP.CONNECT.EINPROGRESS.TIMEOUT"));
				  rTCPConnect.msgDesc = "TCP.CONNECT.EINPROGRESS.TIMEOUT";
				  rTCPConnect.return_code = -4 ;

              break;
        }
    	return rTCPConnect;
	}
    
}


/*
.-------------------------------------------------------------------------------------.
|                                                                                     |
|  Function    : TCPSocketSetBlocking                                                 |
|                                                                                     |
|  Description : Setting Blocking Socket.                                             |
|                                                                                     |
|  Parameter   :  1. sock          int                                                |
|                 2. debug_level   {__INFO__|__WARNING__|__ERROR__}                   |
|                                                                                     |
|  Return-code :  0 Ok                                                                |
|                -1 Set Blocking Socket Error                                         |
|                -2 Set Blocking Socket Error                                         |
|                                                                                     |
|  Dipendenze  : fcntl                                                                |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
'-------------------------------------------------------------------------------------'*/
extern int TCPSocketSetBlocking ( int sock, int debug_level )
{
    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , "", ""                                  }
    };

    long arg;

    PrintMessage(debug_level, TCP_TABMSG, "TCP_SOCKETBLOCKON", TabVariabiliEMPTY, 0, 0, 0);

  //fprintf(stderr, "Error fcntl(..., F_GETFL) (%s)\n", strerror(errno));



    /*
    .------------------------------------------------------------------------.
    |                                                                        |
    | Oltre alle operazioni base esistono tutta una serie di operazioni      |
    | ausiliarie che è possibile eseguire su un file description, che non    |
    | riguardano la normale lettura e scrittura di dati, ma la gestione sia  |
    | delle loro proprietà, che di tutta una serie di ulteriori funzionalità |
    | che il kernel può mettere a disposizione.                              | 
    |                                                                        |
    | Per queste operazioni di manipolazione e di controllo delle varie      |
    | proprietà e caratteristiche di un file descriptor, viene usata la      |
    | funzione fcntl, il cui prototipo è :                                   | 
    |                                                                        |
    | #include <unistd.h>                                                    | 
    | #include <fcntl.h>                                                     | 
    | int fcntl(int fd, int cmd)                                             | 
    | int fcntl(int fd, int cmd, long arg)                                   | 
    | int fcntl(int fd, int cmd, struct flock * lock)                        | 
    |                                                                        |
    | Esegue una delle possibili operazioni specificate da cmd sul file fd.  | 
    |                                                                        |
    | La funzione ha valori di ritorno diversi a seconda dell'operazione. In | 
    | caso di errore il valore di ritorno è sempre -1 ed il codice           |
    | dell'errore è restituito nella variabile errno; i codici possibili     |
    | dipendono dal tipo di operazione, l'unico valido in generale è :       |
    |                                                                        | 
    | EBADF fd non è un file aperto.                                         | 
    |                                                                        |
    | Il primo argomento della funzione è sempre il numero di file           |
    | descriptor fd su cui si vuole operare. Il comportamento di questa      |
    | funzione, il numero e il tipo degli argomenti, il valore di ritorno e  |
    | gli eventuali errori sono determinati dal valore dell'argomento cmd    |
    | che in sostanza corrisponde all'esecuzione di un determinato comando;  |
    | in sez. 6.3.4 abbiamo incontrato un esempio dell'uso di fcntl per la   |
    | duplicazione dei file descriptor, una lista di tutti i possibili       |
    | valori per cmd è riportata di seguito :                                | 
    |                                                                        |
    |                |                                                       |
    | F_DUPFD        | trova il primo file descriptor disponibile di valore  |
    |                | maggiore o uguale ad arg e ne fa una copia di fd.     |
    |                | Ritorna il nuovo file descriptor in caso di successo  |
    |                | e -1 in caso di errore. Gli errori possibili sono     |
    |                | EINVAL se arg è negativo o maggiore del massimo       |
    |                | consentito o EMFILE se il processo ha già raggiunto   |
    |                | il massimo numero di descrittori consentito.          | 
    |                |                                                       |
    | F_SETFD        | imposta il valore del file descriptor flag al valore  |
    |                | specificato con arg. Al momento l'unico bit usato è   |
    |                | quello di close-on-exec, identificato dalla costante  |
    |                | FD_CLOEXEC, che serve a richiedere che il file venga  |
    |                | chiuso nella esecuzione di una exec (vedi sez.        |
    |                | 3.2.7). Ritorna un valore nullo in caso di successo   |
    |                | e -1 in caso di errore.                               |
    |                |                                                       |
    | F_GETFD        | ritorna il valore del file descriptor flag di fd o -1 |
    |                | in caso di errore; se FD_CLOEXEC è impostato i file   |
    |                | descriptor aperti vengono chiusi attraverso una exec  | 
    |                | altrimenti (il comportamento predefinito) restano     |
    |                | aperti.                                               |
    |                |                                                       |
    | F_GETFL        | ritorna il valore del file status flag in caso di     |
    |                | successo o -1 in caso di errore; permette cioè di     |
    |                | rileggere quei bit impostati da open all'apertura del | 
    |                | file che vengono memorizzati (quelli riportati nella  |
    |                | prima e terza sezione di tab. 6.2).                   | 
    |                |                                                       |
    | F_SETFL        | imposta il file status flag al valore specificato da  |
    |                | arg, ritorna un valore nullo in caso di successo o -1 |
    |                | in caso di errore. Possono essere impostati solo i    | 
    |                | bit riportati nella terza sezione di tab. 6.2.17      | 
    |                |                                                       |
    | F_GETLK        | richiede un controllo sul file lock specificato da    |
    |                | lock, sovrascrivendo la struttura da esso puntata con |
    |                | il risultato, ritorna un valore nullo in caso di      | 
    |                | successo o -1 in caso di errore. Questa funzionalità  |
    |                | è trattata in dettaglio in sez. 11.4.3.               | 
    |                |                                                       |
    | F_SETLK        | richiede o rilascia un file lock a seconda di quanto  |
    |                | specificato nella struttura puntata da lock. Se il    |
    |                | lock è tenuto da qualcun'altro ritorna immediatamente | 
    |                | restituendo -1 e imposta errno a EACCES o EAGAIN, in  |
    |                | caso di successo ritorna un valore nullo. Questa      |
    |                | funzionalità è trattata in dettaglio in sez. 11.4.3.  | 
    |                |                                                       |
    | F_SETLKW       | identica a F_SETLK eccetto per il fatto che la        |
    |                | funzione non ritorna subito ma attende che il blocco  |
    |                | sia rilasciato. Se l'attesa viene interrotta da un    |
    |                | segnale la funzione restituisce -1 e imposta errno a  |
    |                | EINTR, in caso di successo ritorna un valore nullo.   |
    |                | Questa funzionalità è trattata in dettaglio in sez.   |
    |                | 11.4.3.                                               | 
    |                |                                                       |
    | F_GETOWN       | restituisce il pid del processo o l'identificatore    |
    |                | del process group18 che è preposto alla ricezione dei |
    |                | segnali SIGIO e SIGURG per gli eventi associati al    |
    |                | file descriptor fd. Nel caso di un process group      |
    |                | viene restituito un valore negativo il cui valore     |
    |                | assoluto corrisponde all'identificatore del process   |
    |                | group. In caso di errore viene restituito -1.         | 
    |                |                                                       |
    | F_SETOWN       | imposta, con il valore dell'argomento arg,            |
    |                | l'identificatore del processo o del process group che |
    |                | riceverà i segnali SIGIO e SIGURG per gli eventi      |
    |                | associati al file descriptor fd, ritorna un valore    |
    |                | nullo in caso di successo o -1 in caso di errore.     |
    |                | Come per F_GETOWN, per impostare un process group si  |
    |                | deve usare per arg un valore negativo, il cui valore  |
    |                | assoluto corrisponde all'identificatore del process   |
    |                | group.                                                | 
    |                |                                                       |
    | F_GETSIG       | restituisce il valore del segnale inviato quando ci   |
    |                | sono dati disponibili in ingresso su un file          |
    |                | descriptor aperto ed impostato per l'I/O asincrono    |
    |                | (si veda sez. 11.2.2). Il valore 0 indica il valore   |
    |                | predefinito (che è SIGIO), un valore diverso da zero  |
    |                | indica il segnale richiesto, (che può essere anche lo |
    |                | stesso SIGIO). In caso di errore ritorna -1.          | 
    |                |                                                       |
    | F_SETSIG       | imposta il segnale da inviare quando diventa          |
    |                | possibile effettuare I/O sul file descriptor in caso  |
    |                | di I/O asincrono, ritorna un valore nullo in caso di  |
    |                | successo o -1 in caso di errore. Il valore zero       |
    |                | indica di usare il segnale predefinito, SIGIO. Un     |
    |                | altro valore diverso da zero (compreso lo stesso      |
    |                | SIGIO) specifica il segnale voluto; l'uso di un       |
    |                | valore diverso da zero permette inoltre, se si è      |
    |                | installato il gestore del segnale come sa_sigaction   |
    |                | usando SA_SIGINFO, (vedi sez. 9.4.3), di rendere      |
    |                | disponibili al gestore informazioni ulteriori         |
    |                | riguardo il file che ha generato il segnale           |
    |                | attraverso i valori restituiti in siginfo_t           |
    |                |                                                       | 
    | F_SETLEASE     | Imposta o rimuove un file lease20 sul file descriptor |
    |                | fd a seconda del valore del terzo argomento, che in   |
    |                | questo caso è un int, ritorna un valore nullo in caso |
    |                | di successo o -1 in caso di errore.                   |
    |                |                                                       |
    | F_GETLEASE     | Restituisce il tipo di file lease che il processo     |
    |                | detiene nei confronti del file descriptor fd o -1 in  |
    |                | caso di errore. Con questo comando il terzo argomento |
    |                | può essere omesso.                                    | 
    |                |                                                       |
    | F_NOTIFY       | Attiva un meccanismo di notifica per cui viene        |
    |                | riportata al processo chiamante, tramite il segnale   |
    |                | SIGIO (o altro segnale specificato con F_SETSIG) ogni |
    |                | modifica eseguita o direttamente sulla directory cui  |
    |                | fd fa riferimento, o su uno dei file in essa          |
    |                | contenuti; ritorna un valore nullo in caso di         |
    |                | successo o -1 in caso di errore.                      |
    |                                                                        |
    | La maggior parte delle funzionalità di fcntl sono troppo avanzate per  |
    | poter essere affrontate in tutti i loro aspetti a questo punto;        |
    | saranno pertanto riprese più avanti quando affronteremo le             |
    | problematiche ad esse relative.                                        |
    |                                                                        |
    | Si tenga presente infine che quando si usa la funzione per determinare |
    | le modalità di accesso con cui è stato aperto il file (attraverso      | 
    | l'uso del comando F_GETFL) è necessario estrarre i bit corrispondenti  |
    | nel file status flag che si è ottenuto. Infatti la definizione         |
    | corrente di quest'ultimo non assegna bit separati alle tre diverse     |
    | modalità O_RDONLY, O_WRONLY e O_RDWR.21 Per questo motivo il valore    |
    | della modalità di accesso corrente si ottiene eseguendo un AND binario |
    | del valore di ritorno di fcntl con la maschera O_ACCMODE (anch'essa    |
    | definita in fcntl.h), che estrae i bit di accesso dal file status      |
    | flag.                                                                  |
    |                                                                        |
    '------------------------------------------------------------------------' */ 

    /*
    .----------------------------------------------------------------------------.
    | Legge (F_GETFL) il valore del file status flag in caso di successo o -1    |
    | in caso di errore (leggi strerror(errno); permette cioè di rileggere quei  |
    | quei bit impostati con open all'apertura del file che vengono memorizzati. |
    '----------------------------------------------------------------------------' */
    if((arg = fcntl(sock, F_GETFL, NULL)) < 0) {
        PrintMessage(debug_level, TCP_TABMSG, "TCP_SOCKETBLOCK_KO", TabVariabiliEMPTY, 0, 0, 0);
        return -1;
    }

    /*
    .---------.
    | setting |      
    '---------' */
    arg &= (~O_NONBLOCK);
    
    /*
    .----------------------------------------------------------------------------.
    | Imposta (F_SETFL) il file status flag al valore specificato da arg, e      |
    | ritorna un valore nullo in caso di successo o -1 in caso di errore.        |
    '----------------------------------------------------------------------------' */ 
    if( fcntl(sock, F_SETFL, arg) < 0) {
        PrintMessage(debug_level, TCP_TABMSG, "TCP_SOCKETBLOCK_KO", TabVariabiliEMPTY, 0, 0, 0);
        return -2;
    }

    PrintMessage(debug_level, TCP_TABMSG, "TCP_SOCKETBLOCK_OK", TabVariabiliEMPTY, 0, 0, 0);
    return 0;
}


/*
.-------------------------------------------------------------------------------------.
|                                                                                     |
|  Function    : TCPSocketSetNotBlocking                                              |
|                                                                                     |
|  Description : Setting Not Blocking Socket.                                         |
|                                                                                     |
|  Parameter   :  1. sock          int                                                |
|                 2. debug_level   {__INFO__|__WARNING__|__ERROR__}                   |
|                                                                                     |
|  Return-code :  0 Ok                                                                |
|                -1 Set Not Blocking Socket Error                                     |
|                -2 Set Not Blocking Socket Error                                     |
|                                                                                     |
|  Dipendenze  : fcntl                                                                |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
'-------------------------------------------------------------------------------------'*/
extern int TCPSocketSetNotBlocking ( int sock, int debug_level )
{
    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , "", ""                                  }
    };

    long arg;

    PrintMessage(debug_level, TCP_TABMSG, "TCP_SOCKETBLOCKOFF", TabVariabiliEMPTY, 0, 0, 0);

    // esempio di come estrarre l'errore   
    // fprintf(stderr, "Error fcntl(..., F_GETFL) (%s)\n", strerror(errno));

    /*
    .----------------------------------------------------------------------------.
    | Legge (F_GETFL) il valore del file status flag in caso di successo o -1    |
    | in caso di errore (leggi strerror(errno); permette cioè di rileggere quei  |
    | quei bit impostati con open all'apertura del file che vengono memorizzati. |
    '----------------------------------------------------------------------------' */
    if((arg = fcntl(sock, F_GETFL, NULL)) < 0) {
        PrintMessage(debug_level, TCP_TABMSG, "TCP_SOCKETBLOCK_KO", TabVariabiliEMPTY, 0, 0, 0);
        return -1;
    }

    /*
    .---------.
    | setting |
    '---------' */
    arg |= O_NONBLOCK;
  
    /*
    .----------------------------------------------------------------------------.
    | Imposta (F_SETFL) il file status flag al valore specificato da arg, e      |
    | ritorna un valore nullo in caso di successo o -1 in caso di errore.        |
    '----------------------------------------------------------------------------' */
    if( fcntl(sock, F_SETFL, arg) < 0) {
        PrintMessage(debug_level, TCP_TABMSG, "TCP_SOCKETBLOCK_KO", TabVariabiliEMPTY, 0, 0, 0);
        return -2;
    }

    PrintMessage(debug_level, TCP_TABMSG, "TCP_SOCKETBLOCK_OK", TabVariabiliEMPTY, 0, 0, 0);
    return 0;
}



/*
.-------------------------------------------------------------------------------------.
|                                                                                     |
|  Funzione    : TCPGetHostByName                                                     |
|                                                                                     |
|  Description : Hostname Resolve                                                     |
|                                                                                     |
|  Return-code :  0 Ok                                                                |
|                -1 Hostname Resolve Error                                            |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
'-------------------------------------------------------------------------------------' */
extern struct tTCPGetHostByName TCPGetHostByName ( char *host, int debug_level )
{

    struct hostent    *hp   ;

    /*
    .--------------------------------------.
    | Dichiarazione strutture utilizzate   |
    '--------------------------------------' */
    struct tTCPGetHostByName  rTCPGetHostByName ;
    struct tTCPGetHostByName *pTCPGetHostByName ;

    /*
    .------------------------------------.
    | Allocazione Spazio                 |
    '------------------------------------' */
    pTCPGetHostByName = malloc(sizeof(struct tTCPGetHostByName));

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pTCPGetHostByName = &rTCPGetHostByName;

    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rTCPGetHostByName, 0, sizeof(rTCPGetHostByName));
    rTCPGetHostByName.idmsg = 0;
    rTCPGetHostByName.return_code = 0;

    /*
    .------------------------------------------------------.
    |                                                      |
    |          Risoluzione dell'hostname server            |
    |                                                      |
    '------------------------------------------------------' */
    if( !( hp = gethostbyname ( host ) ) )
    {
         rTCPGetHostByName.idmsg = 0 ;
         rTCPGetHostByName.return_code = -1 ;
         return rTCPGetHostByName;
    }
    
    /*
    .------------------------------------------------------.
    |                                                      |
    |        Carica gli IP nella struttura di ritorno      |
    |                                                      |
    '------------------------------------------------------' */
    rTCPGetHostByName.tabAddress = malloc (160) ; 
    if ( hp->h_addrtype==AF_INET )
    {
         rTCPGetHostByName.dimTabAddress = 0 ; 
         struct  in_addr sin_addr;
         while ( hp->h_addr_list[rTCPGetHostByName.dimTabAddress] )
         {
                 memcpy(&(sin_addr), hp->h_addr_list[rTCPGetHostByName.dimTabAddress], hp->h_length);
                 rTCPGetHostByName.tabAddress[rTCPGetHostByName.dimTabAddress] = inet_ntoa(sin_addr);    
                 rTCPGetHostByName.dimTabAddress++;
         }
    }

    /*
    |
    | Errore. Non valorizza bene l'array... sempre l'ultimo elemento. problema con **array (forse)
    |
    | printf("o========> [%s]\n", rTCPGetHostByName.tabAddress[0]);
    | printf("o========> [%s]\n", rTCPGetHostByName.tabAddress[1]);
    |
    */


    return rTCPGetHostByName ;
};


/*
.-------------------------------------------------------------------------------------.
|                                                                                     |
|  Funzione    : TCPSend                                                              |
|                                                                                     |
|  Descrizione : Buffer Send to Target Host                                           |
|                                                                                     |
|  Return-code :  0 Ok                                                                |
|                -1 Send Buffer Error                                                 |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
'-------------------------------------------------------------------------------------'*/
extern struct tTCPSend TCPSend (int sock, char *request, int debug_level) 
{
    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , "", ""                                  }
    };

    /*
    .--------------------------------------.
    | Dichiarazione strutture utilizzate   |
    '--------------------------------------' */
    struct tTCPSend  rTCPSend ;
    struct tTCPSend *pTCPSend ;

    /*
    .------------------------------------.
    | Allocazione Spazio                 |
    '------------------------------------' */
    pTCPSend = malloc(sizeof(struct tTCPSend));

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pTCPSend = &rTCPSend;

    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rTCPSend, 0, sizeof(rTCPSend));
    rTCPSend.idmsg = 0;
    rTCPSend.return_code = 0;

    /*
    .-------------------------------------------.
    | Calcola la lunghezza se non viene passata |
    '-------------------------------------------' */
    PrintMessage(debug_level, TCP_TABMSG, "TCP_SEND_CALCULATE_LEN_REQUEST", TabVariabiliEMPTY, 0, 0, 0);
    rTCPSend.requestLen = strlen(request);
    PrintMessage(debug_level, TCP_TABMSG, "TCP_SEND_OK", TabVariabiliEMPTY, 0, 0, 0);

    /*
    .--------------------------------------------.
    | Alloca e valorizza la richiesta da inviare |
    '--------------------------------------------' */ 
    rTCPSend.request = malloc (rTCPSend.requestLen);
    rTCPSend.request = request ;



    /*
    .------------------------------------------------------------------------.
    | PASSO 1. Send                                                          |
    +------------------------------------------------------------------------+
    |                                                                        |
    | The send() function shall initiate transmission of a message from the  |
    | specified socket to its peer. The send() function shall send a message |
    | only when the socket is connected (including when the peer of          | 
    | a connectionless socket has been set via connect()).                   | 
    |                                                                        |
    | The send() function takes the following arguments :                    | 
    |                                                                        |
    | socket           |  Specifies the socket file descriptor.              | 
    | buffer           |  Points to the buffer containing the message        |
    |                  |  to send.                                           | 
    | length           |  Specifies the length of the message in bytes.      | 
    | flags            |  Specifies the type of message transmission. Values |  
    |                  |  of this argument are formed by logically OR'ing    | 
    |                  |  zero or more of the following flags :              | 
    |                  |                                                     |
    |                  |  MSG_EOR    Terminates a record (if supported       |
    |                  |             by the protocol).                       | 
    |                  |                                                     |
    |                  |  MSG_OOB    Sends out-of-band data on sockets that  |
    |                  |             support out-of-band communications.     | 
    |                  |             The significance and semantics of       |
    |                  |             out-of-band data are protocol-specific. | 
    |                                                                        |
    | The length of the message to be sent is specified by the length        |
    | argument. If the message is too long to pass through the underlying    |
    | protocol, send() shall fail and no data shall be transmitted.          | 
    |                                                                        |
    | Successful completion of a call to send() does not guarantee           |
    | delivery of the message. A return value of -1 indicates only           |
    | locally-detected errors.                                               | 
    |                                                                        |
    | If space is not available at the sending socket to hold the message to |
    | to be transmitted, and the socket file descriptor does not have        |
    | O_NONBLOCK set, send() shall block until space is available.           | 
    | If space is not available at the sending socket to hold the message to |
    | be transmitted, and the socket file descriptor does have O_NONBLOCK    |
    | set, send() shall fail.                                                | 
    | The select() and poll() functions can be used to determine when it is  |
    | possible to send more data.                                            | 
    |                                                                        |
    | The socket in use may require the process to have appropriate          |
    | privileges to use the send() function.                                 | 
    |                                                                        |
    | RETURN VALUE                                                           | 
    |                                                                        |
    | Upon successful completion, send() shall return the number of bytes    |
    | sent. Otherwise, -1 shall be returned and errno set to indicate        |
    | the error.                                                             | 
    |                                                                        |
    | ERRORS                                                                 | 
    |                                                                        |
    | EAGAIN           |                                                     | 
    | EWOULDBLOCK      |  The socket's file descriptor is marked O_NONBLOCK  |
    |                  |  and the requested operation would block.           |
    | EBADF            |  The socket argument is not a valid file            |
    | ECONNRESET       |  A connection was forcibly closed by a peer.        |  
    | EDESTADDRREQ     |  The socket is not connection-mode and no peer      |
    |                  |  address is set.                                    | 
    | EINTR            |  A signal interrupted send() before any data was    |
    |                  |  transmitted.                                       |  
    | EMSGSIZE         |  The message is too large to be sent all at once,   |
    |                  |  as the socket requires.                            | 
    | ENOTCONN         |  The socket is not connected or otherwise has not   |
    |                  |  had the peer pre-specified.                        |  
    | ENOTSOCK         |  The socket argument does not refer to a socket.    | 
    | EOPNOTSUPP       |  The socket argument is associated with a socket    |
    |                  |  that does not support one or more of the           |
    |                  |  values set in flags.                               | 
    | EPIPE            |  The socket is shut down for writing, or the socket |
    |                  |  is connection-mode and is no longer connected. In  | 
    |                  |  the latter case, and if the socket is of type      |
    |                  |  SOCK_STREAM, the SIGPIPE signal is generated to    |
    |                  |  the calling thread.                                | 
    | EACCES           |  The calling process does not have the appropriate  |
    |                  |  privileges.                                        | 
    | EIO              |  An I/O error occurred while reading from or        |
    |                  |  writing to the file system.                        | 
    | ENETDOWN         |  The local network interface used to reach the      |
    |                  |  destination is down.                               | 
    | ENETUNREACH      |  No route to the network is present.                | 
    | ENOBUFS          |  Insufficient resources were available in the       |
    |                  |  system to perform the operation.                   | 
    |                                                                        |
    |                                                                        |
    '------------------------------------------------------------------------' */
    PrintMessage(debug_level, TCP_TABMSG, "TCP_SEND_REQUEST", TabVariabiliEMPTY, 0, 0, 0);
    if ( debug_level == __INFO__ ) printf("(%s)(len:%d) ", rTCPSend.request, rTCPSend.requestLen);
    if (send(sock, rTCPSend.request, rTCPSend.requestLen, 0)<0) 
    {
         rTCPSend.idmsg = errno ;
         rTCPSend.return_code = -1 ;

         PrintMessage(debug_level, TCP_TABMSG, "TCP_SEND_KO", TabVariabiliEMPTY, 0, 0, 0);
         switch ( rTCPSend.idmsg )                                                   
         {
                  case EAGAIN       : PrintMessage(debug_level, TCP_TABMSG, "EAGAIN", TabVariabiliEMPTY, 0, 0, 0);  
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.EAGAIN"));
                                      rTCPSend.msgDesc = "TCP.SEND.EAGAIN";
                                      break;               

               // case EWOULDBLOCK  : PrintMessage(debug_level, TCP_TABMSG, "EWOULDBLOCK", TabVariabiliEMPTY, 0, 0, 0);
               //                     rTCPSend.msgDesc = malloc(strlen("TCP.SEND.EWOULDBLOCK"));
               //                     rTCPSend.msgDesc = "TCP.SEND.EWOULDBLOCK";
               //                     break;                   

                  case EBADF        : PrintMessage(debug_level, TCP_TABMSG, "EBADF", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.EBADF"));                 
                                      rTCPSend.msgDesc = "TCP.SEND.EBADF";
                                      break;
                  
                  case ECONNRESET   : PrintMessage(debug_level, TCP_TABMSG, "ECONNRESET", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.ECONNRESET"));                 
                                      rTCPSend.msgDesc = "TCP.SEND.ECONNRESET";
                                      break;
                  
                  case EDESTADDRREQ : PrintMessage(debug_level, TCP_TABMSG, "EDESTADDRREQ", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.EDESTADDRREQ"));                 
                                      rTCPSend.msgDesc = "TCP.SEND.EDESTADDRREQ";
                                      break;
                  
                  case EINTR        : PrintMessage(debug_level, TCP_TABMSG, "EINTR", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.EINTR"));                 
                                      rTCPSend.msgDesc = "TCP.SEND.EINTR";
                                      break;
                  
                  case EMSGSIZE     : PrintMessage(debug_level, TCP_TABMSG, "EMSGSIZE", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.EMSGSIZE"));                 
                                      rTCPSend.msgDesc = "TCP.SEND.EMSGSIZE";
                                      break;
                  
                  case ENOTCONN     : PrintMessage(debug_level, TCP_TABMSG, "ENOTCONN", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.ENOTCONN"));                 
                                      rTCPSend.msgDesc = "TCP.SEND.ENOTCONN";
                                      break;
                  
                  case ENOTSOCK     : PrintMessage(debug_level, TCP_TABMSG, "ENOTSOCK", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.ENOTSOCK"));                 
                                      rTCPSend.msgDesc = "TCP.SEND.ENOTSOCK";
                                      break;
                  
                  case EOPNOTSUPP   : PrintMessage(debug_level, TCP_TABMSG, "EOPNOTSUPP", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.EOPNOTSUPP"));                 
                                      rTCPSend.msgDesc = "TCP.SEND.EOPNOTSUPP";
                                      break;
                  
                  case EPIPE        : PrintMessage(debug_level, TCP_TABMSG, "EPIPE", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.EPIPE"));                 
                                      rTCPSend.msgDesc = "TCP.SEND.EPIPE";
                                      break;
                  
                  case EACCES       : PrintMessage(debug_level, TCP_TABMSG, "EACCES", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.EACCES")); 
                                      rTCPSend.msgDesc = "TCP.SEND.EACCES";
                                      break;
                  
                  case EIO          : PrintMessage(debug_level, TCP_TABMSG, "EIO", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.EIO"));                 
                                      rTCPSend.msgDesc = "TCP.SEND.EIO";
                                      break;
                  
                  case ENETDOWN     : PrintMessage(debug_level, TCP_TABMSG, "ENETDOWN", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.ENETDOWN"));                 
                                      rTCPSend.msgDesc = "TCP.SEND.ENETDOWN";
                                      break;
                  
                  case ENETUNREACH  : PrintMessage(debug_level, TCP_TABMSG, "ENETUNREACH", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.ENETUNREACH"));                 
                                      rTCPSend.msgDesc = "TCP.SEND.ENETUNREACH";
                                      break;
                  
                  case ENOBUFS      : PrintMessage(debug_level, TCP_TABMSG, "ENOBUFS", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.ENOBUFS"));                 
                                      rTCPSend.msgDesc = "TCP.SEND.ENOBUFS";
                                      break;
                  
                  default           : PrintMessage(debug_level, TCP_TABMSG, "SEND_TCP_UNKNOWN_ERROR", TabVariabiliEMPTY, 0, 0, 0);
                                      rTCPSend.msgDesc = malloc(strlen("TCP.SEND.SEND_TCP_UNKNOWN_ERROR"));                 
                                      rTCPSend.msgDesc = "TCP.SEND.SEND_TCP_UNKNOWN_ERROR";
                                      break;
                  
          };
    };
    PrintMessage(debug_level, TCP_TABMSG, "TCP_SEND_OK", TabVariabiliEMPTY, 0, 0, 0);
 
    return rTCPSend;
};


/*
.-------------------------------------------------------------------------------------.
|                                                                                     |
|  Funzione    : TCPReceive                                                           |
|                                                                                     |
|  Descrizione : Receive Buffer from Target Host                                      |
|                                                                                     |
|  Return-code :  0 Ok                                                                |
|                -1 Receive Buffer Error                                              |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
'-------------------------------------------------------------------------------------'*/
extern struct tTCPReceive TCPReceive (int sock, long int timeout, char *OutputFile, int debug_level)
{
    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , "", ""                                  }
    };

    /*
    .--------------------------------------.
    | Dichiarazione strutture utilizzate   |
    '--------------------------------------' */
    struct tTCPReceive  rTCPReceive ;
    struct tTCPReceive *pTCPReceive ;

    /*
    .------------------------------------.
    | Allocazione Spazio                 |
    '------------------------------------' */
    pTCPReceive = malloc(sizeof(struct tTCPReceive));

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pTCPReceive = &rTCPReceive;

    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rTCPReceive, 0, sizeof(rTCPReceive));

    rTCPReceive.idmsg       = 0;
    rTCPReceive.return_code = 0;


    /*
    .------------------------------------------------------------------------.
    | PASSO 1. Create|Open OUTPUT                                            |
    +------------------------------------------------------------------------+
    | Crea il file di Output prima di iniziare a leggere dal socket.         |
    | leggere dal socket. Se non riesce ad aprire il file esce per errore.   |
    '------------------------------------------------------------------------' */

    /*
    .------------------------------------------------.
    | Imposta il default se non impostato OutputFile |
    '------------------------------------------------' */
    if ( OutputFile == NULL ) {
         OutputFile = malloc(1024);
         strcpy (OutputFile, "/tmp/.myTCPClient.OutputFile.txt");
    };

    /*
    .-----------------.
    | Open Datastore  |
    '-----------------' */
    FILE *pOutputFile ;
    PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_OPEN_DATASTORE", TabVariabiliEMPTY, 0, 0, 0);
    pOutputFile = fopen(OutputFile, "w");
    if (!pOutputFile)
    {
        PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_KO", TabVariabiliEMPTY, 0, 0, 0);
        PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_OPEN_DATASTORE_KO", TabVariabiliEMPTY, 0, 0, 0);
        rTCPReceive.return_code = -1;
        return rTCPReceive ;
    }
    PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_OK", TabVariabiliEMPTY, 0, 0, 0);

    if ( debug_level == __INFO__ )
         printf("+Datastore [%s] \n", OutputFile );


    /*
    .------------------------------------------------------------------------.
    | PASSO 2. Set Receive Timeout                                           |
    +------------------------------------------------------------------------+
    |                                                                        |
    |                                                                        |
    |                                                                        |
    '------------------------------------------------------------------------' */
    PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_SET_TIMEOUT", TabVariabiliEMPTY, 0, 0, 0);
    int rTCPReceiveSetTimeout = TCPReceiveSetTimeout (sock, timeout, OutputFile, debug_level);
    if ( rTCPReceiveSetTimeout < 0 ) 
    {
        PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_KO", TabVariabiliEMPTY, 0, 0, 0);
        PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_SET_TIMEOUT_KO", TabVariabiliEMPTY, 0, 0, 0);
        rTCPReceive.return_code = -1;
        return rTCPReceive ;
    }
    PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_OK", TabVariabiliEMPTY, 0, 0, 0);

    if ( debug_level == __INFO__ )
         printf("+Value Timestamp [%ld] \n", timeout );


    /*
    .------------------------------------------------------------------------.
    | PASSO 2. Receive                                                       |
    +------------------------------------------------------------------------+
    |                                                                        |
    |                                                                        |
    |                                                                        |
    '------------------------------------------------------------------------' */
    int numByteReceive = -1;
    int totByteReceive =  0; 
    char *response;
    response = malloc (__TCP_LEN_BUFFER_RECEIVE__);

    PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_START", TabVariabiliEMPTY, 0, 0, 0);
    for (;;)
    {
          /*
          .------------------------------------------------------------------------.
          | PASSO 2.1 Receive                                                      |
          +------------------------------------------------------------------------+
          |                                                                        |
          | The recv() function shall receive a message from a connection-mode or  |
          | connectionless-mode socket. It is normally used with connected sockets |
          | because it does not permit the application to retrieve the source      | 
          | address of received data.                                              | 
          |                                                                        |   
          | The recv() function takes the following arguments :                    | 
          |                                                                        |
          | socket           |  Specifies the socket file descriptor.              |
          | buffer           |  Points to a buffer where the message should        |
          |                  |  be stored.                                         |
          | length           |  Specifies the length in bytes of the buffer        |
          |                  |  pointed to by the buffer argument.                 |
          | flags            |  Specifies the type of message reception. Values of |
          |                  |  this argument are formed by logically OR'ing zero  |
          |                  |  or more of the following values :                  |
          |                  |                                                     |
          |                  |  MSG_PEEK    Peeks at an incoming message. The data |
          |                  |              is treated as unread and the next      | 
          |                  |              recv() or similar function shall still |
          |                  |              return this data.                      | 
          |                  |                                                     |
          |                  |  MSG_OOB     Requests out-of-band data. The         |
          |                  |              significance and semantics of          | 
          |                  |              out-of-band data are protocol-specific | 
          |                  |                                                     |
          |                  |  MSG_WAITALL On SOCK_STREAM sockets this requests   |
          |                  |              that the function block until the full | 
          |                  |              amount of data can be returned. The    |
          |                  |              function may return the smaller amount | 
          |                  |              of data if the socket is a             |
          |                  |              message-based socket, if a signal is   |
          |                  |              caught, if the connection is           |
          |                  |              terminated, if MSG_PEEK was specified, |
          |                  |              or if an error is pending for the      |
          |                  |              socket.                                |
          |                  |                                                     |
          |                                                                        |
          |                                                                        |
          | The recv() function shall return the length of the message written to  |
          | the buffer pointed to by the buffer argument. For message-based        |
          | sockets, such as SOCK_DGRAM and SOCK_SEQPACKET, the entire message     | 
          | shall be read in a single operation. If a message is too long to fit   |
          | in the supplied buffer, and MSG_PEEK is not set in the flags argument, |
          | the excess bytes shall be discarded. For stream-based sockets, such as | 
          | SOCK_STREAM, message boundaries shall be ignored. In this case, data   |
          | shall be returned to the user as soon as it becomes available, and no  |
          | data shall be discarded.                                               | 
          |                                                                        |
          | If the MSG_WAITALL flag is not set, data shall be returned only up     |
          | to the end of the first message.                                       | 
          |                                                                        |
          | If no messages are available at the socket and O_NONBLOCK is not set   |
          | on the socket's file descriptor, recv() shall block until a message    |
          | arrives. If no messages are available at the socket and O_NONBLOCK is  | 
          | set on the socket's file descriptor, recv() shall fail and set errno   |
          | to [EAGAIN] or [EWOULDBLOCK].                                          | 
          |                                                                        |
          |                                                                        |
          | RETURN VALUE                                                           | 
          |                                                                        | 
          | Upon successful completion, recv() shall return the length of the      |
          | message in bytes. If no messages are available to be received and the  |
          | peer has performed an orderly shutdown, recv() shall return 0.         | 
          | Otherwise, -1 shall be returned and errno set to indicate the error.   | 
          |                                                                        |
          | ERRORS                                                                 | 
          |                                                                        | 
          | The recv() function shall fail if :                                    | 
          |                                                                        |
          | [EAGAIN] ||       |  The socket's file descriptor is marked O_NONBLOCK |
          | [EWOULDBLOCK]     |  and no data is waiting to be received; or MSG_OOB | 
          |                   |  is set and no out-of-band data is available and   |
          |                   |  either the socket's file descriptor is marked     |
          |                   |  O_NONBLOCK or the socket does not support         |
          |                   |  blocking to await out-of-band data.               | 
          | [EBADF]           |  The socket argument is not a valid file           |
          |                   |  descriptor.                                       | 
          | [ECONNRESET]      |  A connection was forcibly closed by a peer.       | 
          | [EINTR]           |  The recv() function was interrupted by a signal   |
          |                   |  that was caught, before any data was available.   | 
          | [EINVAL]          |  The MSG_OOB flag is set and no out-of-band data   |
          |                   |  is available.                                     | 
          | [ENOTCONN]        |  A receive is attempted on a connection-mode       |
          |                   |  socket that is not connected.                     | 
          | [ENOTSOCK]        |  The socket argument does not refer to a socket.   | 
          | [EOPNOTSUPP]      |  The specified flags are not supported for this    |
          |                   |  socket type or protocol.                          | 
          | [ETIMEDOUT]       |  The connection timed out during connection        |
          |                   |  establishment, or due to a transmission timeout   |
          |                   |  on active connection.                             | 
          |                                                                        |
          | The recv() function may fail if :                                      | 
          |                                                                        |
          | [EIO]             |  An I/O error occurred while reading from or       |
          |                   |  writing to the file system.                       | 
          | [ENOBUFS]         |  Insufficient resources were available in the      |
          |                   |  system to perform the operation.                  | 
          | [ENOMEM]          |  Insufficient memory was available to fulfill      |
          |                   |  the request.                                      | 
          |                                                                        | 
          |                                                                        |
          | APPLICATION USAGE                                                      | 
          |                                                                        |
          | The recv() function is equivalent to recvfrom() with a zero            |
          | address_len argument, and to read() if no flags are used.              | 
          |                                                                        |
          | The select() and poll() functions can be used to determine when data   |
          | is available to be received.                                           | 
          |                                                                        |
          |                                                                        |
          '------------------------------------------------------------------------' */
          PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_WAITING_RESPONSE", TabVariabiliEMPTY, 0, 0, 0);   
          memset(response, 0, __TCP_LEN_BUFFER_RECEIVE__);
          numByteReceive = recv (sock, response, __TCP_LEN_BUFFER_RECEIVE__, 0 );
          PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_RESPONSE", TabVariabiliEMPTY, 0, 0, 0);
          if (debug_level == __INFO__) printf("%d", numByteReceive);


          /*
          .----------------------------------------------.
          |                 Lettura OK                   |
          +----------------------------------------------+
          | Se il numero di byte ricevuti è maggiore di  |
          | zero significa che il server gli ha          |
          | restituito qualcosa in risposta              | 
          '----------------------------------------------' */
          if (numByteReceive == 0)
          {
              /*
              | =========================X Attenzione! X=========================
              | ho riscontrato che alcuni server non rilasciano stringhe della
              | lunghezza corretta nel caso in cui la stringa non viene riempita tutta.
              | Ad esempio succede questo nell'ultimo pezzo dell'header e del Body
              | quando il server non riesce a riempirla tutta, e allora succede
              | non inserendo il fine stringa (il problema è tutto li), se il
              | client la utilizza così come gli arriva nel caso migliore attiene
              | una stringa con in coda caratteri con previsti, e nel caso peggiore
              | stringa un buffer overflow. Diciamo che in conclusione ciò è
              | attendibile la lunghezza della stringa dati rilasciata
              | in risposta dalle api di read e non il contenuto del buffer di
              | output, a quetso il client dovrà in qualche modo inserire il
              | fine stringa.
              |
              */
              if ( numByteReceive != strlen(response) )
              {
                   char *response_bak;
                   response_bak = malloc (__TCP_LEN_BUFFER_RECEIVE__);
                   memset (response_bak, 0, __TCP_LEN_BUFFER_RECEIVE__);
                   strncpy(response_bak, response, numByteReceive);
                   strcpy (response, response_bak);
                   free(response_bak);
              } /*---*/
          }
           
          /*
          .----------------------------------------------.
          |                 E r r o r e                  |
          +----------------------------------------------+
          | Se il numero di byte ricevuti è -1 significa |
          | che c'è stato un errore... l'errore è        |
          | contenuto nella variabile globale errno      |
          | dichiarata nell'header <errno.h>             |
          '----------------------------------------------' */
          if (numByteReceive == -1)
          {
              rTCPReceive.idmsg       =  errno ;
              rTCPReceive.return_code = -1     ;

              PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_KO", TabVariabiliEMPTY, 0, 0, 0);
              PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_RESPONSE_KO", TabVariabiliEMPTY, 0, 0, 0);

              switch ( rTCPReceive.idmsg )
              {
                     //case EAGAIN      : PrintMessage(debug_level, TCP_TABMSG, "EAGAIN", TabVariabiliEMPTY, 0, 0, 0); 
                     //                   rTCPReceive.msgDesc = malloc(strlen("TCP.RECV.EAGAIL"));
                     //                   rTCPReceive.msgDesc = "TCP.RECV.EAGAIL"; 
                     //                   break; 

                       case EWOULDBLOCK : PrintMessage(debug_level, TCP_TABMSG, "EWOULDBLOCK", TabVariabiliEMPTY, 0, 0, 0);  
                                          rTCPReceive.msgDesc = malloc(strlen("TCP.RECV.EWOULDBLOCK"));
                                          rTCPReceive.msgDesc = "TCP.RECV.EWOULDBLOCK";
                                          break;
                    
                       case EBADF       : PrintMessage(debug_level, TCP_TABMSG, "EBADF", TabVariabiliEMPTY, 0, 0, 0);  
                                          rTCPReceive.msgDesc = malloc(strlen("TCP.RECV.EBADF"));
                                          rTCPReceive.msgDesc = "TCP.RECV.EBADF";
                                          break;
                    
                       case ECONNRESET  : PrintMessage(debug_level, TCP_TABMSG, "ECONNRESET", TabVariabiliEMPTY, 0, 0, 0);  
                                          rTCPReceive.msgDesc = malloc(strlen("TCP.RECV.ECONNRESET"));
                                          rTCPReceive.msgDesc = "TCP.RECV.ECONNRESET";
                                          break;
                    
                       case EINTR       : PrintMessage(debug_level, TCP_TABMSG, "EINTR", TabVariabiliEMPTY, 0, 0, 0);  
                                          rTCPReceive.msgDesc = malloc(strlen("TCP.RECV.EINTR"));
                                          rTCPReceive.msgDesc = "TCP.RECV.EINTR";
                                          break;
                    
                       case EINVAL      : PrintMessage(debug_level, TCP_TABMSG, "EINVAL", TabVariabiliEMPTY, 0, 0, 0);  
                                          rTCPReceive.msgDesc = malloc(strlen("TCP.RECV.EINVAL"));
                                          rTCPReceive.msgDesc = "TCP.RECV.EINVAL";
                                          break;
                    
                       case ENOTCONN    : PrintMessage(debug_level, TCP_TABMSG, "ENOTCONN", TabVariabiliEMPTY, 0, 0, 0);  
                                          rTCPReceive.msgDesc = malloc(strlen("TCP.RECV.ENOTCONN"));
                                          rTCPReceive.msgDesc = "TCP.RECV.ENOTCONN";
                                          break;
                    
                       case ENOTSOCK    : PrintMessage(debug_level, TCP_TABMSG, "ENOTSOCK", TabVariabiliEMPTY, 0, 0, 0);  
                                          rTCPReceive.msgDesc = malloc(strlen("TCP.RECV.ENOTSOCK"));
                                          rTCPReceive.msgDesc = "TCP.RECV.ENOTSOCK";
                                          break;
                    
                       case EOPNOTSUPP  : PrintMessage(debug_level, TCP_TABMSG, "EOPNOTSUPP", TabVariabiliEMPTY, 0, 0, 0);  
                                          rTCPReceive.msgDesc = malloc(strlen("TCP.RECV.EOPNOTSUPP"));
                                          rTCPReceive.msgDesc = "TCP.RECV.EOPNOTSUPP";
                                          break;
                    
                       case ETIMEDOUT   : PrintMessage(debug_level, TCP_TABMSG, "ETIMEDOUT", TabVariabiliEMPTY, 0, 0, 0);  
                                          rTCPReceive.msgDesc = malloc(strlen("TCP.RECV.ETIMEDOUT"));
                                          rTCPReceive.msgDesc = "TCP.RECV.ETIMEDOUT";
                                          break;
                    
                       case EIO         : PrintMessage(debug_level, TCP_TABMSG, "EIO", TabVariabiliEMPTY, 0, 0, 0); 
                                          rTCPReceive.msgDesc = malloc(strlen("TCP.RECV.EIO"));
                                          rTCPReceive.msgDesc = "TCP.RECV.EIO";
                                          break;
                    
                       case ENOBUFS     : PrintMessage(debug_level, TCP_TABMSG, "ENOBUFS", TabVariabiliEMPTY, 0, 0, 0);  
                                          rTCPReceive.msgDesc = malloc(strlen("TCP.RECV.ENOBUFS"));
                                          rTCPReceive.msgDesc = "TCP.RECV.ENOBUFS";
                                          break;
                    
                       case ENOMEM      : PrintMessage(debug_level, TCP_TABMSG, "ENOMEM", TabVariabiliEMPTY, 0, 0, 0);  
                                          rTCPReceive.msgDesc = malloc(strlen("TCP.RECV.ENOMEM"));
                                          rTCPReceive.msgDesc = "TCP.RECV.ENOMEM";
                                          break;
                    
                       default          : PrintMessage(debug_level, TCP_TABMSG, "SEND_TCP_UNKNOWN_ERROR", TabVariabiliEMPTY, 0, 0, 0);  
                                          rTCPReceive.msgDesc = malloc(strlen("TCP.RECV.SEND_TCP_UNKNOWN_ERROR"));
                                          rTCPReceive.msgDesc = "TCP.RECV.SEND_TCP_UNKNOWN_ERROR";
                                          break;
              };

              break;
          };

          /*
          .----------------------------------------------.
          |                     E O F                    |
          +----------------------------------------------+
          | Se il numero di byte ricevuti è 0 significa  |
          | che il server non ha più nulla da            |
          | trasmettere al client.                       |
          |                                              |
          '----------------------------------------------' */
          if (numByteReceive ==  0)
          {
              PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_OK", TabVariabiliEMPTY, 0, 0, 0);
              rTCPReceive.idmsg       =  0 ;
              rTCPReceive.return_code =  0 ;
              break;   
          };


          /*
          .----------------------------------------------.
          | Mantiene il conteggio dei Byte ricevuti      |
          '----------------------------------------------' */
          totByteReceive = totByteReceive + numByteReceive ;

          /*
          .----------------------------------------------.
          | Accoda i Byte ricevuti nel file di output    |
          '----------------------------------------------' */
          PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_WRITE_DATASTORE", TabVariabiliEMPTY, 0, 0, 0);
           fwrite(response, 1, strlen(response), pOutputFile);
    }


    /*
    .----------------------------------------------------.
    | Close OUTPUT                                       |
    +----------------------------------------------------+
    | Chiude il file di Output dopo aver terminato la    |
    | la lettura della coda.                             |
    '----------------------------------------------------' */
    PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_CLOSE_DATASTORE", TabVariabiliEMPTY, 0, 0, 0);
    fclose (pOutputFile);
    PrintMessage(debug_level, TCP_TABMSG, "TCP_RECEIVE_OK", TabVariabiliEMPTY, 0, 0, 0);

    /*
    .----------------------------------------------------.
    | Creare Output Structure                            |
    +----------------------------------------------------+
    | Prepara la struttura dati da rilasciare al         |
    | chiamante.                                         |
    '----------------------------------------------------' */
    rTCPReceive.response = malloc ( __TCP_LEN_BUFFER_RECEIVE__);    
    memset(rTCPReceive.response, 0, __TCP_LEN_BUFFER_RECEIVE__);
 // rTCPReceive.response    = response;    
    rTCPReceive.responseLen = totByteReceive; 

    return rTCPReceive;
};



/*
.-------------------------------------------------------------------------------------.
|                                                                                     |
|  Funzione    : TCPReceiveSetTimeout                                                 |
|                                                                                     |
|  Descrizione : Disconnect and close session TCP                                     |
|                                                                                     |
|  Return-code :                                                                      |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
'-------------------------------------------------------------------------------------'*/
extern int TCPReceiveSetTimeout (int sock, long int timeout, char *OutputFile, int debug_level)
{
       struct timeval tvtimeout ;

       tvtimeout.tv_sec  = timeout ; /* secondi */
       tvtimeout.tv_usec = 0       ; /* microsecondi ( secondi = microsecondi / 1000000 ) */

       //printf("\ntv_sec  : %ld", tvtimeout.tv_sec    );
       //printf("\ntv_usec : %ld", tvtimeout.tv_usec   );


       /*
       .------------------------------------------------------------------------------.
       |                                                                              |
       |                                                                              |
       |    setsockopt - set the socket options                                       |
       |                                                                              |
       |       The setsockopt() function shall set the  option  specified  by  the    |
       |    option_name argument, at the protocol level  specified  by  the  level    |
       |    argument, to the value pointed to by the option_value argument for the    |
       |    socket associated with the file descriptor  specified  by  the  socket    |
       |    argument.                                                                 | 
       |                                                                              |
       |                                                                              |
       |    PROTOTIPO                                                                 |
       |                                                                              |
       |    int setsockopt ( int         socket       ,                               | 
       |                     int         level        ,                               | 
       |                     int         option_name  ,                               | 
       |                     const void *option_value ,                               | 
       |                     socklen_t   option_len   );                              | 
       |                                                                              | 
       |                                                                              |
       |    PARAMETRI                                                                 |
       |                                                                              |
       |    [socket]                                                                  |
       |      ...                                                                     |
       |                                                                              |
       |                                                                              |
       |    [level]                                                                   |
       |                                                                              |
       |      The level argument specifies the protocol level at which the  option    |
       |    resides. To set  options  at  the  socket  level,  specify  the  level    |
       |    argument as SOL_SOCKET. To set options at  other  levels,  supply  the    |
       |    appropriate level identifier for the protocol controlling the  option.    |
       |    For example, to indicate that an option  is  interpreted  by  the  TCP    |
       |    (Trasport Control Protocol), set level to IPPROTO_TCP  as  defined  in    |
       |    the <netstat/in.h> header.                                                | 
       |                                                                              |
       |            |      SO_DEBUG  ...                                              |
       |            |                                                                 |
       |            |  SO_BROADCAST  ...                                              |
       |                                                                              |
       |                                                                              |
       |    [option_name]                                                             |
       |                                                                              |
       |      The option_name argument specifies  a  single  option  to  set.  The    |
       |    option_name  argument   and   any   specified   options   are   passed    |
       |    uninterpreted to the appropriate protocol module  for interpretations.    |
       |    The  <sys/socket.h>  header  defines  the  socket-level  options.  The    |
       |    options are as follows :                                                  |  
       |                                                                              |
       |            |      SO_DEBUG  Turns on recording of debugging  information.    |
       |            |                This option enables or disables debugging  in    |
       |            |                the underlying protocol modules. This  option    |
       |            |                takes an int value. This is a Boolean option.    | 
       |            |                                                                 | 
       |            |  SO_BROADCAST  Permits sending  of  broadcast  messages,  if    |
       |            |                this  is  supported  by  the  protocol.  This    |
       |            |                option takes an int value. This is a  Boolean    |
       |            |                option.                                          |  
       |            |                                                                 | 
       |            |  SO_REUSEADDR  Specifies that the rules used  in  validating    |
       |            |                addresses supplied  to  bind()  should  allow    |
       |            |                reuse   of   local   addresses,  if  this  is    |
       |            |                supported by the protocol. This option  takes    |
       |            |                an int value. This is a Boolean option.          | 
       |            |                                                                 |
       |            |  SO_KEEPALIVE  Keeps  connections  active  by  enabling  the    |
       |            |                periodic transmission of messages, if this is    |
       |            |                supported by the protocol. This option  takes    |
       |            |                an int value.                                    |  
       |            |                                                                 |
       |            |                If the connected socket fails to  respond  to    |
       |            |                these messages, the connection is broken  and    |
       |            |                threads writing to that socket  are  notified    |
       |            |                with a SIGPIPE  signal.  This  is  a  Boolean    |
       |            |                option.                                          |  
       |            |                                                                 |
       |            |     SO_LINGER  Lingers on a close() if data is present. This    |
       |            |                option controls the action taken when  unsent    |
       |            |                messages queue on a  socket  and  close()  is    |
       |            |                performed. If SO_LINGER is  set,  the  system    |
       |            |                shall block the calling thread during close()    |
       |            |                until it can transmit the data or  until  the    |
       |            |                time expires. If SO_LINGER is  not specified,    |
       |            |                and close() is issued, the system handles the    |
       |            |                call in a way that allows the calling  thread    |
       |            |                to continue  as  quickly  as  possible.  This    |
       |            |                option takes a linger structure,  as  defined    |
       |            |                in the <sys/socket.h> header, to specify  the    |
       |            |                state of the option and linger interval.         | 
       |            |                                                                 |
       |            |  SO_OOBINLINE  Leaves received out-of-band data (data marked    |
       |            |                urgent)  inline.  This  option  takes  an int    |
       |            |                value. This is a Boolean option.                 |
       |            |                                                                 | 
       |            |     SO_SNDBUF  Sets send buffer size. This option  takes  an    |
       |            |                int value.                                       | 
       |            |                                                                 |
       |            |     SO_RCVBUF  Sets  receive  buffer size. This option takes    |
       |            |                an int value.                                    | 
       |            |                                                                 |
       |            |  SO_DONTROUTE  Requests that outgoing  messages  bypass  the    |
       |            |                standard routing facilities. The  destination    |
       |            |                shall be on a directly-connected network, and    |
       |            |                messages  are  directed  to  the  appropriate    |
       |            |                network    interface    according    to   the    |
       |            |                destination address.  The effect,  if any, of    |
       |            |                this  option  depends  on what protocol is in    |
       |            |                use. This option takes an int value. This  is    |
       |            |                a Boolean option.                                | 
       |            |                                                                 |
       |            |   SO_RCVLOWAT  Sets the minimum number of bytes  to  process    |
       |            |                for  socket  input  operations.  The  default    |
       |            |                value for SO_RCVLOWAT is 1. If SO_RCVLOWAT is    |
       |            |                set to a larger value, blocking receive calls    |
       |            |                normally wait until they  have  received  the    |
       |            |                smaller of the low water mark  value  or  the    |
       |            |                requested amount. (They may return less  than    |
       |            |                the low water mark  if  an  error  occurs,  a    |
       |            |                signal is caught, or the type of data next in    |
       |            |                the receive  queue  is  different  from  that    |
       |            |                returned;  for  example,  out-of-band  data.)    |
       |            |                This option takes an int value. Note that not    |
       |            |                all implementations allow this option  to  be    |
       |            |                set.                                             |
       |            |                                                                 | 
       |            |   SO_RCVTIMEO  Sets the timeout  value  that  specifies  the    |
       |            |                maximum amount  of  time  an  input  function    |
       |            |                waits   until  it  completes.  It  accepts  a    |
       |            |                timeval structure with the number of  seconds    |
       |            |                and microseconds specifying the limit on  how    |
       |            |                long  to  wait  for  an  input  operation  to    |
       |            |                complete. If a receive operation has  blocked    |
       |            |                for  this   much   time   without   receiving    |
       |            |                additional  data,  it  shall  return  with  a    |
       |            |                partial count or errno  set  to  [EAGAIN]  or    |
       |            |                [EWOULDBLOCK] if no  data  is  received.  The    |
       |            |                received.  The  default  for  this  option is    |
       |            |                zero,  which   indicates   that   a   receive    |
       |            |                operation shall not  time  out.  This  option    |
       |            |                takes a timeval structure.  Note that not all    |
       |            |                implementation allow this option to be set.      | 
       |            |                                                                 |
       |            |   SO_SNDLOWAT  Sets the minimum number of bytes  to  process    |
       |            |                for socke   output  operations.  Non-blocking    |
       |            |                output operations shall process  no  data  if    |
       |            |                flow control does not allow  the  smaller  of    |
       |            |                the send low water mark value or  the  entire    |
       |            |                prequest to be rocessed. This option takes an    |
       |            |                int value. Note that not all  implementations    |
       |            |                allow this option to be set.                     | 
       |            |                                                                 | 
       |            |   SO_SNDTIMEO  Sets the timeout value specifying the  amount    |
       |            |                of   time  that  an  output  function  blocks    |
       |            |                because flow control prevents data from being    |
       |            |                sent.  If  a  send  operation has blocked for    |
       |            |                this  time,  it  shall  return with a partial    |
       |            |                count  or  with  errno  set  to  [EAGAIN]  or    |
       |            |                [EWOULDBLOCK] if no data is sent. The default    |
       |            |                for this option is zero, which indicates that    |
       |            |                a send operation shall  not  time  out.  This    |
       |            |                option stores a timeval structure.  Note that    |
       |            |                not all implementations allow this option  to    |
       |            |                be set.                                          | 
       |                                                                              |
       |   For  Boolean  options,  0  indicates  that the option is disabled and 1    |
       |   indicates that the option is enabled.                                      | 
       |                                                                              |
       |   Options at other protocol levels vary in format and name.                  | 
       |                                                                              |
       |                                                                              |
       |                                                                              |
       |   RETURN VALUE                                                               |
       |                                                                              |
       |     Upon  successful completion, setsockopt() shall return 0.  Otherwise,    |
       |   -1 shall be returned and errno set to indicate the error.                  | 
       |                                                                              |
       |                                                                              |
       |                                                                              |
       |   ERRORS                                                                     | 
       |                                                                              |
       |     The setsockopt() function shall fail if :                                | 
       |                                                                              |
       |           [EBADF] | The socket argument is not a valid  file  descriptor.    |  
       |            [EDOM] | The send and receive timeout values are  too  big  to    |
       |                   | fit into the timeout fields in the socket  structure.    |
       |                   |                                                          | 
       |          [EINVAL] | The specified option  is  invalid  at  the  specified    |
       |                   | socket level  or  the  socket  has  been  shut  down.    |
       |                   |                                                          |  
       |         [EISCONN] | The socket is  already  connected,  and  a  specified    |
       |                   | option cannot be set while the socket is connected.      |
       |                   |                                                          | 
       |     [ENOPROTOOPT] | The option is not supported by the protocol.             |
       |                   |                                                          |  
       |        [ENOTSOCK] | The socket argument does not refer to a socket.          | 
       |                   |                                                          |
       |                                                                              |
       |     The setsockopt() function may fail if :                                  | 
       |                                                                              |
       |          [ENOMEM] | There  was  insufficient  memory  available  for  the    |
       |                   | operation to complete.                                   | 
       |                   |                                                          |
       |         [ENOBUFS] | Insufficient resources are available  in  the  system    |
       |                   | to complete the call.                                    | 
       |                                                                              |
       |  The following sections are informative.                                     | 
       |                                                                              |
       |                                                                              |
       |  APPLICATION USAGE                                                           | 
       |                                                                              |
       |    The setsockopt() function provides an  application  program  with  the    |
       |  means  to  control  socket  behavior. An  application  program  can  use    |
       |  setsockopt() to allocate  buffer  space,  control  timeouts,  or  permit    |
       |  socket  data  broadcasts.  The   <sys/socket.h>   header   defines   the    |
       |  socket-level options available to setsockopt().                             |
       |                                                                              |
       |    Options may exist at multiple protocol levels.  The  SO_  options  are    |
       |  always present at the uppermost socket level.                               | 
       |                                                                              |
       |                                                                              |
       |                                                                              |
       |                                                                              |
       '------------------------------------------------------------------------------'*/
       if ( setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tvtimeout,  sizeof tvtimeout) )
       {
            return -1;
       }
       return 0;
};



/*
.-------------------------------------------------------------------------------------.
|                                                                                     |
|  Funzione    : TCPDisconnect                                                        |
|                                                                                     |
|  Descrizione : Disconnect and close session TCP                                     |
|                                                                                     |
|  Return-code : 0 Ok                                                                 |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
'-------------------------------------------------------------------------------------'*/
extern struct tTCPDisconnect TCPDisconnect(int sock, int debug_level)
{
    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , "", ""                                  }
    };

    /*
    .--------------------------------------.
    | Dichiarazione strutture utilizzate   |
    '--------------------------------------' */
    struct tTCPDisconnect      rTCPDisconnect      ;
    struct tTCPDisconnect     *pTCPDisconnect      ;

    /*
    .------------------------------------.
    | Allocazione Spazio                 |
    '------------------------------------' */
    pTCPDisconnect           = malloc(sizeof(struct tTCPDisconnect));

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pTCPDisconnect           = &rTCPDisconnect;

    /*
    .-----------------------------------------.
    | Inizializzazione                        |
    '-----------------------------------------' */
    memset((void*)&rTCPDisconnect, 0, sizeof(rTCPDisconnect));
    rTCPDisconnect.idmsg           = 0 ;
    rTCPDisconnect.return_code     = 0 ;

    /*
    .------------------------.
    | Shutdown Socket        |
    '------------------------' */
    PrintMessage(debug_level, TCP_TABMSG, "TCP_DISCONNECT_SHUTDOWN", TabVariabiliEMPTY, 0, 0, 0);
    shutdown(sock, SHUT_RDWR);
    PrintMessage(debug_level, TCP_TABMSG, "TCP_DISCONNECT_OK", TabVariabiliEMPTY, 0, 0, 0);

    /*
    .------------------------.
    | Close Socket           |
    '------------------------' */
    PrintMessage(debug_level, TCP_TABMSG, "TCP_DISCONNECT_CLOSE", TabVariabiliEMPTY, 0, 0, 0);
    close(sock);
    PrintMessage(debug_level, TCP_TABMSG, "TCP_DISCONNECT_OK", TabVariabiliEMPTY, 0, 0, 0);


    rTCPDisconnect.idmsg       = 0                                                              ;
    rTCPDisconnect.msgDesc     = malloc(strlen("TCP.DISCONNECT.OK"))                            ;
    rTCPDisconnect.msgDesc     = "TCP.DISCONNECT.OK"                                            ;
    rTCPDisconnect.return_code = 0                                                              ;

    return rTCPDisconnect;
}



/*
.-------------------------------------------------------------------------------------.
|                                                                                     |
|  Funzione    : TCPClientConnect                                                     |
|                                                                                     |
|  Descrizione : Minimal Client TCP                                                   |
|                                                                                     |
|  Return-code :  0 Ok                                                                |
|                -1 TCP Connect Error                                                 |
|                -2 SSL Connect Error *NotUsed*                                       |
|                -3 SSL Authentication Server Error *NotUsed*                         |
|                -4 Send TCP|SSL Error                                                |
|                -5 Receive TCP|SSL Error                                             |
|                -6 SSL Disconnect Error *NotUsed*                                    |
|                -7 TCP Disconnect                                                    |
|                                                                                     |
'-------------------------------------------------------------------------------------'*/
extern struct tClientConnect TCPClientConnect ( struct tClientConnectParameter rClientConnectParameter )
{	
    /*                    
    .--------------------------------------.
    | Dichiarazione strutture utilizzate   |
    '--------------------------------------' */
    struct tClientConnect      rClientConnect      ;
    struct tClientConnect     *pClientConnect      ;

    struct tTCPConnect         rTCPConnect         ;
    struct tTCPConnect        *pTCPConnect         ;

    struct tTCPSend            rTCPSend            ;
    struct tTCPSend           *pTCPSend            ;

    struct tTCPReceive         rTCPReceive         ;
    struct tTCPReceive        *pTCPReceive         ;

    struct tTCPDisconnect      rTCPDisconnect      ;
    struct tTCPDisconnect     *pTCPDisconnect      ;

    /*
    .------------------------------------.
    | Allocazione Spazio                 |
    '------------------------------------' */
    pClientConnect           = malloc(sizeof(struct tClientConnect));
    pTCPConnect              = malloc(sizeof(struct tTCPConnect));
    pTCPSend                 = malloc(sizeof(struct tTCPSend));
    pTCPReceive              = malloc(sizeof(struct tTCPReceive));
    pTCPDisconnect           = malloc(sizeof(struct tTCPDisconnect));

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pClientConnect           = &rClientConnect;
    pTCPConnect              = &rTCPConnect;
    pTCPSend                 = &rTCPSend;
    pTCPReceive              = &rTCPReceive;
    pTCPDisconnect           = &rTCPDisconnect;

    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rClientConnect     , 0, sizeof(rClientConnect));
    memset((void*)&rTCPConnect        , 0, sizeof(rTCPConnect));
    memset((void*)&rTCPSend           , 0, sizeof(rTCPSend));
    memset((void*)&rTCPReceive        , 0, sizeof(rTCPReceive));
    memset((void*)&rTCPDisconnect     , 0, sizeof(rTCPDisconnect));

    /*
    .------------------------------------------.
    |  Determina il livello di DEBUG           |
    '------------------------------------------' */
    int debug_level = GetDebugLevel ( rClientConnectParameter.debug, "tcp" ) ; 

    /*
    .-------------.
    | TCP Connect |
    '-------------' */
    rClientConnect.return_code = 0;
    rTCPConnect = TCPConnect(rClientConnectParameter.host, rClientConnectParameter.porta, 
	                                        rClientConnectParameter.TCPReceiveTimeout, debug_level);
    rClientConnect.TCPIp             = rTCPConnect.ip ; 
    rClientConnect.TCPPort           = rTCPConnect.port ;
    rClientConnect.TCPConnectMsg     = rTCPConnect.idmsg ;
    rClientConnect.TCPConnectMsgDesc = rTCPConnect.msgDesc;
    rClientConnect.TCPConnectReturn  = rTCPConnect.return_code ;
    if ( rTCPConnect.return_code != 0) 
    {
         rClientConnect.return_code = -1 ; 
         return rClientConnect ;
    };

    /*
    .-----------------------.
    | Request and Response  |
    '-----------------------' */
    if ( rClientConnectParameter.message != NULL )
    {
         /*
         .--------------.
         | Send Request |
         '--------------' */
         rTCPSend = TCPSend (rTCPConnect.sock, rClientConnectParameter.message, debug_level);
         rClientConnect.ServerRequestBufferLen    = rTCPSend.requestLen;
         rClientConnect.ServerRequestBuffer       = rTCPSend.request;
         rClientConnect.ServerRequestMsg          = rTCPSend.idmsg;
         rClientConnect.ServerRequestMsgDesc      = rTCPSend.msgDesc;
         rClientConnect.ServerRequestReturn       = rTCPSend.return_code;
         if ( rTCPSend.return_code != 0)
         {
              rClientConnect.return_code = -4 ;
         };
 
         /*
         .------------------.
         | Receive Response |
         '------------------' */
         if ( rTCPSend.return_code == 0 )  
         { 
              rTCPReceive = TCPReceive (rTCPConnect.sock, rClientConnectParameter.TCPReceiveTimeout, rClientConnectParameter.OutputFile, debug_level);
              rClientConnect.ServerResponseBufferNum   = 0; 
              rClientConnect.ServerResponseBufferLen   = rTCPReceive.responseLen;
              rClientConnect.ServerResponseBuffer      = rTCPReceive.response;
              rClientConnect.ServerResponseMsg         = rTCPReceive.idmsg;
              rClientConnect.ServerResponseMsgDesc     = rTCPReceive.msgDesc;
              rClientConnect.ServerResponseReturn      = rTCPReceive.return_code;
              if ( rTCPReceive.return_code != 0)
              {
                   rClientConnect.return_code = -5 ;
              };
         };
    };

    /*
    .----------------.
    | TCP Disconnect |
    '----------------' */
    rTCPDisconnect = TCPDisconnect(rTCPConnect.sock, debug_level);
    rClientConnect.TCPDisconnectMsg    = rTCPDisconnect.idmsg ;
    rClientConnect.TCPDisconnectReturn = rTCPDisconnect.return_code ;
    if ( rTCPDisconnect.return_code != 0)
    {        
         rClientConnect.return_code = -7 ;
    };

    return rClientConnect ; 
};


// ______EOF_
