/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 

#define __NONE__    0
#define __INFO__    1
#define __WARN__    2
#define __ERR__     3

 
/*
.--------------------------------------------------------------------------------------------.
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                   TCP Messages Table                                       |
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                                                                            |
'--------------------------------------------------------------------------------------------' */
char *TCP_TABMSG[][3] = { 


    { "TCP_CONNECT_OK"                  ,"__INFO__" , " done.\n"                                                  }, 
    { "TCP_CONNECT_KO"                  ,"__INFO__" , " error.\n"                                                 },
    { "TCP_CONNECT_VERIFYPORTRANGE"     ,"__INFO__" , "TCP::Verify Port Range..."                                 },
    { "TCP_CONNECT_SOCKETOPEN"          ,"__INFO__" , "TCP::Opening the socket..."                                },
    { "TCP_CONNECT_SOCKETOPEN_KO"       ,"__ERR__"  , "I can not open the socket.\n\n"                            }, 
    { "TCP_CONNECT_SOCKET"              ,"__INFO__" , "TCP::Connectiong to socket server..."                      },
    { "TCP_CONNECT_SOCKET_KO"           ,"__ERR__"  , "I opened the socket but I can not connect to server.\n"    }, 

    { "TCP_SOCKETBLOCK_OK"              ,"__INFO__" , " done.\n"                                                  },
    { "TCP_SOCKETBLOCK_KO"              ,"__INFO__" , " error.\n"                                                 },

    { "TCP_SOCKETBLOCKON"               ,"__INFO__" , "TCP::Socket Blocking..."                                   },
    { "TCP_SOCKETBLOCKOFF"              ,"__INFO__" , "TCP::Socket Not Blocking..."                               },

    { "TCP_RESOLVHOST_OK"               ,"__INFO__" , " done.\n"                                                  },
    { "TCP_RESOLVHOST_KO"               ,"__INFO__" , " error.\n"                                                 },
    { "TCP_RESOLVHOST_RESOLVHOST"       ,"__INFO__" , "TCP::Resolving hostname..."                                },
    { "TCP_RESOLVHOST_RESOLVHOST_KO"    ,"__ERR__"  , "I can not resolve the hostname.\n\n"                       },

    { "TCP_SEND_OK"                     ,"__INFO__" , " done.\n"                                                  },
    { "TCP_SEND_KO"                     ,"__INFO__" , " error.\n"                                                 },
    { "TCP_SEND_CALCULATE_LEN_REQUEST"  ,"__INFO__" , "TCP::Calculate len request..."                             },
    { "TCP_SEND_REQUEST"                ,"__INFO__" , "TCP::Send request to Server..."                            },

    { "TCP_RECEIVE_OK"                  ,"__INFO__" , " done.\n"                                                  },
    { "TCP_RECEIVE_KO"                  ,"__INFO__" , " error.\n"                                                 },
    { "TCP_RECEIVE_OPEN_DATASTORE"      ,"__INFO__" , "TCP::opening datastore..."                                 }, 
    { "TCP_RECEIVE_OPEN_DATASTORE_KO"   ,"__INFO__" , "TCP::I cant to open the datastore.\n\n"                    }, 
    { "TCP_RECEIVE_WRITE_DATASTORE"     ,"__INFO__" , "S|"                                                        },
    { "TCP_RECEIVE_CLOSE_DATASTORE"     ,"__INFO__" , "TCP::closing datastore..."                                 },
    { "TCP_RECEIVE_SET_TIMEOUT"         ,"__INFO__" , "TCP::setting timeout..."                                   },
    { "TCP_RECEIVE_SET_TIMEOUT_KO"      ,"__INFO__" , "TCP::I cant to set the timeout.\n\n"                       },
    { "TCP_RECEIVE_WAITING_RESPONSE"    ,"__INFO__" , "W"                                                         },
    { "TCP_RECEIVE_RESPONSE"            ,"__INFO__" , "R"                                                         },
    { "TCP_RECEIVE_RESPONSE_KO"         ,"__INFO__" , "I received a error from server.\n"                         },
    { "TCP_RECEIVE_START"               ,"__INFO__" , "TCP::waiting, reading and storing response from server : " }, 

    { "TCP_DISCONNECT_OK"               ,"__INFO__" , " done.\n"                                                  },
    { "TCP_DISCONNECT_KO"               ,"__INFO__" , " error.\n"                                                 },
    { "TCP_DISCONNECT_SHUTDOWN"         ,"__INFO__" , "TCP::shutdown the socket..."                               },
    { "TCP_DISCONNECT_CLOSE"            ,"__INFO__" , "TCP::close the socket..."                                  },

    /*
    .------------------------------------------------------.
    | Create Socket Error Code                             |
    '------------------------------------------------------' */
    { "EAFNOSUPPORT"                    ,"__ERR__"  , "The implementation does not support the specified address family.\n\n"             }, 
    { "EMFILE"                          ,"__ERR__"  , "No more file descriptors are available for this process.\n\n"                      }, 
    { "ENFILE"                          ,"__ERR__"  , "No more file descriptors are available for the system.\n\n"                        }, 
//  { "EPROTONOSUPPORT"                 ,"__ERR__"  , "The protocol is not supported by the address family, "
//                                                    "or the protocol is not supported by the implementation.\n\n")                      },
    { "EPROTOTYPE"                      ,"__ERR__"  , "The socket type is not supported by the protocol.\n\n"                             }, 
    { "EACCES"                          ,"__ERR__"  , "The process does not have appropriate privileges.\n\n"                             }, 
    { "ENOBUFS"                         ,"__ERR__"  , "Insufficient resources were available in the system to perform the operation.\n\n" }, 
    { "ENOMEM"                          ,"__ERR__"  , "Insufficient memory was available to fulfill the request.\n\n"                     }, 
    { "CREATE_SOCKET_UNKNOWN_ERROR"     ,"__ERR__"  , "Unknown error.\n\n"                                                                }, 

    /*
    .------------------------------------------------------.
    | Connect Socket Error Code                            |
    '------------------------------------------------------' */
    { "EADDRNOTAVAIL"                   ,"__ERR__"  , "The specified address is not available from the local machine.\n\n"                  },
    { "EAFNOSUPPORT"                    ,"__ERR__"  , "The specified address is not a valid address for the address family "
                                                      "of the specified socket.\n\n"                                                        },
    { "EALREADY"                        ,"__ERR__"  , "A connection request is already in progress for the specified socket.\n\n"           }, 
    { "EBADF"                           ,"__ERR__"  , "The socket argument is not a valid file descriptor.\n\n"                             }, 
    { "ECONNREFUSED"                    ,"__ERR__"  , "The target address was not listening for connections or refused "
                                                      "the connection request.\n\n"                                                         }, 
    { "EINPROGRESS"                     ,"__ERR__"  , "O_NONBLOCK is set for the file descriptor for the socket and the connection cannot "
                                                      "be immediately established; the connection shall be established asynchronously.\n\n" }, 
    { "EINTR"                           ,"__ERR__"  , "The attempt to establish a connection was interrupted by delivery of a signal "
                                                      "that was caught; the connection shall be established asynchronously.\n\n"            }, 
    { "EISCONN"                         ,"__ERR__"  , "The specified socket is connection-mode and is already connected.\n\n"               }, 
    { "ENETUNREACH"                     ,"__ERR__"  , "No route to the network is present.\n\n"                                             }, 
    { "ENOTSOCK"                        ,"__ERR__"  , "The socket argument does not refer to a socket.\n\n"                                 }, 
    { "EPROTOTYPE"                      ,"__ERR__"  , "The specified address has a different type than the socket bound to the "
                                                      "specified peer address.\n\n"                                                         },
    { "ETIMEDOUT"                       ,"__ERR__"  , "The attempt to connect timed out before a connection was made.\n\n"                  }, 
    { "EIO"                             ,"__ERR__"  , "An I/O error occurred while reading from or writing to the file system.\n\n"         }, 
    { "ELOOP"                           ,"__ERR__"  , "A loop exists in symbolic links encountered during resolution of the "
                                                      "pathname in address.\n\n"                                                            }, 
    { "ENAMETOOLONG"                    ,"__ERR__"  , "A component of a pathname exceeded {NAME_MAX} characters, or an entire pathname "     
                                                      "exceeded {PATH_MAX} characters\n\n"                                                  }, 
    { "ENOENT"                          ,"__ERR__"  , "A component of the pathname does not name an existing file or the pathname "
                                                      "is an empty string.\n\n"                                                             }, 
    { "ENOTDIR"                         ,"__ERR__"  , "A component of the path prefix of the pathname in address is not a directory.\n\n"   }, 
    { "EACCES"                          ,"__ERR__"  , "Search permission is denied for a component of the path prefix; "   
                                                      "or write access to the named socket is denied.\n\n"                                  }, 
    { "EADDRINUSE"                      ,"__ERR__"  , "Attempt to establish a connection that uses addresses that are already in use.\n\n"  }, 
    { "ECONNRESET"                      ,"__ERR__"  , "Remote host reset the connection request.\n\n"                                       }, 
    { "EHOSTUNREACH"                    ,"__ERR__"  , "The destination host cannot be reached (probably because the host is down or "
                                                      "a remote router cannot reach it).\n\n"                                               }, 
    { "EINVAL"                          ,"__ERR__"  , "The address_len argument is not a valid length for the address family; "
                                                      "or invalid address family in the sockaddr structure.\n\n"                            }, 
  //{ "ELOOP"                           ,"__ERR__"  , "More than {SYMLOOP_MAX} symbolic links were encountered during resolution "
  //                                                  "of the pathname in address.\n\n"                                                     }, 
  //{ "ENAMETOOLONG"                    ,"__ERR__"  , "Pathname resolution of a symbolic link produced an intermediate result "
  //                                                  "whose length exceeds {PATH_MAX}.\n\n"                                                }, 
    { "ENETDOWN"                        ,"__ERR__"  , "The local network interface used to reach the destination is down.\n\n"              }, 
    { "ENOBUFS"                         ,"__ERR__"  , "No buffer space is available.\n\n"                                                   }, 
    { "EOPNOTSUPP"                      ,"__ERR__"  , "The socket is listening and cannot be connected.\n\n"                                }, 
    { "CONNECT_SOCKET_UNKNOWN_ERROR"    ,"__ERR__"  , "Unknown error.\n\n"                                                                  }, 

    /*
    .------------------------------------------------------.
    | Send TCP Error Code                                  |
    '------------------------------------------------------' */
    { "EAGAIN"                          ,"__ERR__"  , "The socket's file descriptor is marked O_NONBLOCK and the requested operation would block.\n\n" },
 // { "EWOULDBLOCK"                     ,"__ERR__"  , "The socket's file descriptor is marked O_NONBLOCK and the requested operation would block.\n\n" }, 
    { "EBADF"                           ,"__ERR__"  , "The socket argument is not a valid file descriptor.\n\n"                                        }, 
    { "ECONNRESET"                      ,"__ERR__"  , "A connection was forcibly closed by a peer.\n\n"                                                },
    { "EDESTADDRREQ"                    ,"__ERR__"  , "The socket is not connection-mode and no peer address is set.\n\n"                              }, 
    { "EINTR"                           ,"__ERR__"  , "A signal interrupted send() before any data was transmitted.\n\n"                               },
    { "EMSGSIZE"                        ,"__ERR__"  , "The message is too large to be sent all at once, as the socket requires.\n\n"                   }, 
    { "ENOTCONN"                        ,"__ERR__"  , "The socket is not connected or otherwise has not had the peer pre-specified.\n\n"               }, 
    { "ENOTSOCK"                        ,"__ERR__"  , "The socket argument does not refer to a socket.\n\n"                                            }, 
    { "EOPNOTSUPP"                      ,"__ERR__"  , "The socket argument is associated with a socket that does not support one or more "
                                                      "of the values set in flags\n\n"                                                                 }, 
    { "EPIPE "                          ,"__ERR__"  , "The socket is shut down for writing, or the socket is connection-mode and is no longer "
                                                      "connected. In the latter case, and if the socket is of type SOCK_STREAM, the SIGPIPE signal "
                                                      "is generated to the calling thread.\n\n"                                                        }, 
    { "EACCES"                          ,"__ERR__"  , "The calling process does not have the appropriate privileges.\n\n"                              }, 
    { "EIO"                             ,"__ERR__"  , "An I/O error occurred while reading from or writing to the file system.\n\n"                    }, 
    { "ENETDOWN"                        ,"__ERR__"  , "The local network interface used to reach the destination is down.\n\n"                         }, 
    { "ENETUNREACH"                     ,"__ERR__"  , "No route to the network is present.\n\n"                                                        }, 
    { "ENOBUFS"                         ,"__ERR__"  , "Insufficient resources were available in the system to perform the operation.\n\n"              }, 
    { "SEND_TCP_UNKNOWN_ERROR"          ,"__ERR__"  , "Unknown error.\n\n"                                                                             }, 


    /*
    .------------------------------------------------------.
    | Receive TCP Error Code                               |
    '------------------------------------------------------' */
    { "EAGAIN"                          ,"__ERR__"  , "The socket's file descriptor is marked O_NONBLOCK and no data is waiting to be received; "
                                                      "or MSG_OOB is set and no out-of-band data is available and either the socket's file "
                                                      "descriptor is marked O_NONBLOCK or the socket does not support blocking to await "
                                                      "out-of-band data.\n\n"                                                                          }, 
 // { "EWOULDBLOCK"                     ,"__ERR__"  , "The socket's file descriptor is marked O_NONBLOCK and no data is waiting to be received; "
 //                                                   "or MSG_OOB is set and no out-of-band data is available and either the socket's file "
 //                                                   "descriptor is marked O_NONBLOCK or the socket does not support blocking to await "
 //                                                   "out-of-band data.\n\n"                                                                          }, 
    { "EBADF"                           ,"__ERR__"  , "The socket argument is not a valid file descriptor.\n\n"                                        }, 
    { "ECONNRESET"                      ,"__ERR__"  , "A connection was forcibly closed by a peer.\n\n"                                                }, 
    { "EINTR"                           ,"__ERR__"  , "The recv() function was interrupted by a signal that was caught, "
                                                      "before any data was available.\n\n"                                                             }, 
    { "EINVAL"                          ,"__ERR__"  , "The MSG_OOB flag is set and no out-of-band data is available.\n\n"                              }, 
    { "ENOTCONN"                        ,"__ERR__"  , "A receive is attempted on a connection-mode socket that is not connected.\n\n"                  }, 
    { "ENOTSOCK"                        ,"__ERR__"  , "The socket argument does not refer to a socket.\n\n"                                            }, 
    { "EOPNOTSUPP"                      ,"__ERR__"  , "The specified flags are not supported for this socket type or protocol.\n\n"                    }, 
    { "ETIMEDOUT"                       ,"__ERR__"  , "The connection timed out during connection establishment, "                                       
                                                      "or due to a transmission timeout on active connection.\n\n"                                     }, 
    { "EIO"                             ,"__ERR__"  , "An I/O error occurred while reading from or writing to the file system.\n\n"                    }, 
    { "ENOBUFS"                         ,"__ERR__"  , "Insufficient resources were available in the system to perform the operation.\n\n"              }, 
    { "ENOMEM"                          ,"__ERR__"  , "Insufficient memory was available to fulfill the request.\n\n"                                  }, 
    { "RECEIVE_TCP_UNKNOWN_ERROR"       ,"__ERR__"  , "Unknown error.\n\n"                                                                             }, 


    { "END"                             ,"__NONE__" , "End of Message Table. *Warning* Message not found.\n"      }};

