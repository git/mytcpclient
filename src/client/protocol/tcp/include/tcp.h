/*
# Copyright (C) 2008
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/


#define __TCP_LEN_BUFFER_RECEIVE__ 4096 

/*
.-----------------------------------------------------.
|                                                     |
|                      Connessione                    |
|                                                     |
'-----------------------------------------------------' */
struct tTCPConnect 
{
       int sock        ;
       char *ip        ;
       int  port       ;
       long idmsg      ;
       char *msgDesc   ;
       int return_code ;
};
extern struct tTCPConnect TCPConnect( char *host, int porta, long int timeout, int debug_level ); 
extern struct tTCPConnect TCPConnectORIGINALE( char *host, int porta, long int timeout, int debug_level ); 

extern int TCPSocketSetBlocking ( int sock, int debug_level ); 
extern int TCPSocketSetMotBlocking ( int sock, int debug_level ); 

/*
.-----------------------------------------------------.
|                                                     |
|                    Risolve Hostname                 |
|                                                     |
'-----------------------------------------------------' */
struct tTCPGetHostByName
{
       char **tabAddress ; 
       int dimTabAddress ; 
       long idmsg        ;
       int return_code   ;
};
extern struct tTCPGetHostByName TCPGetHostByName ( char *host, int debug_level ); 

/*
.-----------------------------------------------------.
|                                                     |
|                       Trasmette                     |
|                                                     |
'-----------------------------------------------------' */
struct tTCPSend 
{
       char *request   ;
       long requestLen ;
       long idmsg      ;
       char *msgDesc   ;
       int return_code ;
};
extern struct tTCPSend TCPSend (int sock, char *request, int debug_level); 

/*
.-----------------------------------------------------.
|                                                     |
|                         Riceive                     |
|                                                     |
'-----------------------------------------------------' */
struct tTCPReceive 
{
       char *response   ;
       long responseLen ;
       long idmsg       ;
       char *msgDesc    ;
       int return_code  ;
};
extern struct tTCPReceive TCPReceive (int sock, long int timeout, char *OutputFile, int debug_level); 

/*
.-----------------------------------------------------.
|                                                     |
|                  RiceiveSetTimeout                  |
|                                                     |
'-----------------------------------------------------' */
extern int TCPReceiveSetTimeout (int sock, long int timeout, char *OutputFile, int debug_level); 

/*
.-----------------------------------------------------.
|                                                     |
|                   Disconnessione                    |
|                                                     |
'-----------------------------------------------------' */
struct tTCPDisconnect 
{
       long idmsg      ;
       char *msgDesc   ;
       int return_code ;
};
extern struct tTCPDisconnect TCPDisconnect(int sock, int debug_level); 

/*
.-----------------------------------------------------.
|                                                     |
|                 Client TCP Minimale                 |
|                                                     |
'-----------------------------------------------------' */
extern struct tClientConnect TCPClientConnect ( struct tClientConnectParameter rClientConnectParameter ); 


// __EOF__

