/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 

#define __NONE__    0
#define __INFO__    1
#define __WARN__    2
#define __ERR__     3


/*
.--------------------------------------------------------------------------------------------.
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                   SSL Messages Table                                       |
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                                                                            |
'--------------------------------------------------------------------------------------------' */
char *SSL_TABMSG[][3] = {

                     /*
                     .---------------------------------.
                     |                                 |
                     |         Create Context          |
                     |                                 |
                     '---------------------------------' */
                     { "SSL_CTX_CREATE",
                       "__INFO__",
                       "SSL::Context::creating context client... \n" },

                     { "SSL_CTX_CREATE_OK", 
                       "__INFO__", 
                       "+OK\n\n" }, 

                     { "SSL_CTX_CREATE_ERROR", 
                       "__INFO__", 
                       "SSL::Context::create context client... fail\n" }, 
                      
                     { "SSL_CTX_CREATE_SSLVERSION_ERROR",
                       "__INFO__",
                       "+I can not to set SSL/TLS version in ssl context. It isnt supported.\n" },
 
                     { "SSL_CTX_CREATE_CIPHERSUITE_ERROR",
                       "__INFO__", 
                       "SSL::Context::create context client... fail\n"
                       "+I can not to set cipher suite in ssl context.\n" }, 
                       
                     { "SSL_CTX_CREATE_CERT_ERROR",
                       "__INFO__", 
                       "SSL::Context::create context client... fail\n"
                       "+I can not to read X.509 certificate.\n" }, 

                     { "SSL_CTX_CREATE_KEY_ERROR",
                       "__INFO__", 
                       "SSL::Context::create context client... fail\n"
                       "+I can not read private key.\n" }, 

                     { "SSL_CTX_CREATE_TRUSTSTORE_ERROR",
                       "__INFO__", 
                       "SSL::Context::create context client... fail\n"
                       "+I can not read trustore file.\n" }, 

                     { "SSL_CTX_CREATE_UNKNOWN_ERROR",
                       "__INFO__", 
                       "SSL::Context::create context client... fail\n"
                       "+Unknown error.\n" }, 


                     /*
                     .---------------------------------.
                     |                                 |
                     |         Destroy Context         |
                     |                                 |
                     '---------------------------------' */
                     { "SSL_CTX_DESTROY_OK",     "__INFO__", "SSL::disconnect... Context Destroy. done\n" },


                     /*
                     .---------------------------------.
                     |                                 |
                     |         Create Socket           |
                     |                                 | 
                     '---------------------------------' */ 
                     { "SSL_CONNECT_SOCKET_CREATE_OK",
                        "__INFO__",
                       "SSL::Create Socket SSL... done\n" },

                     { "SSL_CONNECT_SOCKET_CREATE_KO",
                        "__INFO__",
                       "SSL::Create Socket SSL... fail\n" },  

                     /*
                     .---------------------------------.
                     |                                 |
                     |         Socket CTX Get          |
                     |                                 |
                     '---------------------------------' */
                     { "SSL_CONNECT_SOCKETCTX_GETVERSION_OK",
                        "__INFO__",
                       "SSL::SocketCTXGet::get version SSL/TLS client... done\n" },

                     { "SSL_CONNECT_SOCKETCTX_GETVERSION_ERROR",
                        "__INFO__",
                       "SSL::SocketCTXGet::get version SSL/TLS client... fail\n" },

                     { "SSL_CONNECT_SOCKETCTX_GETCIPHER_OK",
                        "__INFO__",
                       "SSL::SocketCTXGet::get ciphersuite client... done\n" },

                     { "SSL_CONNECT_SOCKETCTX_GETCIPHER_ERROR",
                        "__INFO__",
                       "SSL::SocketCTXGet::get ciphersuite client... fail\n" },

                     /*
                     .---------------------------------.
                     |                                 |
                     |         Handeshake SSL          |
                     |                                 |
                     '---------------------------------' */

                     { "SSL_CONNECT_HANDESHAKE_OK",
                        "__INFO__", 
                       "SSL::Handeshake... ok\n" },

                     { "SSL_CONNECT_HANDESHAKE_NOCIPHER_ERROR", 
                        "__INFO__", 
                       "SSL::Handeshake... ko\n"
                       "SSL::Handeshake::The client has not proposed any cipher suite ... ko\n\n" }, 

                     { "SSL_CONNECT_HANDESHAKE_CONNECT_ERROR",
                        "__INFO__", 
                       "SSL::Handeshake... ko\n"
                       "unknown error.\n" },  

                     { "SSL_CONNECT_HANDESHAKE_GET_VERSION_ERROR",
                        "__INFO__", 
                       "SSL::Handeshake::get version on server... ko\n\n" }, 

                     { "SSL_CONNECT_HANDESHAKE_UNKNOWN_ERROR",
                        "__INFO__", 
                       "SSL::Handeshake::Unknown error... ko\n\n" }, 



                     /*
                     .---------------------------------.
                     |                                 |
                     |    Authentication Server SSL    |
                     |                                 |
                     '---------------------------------' */
                     { "SSL_IDENTITYSERVER_OK", 
                        "__INFO__", 
                       "SSL::ServerAuthentication::Getting certificate server... done\n" 
                       "SSL::ServerAuthentication::Verify certificate... done\n"
                       "+The certificate received is valid.\n" }, 

                     { "SSL_IDENTITYSERVER_NOCERTIFICATE_ERROR",
                        "__INFO__",
                       "SSL::ServerAuthentication::Getting certificate server... fail\n"
                       "+I have not received any certificate from the server.\n\n" },

                     { "SSL_IDENTITYSERVER_IDENTITY_WARNING", 
                        "__INFO__", 
                       "SSL::ServerAuthentication::Getting certificate server... done\n" 
                       "SSL::ServerAuthentication::Verify certificate... fail\n"
                       "+The certificate received is *NOT* valid. *Warning* \n\n" }, 

                     { "SSL_IDENTITYSERVER_COMMONNAME_WARNING", 
                        "__INFO__", 
                       "SSL::ServerAuthentication::Getting certificate server... done\n"
                       "SSL::ServerAuthentication::Verify certificate... fail\n"
                       "+The certificate received from the server is not entitled to the sender. *Warning*\n\n" }, 

                     { "SSL_IDENTITYSERVER_UNKNOWN_ERROR", 
                        "__INFO__", 
                       "SSL::ServerAuthentication::Getting certificate server... done\n"
                       "SSL::ServerAuthentication::Verify certificate... fail\n"
                       "+Unknown error.\n\n" }, 



                     /*
                     .---------------------------------.
                     |                                 |
                     |    Verifica certificato X.509   |
                     |      crypto/x509/x509_vfy.h     | 
                     |                                 |
                     '---------------------------------' */

                     { "X509_V_OK",
                        "__INFO__", 
                       "X509_V_OK\n"
                       "The certificate was valid or no certificate was provided.\n"
                       "Use the SSL_get_peer_certificate function to determine whether the\n"
                       "certificate was provided or not.\n\n" }, 

                     { "X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT",
                        "__INFO__", 
                       "X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT\n"
                       "Unable to find the certificate for one of the certificate authorities\n"
                       "(CAs) in the signing hierarchy and that CA is not trusted by the local\n"
                       "application.\n\n" }, 

                     { "X509_V_ERR_UNABLE_TO_DECRYPT_CERT_SIGNATURE",
                        "__INFO__", 
                       "X509_V_ERR_UNABLE_TO_DECRYPT_CERT_SIGNATURE\n"
                       "Unable to decrypt the signature of the certificate.\n\n" }, 

                     { "X509_V_ERR_UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY",
                        "__INFO__", 
                       "X509_V_ERR_UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY\n"
                       "The public key in the certificate could not be read.\n\n" }, 

                     { "X509_V_ERR_CERT_SIGNATURE_FAILURE",
                        "__INFO__", 
                       "X509_V_ERR_CERT_SIGNATURE_FAILURE\n"
                       "The signature of the certificate is not valid.\n\n" }, 

                     { "X509_V_ERR_CERT_NOT_YET_VALID",
                        "__INFO__", 
                       "X509_V_ERR_CERT_NOT_YET_VALID\n"
                       "The certificate is not valid until a date in the future.\n\n" }, 

                     { "X509_V_ERR_CERT_HAS_EXPIRED",
                        "__INFO__", 
                       "X509_V_ERR_CERT_HAS_EXPIRED\n"
                       "The certificate has expired.\n\n" }, 

                     { "X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD",
                        "__INFO__", 
                       "X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD\n"
                       "There is a format error in the notBefore field of the certificate.\n\n" }, 

                     { "X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD",
                        "__INFO__", 
                       "X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD\n"
                       "There is a format error in the notAfter field of the certificate.\n\n" }, 

                     { "X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT",
                        "__INFO__", 
                       "X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT\n"
                       "The passed certificate is self-signed and the same certificate\n"
                       "cannot be found in the list of trusted certificates.\n\n" }, 

                     { "X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN",
                        "__INFO__", 
                       "X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN\n"
                       "A self-signed certificate exists in the certificate chain.\n"
                       "The certificate chain could be built up using the untrusted certificates,\n"
                       "but the root CA could not be found locally.\n\n" }, 

                     { "X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY",
                        "__INFO__", 
                       "X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY\n"
                       "The issuer certificate of a locally looked up certificate could\n"
                       "not be found. This normally means that the list of trusted certificates\n"
                       "is not complete.\n\n" }, 

                     { "X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE",
                        "__INFO__", 
                       "X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE\n"
                       "No signatures could be verified because the certificate\n"
                       "chain contains only one certificate, it is not self-signed,\n"
                       "and the issuer is not trusted.\n\n" }, 

                     { "X509_V_ERR_INVALID_CA",
                        "__INFO__", 
                       "X509_V_ERR_INVALID_CA\n"
                       "A CA certificate is not valid because it is not a CA or its extensions\n"
                       "are not consistent with the intended purpose.\n\n" }, 

                     { "X509_V_ERR_PATH_LENGTH_EXCEEDED",
                        "__INFO__", 
                       "X509_V_ERR_PATH_LENGTH_EXCEEDED\n"
                       "The basicConstraints pathlength parameter was exceeded.\n\n" }, 

                     { "X509_V_ERR_INVALID_PURPOSE",
                        "__INFO__", 
                       "X509_V_ERR_INVALID_PURPOSE\n"
                       "The certificate that was provided cannot be used for its intended purpose.\n\n" }, 

                     { "X509_V_ERR_CERT_UNTRUSTED",
                        "__INFO__", 
                       "X509_V_ERR_CERT_UNTRUSTED\n"
                       "The root CA is not marked as trusted for its intended purpose.\n\n" }, 

                     { "X509_V_ERR_CERT_REJECTED",
                        "__INFO__", 
                       "X509_V_ERR_CERT_REJECTED\n"
                       "The root CA is marked to reject the purpose specified.\n\n" }, 

                     { "X509_V_ERR_SUBJECT_ISSUER_MISMATCH",
                        "__INFO__", 
                       "X509_V_ERR_SUBJECT_ISSUER_MISMATCH\n"
                       "The issuer certificate was rejected because its subject name did not match\n"
                       "the issuer name of the current certificate.\n\n" }, 

                     { "X509_V_ERR_AKID_SKID_MISMATCH",
                        "__INFO__", 
                       "X509_V_ERR_AKID_SKID_MISMATCH\n"
                       "The issuer certificate was rejected because its subject key identifier\n"
                       "was present nand did not match the authority key identifier of the\n"
                       "current certificate.\n\n" }, 

                     { "X509_V_ERR_AKID_ISSUER_SERIAL_MISMATCH",
                        "__INFO__", 
                       "X509_V_ERR_AKID_ISSUER_SERIAL_MISMATCH\n"
                       "The issuer certificate was rejected because its issuer name\n"
                       "and serial number was present and did not match the authority\n"
                       "key identifier of the current certificate.\n\n" }, 

                     { "X509_V_ERR_KEYUSAGE_NO_CERTSIGN",
                        "__INFO__", 
                       "X509_V_ERR_KEYUSAGE_NO_CERTSIGN\n"
                       "The issuer certificate was rejected because its keyUsage extension does\n"
                       "not permit certificate signing.\n\n" }, 

                     { "X509_V_ERR_CERT_REVOKED",
                        "__INFO__", 
                       "X509_V_ERR_CERT_REVOKED\n"
                       "The certificate was revoked by the issuer.\n\n" }, 


                     /*
                     .---------------------------------.
                     |                                 |
                     |             Send SSL            |
                     |                                 |
                     '---------------------------------' */

                     { "SSL_SEND_OK"                     ,"__INFO__" , " done.\n"                                                  },
                     { "SSL_SEND_KO"                     ,"__INFO__" , " error.\n"                                                 },
                     { "SSL_SEND_CALCULATE_LEN_REQUEST"  ,"__INFO__" , "SSL::Calculate len request..."                             },
                     { "SSL_SEND_REQUEST"                ,"__INFO__" , "SSL::Send request to Server..."                            },


                     /*
                     .---------------------------------.
                     |                                 |
                     |           Receive SSL           |
                     |                                 |
                     '---------------------------------' */

                     { "SSL_RECEIVE_OK"                  ,"__INFO__" , " done.\n"                                                  },
                     { "SSL_RECEIVE_KO"                  ,"__INFO__" , " error.\n"                                                 },
                     { "SSL_RECEIVE_OPEN_DATASTORE"      ,"__INFO__" , "SSL::opening datastore..."                                 },
                     { "SSL_RECEIVE_OPEN_DATASTORE_KO"   ,"__INFO__" , "SSL::I cant to open the datastore.\n\n"                    },
                     { "SSL_RECEIVE_WRITE_DATASTORE"     ,"__INFO__" , "S|"                                                        },
                     { "SSL_RECEIVE_CLOSE_DATASTORE"     ,"__INFO__" , "SSL::closing datastore..."                                 },
                     { "SSL_RECEIVE_WAITING_RESPONSE"    ,"__INFO__" , "W"                                                         },
                     { "SSL_RECEIVE_RESPONSE"            ,"__INFO__" , "R"                                                         },
                     { "SSL_RECEIVE_RESPONSE_KO"         ,"__INFO__" , "I received a error from server.\n"                         },
                     { "SSL_RECEIVE_START"               ,"__INFO__" , "SSL::waiting, reading and storing response from server : " },


                     /*
                     .---------------------------------.
                     |                                 |
                     |         Disconnect SSL          |
                     |                                 |
                     '---------------------------------' */

                     { "SSL_DISCONNECT_OK",                   "__INFO__", "SSL::disconnect... Shutdown Session. done\n" },


                     { "SSL_DISCONNECT_SHUTDOWN_WAIT_ERROR",  "__INFO__", "SSL::disconnect... Shutdown Session. fail\n"
                                                                          "+Shutdown wait error.\n"    },

                     { "SSL_DISCONNECT_GENERIC_ERROR",        "__INFO__", "SSL::disconnect... Shutdown Session. fail\n"
                                                                          "+Shutdown generic error.\n" },

                     { "SSL_DISCONNECT_SHUTDOWN_ERROR",       "__INFO__", "SSL::disconnect... Shutdown Session. fail\n"
                                                                          "+Unknown error in disconnect function.\n" },

                     { "SSL_DISCONNECT_UNKNOWN_ERROR",        "__INFO__", "SSL::disconnect... Shutdown Session. fail\n"
                                                                          "+Unknown error.\n" },

                     { "SSL_DISCONNECT_SOCKET_DESTROY_OK",    "__INFO__", "SSL::disconnect... Socket Destroy. done\n" }, 
                                                                 
 

                    /*
                     .---------------------------------.
                     |                                 |
                     |           SSL Message           |
                     |          get_message()          |
                     |                                 |
                     '---------------------------------' */

                     { "SSL_ERROR_NONE",
                        "__INFO__", 
                       "SSL_ERROR_NONE\n"
                       "No error to report. This is set when the value of the ret parameter is greater than 0.\n\n" },

                     { "SSL_ERROR_SSL",
                        "__INFO__", 
                       "SSL_ERROR_SSL\n"
                       "An error occurred in the SSL library.\n\n" },

                     { "SSL_ERROR_WANT_READ",
                        "__INFO__", 
                       "SSL_ERROR_WANT_READ\n"
                       "Processing was not completed successfully because there was no data available\n"
                       "for reading, and the socket available for the SSL session is in nonblocking mode.\n"
                       "Try the function again at a later time.\n\n" },

                     { "SSL_ERROR_WANT_WRITE",
                        "__INFO__", 
                       "SSL_ERROR_WANT_WRITE\n"
                       "Processing was not completed successfully because the socket associated with the SSL\n"
                       "session is blocked from sending data. Try the function again at a later time.\n\n" },

                     { "SSL_ERROR_SYSCALL",
                        "__INFO__", 
                       "SSL_ERROR_SYSCALL\n"
                       "An I/O error occurred.\n"
                       "Issue the sock_errno function to determine the cause of the error.\n\n" },

                     { "SSL_ERROR_ZERO_RETURN",
                        "__INFO__", 
                       "SSL_ERROR_ZERO_RETURN\n"
                       "The remote application shut down the SSL connection normally. Issue the SSL_shutdown\n"
                       "function to shut down data flow for an SSL session.\n\n" },

                     { "SSL_ERROR_WANT_CONNECT",
                        "__INFO__", 
                       "SSL_ERROR_WANT_CONNECT\n"
                       "Processing was not completed successfully because the SSL session was in the process of\n"
                       "starting the session, but it has not completed yet.\n"
                       "Try the function again at a later time.\n\n" },

                     { "SSL_ERROR_UNKNOWN",
                        "__INFO__", 
                       "Unknown error.\n\n" },


                     { "END", 
                        __NONE__, 
                       "End of Message Table. *Warning* Message not found.\n" }

    }; 


