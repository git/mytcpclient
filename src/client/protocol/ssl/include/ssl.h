/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 

/*
.----------------.
| header OpenSSL |
'----------------' */
#include <openssl/ssl.h>
#include <openssl/err.h>


#define __SSL_LEN_BUFFER_RECEIVE__ 1024


/*
.-----------------------------------------------------. 
|                                                     |
|                    Crea il Contesto                 |
|                                                     |
'-----------------------------------------------------' */
struct tSSLCreateContext
{
       SSL_CTX    *ctx               ;
       long        idmsg             ;
       char       *msgDesc           ;
       int         return_code       ;
};
extern struct tSSLCreateContext 
      SSLCreateContext ( char *keystore, char *password, char *truststore, char *ciphersuite, char *sslversion, int debug_level );

/*
.-----------------------------------------------------.
|                                                     |
|                 Distruggi il Contesto               |
|                                                     |
'-----------------------------------------------------' */
void SSLDestroyContext ( SSL_CTX *ctx, int debug_level );


/*
.----------------------------------------------------.
|                                                    |
|                      Connessione                   |
|                                                    |
'----------------------------------------------------' */
struct tSSLConnect
{
       SSL_CTX     *ctx                                           ;
       SSL         *ssl                                           ;
       char        *ssl_version                                   ; 
       char         ssl_ciphersuite_client_list_proposals[2048]   ; 
       char         ssl_ciphersuite_client_list_proposals_num     ;
       char        *ssl_ciphersuite                               ; 
       int          ssl_ciphersuite_algsim_lenkey                 ; 
       char        *ssl_ciphersuite_algsim_sslversion             ; 
       char         ssl_cipher_noused[2048]                       ; 
       int          ssl_cipher_noused_num                         ;
       long         idmsg                                         ;
       char        *msgDesc                                       ;
       int          return_code                                   ;
};
extern struct tSSLConnect SSLConnect 
    (int sock, char *keystore, char *password, char *truststore, char *ciphersuite, char *sslversion, int debug_level);


/*
.----------------------------------------------------.
|                                                    |
|         Rimuove CipherSuite da un elenco           |
|                                                    |
'----------------------------------------------------' */
struct tSSLRemoveCipherSuite
{    
       int  ElencoCipherSuiteNum        ;
       char NewElencoCipherSuite [2048] ;  
       int  NewElencoCipherSuiteNum     ;
       int  return_code                 ;
};
extern struct tSSLRemoveCipherSuite SSLRemoveCipherSuite ( char ElencoCipherSuite[2048], char *cipherSuite ) ; 


/*
.----------------------------------------------------.
|                                                    |
|                    Disconnessione                  |
|                                                    |
'----------------------------------------------------' */
struct tSSLDisconnect
{
       long         idmsg            ;
       char        *msgDesc          ;
       int          return_code      ;
};
extern struct tSSLDisconnect SSLDisconnect (SSL_CTX *ctx, SSL *ssl, int debug_level);


/*
.----------------------------------------------------.
|                                                    |
|                 Get and Print Identity             |
|                                                    |
'----------------------------------------------------' */
int SSLPrintCertificateX509 ( long   x509_Version                     , 
                              long   x509_SerialNumber                , 
                              int    x509_SignatureType               , 
                              char   x509_IssuerName[256]             , 
                              char   x509_NotBefore[15]               , 
                              char   x509_NotAfter[15]                , 
                              char   x509_SubjectName[256]            , 
                              char   x509_SubjectName_commonname[256] , 
                              int    x509_CertiticateType              ); 

/*
.----------------------------------------------------.
|                                                    |
|                 SSLDoAuthentication                |
|                                                    |
'----------------------------------------------------' */
struct tSSLDoAuthentication
{
       long   x509_Version                     ; 
       long   x509_SerialNumber                ;
       int    x509_SignatureType               ;
       char   x509_IssuerName[256]             ;
       char   x509_NotBefore[15]               ;
       char   x509_NotAfter[15]                ;
       char   x509_SubjectName[256]            ;
       char   x509_SubjectName_commonname[256] ; 
       int    x509_CertificateType             ;
       long   idmsg                            ;
       char  *msgDesc                          ;
       int    return_code                      ;
};
extern struct tSSLDoAuthentication SSLDoAuthentication (SSL *ssl, char *host, int debug_level);


/*
.----------------------------------------------------.
|                                                    |
|                                                    |
|                                                    |
'----------------------------------------------------' */
void sigpipe_handle (int x);



/*
.----------------------------------------------------.
|                                                    |
|               Trasmissione sul CANALE              |
|                                                    |
|                                                    |
'----------------------------------------------------' */
struct tSSLSend 
{
       char *request     ;
       int   requestLen  ;
       long  idmsg       ;
       char *msgDesc     ;
       int   return_code ;
};
extern struct tSSLSend SSLSend (SSL *ssl, char *request, int debug_level);


/*
.----------------------------------------------------.
|                                                    |
|                 Ricezione dal CANALE               |
|                                                    |
|                                                    |
'----------------------------------------------------' */
struct tSSLReceive 
{
       char *response    ;
       int   responseLen ;
       long  idmsg       ;
       char *msgDesc     ;
       int   return_code ;
};
extern struct tSSLReceive SSLReceive (SSL *ssl, char *OutputFile, int debug_level); 

/*
.----------------------------------------------------.
|                                                    |
|                    Client minimale                 |
|             (connessione/disconnessione)           |
|                                                    |
'----------------------------------------------------' */
extern struct tClientConnect SSLClientConnect ( struct tClientConnectParameter rClientConnectParameter );


// __EOF__


