/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/*
.=====================================================================.
|                                                                     |
    Libreria         : client.protocol.SSL 

    Description      :


    Elenco funzioni

       funzione                 descrizione
    ------------------------------------------------------------
    1. SSLRemoveCipherSuite     Remove a ciphersuite from ciphersuite array in openssl style. 
    2. SSLCreateContext         Create new ssl context 
    3. SSLDestroyContext        Destroy the ssl context 
    4. SSLConnect               Open SSL Sessione 
    5. SSLDoAuthentication      Certificate Verify  
    6. SSLPrintCertificateX509  Print the X509 Certificate info 
    7. SSLSend                  Send string to server
    8. SSLReceive               Receive string from server
    9. SSLDisconnect            Close SSL Session 
   10. SSLClientConnect         Minimal SSL Client
|                                                                     |
'=====================================================================' */ 
/*
.--------------.
| headers LIBC |
'--------------' */
#include <signal.h>
#include <sys/stat.h>
#include <string.h>

/*
.-------------------------.
| header common           |
'-------------------------' */
#include "client.h"
#include "output.h"

/*
.-----------------.
| header protolli |
'-----------------' */
#include "tcp.h"

/*
.-----------------.
| header locale   |
'-----------------' */
#include "ssl.h"
#include "ssl_messages.h"

/*
.------------------------------------------.
| OpenSSL deve ESSERE come minimo la 0.9.6 |
'------------------------------------------' */
#ifndef ALLOW_OLD_VERSIONS
#if (OPENSSL_VERSION_NUMBER < 0x00905100L)
#error "Devi usare OpenSSL 0.9.6 o superiore..."
#endif
#endif


/*
.-------------------------------------------------------------------------------------------.
|                                                                                           |
|  Funzione   : SSLRemoveCipherSuite                                                        |
|                                                                                           |
|  Descrizione: Elimina da un elenco di ciphersuite un'elemento, restituendo il nuovo       |
|               elenco, ed il numero di elementi. La funziona restituisce -1 nel caso in    |
|               l'elemento da rimuovere non è stato trovato                                 |
|                                                                                           |
|  return-code:  0  OK                                                                      |
|               -1  Non trovato nessun elemento da rimuovere                                |
|                                                                                           |
|                                                                                           |
|                                                                                           |
|                                                                                           |
|                                                                                           |
'-------------------------------------------------------------------------------------------' */
extern struct tSSLRemoveCipherSuite SSLRemoveCipherSuite ( char ElencoCipherSuite[2048], char *cipherSuite )
{
    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tSSLRemoveCipherSuite  rSSLRemoveCipherSuite  ;
    struct tSSLRemoveCipherSuite *pSSLRemoveCipherSuite  ;
    /*
    .------------------------------------------.
    | Allocazione SPAZIO                       |
    '------------------------------------------' */
    pSSLRemoveCipherSuite = malloc(sizeof(struct tSSLRemoveCipherSuite));

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pSSLRemoveCipherSuite = &rSSLRemoveCipherSuite;

    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rSSLRemoveCipherSuite,0,sizeof(rSSLRemoveCipherSuite));
    rSSLRemoveCipherSuite.ElencoCipherSuiteNum    = 0 ; 
    rSSLRemoveCipherSuite.NewElencoCipherSuite[0] = 0 ; 
    rSSLRemoveCipherSuite.NewElencoCipherSuiteNum = 0 ; 
    rSSLRemoveCipherSuite.return_code = 0;



    int indiceElenco = 0; 
    int indiceCipherSuiteBuffer = 0; 
    char cipherSuiteBuffer[256]; 

    while ( ElencoCipherSuite[indiceElenco] != 0 )
    {
            /*
            .-----------------------------------------------------------------------------.
            | Legge l'elenco delle ciphersuite proposte dal client al server nel          |
            | precedente handshake, appena trovata un cipher successiva esce dal while    |
            | interno per analizzarla.                                                    |
            '-----------------------------------------------------------------------------' */
            indiceCipherSuiteBuffer = 0;
            cipherSuiteBuffer[indiceCipherSuiteBuffer] = 0; 
            while ( ElencoCipherSuite[indiceElenco] != 0 &&  ElencoCipherSuite[indiceElenco] != ':' ) 
            { 
                    cipherSuiteBuffer[indiceCipherSuiteBuffer]   = ElencoCipherSuite[indiceElenco] ; 
                    cipherSuiteBuffer[indiceCipherSuiteBuffer+1] = 0 ; 
                    indiceCipherSuiteBuffer++; 
                    indiceElenco++;
            }

            /*
            .----------------------------------------------------------------------------.
            | E' stato estratta dall'elenco delle ciphers proposte dal client nel        |
            | precedente handshake la cipher successiva o la prima dell'elenco.          |
            | A questo punto confronto la cipher estratta con quella utilizzata per      |
            | l'handshake precedente dal server e solo se quella estratta è differente   |
            | cioè se risulta non utilizzata viene salvata nel buffer delle cipher non   |
            | ancora utilizzate.                                                         |
            '----------------------------------------------------------------------------' */
            rSSLRemoveCipherSuite.ElencoCipherSuiteNum++;
            if ( strcmp ( cipherSuiteBuffer, cipherSuite ) != 0 )         
            {
                 if ( rSSLRemoveCipherSuite.NewElencoCipherSuiteNum > 0 )
                      strcat(rSSLRemoveCipherSuite.NewElencoCipherSuite, ":\0"); 

                 strcat(rSSLRemoveCipherSuite.NewElencoCipherSuite, cipherSuiteBuffer);
                 rSSLRemoveCipherSuite.NewElencoCipherSuiteNum++;
            };

            /*
            .---------------------------------------------------------------------------.
            | Se il buffer che contiene le ciphersuite proposte da client nel           |
            | precedente handshake non è terminato, quindi è posizionato sul carattere  |
            | di separazione, avanzo di 1 per posizionarmi sulla successiva cipher      |
            | presente in elenco.                                                       |
            '---------------------------------------------------------------------------' */ 
            if ( ElencoCipherSuite[indiceElenco] == ':' ) 
                 indiceElenco++; 
    };      

    /*
    .-----------------------------------------------------------------------.
    |                                                                       |
    +-----------------------------------------------------------------------+  
    |                                                                       |
    |                                                                       |
    |                                                                       |
    |                                                                       |
    '-----------------------------------------------------------------------' */ 
    int DEBUG = 0 ;
    if ( DEBUG == 1 )
    {
         printf ("\nOriginal CipherSuite Array : [%d][%s]", rSSLRemoveCipherSuite.ElencoCipherSuiteNum, ElencoCipherSuite );  
         printf ("\nCipher removed             : [%s]    ", cipherSuite);
         printf ("\nNew CipherSuite Array      : [%d][%s]", rSSLRemoveCipherSuite.NewElencoCipherSuiteNum, rSSLRemoveCipherSuite.NewElencoCipherSuite );
         printf ("\n");
    };

    return rSSLRemoveCipherSuite ;
}



/*
.-------------------------------------------------------------------------------------------.
|                                                                                           |
|  Funzione    : SSLCreateContext                                                           |
|                                                                                           |
|  Descrizione : Create new SSL context (*).                                                |
|                                                                                           |
|  Return-Code :  0   Ok                                                                    |
|                -1   Inizializzazione SSL non riuscita.                                    |
|                -2   Versione SSL/TLS non valida.                                          |
|                -3   Impostazione CipherSuite non riuscita.                                |
|                -4   Certificato non valido o formato non supportato.                      |
|                -5   Chiave Privata non valida o decrypt fallito                           |
|                -6   Truststore non valido o formato non supportato.                       |
|                                                                                           |
|                                                                                           |
|  (*) Il contesto SSL è una struttura di memoria OpenSSL che contiene l'identità           |
|      (quindi privata, certificato) e truststore. La creazione del contesto serve          |
|      a verificare che i documenti siano tutti Ok, per essere poi passate alla funzione    |
|      di connect.                                                                          |
|                                                                                           |
|                                                                                           |
'-------------------------------------------------------------------------------------------'*/
extern struct tSSLCreateContext 
SSLCreateContext ( char *keystore, char *password, char *truststore, char *ciphersuite, char *sslversion, int debug_level )
{
    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , "", ""                                  }
    };

    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tSSLCreateContext    rSSLCreateContext;
    struct tSSLCreateContext   *pSSLCreateContext;

    /*
    .------------------------------------.
    | Allocazione Spazio                 |
    '------------------------------------' */
    pSSLCreateContext     = malloc(sizeof(struct tSSLCreateContext));
    rSSLCreateContext.ctx = malloc(1024); 

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pSSLCreateContext = &rSSLCreateContext;

    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rSSLCreateContext,0,sizeof(rSSLCreateContext));
    rSSLCreateContext.return_code = 0; 


    /*
    .-----------------------------.
    | Visualizza BANNER di START  | 
    '-----------------------------' */
    PrintMessage(debug_level, SSL_TABMSG, "SSL_CTX_CREATE", TabVariabiliEMPTY, 0, 0, 0);

    /*
    .------------------------------------------------------------------------.
    | PASSO 1. Inizializzazione SSL.                                         |
    +------------------------------------------------------------------------+
    | Operazione eseguita con successo se la funzione ritorna il valore 1.   | 
    | Non ho capito bene a cosa serve questa funzione, forse alloca spazio   |
    | oppure verifica la presenza delle api di openssl o la versione.        |
    | Da approfondire l'argomento.                                           |
    '------------------------------------------------------------------------' */
    if ( debug_level == __INFO__ )
         printf("+Initialize SSL Library.\n");

    int rSSL_library_init = SSL_library_init();
    if (rSSL_library_init != 1) 
    { 
        PrintMessage(debug_level, SSL_TABMSG, "SSL_CTX__CREATE_ERROR", TabVariabiliEMPTY, 0, 0, 0);
        rSSLCreateContext.idmsg   = 0 ; 
        rSSLCreateContext.msgDesc = malloc(strlen("SSL.CTX.CREARE.ERROR"));
        rSSLCreateContext.msgDesc = "SSL.CTX.CREATE.ERROR";
        rSSLCreateContext.return_code = -1 ;          
        goto SSLCreateContext_ERROR ; 
    }; 

    /*
    .-----------------------------------------------------------------------.
    | OpenSSL :: Gestione degli errori                                      |
    +-----------------------------------------------------------------------+
    | Carica le stringhe di errore. Visualizza l'errore nel dispositivo     |
    | standard di errore. Al momento questa questione degli errori non è    |
    | utilizzata in maniera tale da personalizzare l'output verso           |
    | l'utente. Argomento da approfondire.                                  |
    '-----------------------------------------------------------------------' */ 
    /*
    |
    | BIO *bio_err     ;
    |
    | SSL_load_error_strings();
    |  
    | // An error write context 
    | bio_err = BIO_new_fp ( stderr, BIO_NOCLOSE );
    | if ( bio_err == NULL ) 
    | { 
    |      // ERRORE  
    | }; 
    |
    */

    /*
    .-----------------------------------------------------------------------.
    | Set up a SIGPIPE hendler                                              |
    +-----------------------------------------------------------------------+
    | Approfondire la gestione dei segnali.                                 |
    '-----------------------------------------------------------------------' */
    /*
    |
    | signal ( SIGPIPE, sigpipe_handle );
    |
    */

 
    /*
    .----------------------------------------------------------------------.
    | PASSO 2. Crea il contesto per la versione di SSL selezionata.        |
    +----------------------------------------------------------------------+
    | Le versioni si SSL supportate sono 2, 3 e TLS versione 1. Se non è   |
    | specificata viene utilizzata la versione TLS versione 1 di default.  |
    | A seconda della versione prescelta o selezionata di default è        |
    | necessario invocare una funzione differente. Tali funzioni allocano  |
    | una struttura in memoria differente per ogni versione di SSL/TLS     |
    | da utilizzare (questo è il motivo per cui esistono più funzioni)     |
    | il cui puntatore viene salvato in una variabile di appoggio          |
    | denominata meth. Tale puntatore alla struttura di contesto SSL viene |
    | passato alla funzione SSL_CTX_new che crea il contesto e ritorna il  |
    | puntatore che viene salvato nella struttura di ritorno della         |
    | funzione oltre ad essere utilizzato di seguito quando si farà        |   
    | farà riferimento al contesto stesso.                                 |  
    '----------------------------------------------------------------------' */
    if ( debug_level == __INFO__ )
         printf("+Selected SSLVersion.\n");

    if ( sslversion                       != NULL && 
         strcmp ( sslversion, "SSLv2"   ) != 0    && 
         strcmp ( sslversion, "SSLv3"   ) != 0    && 
         strcmp ( sslversion, "TLSv1"   ) != 0    &&
         strcmp ( sslversion, "TLSv1_1" ) != 0    &&
         strcmp ( sslversion, "TLSv1_2" ) ) 
    {
         PrintMessage(debug_level, SSL_TABMSG, "SSL_CTX_CREATE_SSLVERSION_ERROR", TabVariabiliEMPTY, 0, 0, 0);
         rSSLCreateContext.idmsg   = 0 ;
         rSSLCreateContext.msgDesc = malloc(strlen("SSL.CTX.CREATE.SSLVERSION.ERROR"));
         rSSLCreateContext.msgDesc = "SSL.CTX.CREATE.SSLVERSION.ERROR";
         rSSLCreateContext.return_code = -2 ;
         goto SSLCreateContext_ERROR ;
    };

    SSL_METHOD *meth;

    meth = malloc(1024);

    /* set default sslversion */
    if ( sslversion == NULL )
    {
         meth = TLSv1_2_method() ;  
    }
    else
    {
/*
.------------------------------------------.
| Abilita SSL2 ed SSL3 solo per versioni   | 
| openssl precedenti alla 1.0.2o           |
'------------------------------------------' */
//#if (OPENSSL_VERSION_NUMBER < 0x100020ffL )
//         if ( strcmp ( sslversion, "SSLv2"   ) == 0 ) meth = SSLv2_method()   ; // deprecati da openssl 1.1.0 
         if ( strcmp ( sslversion, "SSLv3"   ) == 0 ) meth = SSLv3_method()   ; // deprecati da openssl 1.1.0 
         if ( strcmp ( sslversion, "TLSv1"   ) == 0 ) meth = SSLv23_method()  ; // deprecati da openssl 1.1.0
//#else
/*
.------------------------------------------.
| Abilita TLS1.2 TLS1.2 solo per versioni  |
| openssl 1.0.2o o superiori.              |
'------------------------------------------' */ 
         if ( strcmp ( sslversion, "TLSv1_1" ) == 0 ) meth = TLSv1_1_method() ; // attivi da openssl 1.1.0 NEWWWWWWWWWWWWWWWWWWWWWWWWWWWWW 
         if ( strcmp ( sslversion, "TLSv1_2" ) == 0 ) meth = TLSv1_2_method() ; // attivi da openssl 1.1.0 NEWWWWWWWWWWWWWWWWWWWWWWWWWWWWW 
//#endif
    };

    if ( debug_level == __INFO__ )
         printf("+Create SSL Context\n");

    rSSLCreateContext.ctx = SSL_CTX_new ( meth );


    /*
    .----------------------------------------------------------------------.
    | PASSO 3. Carica nel contesto le Ciphersuite.                         |
    +----------------------------------------------------------------------+
    | Il client può specificare solamente alcune ciphersuite disponibili   |
    | ed implementate nella versione di SSL selezionato. Se non vengono    |
    | specificate valgono tutte, cioè il client le propone tutte al        |
    | server e questo pi sceglierà quella preferita.                       |
    | La funzione che solge quetso lavoro ha bisogno chiaramente del       |
    | puntatore al contesto e dell'elenco delle ciphersuite secondo la     |
    | terminologia standard separate l'una dall'altra tramite un carattere |
    | di separazione, che è il carattere dei due punti (:).                |
    | Se la funziona trova una sola ciphersuite non valida fallisce.       |
    '----------------------------------------------------------------------' */ 
    if ( ciphersuite != NULL)  
    {
         if ( debug_level == __INFO__ )      
              printf("+add CipherSuite [%s].\n", ciphersuite);

         if ( SSL_CTX_set_cipher_list ( rSSLCreateContext.ctx, ciphersuite ) <= 0) 
         {
              PrintMessage(debug_level, SSL_TABMSG, "SSL_CTX_CREATE_CIPHERSUITE_ERROR", TabVariabiliEMPTY, 0, 0, 0);
              rSSLCreateContext.idmsg   = 0 ;
              rSSLCreateContext.msgDesc = malloc(strlen("SSL.CTX.CREATE.CIPHERSUITE.ERROR"));
              rSSLCreateContext.msgDesc = "SSL.CTX.CREATE.CIPHERSUITE.ERROR";
              rSSLCreateContext.return_code = -3 ;
              goto SSLCreateContext_ERROR ;
         };
    };

    if ( ciphersuite == NULL )
         if ( debug_level == __INFO__ )
              printf("+add CipherSuite [none]. Setting Default.\n");

    /*
    .---------------------------------------------------------------------.
    | PASSO 4. Carica la nostro Certificato.                              |
    +---------------------------------------------------------------------+
    | Carica il certificato nel nostro contesto tramite una funzione      |
    | specifica cui è necessario passare puntatore al contesto e          |
    | file che lo contiene nel formato PEM, unico formato al momento      |
    | dalla funzione. Se il file contiene altro o il certificato non      |
    | valido o in formato diverso l'acquisizione del certifciato          |
    | chiaramente fallisce. Si può anche non passare il certificato in    |
    | quel caso il certificato non verrà passato al server durante        |
    | l'handeshake. Nel caso di mutua autenticazione cioè se il server    |
    | si vuole accertare dell'identità del client l'handeshake poi        |
    | fallisce e la sessione ssl non ha inizio.                           |
    '---------------------------------------------------------------------' */
    if ( keystore != NULL )
    {
         if ( debug_level == __INFO__ )      
              printf("+add Cerificate Client [file: %s].\n", keystore);

         /*
         .----------------------------------------------.
         | Verifica la presenza del keystore file       |
         '----------------------------------------------' */
         FILE *fp ;
         fp = fopen(keystore, "r");
         if (!fp)
         {
             PrintMessage(debug_level, SSL_TABMSG, "SSL_CTX_CREATE_CERT_ERROR", TabVariabiliEMPTY, 0, 0, 0);
             rSSLCreateContext.idmsg   = 0 ; 
             rSSLCreateContext.msgDesc = malloc(strlen("SSL.CTX.CREATE.CERT.ERROR"));
             rSSLCreateContext.msgDesc = "SSL.CTX.CREATE.CERT.ERROR";
             rSSLCreateContext.return_code = -4 ;
             goto SSLCreateContext_ERROR ;
         };
         fclose(fp);

         /*
         .----------------------------------------------.
         | Prova a caricare nel contesto il certificato |
         '----------------------------------------------' */
         if(!(SSL_CTX_use_certificate_chain_file(rSSLCreateContext.ctx, keystore))) 
         { 
             PrintMessage(debug_level, SSL_TABMSG, "SSL_CTX_CREATE_CERT_ERROR", TabVariabiliEMPTY, 0, 0, 0);
             rSSLCreateContext.idmsg   = 0 ;
             rSSLCreateContext.msgDesc = malloc(strlen("SSL.CTX.CREATE.CERT.ERROR"));
             rSSLCreateContext.msgDesc = "SSL.CTX.CREATE.CERT.ERROR";
             rSSLCreateContext.return_code = -4 ; 
             goto SSLCreateContext_ERROR ; 
         } 
    };

    if ( keystore == NULL )
         if ( debug_level == __INFO__ )
              printf("+add Certificate Client [file: missing]. No mutual authentication *Warning*\n");


    /*
    .--------------------------------------------------------------------.
    | PASSO 5. Carica la nostra Chiave Privata                           |
    +--------------------------------------------------------------------+
    | Carica la chiave privata nel contesto, questa deve essere          |
    | memorizzata in un keystore di tipo PEM, altri formati formati al   |
    | momento non sono supportati. Alla funzione viene passato il        |
    | puntatore al contesto SSL, il tipo di file impostato a PEM come    |
    | detto ed il file che contiene la chiave privata. Nel caso in cui   |
    | precedentemente è stato memorizzato la password nel contesto la    |
    | funzione cerca di utilizzarla per il decrypt della chiave privata  |
    | Se la password non viene passata nel contesto la funzione da per   |
    | scontato che la chiave privata sia stata memorizzata in chiaro.    |
    | Il caricamento può fallire per vari motivi: password errata quindi |
    | decrypt fallito o chiave privata non corrispondente al certificato | 
    '--------------------------------------------------------------------' */
    if ( keystore != NULL )
    {
         if ( debug_level == __INFO__ )
              printf("+add Private Key Client [file: %s].\n", keystore);

         /*
         .----------------------------------------------.
         | Verifica la presenza del keystore file       |
         '----------------------------------------------' */
         FILE *fp ; 
         fp = fopen(keystore, "r");
         if (!fp)
         {
              PrintMessage(debug_level, SSL_TABMSG, "SSL_CTX_CREATE_KEY_ERROR", TabVariabiliEMPTY, 0, 0, 0);
              rSSLCreateContext.idmsg   = 0 ; 
              rSSLCreateContext.msgDesc = malloc(strlen("SSL.CTX.CREATE.KEY.ERROR"));
              rSSLCreateContext.msgDesc = "SSL.CTX.CREATE.KEY.ERROR";
              rSSLCreateContext.return_code = -5 ;
              goto SSLCreateContext_ERROR ; 
         };
         fclose(fp);

         /*
         .----------------------------------------------.
         | Carica la chiave privata nel contesto        |
         '----------------------------------------------' */
         if(!(SSL_CTX_use_PrivateKey_file(rSSLCreateContext.ctx, keystore, SSL_FILETYPE_PEM))) 
         { 
              PrintMessage(debug_level, SSL_TABMSG, "SSL_CTX_CREATE_KEY_ERROR", TabVariabiliEMPTY, 0, 0, 0);
              rSSLCreateContext.idmsg   = 0;
              rSSLCreateContext.msgDesc = malloc(strlen("SSL.CTX.CREATE.KEY.ERROR"));
              rSSLCreateContext.msgDesc = "SSL.CTX.CREATE.KEY.ERROR";
              rSSLCreateContext.return_code = -5 ;
              goto SSLCreateContext_ERROR ; 
         } 
    }; 

    /*
    .---------------------------------------------------------------------.
    | PASSO 6. Password per il decrypt della chiave privata.              |
    +---------------------------------------------------------------------+
    | Specifica la password da utilizzare per il decrypt della chiave     |
    | privata. La funzione non prevede return code, non fa altro che      |
    | memorizzare la password nel contesto. Se la password è errata, cioè |
    | se non riesce ad effettuare il decrypt della chiave privata il      |
    | problema sarà evidente solamente durante l'acquisizione nel         |
    | contesto di quest'ultima.                                           |
    '---------------------------------------------------------------------' */
    if ( keystore != NULL && password != NULL )
    {
         if ( debug_level == __INFO__ )
              printf("+add Password Decrypt Private Key Client.\n");
         
         SSL_CTX_set_default_passwd_cb_userdata(rSSLCreateContext.ctx, password);
    };

    /*
    .--------------------------------------------------------------------.
    | PASSO 7. Carica le CA presenti nel nostro Truststore.              |
    +--------------------------------------------------------------------+
    | Viene caricato nel contesto il truststore, al momento è possibile  |
    | acquisire un'unica CA quindi catene di CA legate tra loro non sono |
    | supportate.                                                        | 
    |                                                                    |
    |                                                                    |
    '--------------------------------------------------------------------' */
    if ( truststore != NULL )
    {
         if ( debug_level == __INFO__ )
              printf("+add Truststore CA [file: %s].\n", truststore);

         /*
         .---------------------------------------------------------.
         | PASSO 7.1. check presenza e accesso al truststore       |
         '---------------------------------------------------------' */
         FILE *fp ;
         fp = fopen(truststore, "r");
         if (!fp)
         {
             PrintMessage(debug_level, SSL_TABMSG, "SSL_CTX_CREATE_TRUSTSTORE_ERROR", TabVariabiliEMPTY, 0, 0, 0);
             rSSLCreateContext.idmsg   = 0 ;
             rSSLCreateContext.msgDesc = malloc(strlen("SSL.CTX.CREATE.TRUSTSTORE.ERROR"));
             rSSLCreateContext.msgDesc = "SSL.CTX.CREATE.TRUSTSTORE.ERROR";
             rSSLCreateContext.return_code = -6 ;
             goto SSLCreateContext_ERROR ;
         };
         fclose(fp);

         /*
         .---------------------------------------------------------.
         | PASSO 7.2. check tipologia file (regolare, directory)   |
         '---------------------------------------------------------' */
         struct stat iTruststore ;
         char *CAfile, *CApath ;

         if (lstat(truststore, &iTruststore) < 0) { 
             PrintMessage(debug_level, SSL_TABMSG, "SSL_CTX_CREATE_TRUSTSTORE_ERROR", TabVariabiliEMPTY, 0, 0, 0);
             rSSLCreateContext.idmsg   = 0 ;
             rSSLCreateContext.msgDesc = malloc(strlen("SSL.CTX.CREATE.TRUSTSTORE.ERROR"));
             rSSLCreateContext.msgDesc = "SSL.CTX.CREATE.TRUSTSTORE.ERROR";
             rSSLCreateContext.return_code = -6 ;
             goto SSLCreateContext_ERROR ; };

         if (S_ISREG(iTruststore.st_mode)) {
             /* if ( debug_level == __INFO__ ) printf("+The truststore is a regolar file.\n"); */ 
             CApath = 0 ;
             CAfile = truststore ; };

         if (S_ISLNK(iTruststore.st_mode)) {
             /* if ( debug_level == __INFO__ ) printf("+The truststore is a simbolic link.\n"); */ 
             CApath = 0 ;
             CAfile = truststore ; };

         if (S_ISDIR(iTruststore.st_mode)) {
             /* if ( debug_level == __INFO__ ) printf("+The truststore is a directory.\n"); */ 
             CApath = truststore ;
             CAfile = 0 ; };

         if ((!S_ISREG(iTruststore.st_mode)) && (!S_ISDIR(iTruststore.st_mode)) && (!S_ISLNK(iTruststore.st_mode))) { 
             PrintMessage(debug_level, SSL_TABMSG, "SSL_CTX_CREATE_TRUSTSTORE_ERROR", TabVariabiliEMPTY, 0, 0, 0);
             rSSLCreateContext.idmsg   = 0 ;
             rSSLCreateContext.msgDesc = malloc(strlen("SSL.CTX.CREATE.TRUSTSTORE.ERROR"));
             rSSLCreateContext.msgDesc = "SSL.CTX.CREATE.TRUSTSTORE.ERROR";
             rSSLCreateContext.return_code = -6 ;
             goto SSLCreateContext_ERROR ; };

         /*
         .--------------------------------------------------------.
         | PASSO 7.3. Specifica la posizione dell CA              |
         '--------------------------------------------------------' */

         /*
              ATTENZIONE!!!! Forse il metodo SSL_CTX_load_verify_locations
              funziona solo con SSLv3/TLSv1 perchè sulla stessa connessione
              con SSLv3/TLSv1 trova il certificato CA e con la versione SSLv2
              dice che non trova il certificato dell'Issuer ricevuto dal
              certificato server.
         */

         /*
         if ( debug_level == __INFO__ )
              printf("+Truststore Verify Location: CApath [%s] - CAfile [%s]\n", CApath, CAfile);
         */

         if(!(SSL_CTX_load_verify_locations(rSSLCreateContext.ctx, CAfile, CApath))) 
         { 
             PrintMessage(debug_level, SSL_TABMSG, "SSL_CTX_CREATE_TRUSTSTORE_ERROR", TabVariabiliEMPTY, 0, 0, 0);
             rSSLCreateContext.idmsg   = rSSLCreateContext.return_code;
             rSSLCreateContext.msgDesc = malloc(strlen("SSL.CTX.CREATE.TRUSTSTORE.ERROR"));
             rSSLCreateContext.msgDesc = "SSL.CTX.CREATE.TRUSTSTORE.ERROR";
             rSSLCreateContext.return_code = -6 ;
             goto SSLCreateContext_ERROR; 
         }; 
    };

    if ( truststore == NULL )
         if ( debug_level == __INFO__ )
              printf("+add Truststore CA [file: missing]. No Authentication Server *Warning*\n");


SSLCreateContext_DONE:

    PrintMessage(debug_level, SSL_TABMSG, "SSL_CTX_CREATE_OK", TabVariabiliEMPTY, 0, 0, 0);

SSLCreateContext_ERROR:

    return rSSLCreateContext;
}



/*
.-------------------------------------------------------------------------------------------.
|                                                                                           |
|  Funzione    : SSLDestroyContext                                                          | 
|                                                                                           |
|  Descrizione : Rilascia la memoria in cui è contenuto il contesto ssl con relativi        |
|                vi parametri. Alla funzione va passato il                                  |
|                puntatore al contesto e non ritorna nulla [approfond].                     |
|                                                                                           |
|  Return-Code : Nessuno                                                                    |
|                                                                                           |
|                                                                                           |
|                                                                                           |
|                                                                                           |
|                                                                                           |
|                                                                                           |
|                                                                                           |
'-------------------------------------------------------------------------------------------'*/
void SSLDestroyContext (SSL_CTX *ctx, int debug_level)
{
    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] = {
            { "EOT"                    , "", ""                                  }
    };

    PrintMessage(debug_level, SSL_TABMSG, "SSL_CTX_DESTROY_OK", TabVariabiliEMPTY, 0, 0, 0);

    SSL_CTX_free(ctx);
}



/*
.-------------------------------------------------------------------------------------------.
|                                                                                           |
|  Funzione    : SSLConnect                                                                 |
|                                                                                           |
|  Descrizione : Effettua la connessione SSL: handeshake + Open Session                     |
|                                                                                           |
|  Return-Code :  0  ok                                                                     |
|                -1  create context error.                                                  |
|                -2  create socket error.                                                   |  
|                -3  get version ssl from context error.                                    | 
|                -4  get ciphersuite list from context error.                               | 
|                -5  handeshake error.                                                      | 
|                                                                                           |
|                                                                                           |
|                                                                                           |
|                                                                                           |
'-------------------------------------------------------------------------------------------'*/
extern struct tSSLConnect SSLConnect (int sock, char *keystore, char *password, char *truststore, char *ciphersuite, char *sslversion, int debug_level)
{
    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , "", ""                                  }
    };

    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tSSLCreateContext      rSSLCreateContext ;
    struct tSSLCreateContext     *pSSLCreateContext ;

    struct tSSLRemoveCipherSuite  rSSLRemoveCipherSuite ; 
    struct tSSLRemoveCipherSuite *pSSLRemoveCipherSuite ;

    struct tSSLConnect            rSSLConnect ;
    struct tSSLConnect            *pSSLConnect ;

    /*
    .------------------------------------------.
    | Allocazione SPAZIO                       |
    '------------------------------------------' */
    pSSLCreateContext     = malloc(sizeof(struct tSSLCreateContext));
    pSSLRemoveCipherSuite = malloc(sizeof(struct tSSLRemoveCipherSuite));
    pSSLConnect           = malloc(sizeof(struct tSSLConnect));

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pSSLCreateContext     = &rSSLCreateContext;
    pSSLRemoveCipherSuite = &rSSLRemoveCipherSuite;
    pSSLConnect           = &rSSLConnect;
    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rSSLCreateContext,0,sizeof(rSSLCreateContext));
    memset((void*)&rSSLRemoveCipherSuite,0,sizeof(rSSLRemoveCipherSuite));
    memset((void*)&rSSLConnect,0,sizeof(rSSLConnect));
    rSSLCreateContext.return_code     = 0 ;
    rSSLRemoveCipherSuite.return_code = 0 ;
    rSSLConnect.return_code           = 0 ;


    /*
    .------------------------------------------------------------------------.
    | PASSO 1. Crea il contesto SSL.                                         |
    +------------------------------------------------------------------------+
    |                                                                        |
    |                                                                        | 
    |                                                                        |
    |                                                                        |
    |                                                                        |
    |                                                                        |
    '------------------------------------------------------------------------' */
    rSSLCreateContext.ctx = malloc(1024);
    rSSLCreateContext = SSLCreateContext ( keystore, password, truststore, ciphersuite, sslversion, debug_level );
    rSSLConnect.idmsg   = rSSLCreateContext.idmsg ; 
    rSSLConnect.msgDesc = rSSLCreateContext.msgDesc ; 
    if (rSSLCreateContext.return_code != 0) { 
        rSSLConnect.return_code = -1 ; 
        goto SSLConnect_ERROR ; 
    };

    /*
    .------------------------------------------------------------------------.
    | PASSO 2. Create SOCKET SSL                                             |
    +------------------------------------------------------------------------+
    | Crea il SOCKET per la connessione SSL.                                 |
    |                                                                        |
    |                                                                        |   
    |                                                                        |
    |                                                                        |
    |                                                                        |
    '------------------------------------------------------------------------' */
    rSSLConnect.ssl = SSL_new ( rSSLCreateContext.ctx );
    if (!rSSLConnect.ssl) 
    {
        PrintMessage(debug_level, SSL_TABMSG, "SSL_CONNECT_SOCKET_CREATE_KO", TabVariabiliEMPTY, 0, 0, 0);
        rSSLConnect.idmsg   = 0 ;
        rSSLConnect.msgDesc = malloc(strlen("SSL.CONNECT.SOCKET.CREATE.ERROR"));
        rSSLConnect.msgDesc = "SSL.CONNECT.SOCKET.CREATE.ERROR";
        rSSLConnect.return_code = -2;
        goto SSLConnect_ERROR ;
    }
    PrintMessage(debug_level, SSL_TABMSG, "SSL_CONNECT_SOCKET_CREATE_OK", TabVariabiliEMPTY, 0, 0, 0);

    /*
    .------------------------------------------------------------------------.
    | PASSO 3. Versione SSL/TLS proposta al Server.                          |
    +------------------------------------------------------------------------+
    | Preleva la versione SSL che si andrà a proporre al server. Se la       |
    | funzione SSL_get_version torna ZERO significa che non riesce a         |
    | recuperarla dal contesto. Questo è un errore fatale, cioè non          |
    | dovrebbe mai capitare se ho capito bene quel che fa, se il contesto    |
    | è stato creato correttamente e la socket è stata creata correttamente  |
    | non c'è alcuna ragione per andar male.                                 |
    '------------------------------------------------------------------------' */
    rSSLConnect.ssl_version = malloc(100);
    strcpy ( rSSLConnect.ssl_version, SSL_get_version(rSSLConnect.ssl) );
    if (rSSLConnect.ssl_version == 0)
    {
        PrintMessage(debug_level, SSL_TABMSG, "SSL_CONNECT_SOCKETCTX_GETVERSION_ERROR", TabVariabiliEMPTY, 0, 0, 0); 
        rSSLConnect.idmsg   = 0 ;
        rSSLConnect.msgDesc = malloc(strlen("SSL.CONNECT.SOCKETCTX.GETVERSION.ERROR"));
        rSSLConnect.msgDesc = "SSL.CONNECT.SOCKETCTX.GETVERSION.ERROR";
        rSSLConnect.return_code = -3;
        goto SSLConnect_ERROR ;
    };
    PrintMessage(debug_level, SSL_TABMSG, "SSL_CONNECT_SOCKETCTX_GETVERSION_OK", TabVariabiliEMPTY, 0, 0, 0);

    if (debug_level == __INFO__) 
        printf("+SSL Version Client [%s]\n", rSSLConnect.ssl_version);


    /*
    .------------------------------------------------------------------------.
    | PASSO 4. Elenco Ciphersuite proposte al Server                         |
    +------------------------------------------------------------------------+
    | Determina l'elenco completo delle ciphersuite che                      |
    | il client proporrà al server in ordine di preferenza.                  |  
    | Non ho ben capito perchè è necessario passare la socket SSL alla       |
    | funzione per ottenere l'elenco delle ciphersuite che il client         |
    | proporrà al server durante l'handshake. Sembra una simulazione di      |
    | quello che avverrà. L'informazione comunque è ricavabile anche         |
    | diversamente anche se non con la stessa sicurezza che si ha facendolo  |
    | in questo modo. Ad esempio se le ciphersuite son impostate dall'utente |
    | il dato è già ricavato anche perchè se le ha accettate durante la      |
    | creazione del contesto devono essere tutte considerate valide e se     |
    | ciphers non son state valorizzate è necessario ottennere la lista      |
    | delle ciphers implementate nella versione ssl/tls utilizzata.          |
    | Probabilmnete è più semplice e veloce usare il metodo utilizzato sotto |
    | anzichè gestiore le varie accezione descritte sopra per applicare il   |
    | metodo alternativo.                                                    |
    |                                                                        |
    |                                                                        |
    '------------------------------------------------------------------------' */
    STACK_OF(SSL_CIPHER) *sk;
    sk = SSL_get_ciphers(rSSLConnect.ssl);

    rSSLConnect.ssl_ciphersuite_client_list_proposals_num = sk_SSL_CIPHER_num(sk) ; 

    if (rSSLConnect.ssl_ciphersuite_client_list_proposals_num == 0)
    {
        PrintMessage(debug_level, SSL_TABMSG, "SSL_CONNECT_SOCKETCTX_GETCIPHER_ERROR", TabVariabiliEMPTY, 0, 0, 0);
        rSSLConnect.idmsg   = 0 ;
        rSSLConnect.msgDesc = malloc(strlen("SSL.CONNECT.SOCKETCTX.GETCIPHER.ERROR"));
        rSSLConnect.msgDesc = "SSL.CONNECT.SOCKETCTX.GETCIPHER.ERROR";
        rSSLConnect.return_code = -4;
        goto SSLConnect_ERROR ;
    };

    rSSLConnect.ssl_ciphersuite_client_list_proposals[0] = 0;
    int indice;
    for (indice = 0; indice < rSSLConnect.ssl_ciphersuite_client_list_proposals_num; indice++)
    {
         if (indice > 0) 
             strcat(rSSLConnect.ssl_ciphersuite_client_list_proposals,":");

         strcat(rSSLConnect.ssl_ciphersuite_client_list_proposals,SSL_CIPHER_get_name(sk_SSL_CIPHER_value(sk,indice))); 
    };

    PrintMessage(debug_level, SSL_TABMSG, "SSL_CONNECT_SOCKETCTX_GETCIPHER_OK", TabVariabiliEMPTY, 0, 0, 0);

    if (debug_level == __INFO__) 
        printf("+SSL CipherSuite Client [%d][%s]\n", 
           rSSLConnect.ssl_ciphersuite_client_list_proposals_num, 
           rSSLConnect.ssl_ciphersuite_client_list_proposals);

    /*
    .------------------------------------------------------------------------.
    | PASSO 5. Handshake + Open Session SSL                                  |
    +------------------------------------------------------------------------+
    |                                                                        |
    |                                                                        |
    |                                                                        |
    |                                                                        |
    '------------------------------------------------------------------------' */
    BIO *sbio;
    sbio = BIO_new_socket( sock, BIO_NOCLOSE );
    SSL_set_bio( rSSLConnect.ssl, sbio, sbio );
    int rSSL_connect = SSL_connect(rSSLConnect.ssl);
    if ( rSSL_connect < 1 )
    {
        PrintMessage(debug_level, SSL_TABMSG, "SSL_CONNECT_HANDESHAKE_CONNECT_ERROR", TabVariabiliEMPTY, 0, 0, 0);

        rSSLConnect.idmsg = SSL_get_error( rSSLConnect.ssl, rSSL_connect );
        switch(rSSLConnect.idmsg)
        {
               case SSL_ERROR_NONE         : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_NONE", TabVariabiliEMPTY, 0, 0, 0);
                                             rSSLConnect.idmsg   = SSL_ERROR_NONE ;
                                             rSSLConnect.msgDesc = malloc(strlen("SSL.CONNECT.HANDESHAKE.OK"));
                                             rSSLConnect.msgDesc = "SSL.CONNECT.HANDESHAKE.OK";
                                             break;

               case SSL_ERROR_SSL          : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_SSL", TabVariabiliEMPTY, 0, 0, 0);
                                             rSSLConnect.idmsg   = SSL_ERROR_SSL ;
                                             rSSLConnect.msgDesc = malloc(strlen("SSL.CONNECT.HANDESHAKE.SSL.ERROR"));
                                             rSSLConnect.msgDesc = "SSL.CONNECT.HANDESHAKE.SSL.ERROR";
                                             break;

               case SSL_ERROR_WANT_READ    : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_WANT_READ", TabVariabiliEMPTY, 0, 0, 0);
                                             rSSLConnect.idmsg   = SSL_ERROR_WANT_READ ;
                                             rSSLConnect.msgDesc = malloc(strlen("SSL.CONNECT.HANDESHAKE.WANT.READ.ERROR"));
                                             rSSLConnect.msgDesc = "SSL.CONNECT.HANDESHAKE.WANT.READ.ERROR";
                                             break;

               case SSL_ERROR_WANT_WRITE   : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_WANT_WRITE", TabVariabiliEMPTY, 0, 0, 0);
                                             rSSLConnect.idmsg   = SSL_ERROR_WANT_WRITE ;
                                             rSSLConnect.msgDesc = malloc(strlen("SSL.CONNECT.HANDESHAKE.WANT.WRITE.ERROR"));
                                             rSSLConnect.msgDesc = "SSL.CONNECT.HANDESHAKE.WANT.WRITE.ERROR";
                                             break;

               case SSL_ERROR_SYSCALL      : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_SYSCALL", TabVariabiliEMPTY, 0, 0, 0);
                                             rSSLConnect.idmsg   = SSL_ERROR_SYSCALL ;
                                             rSSLConnect.msgDesc = malloc(strlen("SSL.CONNECT.HANDESHAKE.SYSCALL.ERROR"));
                                             rSSLConnect.msgDesc = "SSL.CONNECT.HANDESHAKE.SYSCALL.ERROR";
                                             break;

               case SSL_ERROR_ZERO_RETURN  : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_ZERO_RETURN", TabVariabiliEMPTY, 0, 0, 0);
                                             rSSLConnect.idmsg   = SSL_ERROR_ZERO_RETURN ;
                                             rSSLConnect.msgDesc = malloc(strlen("SSL.CONNECT.HANDESHAKE.ZERO.RETURN.ERROR"));
                                             rSSLConnect.msgDesc = "SSL.CONNECT.HANDESHAKE.ZERO.RETURN.ERROR";
                                             break;

               case SSL_ERROR_WANT_CONNECT : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_WANT_CONNECT", TabVariabiliEMPTY, 0, 0, 0);
                                             rSSLConnect.idmsg   = SSL_ERROR_WANT_CONNECT ;
                                             rSSLConnect.msgDesc = malloc(strlen("SSL.CONNECT.HANDESHAKE.WANT.CONNECT.ERROR"));
                                             rSSLConnect.msgDesc = "SSL.CONNECT.HANDESHAKE.WANT.CONNECT.ERROR";
                                             break;

               default                     : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_UNKNOWN", TabVariabiliEMPTY, 0, 0, 0);
                                             rSSLConnect.idmsg   = 0 ;
                                             rSSLConnect.msgDesc = malloc(strlen("SSL.CONNECT.HANDESHAKE.UNKNOWN.ERROR"));
                                             rSSLConnect.msgDesc = "SSL.CONNECT.HANDESHAKE.UNKNOWN.ERROR";
                                             break;

        };

        rSSLConnect.return_code = -5;
        goto SSLConnect_ERROR ;
    };


    /*
    .------------------------------------------------------------------------.
    | PASSO 6. Legge la CipherSuite negoziata col Server                     |
    +------------------------------------------------------------------------+
    |                                                                        |
    |                                                                        |
    |                                                                        |
    |                                                                        |
    '------------------------------------------------------------------------' */

    SSL_CIPHER *sslCipher ;

    /*
    .----------------------------------------------------------.
    | Preleva la cipher suite utilizzata per le comunicazioni. |
    | nel formato per SSL_CIPHER_get_name, SSL_CIPHER_get_bit  |
    | ed SSL_CIPHER_get_version.                               |
    '----------------------------------------------------------' */
    sslCipher = SSL_get_current_cipher(rSSLConnect.ssl);

    /*
    .----------------------------------------------------------.
    | Nome della Cipher Suite utilizzata per le comunicazioni. |
    '----------------------------------------------------------' */
    rSSLConnect.ssl_ciphersuite = malloc(100);
    strcpy ( rSSLConnect.ssl_ciphersuite, SSL_CIPHER_get_name(sslCipher) );

    /*
    .----------------------------------------------------------.
    | Lunghezza della chiave dell'algoritmo simmetrico         |
    '----------------------------------------------------------' */
    rSSLConnect.ssl_ciphersuite_algsim_lenkey = SSL_CIPHER_get_bits(sslCipher,0);

    /*
    .----------------------------------------------------------.
    | Versioni di SSL che prevedono l'algoritmo simmetrico     |
    '----------------------------------------------------------' */
    rSSLConnect.ssl_ciphersuite_algsim_sslversion = malloc(100);
    strcpy(rSSLConnect.ssl_ciphersuite_algsim_sslversion,SSL_CIPHER_get_version(sslCipher));

    if ( debug_level == __INFO__ )
    {
         if ( strcmp(rSSLConnect.ssl_ciphersuite, "(NONE)" ) == 0 )
         {
              printf("+Error. No shared ciphers negotiated.\n");
         }
         else
         {
              printf("+Version SSL, symmetric encryption algorithm negoziated [%s %s %d]\n",
                      rSSLConnect.ssl_ciphersuite_algsim_sslversion,
                      rSSLConnect.ssl_ciphersuite,
                      rSSLConnect.ssl_ciphersuite_algsim_lenkey );
         }
    };


    /*
    .------------------------------------------------------------------------.
    | PASSO 7. Elenca le ciphersuite proposte ma non utilizzate              |
    +------------------------------------------------------------------------+
    |                                                                        |
    |                                                                        |
    |                                                                        |
    |                                                                        |
    '------------------------------------------------------------------------' */
    rSSLRemoveCipherSuite = SSLRemoveCipherSuite ( rSSLConnect.ssl_ciphersuite_client_list_proposals, rSSLConnect.ssl_ciphersuite ) ;
    if ( rSSLRemoveCipherSuite.return_code == 0 )
    {
         strcpy ( rSSLConnect.ssl_cipher_noused, rSSLRemoveCipherSuite.NewElencoCipherSuite );
         rSSLConnect.ssl_cipher_noused_num = rSSLRemoveCipherSuite.NewElencoCipherSuiteNum ;
    };


//DISABILITATO BLOCCO MOMENTANEAMENTE PERCHE' DA ERRORE IN COMPILAZIONE SULLA STRUTTURA s
//CON OPENSSL 1.1.0.... POI APPROFONDIRE
#if (OPENSSL_VERSION_NUMBER < 0x10100000L)       
    /*
    .------------------------------------------------------------------------.
    | PASSO 8. Query parametri di SESSION (ssl.h)                            |
    +------------------------------------------------------------------------+
    | La sessione è una struttura dati condivisa tra C/S. Per SSL/OpenSSL    |
    | tale struttura (ssl_session_st/SSL_SESSION) è dichiara nell'header     |
    | $HOME_SOURCE/ssl/ssl.h. Inoltre risultano implementate 2 funzioni per  |
    | stampare i parametri di sessione: SSL_SESSION_print e                  |
    | SSL_SESSION_print_pf. Queste si trovano nella libreria                 |
    | $HOME_SOURCE/ssl/ssl_txt.c. La prima estrae le info e le scrive in una |
    | area temporanea (BIO). La seconda scarica in un file il contenuto      |
    | dell'area riempita precedentemente. Per maggiori dettagli sulla        | 
    | struttura di sessione è possibile far riferimento alla libreria        |
    | $HOME_SOURCE/ssl/ssl_asn1.c                                            |
    |                                                                        |
    | In questo blocco estraggo e stampo senza usare le due API :            |
    | int SSL_SESSION_print_fp(FILE *fp, const SSL_SESSION *x)               | 
    | int SSL_SESSION_print(BIO *bp, const SSL_SESSION *x)                   |
    | Sto studiando SSL quindi mi interessa più che altro esplorare il più   |
    | possibile al momento anche sbagliando o rifacendo nuovamente la ruota. |
    '------------------------------------------------------------------------' */
    int DEBUG = 0 ;
    if (DEBUG == 1 && debug_level == __INFO__)
    {
        int i; 
        SSL_SESSION *s;
        s = SSL_get_session(rSSLConnect.ssl);

        /*
        .-----------------------------------------------.  
        | Decodifica la versione                        |
        '-----------------------------------------------' */
        char *version_ssl_desc ;  

        if (s->ssl_version == SSL2_VERSION) version_ssl_desc="SSLv2";
        if (s->ssl_version == TLS1_VERSION) version_ssl_desc="TLSv1";
        if (s->ssl_version != SSL2_VERSION && 
            s->ssl_version != SSL3_VERSION && 
            s->ssl_version != TLS1_VERSION)  
            version_ssl_desc="unknown";
//      ------------------------------------------------------
        printf("\nsession->ssl_version               : [%d][%s]", s->ssl_version, version_ssl_desc);
//      ------------------------------------------------------
        printf("\nsession->key_arg_length            : [%u]", s->key_arg_length); 
        printf("\nsession->key_arg                   : ["); for (i=0; i<s->key_arg_length; i++) printf("%02X",s->key_arg[i]); printf("]"); 
//      ------------------------------------------------------
        printf("\nsession->master_key_lenght         : [%u]", s->master_key_length); 
        printf("\nsession->master_key                : ["); for (i=0; i<s->master_key_length; i++) printf("%02X",s->master_key[i]); printf("]");
//      ------------------------------------------------------
        printf("\nsession->session_id_length         : [%u]", s->session_id_length);
        printf("\nsession->session_id                : ["); for (i=0; i<s->session_id_length; i++) printf("%02X",s->session_id[i]); printf("]"); 
//      ------------------------------------------------------
        printf("\nsession->sid_ctx_length            : [%u]", s->sid_ctx_length);
        printf("\nsession->sid_ctx                   : ["); for (i=0; i<s->sid_ctx_length; i++) printf("%02X",s->sid_ctx[i]); printf("]");  
//      ------------------------------------------------------ 
#ifndef OPENSSL_NO_KRB5
        printf("\nsession->krb5_client_princ_len     : [%u]" , s->krb5_client_princ_len);
        printf("\nsession->krb5_client_princ         : ["); for(i=0; i<s->krb5_client_princ_length; i++) printf("%02X",s->krb5_client_princ[i]); printf("]");
#endif 
//      ------------------------------------------------------
        printf("\nsession->not_resumable             : [%d]" , s->not_resumable);
//      ------------------------------------------------------
        printf("\nsession->sess_cert                 : [??]");
//      struct sess_cert_st *sess_cert;
//      ------------------------------------------------------
        printf("\nsession->peer                      : [??]");
//      X509 *peer;
//      ------------------------------------------------------
        printf("\nsession->verify_result             : [%ld]", s->verify_result); 
        printf("\nsession->references                : [%d]" , s->references); 
        printf("\nsession->timeout                   : [%ld] (s)", SSL_SESSION_get_timeout(s));  
        printf("\nsession->time (start)              : [%ld]", SSL_SESSION_get_time(s)); 
        printf("\nsession->compress_meth             : [%d]" , s->compress_meth); 

        /*
        .----------------------------------------------.
        | Struttura SSL_CIPHER definita in ssl.h       |
        '----------------------------------------------' */
        printf("\nsession->cipher->valid             : [%d]" , s->cipher->valid);
        printf("\nsession->cipher->name              : [%s]" , s->cipher->name);             // corrisponde a SSL_CIPHER_get_name(s->cipher));               
        printf("\nsession->cipher->id                : [%lu]", s->cipher->id);               
        //---printf("\nsession->cipher->algorithms        : [%lu]", s->cipher->algorithms);       
        printf("\nsession->cipher->algo_strength     : [%lu]", s->cipher->algo_strength);   
        printf("\nsession->cipher->algorithm2        : [%lu]", s->cipher->algorithm2);      
        printf("\nsession->cipher->strength_bits     : [%d]" , s->cipher->strength_bits);   
        printf("\nsession->cipher->alg_bits          : [%d]" , s->cipher->alg_bits);         // corrisponde a SSL_CIPHER_get_bits(sslCipher,0); 
        //---printf("\nsession->cipher->mask              : [%lu]", s->cipher->mask);           
        //---printf("\nsession->cipher->mask_strength     : [%lu]", s->cipher->mask_strength);  
//      ------------------------------------------------------
        printf("\nsession->cipher_id                 : [%lu]", s->cipher_id);        
//      ------------------------------------------------------
        printf("\nsession->ciphers                   : [??]"); 
//      STACK_OF(SSL_CIPHER) *ciphers; 
//      ------------------------------------------------------
        printf("\nsession->ex_data                   : [??]");
//      CRYPTO_EX_DATA ex_data; 
//      ------------------------------------------------------
        printf("\nsession->prev                      : [??]");
        printf("\nsession->next                      : [??]");
//      struct ssl_session_st *prev,*next;
//      ------------------------------------------------------

#ifndef OPENSSL_NO_TLSEXT
        printf("\nsession->tlsext_hostname           : [%s]", s->tlsext_hostname) ;
//      ------------------------------------------------------ 
        printf("\nsession->tlsext_hostname           : [??]");
//      unsigned char *tlsext_tick;     /* Session ticket */
//      ------------------------------------------------------
        printf("\nsession->tlsext_ticklen            : [??]");
//      size_t  tlsext_ticklen;         /* Session ticket length */
//      ------------------------------------------------------
        printf("\nsession->tlsext_tick_lifetime_hint : [%l]", s->tlsext_tick_lifetime_hint) ; 
#endif

        printf("\n\n");
    }
#endif


SSLConnect_DONE:

    PrintMessage(debug_level, SSL_TABMSG, "SSL_CONNECT_HANDESHAKE_OK", TabVariabiliEMPTY, 0, 0, 0);

SSLConnect_ERROR:

    return rSSLConnect;

}



/*
.-------------------------------------------------------------------------------------------.
|                                                                                           |
|  Funzione   : SSLDoAuthentication                                                         |
|                                                                                           |
|  Descrizione: Verifica corrispondenza tra common.name certificato e                       |
|               IP|DQFN della macchina mittente. Preventivamente che                        |
|               quello ricevuto dal server sia una certificato nel                          |
|               formato X.509. Verifica il cert chain. La lunghezza del                     |
|               del chain è automaticamente verificata da OpenSSL quando                    |
|               noi valorizziamo il parametro "verify depth" nel                            |
|               contesto.                                                                   |
|                                                                                           |
|  Return-Code: -1 Non ricevuto Certificato *Error*                                         |
|                0 Ricevuto Certificato Corretto                                            |
|               +1 Ricevuto Certificato Ma Non Corretto *Warning*                           |
|               +2 Server.Certificato.CommonName <> Server.DFQN                             |
|                                                                                           |
|                                                                                           |
'-------------------------------------------------------------------------------------------'*/
extern struct tSSLDoAuthentication SSLDoAuthentication(SSL *ssl, char *host, int debug_level)
{
    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , "", ""                                  }
    };

    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tSSLDoAuthentication  rSSLDoAuthentication;
    struct tSSLDoAuthentication *pSSLDoAuthentication;

    /*
    .------------------------------------.
    | Allocazione Spazio                 |
    '------------------------------------' */
    pSSLDoAuthentication  = malloc(sizeof(struct tSSLDoAuthentication));

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pSSLDoAuthentication  = &rSSLDoAuthentication;

    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rSSLDoAuthentication , 0, sizeof(rSSLDoAuthentication) );
    rSSLDoAuthentication.idmsg       = 0 ;
    rSSLDoAuthentication.return_code = 0 ;


    /*
    .------------------------------------------------------------------------.
    | PASSO 1. TEST: Ricevuto certificato x.509 dal server.                  |
    +------------------------------------------------------------------------+
    | Verifica della presenza del'certificato x509 del server. Se il server  |
    | non ha inviato il certificato la funzione SSL_get_peer_certificate     |
    | torna NULL. In questo caso è inutile andare avanti, l'obbiettivo della |
    | funzione è quella di analizzare l'identità del server... pertanto      |
    |                                                                        |
    '------------------------------------------------------------------------' */
    if (SSL_get_peer_certificate(ssl) == NULL ) 
    {
        PrintMessage(debug_level, SSL_TABMSG, "SSL_IDENTITYSERVER_NOCERTIFICATE_ERROR", TabVariabiliEMPTY, 0, 0, 0) ;
        rSSLDoAuthentication.idmsg       = 0 ;
        rSSLDoAuthentication.msgDesc     = malloc(strlen("SSL.IDENTITYSERVER.NOCERTIFICATE.ERROR")) ;
        rSSLDoAuthentication.msgDesc     = "SSL.IDENTITYSERVER.NOCERTIFICATE.ERROR" ;
        rSSLDoAuthentication.return_code = -1 ; 
        goto SSLDoAuthentication_ERROR ; 
    };

    /*
    .------------------------------------------------------------------------.
    | PASSO 2. Verifica certificato x.509 ricevuto dal server                |
    +------------------------------------------------------------------------+
    | Il certificato del server è stato acquisito dopo il controllo          |
    | precedente. A questo punto è possibile quindi verificare che sia un    |
    | documento valido :                                                     | 
    |                                                                        |
    | [1] nel formato X.509                                                  |
    | [2] attivo e non ancora scaduto                                        |
    | [3] emesso da Certification Authority conosciuta                       |
    |                                                                        |
    | Un eventuale errore non risulta però bloccante perchè è possibile      |
    | ancora continuare per svolgere altre azioni.                           |
    |                                                                        |
    '------------------------------------------------------------------------' */
    rSSLDoAuthentication.idmsg = SSL_get_verify_result(ssl);
    if (rSSLDoAuthentication.idmsg != X509_V_OK)
    {
        PrintMessage(debug_level, SSL_TABMSG, "SSL_IDENTITYSERVER_IDENTITY_WARNING", TabVariabiliEMPTY, 0, 0, 0);
        rSSLDoAuthentication.msgDesc     = malloc(strlen("SSL.IDENTITYSERVER.IDENTITY.WARNING"));
        rSSLDoAuthentication.msgDesc     = "SSL.IDENTITYSERVER.IDENTITY.WARNING";
        rSSLDoAuthentication.return_code = +1;   

        switch(rSSLDoAuthentication.idmsg)
        {
               case X509_V_OK:
                    break;

               case X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT";
                    break;

               case X509_V_ERR_UNABLE_TO_DECRYPT_CERT_SIGNATURE:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_UNABLE_TO_DECRYPT_CERT_SIGNATURE", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_UNABLE_TO_DECRYPT_CERT_SIGNATURE"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_UNABLE_TO_DECRYPT_CERT_SIGNATURE";
                    break;

               case X509_V_ERR_UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY";
                    break;

               case X509_V_ERR_CERT_SIGNATURE_FAILURE:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_CERT_SIGNATURE_FAILURE", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_CERT_SIGNATURE_FAILURE"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_CERT_SIGNATURE_FAILURE";
                    break;

               case X509_V_ERR_CERT_NOT_YET_VALID:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_CERT_NOT_YET_VALID", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_CERT_NOT_YET_VALID"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_CERT_NOT_YET_VALID";
                    break;

               case X509_V_ERR_CERT_HAS_EXPIRED:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_CERT_HAS_EXPIRED", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_CERT_HAS_EXPIRED"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_CERT_HAS_EXPIRED";
                    break;

               case X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD";
                    break;

               case X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD";
                    break;

               case X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT";
                    break;

               case X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN";
                    break;

               case X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY";
                    break;

               case X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE";
                    break;

               case X509_V_ERR_INVALID_CA:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_INVALID_CA", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_INVALID_CA"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_INVALID_CA";
                    break;

               case X509_V_ERR_PATH_LENGTH_EXCEEDED:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_PATH_LENGTH_EXCEEDED", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_PATH_LENGTH_EXCEEDED"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_PATH_LENGTH_EXCEEDED";
                    break;

               case X509_V_ERR_INVALID_PURPOSE:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_INVALID_PURPOSE", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_INVALID_PURPOSE"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_INVALID_PURPOSE";
                    break;

               case X509_V_ERR_CERT_UNTRUSTED:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_CERT_UNTRUSTED", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_CERT_UNTRUSTED"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_CERT_UNTRUSTED";
                    break;

               case X509_V_ERR_CERT_REJECTED:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_CERT_REJECTED", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_CERT_REJECTED"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_CERT_REJECTED";
                    break;

               case X509_V_ERR_SUBJECT_ISSUER_MISMATCH:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_SUBJECT_ISSUER_MISMATCH", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_SUBJECT_ISSUER_MISMATCH"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_SUBJECT_ISSUER_MISMATCH";
                    break;

               case X509_V_ERR_AKID_SKID_MISMATCH:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_AKID_SKID_MISMATCH", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_AKID_SKID_MISMATCH"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_AKID_SKID_MISMATCH";
                    break;

               case X509_V_ERR_AKID_ISSUER_SERIAL_MISMATCH:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_AKID_ISSUER_SERIAL_MISMATCH", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_AKID_ISSUER_SERIAL_MISMATCH"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_AKID_ISSUER_SERIAL_MISMATCH";
                    break;

               case X509_V_ERR_KEYUSAGE_NO_CERTSIGN:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_KEYUSAGE_NO_CERTSIGN", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_KEYUSAGE_NO_CERTSIGN"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_KEYUSAGE_NO_CERTSIGN";
                    break;

               case X509_V_ERR_CERT_REVOKED:
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_CERT_REVOKED", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_CERT_REVOKED"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_CERT_REVOKED";
                    break;

               default : 
                    PrintMessage(debug_level, SSL_TABMSG, "X509_V_ERR_ERROR_UNKNOWN", TabVariabiliEMPTY, 0, 0, 0);
                    rSSLDoAuthentication.msgDesc = malloc(strlen("X509_V_ERR_ERROR_UNKNOWN"));
                    rSSLDoAuthentication.msgDesc = "X509_V_ERR_ERROR_UNKNOWN";
                    break;
        };
    };

    /*
    .------------------------------------------------------------------------.
    | PASSO 3. Preleva copia del certificato x509 del server                 |
    +------------------------------------------------------------------------+
    | Preleva una copia del certificato x509 del server per farci dei        |
    | controlli successivamente o per fornire info all'utente. La copia      |
    | ricavata con la SSL_get_peer_certificate non è facilmente leggibile    |
    | cioè son necessarie delle conversioni per averli nei formati ascii     |
    | alcuni campi o solo per estrarli dalla struttura. Non tutti i campi    |
    | vengono prelevati, questa questione è da approfondire.                 |
    |                                                                        |
    '------------------------------------------------------------------------' */
    X509 *Peer ;  
    Peer = SSL_get_peer_certificate(ssl);

    rSSLDoAuthentication.x509_Version          = X509_get_version(Peer);
    
    rSSLDoAuthentication.x509_SerialNumber     = ASN1_INTEGER_get(X509_get_serialNumber(Peer));

    rSSLDoAuthentication.x509_SignatureType    = X509_get_signature_type(Peer);

    X509_NAME_oneline(X509_get_issuer_name(Peer), rSSLDoAuthentication.x509_IssuerName, 256);
 
    rSSLDoAuthentication.x509_NotBefore[0]     = X509_get_notBefore(Peer)->data[4];  
    rSSLDoAuthentication.x509_NotBefore[1]     = X509_get_notBefore(Peer)->data[5];
    rSSLDoAuthentication.x509_NotBefore[2]     = '/';
    rSSLDoAuthentication.x509_NotBefore[3]     = X509_get_notBefore(Peer)->data[2];
    rSSLDoAuthentication.x509_NotBefore[4]     = X509_get_notBefore(Peer)->data[3];
    rSSLDoAuthentication.x509_NotBefore[5]     = '/';
    rSSLDoAuthentication.x509_NotBefore[6]     = X509_get_notBefore(Peer)->data[0];
    rSSLDoAuthentication.x509_NotBefore[7]     = X509_get_notBefore(Peer)->data[1];
    rSSLDoAuthentication.x509_NotBefore[8]     = ' '; 
    rSSLDoAuthentication.x509_NotBefore[9]     = X509_get_notBefore(Peer)->data[6];
    rSSLDoAuthentication.x509_NotBefore[10]    = X509_get_notBefore(Peer)->data[7];
    rSSLDoAuthentication.x509_NotBefore[11]    = ':'; 
    rSSLDoAuthentication.x509_NotBefore[12]    = X509_get_notBefore(Peer)->data[8];
    rSSLDoAuthentication.x509_NotBefore[13]    = X509_get_notBefore(Peer)->data[9];
    rSSLDoAuthentication.x509_NotBefore[14]    = 0;

    rSSLDoAuthentication.x509_NotAfter[0]      = X509_get_notAfter(Peer)->data[4];  
    rSSLDoAuthentication.x509_NotAfter[1]      = X509_get_notAfter(Peer)->data[5];
    rSSLDoAuthentication.x509_NotAfter[2]      = '/';
    rSSLDoAuthentication.x509_NotAfter[3]      = X509_get_notAfter(Peer)->data[2];
    rSSLDoAuthentication.x509_NotAfter[4]      = X509_get_notAfter(Peer)->data[3];
    rSSLDoAuthentication.x509_NotAfter[5]      = '/';
    rSSLDoAuthentication.x509_NotAfter[6]      = X509_get_notAfter(Peer)->data[0];
    rSSLDoAuthentication.x509_NotAfter[7]      = X509_get_notAfter(Peer)->data[1];
    rSSLDoAuthentication.x509_NotAfter[8]      = ' ';
    rSSLDoAuthentication.x509_NotAfter[9]      = X509_get_notAfter(Peer)->data[6];
    rSSLDoAuthentication.x509_NotAfter[10]     = X509_get_notAfter(Peer)->data[7];
    rSSLDoAuthentication.x509_NotAfter[11]     = ':'; 
    rSSLDoAuthentication.x509_NotAfter[12]     = X509_get_notAfter(Peer)->data[8];
    rSSLDoAuthentication.x509_NotAfter[13]     = X509_get_notAfter(Peer)->data[9];
    rSSLDoAuthentication.x509_NotAfter[14]     = 0;

    X509_NAME_oneline(X509_get_subject_name(Peer), rSSLDoAuthentication.x509_SubjectName, 256);
   
    X509_NAME_get_text_by_NID (X509_get_subject_name(Peer), NID_commonName, rSSLDoAuthentication.x509_SubjectName_commonname, 256);

    rSSLDoAuthentication.x509_CertificateType  = X509_certificate_type(Peer,X509_get_pubkey(Peer)); 


    /*
    .------------------------------------------------------------------------.
    | PASSO 3. Print Certificato x509 ricevuto dal server                    |
    +------------------------------------------------------------------------+
    |                                                                        |
    |                                                                        |
    |                                                                        |
    |                                                                        |
    |                                                                        |
    '------------------------------------------------------------------------' */
    if (debug_level == __INFO__)
    {
        SSLPrintCertificateX509 ( rSSLDoAuthentication.x509_Version                ,
                                  rSSLDoAuthentication.x509_SerialNumber           ,
                                  rSSLDoAuthentication.x509_SignatureType          ,
                                  rSSLDoAuthentication.x509_IssuerName             ,
                                  rSSLDoAuthentication.x509_NotBefore              ,
                                  rSSLDoAuthentication.x509_NotAfter               ,
                                  rSSLDoAuthentication.x509_SubjectName            ,
                                  rSSLDoAuthentication.x509_SubjectName_commonname ,
                                  rSSLDoAuthentication.x509_CertificateType       );
    };


    /*
    .------------------------------------------------------------------------.
    | PASSO 4. Altri Controlli                                               |
    +------------------------------------------------------------------------+
    | Controlli non effettuati da SSL_get_verify_result, quindi ad           |
    | integrazione di quelli. Il blocco al momento implementa il controllo   |
    | sul common.name del certificato che deve equivalere all'host           |
    | che il client ha contattato.                                           |
    |                                                                        |
    |                                                                        |
    |                                                                        |
    |                                                                        |
    |                                                                        |
    '------------------------------------------------------------------------' */    
    if ( rSSLDoAuthentication.return_code == 0 ) 
    {
         /*
         .------------------------------------------------------.
         | common.name check                                    |
         '------------------------------------------------------'*/
         if ( strcasecmp ( rSSLDoAuthentication.x509_SubjectName_commonname, host ) != 0 ) 
         {
              rSSLDoAuthentication.idmsg       = 0 ; 
              rSSLDoAuthentication.msgDesc     = malloc(strlen("SSL.IDENTITYSERVER.COMMONNAME.WARNING"));
              rSSLDoAuthentication.msgDesc     = "SSL.IDENTITYSERVER.COMMONNAME.WARNING";
              rSSLDoAuthentication.return_code = +2;
         };
    };

SSLDoAuthentication_DONE:

    if ( rSSLDoAuthentication.return_code == 0 ) PrintMessage(debug_level, SSL_TABMSG, "SSL_IDENTITYSERVER_OK", TabVariabiliEMPTY, 0, 0, 0);

SSLDoAuthentication_ERROR:

    return rSSLDoAuthentication;
};



/*
.-------------------------------------------------------------------------------------.
|                                                                                     |
|  Funzione    : SSLPrintCertificateX509                                              |
|                                                                                     |
|  Descrizione : Print Certificate X.509                                              |
|                                                                                     |
|  Return-code : 0 Ok                                                                 |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
'-------------------------------------------------------------------------------------'*/
int SSLPrintCertificateX509 ( long x509_Version                     , 
                              long x509_SerialNumber                , 
                              int  x509_SignatureType               , 
                              char x509_IssuerName[256]             , 
                              char x509_NotBefore[15]               , 
                              char x509_NotAfter[15]                , 
                              char x509_SubjectName[256]            , 
                              char x509_SubjectName_commonname[256] , 
                              int  x509_CertificateType             ) 
{
    /*

    il parse di issuer e subject andrebbero eseguiti nella routine di
    get certificate e far diventare questa solo una print layout 
    
    */

    printf(".----------------------------------------.\n");
    printf("|                                        |\n");
    printf("|        Certificate X.509 Server        |\n");
    printf("|                                        |\n");
    printf("'----------------------------------------'\n");
 
    printf("Version              | %li\n"        , x509_Version );
    printf("Serial Number        | %li\n"        , x509_SerialNumber ); 
    printf("Signature Type       | %i\n"         , x509_SignatureType );
  //--------------- 
    printf("Issuer               | ");
    int i = 0;
    while (i < strlen(x509_IssuerName))
    {
           if (x509_IssuerName[i]=='/' && i > 0)  printf("\n                     | "); 
           printf("%c", x509_IssuerName[i]);
           i++;
    };
    printf("\n");
  //---------------         
    printf("not Before           | %s\n", x509_NotBefore ); 
    printf("not After            | %s\n", x509_NotAfter  ); 
  //---------------         
    printf("Subject              | ");
    i = 0;
    while (i < strlen(x509_SubjectName))
    {
           if (x509_SubjectName[i]=='/' && i > 0) printf("\n                     | ");
           printf("%c", x509_SubjectName[i]);
           i++;
    };
    printf("\n");
  //---------------
    printf("Type                 | %i\n\n", x509_CertificateType ); 

    return 0;
};


/*
.-------------------------------------------------------------------------------------.
|                                                                                     |
|  Funzione    : sigpipe_handle                                                       |
|                                                                                     |
|  Descrizione :                                                                      |
|                                                                                     |
|  Return-code : Null                                                                 |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
'-------------------------------------------------------------------------------------'*/
/*
+-----------------------------------------------------------------------+
| Approfondire la gestione dei segnali.                                 |
'-----------------------------------------------------------------------' */
/*
| void sigpipe_handle(int x)
| {
| }
*/

/*
.-------------------------------------------------------------------------------------.
|                                                                                     |
|  Function    : SSLSend                                                              |
|                                                                                     |
|  Remark      : SSLSend                                                              |
|                                                                                     |
|  Return-code :  0 Ok                                                                |
|                -1 Write Error                                                       |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
'-------------------------------------------------------------------------------------'*/
extern struct tSSLSend SSLSend(SSL *ssl, char *request, int debug_level) 
{
    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , "", ""                                  }
    };

    int  SSLSendBYTEInviati = 0;

    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tSSLSend          rSSLSend;
    struct tSSLSend         *pSSLSend;
    
    /*
    .------------------------------------.
    | Allocazione Spazio                 |
    '------------------------------------' */
    pSSLSend = malloc(sizeof(struct tSSLSend));
    
    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pSSLSend = &rSSLSend;
    
    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rSSLSend, 0, sizeof(rSSLSend));
    rSSLSend.return_code = 0;
    rSSLSend.idmsg       = 0; 

    /*
    .---------------------------------------------------.
    | Calcola la lunghezza se non viene passata         |
    '---------------------------------------------------' */
    PrintMessage(debug_level, SSL_TABMSG, "SSL_SEND_CALCULATE_LEN_REQUEST", TabVariabiliEMPTY, 0, 0, 0);
    rSSLSend.requestLen = strlen(request) ; 
    PrintMessage(debug_level, SSL_TABMSG, "SSL_SEND_OK", TabVariabiliEMPTY, 0, 0, 0);

    /*
    .---------------------------------------------------.
    | Alloca e valorizza la richiesta da inviare        |
    '---------------------------------------------------' */
    rSSLSend.request = (char *) malloc(rSSLSend.requestLen);  
    rSSLSend.request = request ;

    /*
    .------------------------------------------------------------------.
    | Invia al Server la Richiesta. L'operazione termina correttamente |
    | se son vere due condizioni: [1] return code uguale a             |
    | SSL_ERROR_NONE e [2] se risulta tutta la richiesta trasmessa.    |
    '------------------------------------------------------------------' */
    PrintMessage(debug_level, SSL_TABMSG, "SSL_SEND_REQUEST", TabVariabiliEMPTY, 0, 0, 0);
    if ( debug_level == __INFO__ ) printf("(%s) ", rSSLSend.request);

    SSLSendBYTEInviati = SSL_write ( ssl, rSSLSend.request, rSSLSend.requestLen );
    rSSLSend.idmsg = SSL_get_error ( ssl, SSLSendBYTEInviati );  
    if (( rSSLSend.idmsg != SSL_ERROR_NONE ) || 
        ( rSSLSend.idmsg == SSL_ERROR_NONE && rSSLSend.requestLen != SSLSendBYTEInviati )) 
    {
          switch ( rSSLSend.idmsg )
          {
                   case SSL_ERROR_NONE         : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_NONE", TabVariabiliEMPTY, 0, 0, 0);
                                                 rSSLSend.idmsg   = SSL_ERROR_NONE ;
                                                 rSSLSend.msgDesc = malloc(strlen("SSL.SEND.OK"));
                                                 rSSLSend.msgDesc = "SSL.SEND.OK";
                                                 break;

                   case SSL_ERROR_SSL          : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_SSL", TabVariabiliEMPTY, 0, 0, 0);
                                                 rSSLSend.idmsg   = SSL_ERROR_SSL ;
                                                 rSSLSend.msgDesc = malloc(strlen("SSL.SEND.SSL.ERROR"));
                                                 rSSLSend.msgDesc = "SSL.SEND.SSL.ERROR";
                                                 break;

                   case SSL_ERROR_WANT_READ    : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_WANT_READ", TabVariabiliEMPTY, 0, 0, 0);
                                                 rSSLSend.idmsg   = SSL_ERROR_WANT_READ ;
                                                 rSSLSend.msgDesc = malloc(strlen("SSL.SEND.WANT.READ.ERROR"));
                                                 rSSLSend.msgDesc = "SSL.SEND.WANT.READ.ERROR";
                                                 break;

                   case SSL_ERROR_WANT_WRITE   : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_WANT_WRITE", TabVariabiliEMPTY, 0, 0, 0);
                                                 rSSLSend.idmsg   = SSL_ERROR_WANT_WRITE ;
                                                 rSSLSend.msgDesc = malloc(strlen("SSL.SEND.WANT.WRITE.ERROR"));
                                                 rSSLSend.msgDesc = "SSL.SEND.WANT.WRITE.ERROR";
                                                 break;

                   case SSL_ERROR_SYSCALL      : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_SYSCALL", TabVariabiliEMPTY, 0, 0, 0);
                                                 rSSLSend.idmsg   = SSL_ERROR_SYSCALL ;
                                                 rSSLSend.msgDesc = malloc(strlen("SSL.SEND.SYSCALL.ERROR"));
                                                 rSSLSend.msgDesc = "SSL.SEND.SYSCALL.ERROR";
                                                 break;

                   case SSL_ERROR_ZERO_RETURN  : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_ZERO_RETURN", TabVariabiliEMPTY, 0, 0, 0);
                                                 rSSLSend.idmsg   = SSL_ERROR_ZERO_RETURN ;
                                                 rSSLSend.msgDesc = malloc(strlen("SSL.SEND.ZERO.RETURN.ERROR"));
                                                 rSSLSend.msgDesc = "SSL.SEND.ZERO.RETURN.ERROR";
                                                 break;

                   case SSL_ERROR_WANT_CONNECT : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_WANT_CONNECT", TabVariabiliEMPTY, 0, 0, 0);
                                                 rSSLSend.idmsg   = SSL_ERROR_WANT_CONNECT ;
                                                 rSSLSend.msgDesc = malloc(strlen("SSL.SEND.WANT.CONNECT.ERROR"));
                                                 rSSLSend.msgDesc = "SSL.SEND.WANT.CONNECT.ERROR";
                                                 break;

                   default                     : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_UNKNOWN", TabVariabiliEMPTY, 0, 0, 0    );
                                                 rSSLSend.idmsg   = 0 ;
                                                 rSSLSend.msgDesc = malloc(strlen("SSL.SEND.UNKNOWN.ERROR"));
                                                 rSSLSend.msgDesc = "SSL.SEND.UNKNOWN.ERROR";
                                                 break;
          };

          rSSLSend.return_code = -1; 
          return rSSLSend;
    }; 

    return rSSLSend ;
} 



/*
.-------------------------------------------------------------------------------------.
|                                                                                     |
|  Funzione   : SSLReceive                                                            |
|                                                                                     |
|  Descrizione: SSLReceive                                                            |
|                                                                                     |
|  Return-code :  0 Ok                                                                |
|                -1 Open Datastore Error                                              |
|                -2 Read Error                                                        |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
'-------------------------------------------------------------------------------------'*/
extern struct tSSLReceive SSLReceive ( SSL *ssl, char *OutputFile, int debug_level )
{
    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , "", ""                                  }
    };

    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tSSLReceive      rSSLReceive;
    struct tSSLReceive     *pSSLReceive;

    /*
    .------------------------------------.
    | Allocazione Spazio                 |
    '------------------------------------' */
    pSSLReceive = malloc(sizeof(struct tSSLReceive));

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pSSLReceive = &rSSLReceive;

    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rSSLReceive, 0, sizeof(rSSLReceive));


    /*
    .----------------------------------------------------.
    | Create|Open OUTPUT                                 |
    +----------------------------------------------------+
    | Crea il file di Output prima di iniziare a         |
    | leggere dal socket. Se non riesce ad aprire il     |
    | file esce per errore.                              |
    '----------------------------------------------------' */
    /*
    .------------------------------------------------.
    | Imposta il default se non impostato OutputFile |
    '------------------------------------------------' */
    if ( OutputFile == NULL ) {
         OutputFile = malloc(1024);
         strcpy (OutputFile, "/tmp/.myTCPClient.OutputFile.txt");
    };

    /*
    .-------------------.
    | Open Datastore    |
    '-------------------' */
    FILE *pOutputFile ;
    PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_OPEN_DATASTORE", TabVariabiliEMPTY, 0, 0, 0);
    pOutputFile = fopen(OutputFile, "w");
    if (!pOutputFile)
    {
        PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_KO", TabVariabiliEMPTY, 0, 0, 0);
        PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_OPEN_DATASTORE_KO", TabVariabiliEMPTY, 0, 0, 0);
        rSSLReceive.return_code = -1;
        return rSSLReceive ;
    }
    PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_OK", TabVariabiliEMPTY, 0, 0, 0);

    if ( debug_level == __INFO__ )
         printf("+Datastore [%s] \n", OutputFile ); 

    /*
    .---------------------------------------------------.
    | Receive                                           |
    +---------------------------------------------------+
    | Legge lo stream dal socket.                       |
    '---------------------------------------------------' */
    int numByteReceive = -1;
    int totByteReceive =  0; 
    char *response;
    response = malloc (__SSL_LEN_BUFFER_RECEIVE__);

    PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_START", TabVariabiliEMPTY, 0, 0, 0);
    for (;;)
    {
          PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_WAITING_RESPONSE", TabVariabiliEMPTY, 0, 0, 0);   
          numByteReceive = SSL_read (ssl, response, __SSL_LEN_BUFFER_RECEIVE__);
          PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_RESPONSE", TabVariabiliEMPTY, 0, 0, 0);
          rSSLReceive.idmsg = SSL_get_error (ssl, numByteReceive); 
          if (debug_level == __INFO__) printf("%d", numByteReceive);


          /*
          | =========================X Attenzione! X=========================
          | ho riscontrato che alcuni server non rilasciano stringhe della
          | lunghezza corretta nel caso in cui la stringa non viene riempita tutta.
          | Ad esempio succede questo nell'ultimo pezzo dell'header e del Body
          | quando il server non riesce a riempirla tutta, e allora succede 
          | non inserendo il fine stringa (il problema è tutto li), se il
          | client la utilizza così come gli arriva nel caso migliore attiene 
          | una stringa con in coda caratteri con previsti, e nel caso peggiore
          | stringa un buffer overflow. Diciamo che in conclusione ciò è 
          | attendibile la lunghezza della stringa dati rilasciata 
          | in risposta dalle api di read e non il contenuto del buffer di 
          | output, a quetso il client dovrà in qualche modo inserire il 
          | fine stringa. 
          |  
          */ 
          if ( numByteReceive != strlen(response) )
          {
               char *response_bak;
               response_bak = malloc (__SSL_LEN_BUFFER_RECEIVE__); 
               memset (response_bak, 0, __SSL_LEN_BUFFER_RECEIVE__);
               strncpy(response_bak, response, numByteReceive);
               strcpy (response, response_bak);
               free(response_bak); 
           } /*---*/  
            


          if ( rSSLReceive.idmsg != SSL_ERROR_NONE && 
               rSSLReceive.idmsg != SSL_ERROR_ZERO_RETURN )
          {
               rSSLReceive.return_code = -2 ;

               PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_KO", TabVariabiliEMPTY, 0, 0, 0);
               PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_RESPONSE_KO", TabVariabiliEMPTY, 0, 0, 0);

               switch ( rSSLReceive.idmsg )
               {
                        case SSL_ERROR_NONE         : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_NONE", TabVariabiliEMPTY, 0, 0, 0       );
                                                      rSSLReceive.idmsg   = SSL_ERROR_NONE ;
                                                      rSSLReceive.msgDesc = malloc(strlen("SSL.RECEIVE.OK"));
                                                      rSSLReceive.msgDesc = "SSL.RECEIVE.OK";
                                                      break;

                        case SSL_ERROR_SSL          : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_SSL", TabVariabiliEMPTY, 0, 0, 0        );
                                                      rSSLReceive.idmsg   = SSL_ERROR_SSL ;
                                                      rSSLReceive.msgDesc = malloc(strlen("SSL.RECEIVE.SSL.ERROR"));
                                                      rSSLReceive.msgDesc = "SSL.RECEIVE.SSL.ERROR";
                                                      break;

                        case SSL_ERROR_WANT_READ    : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_WANT_READ", TabVariabiliEMPTY, 0, 0, 0  );
                                                      rSSLReceive.idmsg   = SSL_ERROR_WANT_READ ;
                                                      rSSLReceive.msgDesc = malloc(strlen("SSL.RECEIVE.WANT.READ.ERROR"));
                                                      rSSLReceive.msgDesc = "SSL.RECEIVE.WANT.READ.ERROR";
                                                      break;

                        case SSL_ERROR_WANT_WRITE   : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_WANT_WRITE", TabVariabiliEMPTY, 0, 0, 0 );
                                                      rSSLReceive.idmsg   = SSL_ERROR_WANT_WRITE ;
                                                      rSSLReceive.msgDesc = malloc(strlen("SSL.RECEIVE.WANT.WRITE.ERROR"));
                                                      rSSLReceive.msgDesc = "SSL.RECEIVE.WANT.WRITE.ERROR";
                                                      break;

                        /*
                        | ===========================X A t t e n z i o n e X================================ 
                        | Attenzione! Alcuni server dopo aver inviato tutto mandano un EOF e questo 
                        | viene interpretato dal cliente (ssl_read) come interruzione della comunicazione 
                        | ad esempio con i server di posta exchange mail.hostname.it/owa 
                        | Il problema dell'SSL_ERROR_SYSCALL è conosciuto (cerca "ssl_error_syscall"+"ssl_read"+"openssl") 
                        | http://www.mail-archive.com/openssl-users@openssl.org/msg37972.html 
                        | Aggiunta Workaround successivamente allo switch per non risolvere momentaneamente
                        */ 
                        case SSL_ERROR_SYSCALL      : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_SYSCALL", TabVariabiliEMPTY, 0, 0, 0);
                                                      rSSLReceive.idmsg   = SSL_ERROR_SYSCALL ;
                                                      rSSLReceive.msgDesc = malloc(strlen("SSL.RECEIVE.SYSCALL.ERROR"));
                                                      rSSLReceive.msgDesc = "SSL.RECEIVE.SYSCALL.ERROR";
                                                      break;

                        case SSL_ERROR_ZERO_RETURN  : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_ZERO_RETURN", TabVariabiliEMPTY, 0, 0, 0);
                                                      rSSLReceive.idmsg   = SSL_ERROR_ZERO_RETURN ;
                                                      rSSLReceive.msgDesc = malloc(strlen("SSL.RECEIVE.ZERO.RETURN.ERROR"));
                                                      rSSLReceive.msgDesc = "SSL.RECEIVE.ZERO.RETURN.ERROR";
                                                      break;

                        case SSL_ERROR_WANT_CONNECT : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_WANT_CONNECT", TabVariabiliEMPTY, 0, 0, 0);
                                                      rSSLReceive.idmsg   = SSL_ERROR_WANT_CONNECT ;
                                                      rSSLReceive.msgDesc = malloc(strlen("SSL.RECEIVE.WANT.CONNECT.ERROR"));
                                                      rSSLReceive.msgDesc = "SSL.RECEIVE.WANT.CONNECT.ERROR";
                                                      break;

                        default                     : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_UNKNOWN", TabVariabiliEMPTY, 0, 0, 0);
                                                      rSSLReceive.idmsg   = 0 ;
                                                      rSSLReceive.msgDesc = malloc(strlen("SSL.RECEIVE.UNKNOWN.ERROR"));
                                                      rSSLReceive.msgDesc = "SSL.RECEIVE.UNKNOWN.ERROR";
                                                      break;
               };

               break;
          };


          /*
          .----------------------------------------------.
          |              SSL_ERROR_SYSCALL               |
          +----------------------------------------------+
          | Un EOF inviato da alcuni server interpretato |
          | come fine stringa. Insomma un falso problema |
          | dovuto forse ad uno standard non rispettato  |
          | al 100% (infatto tanto per cambiare i server |
          | che fatto questa cosa sono M$ Exchange ad    |
          | esempio, ma devo scendere nel dettaglio...   |
          '----------------------------------------------' */
          if ( rSSLReceive.idmsg == SSL_ERROR_SYSCALL ) 
          { 
              int iRetVal = ERR_get_error();
              if ( iRetVal == 0 ) 
              {
                   PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_OK", TabVariabiliEMPTY, 0, 0, 0);
                   //rSSLReceive.idmsg   = SSL_ERROR_SYSCALL ;
                   rSSLReceive.msgDesc = malloc(strlen("SSL.RECEIVE.SYSCALL.ERROR"));
                   rSSLReceive.msgDesc = "SSL.RECEIVE.SYSCALL.ERROR";
                   rSSLReceive.return_code   =  0 ;
              }
              break;
          };

          /*
          .----------------------------------------------.
          |            SSL_ERROR_ZERO_RETURN             |
          +----------------------------------------------+
          | Se il numero di byte ricevuti è 0 significa  |
          | che il server non ha più nulla da            |
          | trasmettere al client.                       |
          |                                              |
          '----------------------------------------------' */
          if ( rSSLReceive.idmsg == SSL_ERROR_ZERO_RETURN )
          {
              PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_OK", TabVariabiliEMPTY, 0, 0, 0);
              rSSLReceive.idmsg       =  0 ;
              rSSLReceive.return_code =  0 ;
              break;
          };

          /*
          .----------------------------------------------.
          |                SSL_ERROR_NONE                |
          +----------------------------------------------+
          |                                              |
          |                                              |
          |                                              |
          |                                              |
          '----------------------------------------------' */
          //if ( rSSLReceive.idmsg == SSL_ERROR_NONE )
          //{
          //    PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_OK", TabVariabiliEMPTY, 0, 0, 0);
          //    rSSLReceive.idmsg       =  0 ;
          //    rSSLReceive.return_code =  0 ;
          //    break;   
          //};

          /*
          .----------------------------------------------.
          | Mantiene il conteggio dei Byte ricevuti      |
          '----------------------------------------------' */
          totByteReceive = totByteReceive + numByteReceive ;

          /*
          .----------------------------------------------.
          | Accoda i Byte ricevuti nel file di output    |
          '----------------------------------------------' */
          PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_WRITE_DATASTORE", TabVariabiliEMPTY, 0, 0, 0);
          fwrite(response, 1, strlen(response), pOutputFile);

    }

    /*
    .----------------------------------------------------.
    | Close OUTPUT                                       |
    +----------------------------------------------------+
    | Chiude il file di Output dopo aver terminato la    |
    | la lettura della coda.                             |
    '----------------------------------------------------' */
    PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_CLOSE_DATASTORE", TabVariabiliEMPTY, 0, 0, 0);
    fclose (pOutputFile);
    PrintMessage(debug_level, SSL_TABMSG, "SSL_RECEIVE_OK", TabVariabiliEMPTY, 0, 0, 0);

    /*
    .----------------------------------------------------.
    | Creare Output Structure                            |
    +----------------------------------------------------+
    | Prepara la struttura dati da rilasciare al         |
    | chiamante.                                         |
    '----------------------------------------------------' */
    rSSLReceive.response = malloc ( __SSL_LEN_BUFFER_RECEIVE__);    
    memset(rSSLReceive.response, 0, __SSL_LEN_BUFFER_RECEIVE__);
 // rSSLReceive.response    = response;    
    rSSLReceive.responseLen = totByteReceive;


    return rSSLReceive ;
}



/*
.-------------------------------------------------------------------------------------------.
|                                                                                           |
|  Funzione   : SSLDisconnect                                                               |
|                                                                                           |
|  Descrizione: Chiude la sessione SSL col Server                                           |
|               Deve avvenire prima di chiudere la socket                                   |
|               da entrambi gli attori.                                                     |
|                                                                                           |
|  Return-Code: +1 Shutdown Wait Server                                                     | 
|                0 Shotdown OK                                                              |
|               -1 Shutdown Error Generic                                                   |
|               -2 Unknown Error                                                            |
|                                                                                           |
|                                                                                           |
|                                                                                           |
'-------------------------------------------------------------------------------------------'*/
extern struct tSSLDisconnect SSLDisconnect(SSL_CTX *ctx, SSL *ssl, int debug_level)
{
    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , "", ""                                  }
    };

    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tSSLDisconnect     rSSLDisconnect    ;
    struct tSSLDisconnect    *pSSLDisconnect    ;

    /*
    .------------------------------------------.
    | Allocazione SPAZIO                       |
    '------------------------------------------' */
    pSSLDisconnect = malloc(sizeof(struct tSSLDisconnect));

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pSSLDisconnect = &rSSLDisconnect;

    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rSSLDisconnect,0,sizeof(rSSLDisconnect));
    rSSLDisconnect.return_code = 0;
    rSSLDisconnect.idmsg = 0;


    /*
    .-------------------------------------------------------------------------.
    | PASSO 0. Setta i PARAMETRI.                                             |
    +-------------------------------------------------------------------------+
    | evoluzione possibile, portar fuori i parametri.                         | 
    |                                                                         |
    |                                                                         |
    |                                                                         |
    '-------------------------------------------------------------------------' */
    int __MAX_TENTATIVI_SHUTDOWN__ = 5 ;
    int __STATE_SHUTDOWN__         = 2 ;
    int __DEBUG__                  = 0 ;


    /*
    .-------------------------------------------------------------------------.
    | PASSO 1. Imposta la modalità dello SHUTDOWN da eseguire.                |
    +-------------------------------------------------------------------------+
    | In questa sezione vengono utilizzate le funzioni di manipolazione di    |
    | sessione SSL/TLS aperta: *SSL_set_shutdown* ed *SSL_get_shutdown*       |
    | rispettivamente per impostare e leggere la modalità dello shutdown.     | 
    |                                                                         |
    | Gli stati previsti sono :                                               |
    |                                                                         |
    | - 0                                                                     |
    | - SSL_SENT_SHUTDOWN [1]                                                 |
    | - SSL_RECEIVED_SHUTDOWN [2]                                             |
    |                                                                         |
    |                                                                         |
    '-------------------------------------------------------------------------' */

    /*
       --- test hacking (**) ---

       SSL_set_shutdown SSL_shutdown Description 
       ================ ============ =====================================================================
                      0     *Warn* 0 __MAX_TENTATIVI_SHUTDOWN__ sempre 0. Il PEER chiude alla prima. 
                      1            - Rimane in WAIT di ricevere qualcosa dal PEER che intanto ha chuso. 
                      2       *OK* 1 [OK sempre] (SSLv2/3) --- attenzione però non so che fa *approfondire*   
                      3      *Err*-1 Error con SSLv3.  
                      4     *Warn* 0 Like 0 
                      5     *Warn* 0 Like 0 
                      6     *Warn* 0 Like 0 

           (**) used OpenSSL s_server for test

       --- test hacking ---
    */

    SSL_set_shutdown(ssl, __STATE_SHUTDOWN__);

    if (__DEBUG__ == 1 && debug_level == __INFO__)
        printf ("\n==> SSL_get_shutdown: stato [%d]" , SSL_get_shutdown(ssl));

    
    /*
    .-------------------------------------------------------------------------.
    | PASSO 2. Effettua lo SHUTDOWN della Connessione SSL.                    |
    +-------------------------------------------------------------------------+
    | Chiude la sessione SSL aperta precedentemente dalla funzione SSL_new.   |
    | La funzione ritorna 0 se lo shutdown al momento non è possibile, forse  |
    | perchè è in corso un'operazione, e quindi bisogna aspettare o riprovare |
    | (non l'ho capito) fintanto non restituisce un valore differente.        |
    | La funzione ritorna 1, invece, se l'applicazione remota ha terminato    |
    | l'operazione di shutdown richiesta. Esiste una differenza tra versioni  |
    | di SSL del significato di questo valore però. Nella versione SSLv3 e    |
    | TLSv1 1 significa che entrambi (quindi noi e l'applicazione remota)     |
    | hanno concluso lo shutdown mentre con SSLv2 il valore 1 significa che   |
    | lo ha terminato almeno il sistema remoto. E' possibile inoltre ricevere |
    | dalla funzione anche il valore -1 che è da considerare un errore. In    | 
    | quel caso far riferimento alla SSL_get_error per recuperare maggiori    |
    | dettagli sulle cause. Il problema dello shutdown che tornava sempre 0   |
    | con SSLv3/TLSv1 è stato risulto con l'aggiunta di API get_set_shutdown  |
    | al passo precedente. L'argomento non è proprio chiarissimo.             |
    '-------------------------------------------------------------------------' */ 
    int numero_tentativo          = 0 ;
    int numero_tentativi_previsti = __MAX_TENTATIVI_SHUTDOWN__ ;
    int RetSSLShutdown            = 0 ;
    while ( RetSSLShutdown == 0 && numero_tentativo < numero_tentativi_previsti )
    {
            RetSSLShutdown = SSL_shutdown(ssl);
            numero_tentativo++; 

            /* ----- debug ------- */
            if (__DEBUG__ == 1 && debug_level == __INFO__)
                 printf("\nshutdown - tentativo [%d] esito [%d]", numero_tentativo, RetSSLShutdown);

            switch ( RetSSLShutdown ) 
            {
                     /* 
                     .----------------------------------------------------------.
                     | Terminato lo shutdown. Per SSLv2 almeno sul sistema      |
                     | remoto. Per SSLv3/TLSv1 su entrambi i sistemi.           |
                     | Continua con la SSL_free                                 |
                     '----------------------------------------------------------' */
                     case  1: PrintMessage(debug_level, SSL_TABMSG, "SSL_DISCONNECT_OK", TabVariabiliEMPTY, 0, 0, 0);
                              rSSLDisconnect.return_code =  0;
                              break; 
                     /* 
                     .-------------------------------------------------------.
                     | Dopo aver provato n volte (numero tentativi_previsti) | 
                     | viene risposto che in questo momento non è possibile  |
                     | chiudere la sessione SSL. Non può continuare potrebbe | 
                     | interrompere qualche trasmissione in corso            |
                     '-------------------------------------------------------' */
                     case  0: PrintMessage(debug_level, SSL_TABMSG, "SSL_DISCONNECT_SHUTDOWN_WAIT_ERROR", TabVariabiliEMPTY, 0, 0, 0); 
                              rSSLDisconnect.idmsg   = 0 ;
                              rSSLDisconnect.msgDesc = malloc(strlen("SSL.DISCONNECT.SHUTDOWN.WAIT.ERROR"));
                              rSSLDisconnect.msgDesc = "SSL.DISCONNECT.SHUTDOWN.WAIT.ERROR"; 
                              rSSLDisconnect.return_code = +1; 
                              break;
                     /* 
                     .--------------------------------------------------------.
                     | Errore Generico. E' necessario scendere nel dettaglio  |
                     | dell'errore interpellando la SSL_error                 |
                     '--------------------------------------------------------' */ 
                     case -1: PrintMessage(debug_level, SSL_TABMSG, "SSL_DISCONNECT_GENERIC_ERROR", TabVariabiliEMPTY, 0, 0, 0);
                              rSSLDisconnect.idmsg   = -1 ;
                              rSSLDisconnect.msgDesc = malloc(strlen("SSL.DISCONNECT.GENERIC.ERROR"));
                              rSSLDisconnect.msgDesc = "SSL.DISCONNECT.GENERIC.ERROR";  
                              rSSLDisconnect.return_code = -1; 
                              break;
                     /* 
                     .---------------------------------------------------------.
                     | Nessun altro valore è previsto. E' necessario indagare  |
                     | anche quì come al punto precedente                      |
                     '---------------------------------------------------------' */
                     default: PrintMessage(debug_level, SSL_TABMSG, "SSL_DISCONNECT_SHUTDOWN_ERROR", TabVariabiliEMPTY, 0, 0, 0); 
                              rSSLDisconnect.idmsg   = 0 ;
                              rSSLDisconnect.msgDesc = malloc(strlen("SSL.DISCONNECT.SHUTDOWN.ERROR"));
                              rSSLDisconnect.msgDesc = "SSL.DISCONNECT.SHUTDOWN.ERROR";  
                              rSSLDisconnect.return_code = -2;
                              break; 
            }; 


            if ( RetSSLShutdown < 0 ) 
            { 
                 rSSLDisconnect.idmsg = SSL_get_error (ssl, RetSSLShutdown);
                 switch ( rSSLDisconnect.idmsg )
                 {
                          case SSL_ERROR_NONE         : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_NONE", TabVariabiliEMPTY, 0, 0, 0       );
                                                        rSSLDisconnect.idmsg   = SSL_ERROR_NONE ;
                                                        rSSLDisconnect.msgDesc = malloc(strlen("SSL.DISCONNECT.OK"));
                                                        rSSLDisconnect.msgDesc = "SSL.DISCONNECT.OK";
                                                        break;

                          case SSL_ERROR_SSL          : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_SSL", TabVariabiliEMPTY, 0, 0, 0        );
                                                        rSSLDisconnect.idmsg   = SSL_ERROR_SSL ;
                                                        rSSLDisconnect.msgDesc = malloc(strlen("SSL.DISCONNECT.SSL.ERROR"));
                                                        rSSLDisconnect.msgDesc = "SSL.DISCONNECT.SSL.ERROR";
                                                        break;

                          case SSL_ERROR_WANT_READ    : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_WANT_READ", TabVariabiliEMPTY, 0, 0, 0  );
                                                        rSSLDisconnect.idmsg   = SSL_ERROR_WANT_READ ;
                                                        rSSLDisconnect.msgDesc = malloc(strlen("SSL.DISCONNECT.WANT.READ.ERROR"));
                                                        rSSLDisconnect.msgDesc = "SSL.DISCONNECT.WANT.READ.ERROR";
                                                        break;

                          case SSL_ERROR_WANT_WRITE   : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_WANT_WRITE", TabVariabiliEMPTY, 0, 0, 0);
                                                        rSSLDisconnect.idmsg   = SSL_ERROR_WANT_WRITE ;
                                                        rSSLDisconnect.msgDesc = malloc(strlen("SSL.DISCONNECT.WANT.WRITE.ERROR"));
                                                        rSSLDisconnect.msgDesc = "SSL.DISCONNECT.WANT.WRITE.ERROR";
                                                        break;

                          case SSL_ERROR_SYSCALL      : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_SYSCALL", TabVariabiliEMPTY, 0, 0, 0);
                                                        rSSLDisconnect.idmsg   = SSL_ERROR_SYSCALL ;
                                                        rSSLDisconnect.msgDesc = malloc(strlen("SSL.DISCONNECT.SYSCALL.ERROR"));
                                                        rSSLDisconnect.msgDesc = "SSL.DISCONNECT.SYSCALL.ERROR";
                                                        break;

                          case SSL_ERROR_ZERO_RETURN  : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_ZERO_RETURN", TabVariabiliEMPTY, 0, 0, 0);
                                                        rSSLDisconnect.idmsg   = SSL_ERROR_ZERO_RETURN ;
                                                        rSSLDisconnect.msgDesc = malloc(strlen("SSL.DISCONNECT.ZERO.RETURN.ERROR"));
                                                        rSSLDisconnect.msgDesc = "SSL.DISCONNECT.ZERO.RETURN.ERROR";
                                                        break;

                          case SSL_ERROR_WANT_CONNECT : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_WANT_CONNECT", TabVariabiliEMPTY, 0, 0, 0);
                                                        rSSLDisconnect.idmsg   = SSL_ERROR_WANT_CONNECT ;
                                                        rSSLDisconnect.msgDesc = malloc(strlen("SSL.DISCONNECT.WANT.CONNECT.ERROR"));
                                                        rSSLDisconnect.msgDesc = "SSL.DISCONNECT.WANT.CONNECT.ERROR";
                                                        break;

                          default                     : PrintMessage(debug_level, SSL_TABMSG, "SSL_ERROR_UNKNOWN", TabVariabiliEMPTY, 0, 0, 0);
                                                        rSSLDisconnect.idmsg   = 0 ;
                                                        rSSLDisconnect.msgDesc = malloc(strlen("SSL.DISCONNECT.UNKNOWN.ERROR"));
                                                        rSSLDisconnect.msgDesc = "SSL.DISCONNECT.UNKNOWN.ERROR";
                                                        break;
                 };

            };

    }; 

    /*
    .------------------------------------------------------------------------.
    | PASSO 2. SSL Free                                                      |
    +------------------------------------------------------------------------+
    | Ritorna allo Z/TPF System la struttura SSL associata alla sessione SSL |
    | La funzione non ritorna alcun valore.                                  |
    '------------------------------------------------------------------------' */ 
    PrintMessage(debug_level, SSL_TABMSG, "SSL_DISCONNECT_SOCKET_DESTROY_OK", TabVariabiliEMPTY, 0, 0, 0);
    SSL_free(ssl);

    /*
    .------------------------------------------------------------------------.
    | PASSO 3. Handshake + Open Session SSL                                  |
    +------------------------------------------------------------------------+
    | Distrugge il nostro contesto SSL.                                      |
    |                                                                        |
    '------------------------------------------------------------------------' */
    SSLDestroyContext (ctx, debug_level); 


    if ( rSSLDisconnect.return_code == 0 ) 
         PrintMessage(debug_level, SSL_TABMSG, "SSL_DISCONNECT_OK", TabVariabiliEMPTY, 0, 0, 0);


    return rSSLDisconnect ;
};



/*
.-------------------------------------------------------------------------------------.
|                                                                                     |
|  Funzione    : SSLClientConnect                                                     |
|                                                                                     |
|  Descrizione :                                                                      |
|                                                                                     |
|  Return-code :  0 Ok                                                                |
|                -1 TCP Connect Error                                                 |
|                -2 SSL Connect Error                                                 |
|                -3 SSL Authentication Server Error                                   |
|                -4 Send TCP|SSL Error                                                |
|                -5 Receive TCP|SSL Error                                             |
|                -6 SSL Disconnect Error                                              |
|                -7 TCP Disconnect                                                    |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
|                                                                                     |
'-------------------------------------------------------------------------------------'*/
extern struct tClientConnect SSLClientConnect ( struct tClientConnectParameter rClientConnectParameter ) 
{
    /*
    .----------------------------------------------------.
    | Dichiarazione delle strutture utilizzate           |
    '----------------------------------------------------' */
    struct tTCPConnect           rTCPConnect           ;
    struct tTCPConnect          *pTCPConnect           ;

    struct tSSLConnect           rSSLConnect           ; 
    struct tSSLConnect          *pSSLConnect           ;

    struct tSSLDoAuthentication  rSSLDoAuthentication  ;
    struct tSSLDoAuthentication *pSSLDoAuthentication  ;

    struct tSSLSend              rSSLSend              ;
    struct tSSLSend             *pSSLSend              ;

    struct tSSLReceive           rSSLReceive           ;
    struct tSSLReceive          *pSSLReceive           ;

    struct tSSLDisconnect        rSSLDisconnect        ;
    struct tSSLDisconnect       *pSSLDisconnect        ;

    struct tTCPDisconnect        rTCPDisconnect        ;
    struct tTCPDisconnect       *pTCPDisconnect        ;

    struct tClientConnect        rClientConnect        ;
    struct tClientConnect       *pClientConnect        ;

    /*
    .------------------------------------.
    | Allocazione Spazio                 |
    '------------------------------------' */
    pTCPConnect            = malloc(sizeof(struct tTCPConnect));
    pSSLConnect            = malloc(sizeof(struct tSSLConnect));
    pSSLSend               = malloc(sizeof(struct tSSLSend));
    pSSLReceive            = malloc(sizeof(struct tSSLReceive));
    pSSLDisconnect         = malloc(sizeof(struct tSSLDisconnect));   
    pTCPDisconnect         = malloc(sizeof(struct tTCPDisconnect));
    pClientConnect         = malloc(sizeof(struct tClientConnect));
 
    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pTCPConnect            = &rTCPConnect;
    pSSLConnect            = &rSSLConnect;
    pSSLDoAuthentication   = &rSSLDoAuthentication;
    pSSLSend               = &rSSLSend;
    pSSLReceive            = &rSSLReceive;
    pSSLDisconnect         = &rSSLDisconnect;
    pTCPDisconnect         = &rTCPDisconnect;
    pClientConnect         = &rClientConnect;

    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rTCPConnect            , 0, sizeof(rTCPConnect)          );
    memset((void*)&rSSLConnect            , 0, sizeof(rSSLConnect)          ); 
    memset((void*)&rSSLDoAuthentication   , 0, sizeof(rSSLDoAuthentication) );
    memset((void*)&rSSLSend               , 0, sizeof(rSSLSend)             ); 
    memset((void*)&rSSLReceive            , 0, sizeof(rSSLReceive)          ); 
    memset((void*)&rSSLDisconnect         , 0, sizeof(rSSLDisconnect)       ); 
    memset((void*)&rTCPDisconnect         , 0, sizeof(rTCPDisconnect)       );
    memset((void*)&rClientConnect         , 0, sizeof(rClientConnect)       ); 


    /*
    .------------------------------------------.
    |  Determina il livello di DEBUG           |
    '------------------------------------------' */
    int debug_level_ssl = GetDebugLevel ( rClientConnectParameter.debug, "ssl" ) ; 
    int debug_level_tcp = GetDebugLevel ( rClientConnectParameter.debug, "tcp" ) ;

    /*
    .---------------------------------------------------------------------------------------------.
    | PASSO 1: Difinisce default obbligati.                                                       |
    +---------------------------------------------------------------------------------------------+
    | Effettua la connessione TCP prima di tutto. Se va male inutile andare avanti.               |
    |                                                                                             |
    |                                                                                             |
    |                                                                                             |
    '---------------------------------------------------------------------------------------------' */
    if ( rClientConnectParameter.porta == 0 ) 
    {
         rClientConnectParameter.porta = 443 ;
         if ( debug_level_ssl == __INFO__ )
              printf("+Default SSL Port 443\n");
    };

    /*
    .---------------------------------------------------------------------------------------------.
    | PASSO 2: TCP Connect                                                                        |
    +---------------------------------------------------------------------------------------------+ 
    | Effettua la connessione TCP prima di tutto. Se va male inutile andare avanti.               |
    |                                                                                             |
    |                                                                                             |
    |                                                                                             |
    '---------------------------------------------------------------------------------------------' */
    rClientConnect.return_code = 0;
    rTCPConnect = TCPConnect(rClientConnectParameter.host, rClientConnectParameter.porta, 
										rClientConnectParameter.TCPReceiveTimeout, debug_level_tcp);
										
    rClientConnect.TCPIp             = rTCPConnect.ip ;
    rClientConnect.TCPPort           = rTCPConnect.port ;
    rClientConnect.TCPConnectMsg     = rTCPConnect.idmsg ;
    rClientConnect.TCPConnectMsgDesc = rTCPConnect.msgDesc ;
    rClientConnect.TCPConnectReturn  = rTCPConnect.return_code ;
    if ( rTCPConnect.return_code < 0)
    {
         rClientConnect.return_code = -1 ;
         return rClientConnect ;
    };

//==========================================
/*
         rSSLSend = SSLSend(rSSLConnect.ssl, rClientConnectParameter.message, debug_level_ssl );
         rClientConnect.ServerRequestBufferLen    = rSSLSend.requestLen;
         rClientConnect.ServerRequestBuffer       = rSSLSend.request;
         rClientConnect.ServerRequestMsg          = rSSLSend.idmsg;
         rClientConnect.ServerRequestMsgDesc      = rSSLSend.msgDesc;
         rClientConnect.ServerRequestReturn       = rSSLSend.return_code;
         if ( rSSLSend.return_code != 0)
         {
              rClientConnect.return_code = -4 ;
         };

         rSSLReceive = SSLReceive(rSSLConnect.ssl, rClientConnectParameter.OutputFile, debug_level_ssl );
         rClientConnect.ServerResponseBufferNum    = 1;
         rClientConnect.ServerResponseBufferLen    = rSSLReceive.responseLen;
         rClientConnect.ServerResponseBuffer       = rSSLReceive.response;
         rClientConnect.ServerResponseMsg          = rSSLReceive.idmsg;
         rClientConnect.ServerResponseMsgDesc      = rSSLReceive.msgDesc;
         rClientConnect.ServerResponseReturn       = rSSLReceive.return_code;
         if ( rSSLReceive.return_code != 0)
         {
              rClientConnect.return_code = -5 ;
         };
*/
//====================================================

    /*
    .--------------------------------------------------------------------------------------------.
    | PASSO 3: SSL Connect                                                                       |
    +--------------------------------------------------------------------------------------------+ 
    | Per connessione SSL si intende la creazione del contesto quindi la valorizzazione di tutti |
    | i parametri necessari (key, cert, CA, pwd, etc.) per poi intraprendere la fase di          |
    | handshake. Se il return-code è negativo qualcosa è andato male. Se la creazione del        |
    | contesto è fallita all'handshake nemmeno ci è arrivato. Questo vuol dire che uno dei       |
    | parametri passati dall'utente è errato. E' necessario in ogni caso uscire dopo aver chiuso |
    | la sessione TCP ed avvertire l'utente.                                                     |
    |                                                                                            |
    '--------------------------------------------------------------------------------------------' */
    rSSLConnect = SSLConnect(rTCPConnect.sock, rClientConnectParameter.keystore, rClientConnectParameter.password, 
                       rClientConnectParameter.truststore, rClientConnectParameter.ciphersuite, rClientConnectParameter.sslversion,
                              debug_level_ssl );
    rClientConnect.SSLVersion                                  = rSSLConnect.ssl_version                               ;
    strcpy(rClientConnect.SSLCipherSuite_Client_List_Proposals , rSSLConnect.ssl_ciphersuite_client_list_proposals)    ; 
    rClientConnect.SSLCipherSuite_Client_List_Proposals_Num    = rSSLConnect.ssl_ciphersuite_client_list_proposals_num ; 
    rClientConnect.SSLCipherSuite                              = rSSLConnect.ssl_ciphersuite                           ;
    rClientConnect.SSLCipherSuite_AlgSim_LenKey                = rSSLConnect.ssl_ciphersuite_algsim_lenkey             ;
    rClientConnect.SSLCipherSuite_AlgSim_SSLVersion            = rSSLConnect.ssl_ciphersuite_algsim_sslversion         ;
    strcpy(rClientConnect.SSLCipherSuite_NoUsed                , rSSLConnect.ssl_cipher_noused)                        ;
    rClientConnect.SSLCipherSuite_NoUsed_Num                   = rSSLConnect.ssl_cipher_noused_num                     ;
    rClientConnect.SSLConnectMsg                               = rSSLConnect.idmsg                                     ;
    rClientConnect.SSLConnectMsgDesc                           = rSSLConnect.msgDesc                                   ;
    rClientConnect.SSLConnectReturn                            = rSSLConnect.return_code                               ;
    if ( rSSLConnect.return_code < 0)
    {
         rClientConnect.return_code = -2 ;
         //return rClientConnect ;
    };

    /*
    .---------------------------------------------------------------------------------------------.
    | PASSO 4: Get SSL Certificate                                                                |
    +---------------------------------------------------------------------------------------------+
    | AUTENTICAZIONE SERVER: Visualizza l'esito di tale operazione, cioè se il certificato dopo   |
    | la connessione è arrivato o meno e nel caso non sia arrivato se è completamente corretto o  |
    | se invece presenta degli elementi di Warning quali ad esempio: [1] intestazione del         |
    | certificato non intestata alla macchina server contattata (NOCERTIFICATE) o [2] certificato |
    | scaduto|non ancora attivo|non nel formato X.509|etc (IDENTITY_ERROR)                        |
    '---------------------------------------------------------------------------------------------' */
    if ( rClientConnect.return_code == 0 )
    {
         rSSLDoAuthentication = SSLDoAuthentication ( rSSLConnect.ssl, rClientConnectParameter.host, debug_level_ssl );
         rClientConnect.SSLServerVerifyIdentityX509_Version                       = rSSLDoAuthentication.x509_Version                  ;
         rClientConnect.SSLServerVerifyIdentityX509_SerialNumber                  = rSSLDoAuthentication.x509_SerialNumber             ;
         rClientConnect.SSLServerVerifyIdentityX509_SignatureType                 = rSSLDoAuthentication.x509_SignatureType            ;
         strcpy(rClientConnect.SSLServerVerifyIdentityX509_IssuerName             , rSSLDoAuthentication.x509_IssuerName             ) ;
         strcpy(rClientConnect.SSLServerVerifyIdentityX509_NotBefore              , rSSLDoAuthentication.x509_NotBefore              ) ;
         strcpy(rClientConnect.SSLServerVerifyIdentityX509_NotAfter               , rSSLDoAuthentication.x509_NotAfter               ) ;
         strcpy(rClientConnect.SSLServerVerifyIdentityX509_SubjectName_commonname , rSSLDoAuthentication.x509_SubjectName_commonname ) ;
         rClientConnect.SSLServerVerifyIdentityX509_CertificateType               = rSSLDoAuthentication.x509_CertificateType          ;
         rClientConnect.SSLServerVerifyIdentityMsg                                = rSSLDoAuthentication.idmsg                         ;
         rClientConnect.SSLServerVerifyIdentityMsgDesc                            = rSSLDoAuthentication.msgDesc                       ; 
         rClientConnect.SSLServerVerifyIdentityReturn                             = rSSLDoAuthentication.return_code                   ;
         if ( rSSLDoAuthentication.return_code < 0)
         {
              rClientConnect.return_code = -3 ;
         };
    }; 

    /*
    .---------------------------------------------------------------------------------------------.
    | PASSO 5: Send Request                                                                       |
    +---------------------------------------------------------------------------------------------+
    |                                                                                             |
    |                                                                                             |
    |                                                                                             |
    |                                                                                             |
    |                                                                                             |
    '---------------------------------------------------------------------------------------------' */
    if ( rClientConnect.SSLConnectReturn == 0 && rClientConnectParameter.message != NULL )
    {
         rSSLSend = SSLSend(rSSLConnect.ssl, rClientConnectParameter.message, debug_level_ssl );
         rClientConnect.ServerRequestBufferLen    = rSSLSend.requestLen;
         rClientConnect.ServerRequestBuffer       = rSSLSend.request;
         rClientConnect.ServerRequestMsg          = rSSLSend.idmsg;
         rClientConnect.ServerRequestMsgDesc      = rSSLSend.msgDesc;
         rClientConnect.ServerRequestReturn       = rSSLSend.return_code;
         if ( rSSLSend.return_code != 0)
         {
              rClientConnect.return_code = -4 ;
         }; 
    };

    /*
    .---------------------------------------------------------------------------------------------.
    | PASSO 6: Receive Response                                                                   |
    +---------------------------------------------------------------------------------------------+
    |                                                                                             |
    |                                                                                             | 
    |                                                                                             | 
    |                                                                                             | 
    |                                                                                             | 
    '---------------------------------------------------------------------------------------------' */
    if ( rClientConnect.SSLConnectReturn == 0 && rClientConnect.ServerRequestReturn == 0 && rClientConnectParameter.message != NULL )
    {
         rSSLReceive = SSLReceive(rSSLConnect.ssl, rClientConnectParameter.OutputFile, debug_level_ssl );
         rClientConnect.ServerResponseBufferNum    = 1; 
         rClientConnect.ServerResponseBufferLen    = rSSLReceive.responseLen;
         rClientConnect.ServerResponseBuffer       = rSSLReceive.response;
         rClientConnect.ServerResponseMsg          = rSSLReceive.idmsg;
         rClientConnect.ServerResponseMsgDesc      = rSSLReceive.msgDesc;
         rClientConnect.ServerResponseReturn       = rSSLReceive.return_code;
         if ( rSSLReceive.return_code != 0)
         {
              rClientConnect.return_code = -5 ;
         };
    };

    /*
    .---------------------------------------------------------------------------------------------.
    | PASSO 7: SSL Disconnect                                                                     |
    +---------------------------------------------------------------------------------------------+
    |                                                                                             |
    |                                                                                             | 
    |                                                                                             | 
    |                                                                                             | 
    |                                                                                             | 
    '---------------------------------------------------------------------------------------------' */
    if ( rClientConnect.SSLConnectReturn == 0 )
    {
         rSSLDisconnect = SSLDisconnect(rSSLConnect.ctx, rSSLConnect.ssl, debug_level_ssl );
         rClientConnect.SSLDisconnectMsg     = rSSLDisconnect.idmsg;
         rClientConnect.SSLDisconnectMsgDesc = rSSLDisconnect.msgDesc;
         rClientConnect.SSLDisconnectReturn  = rSSLDisconnect.return_code;
         if ( rSSLDisconnect.return_code != 0)
         {
              rClientConnect.return_code = -6 ;
         };
    };

    /* 
    .---------------------------------------------------------------------------------------------.
    | PASSO 8: TCP Disconnect                                                                     |
    +---------------------------------------------------------------------------------------------+
    |                                                                                             |
    |                                                                                             |
    |                                                                                             |
    |                                                                                             |
    |                                                                                             |
    '---------------------------------------------------------------------------------------------' */
    rTCPDisconnect = TCPDisconnect(rTCPConnect.sock, debug_level_tcp );
    rClientConnect.TCPDisconnectMsg    = rTCPDisconnect.idmsg ;
    rClientConnect.TCPDisconnectReturn = rTCPDisconnect.return_code ;
    if ( rTCPDisconnect.return_code != 0)
    {
         rClientConnect.return_code = -7 ;
    };

    return rClientConnect ; 
};


// __EOF__
