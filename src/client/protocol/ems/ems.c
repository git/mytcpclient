/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/*
.--------------------.
| header LibC        |
'--------------------' */
#include <string.h>

/*
.-------------------------.
| header common           |
'-------------------------' */
#include "client.h"
#include "output.h"

/*
.--------------------.
| header Protocolli  |
'--------------------' */
#include "tcp.h"
#include "ssl.h"

/*
.--------------------.
| header Locale      |
'--------------------' */
#include "ems.h"
#include "ems_messages.h"


/*
.------------------------------------------------------------------------.
|                                                                        |
|  Funzione    : EMSConnect                                              |
|                                                                        |
|  Descrizione : minimal client TIBCO EMS (connect, disconnect)          |
|                                                                        |
|                                                                        |
|                                                                        |
|                                                                        |
|                                                                        |
|                                                                        |
'------------------------------------------------------------------------'*/
extern struct tClientConnect EMSClientConnect ( struct tClientConnectParameter rClientConnectParameter )
{
         struct tTCPConnect          rTCPConnect           ;
         struct tTCPSend             rTCPSend              ;
         struct tTCPReceive          rTCPReceive           ;
         struct tTCPDisconnect       rTCPDisconnect        ;

         struct tSSLConnect          rSSLConnect           ; 
         struct tSSLDoAuthentication rSSLDoAuthentication  ;
         struct tSSLSend             rSSLSend              ;
         struct tSSLReceive          rSSLReceive           ;
         struct tSSLDisconnect       rSSLDisconnect        ;

         struct tClientConnect       rClientConnect        ;


         /*
         .------------------.
         | Inizializzazioni |
         '------------------' */
         rClientConnect.return_code = 0;


         /*
         .-----------------------------------.
         | check connection                  |
         '-----------------------------------' */
         if ( rClientConnectParameter.auth_user == NULL ) {
              printf("The client did not specify any identity. *Warning* \n");
              printf("anonymous access.\n");
         }
         else
         {
              printf("The client did specify as identity [%s]\n", rClientConnectParameter.auth_user);
         };

         if ( strlen(rClientConnectParameter.risorsa) > 0 ) {
              printf("The client requested the access to resource [%s]\n", rClientConnectParameter.risorsa);
         };


         /*
         .------------------------------------------.
         |  Determina il livello di DEBUG           |
         '------------------------------------------' */
         int debug_level = GetDebugLevel ( rClientConnectParameter.debug, "ems" ) ; 


         /*
         .---------------------------------------------------------------------------------------------.
         | PASSO 1: TCP Connect                                                                        |
         +---------------------------------------------------------------------------------------------+
         | Effettua la connessione TCP prima di tutto. Se va male inutile andare avanti.               |
         |                                                                                             |
         |                                                                                             |
         |                                                                                             |
         '---------------------------------------------------------------------------------------------' */
         rClientConnect.return_code = 0;
         rTCPConnect = TCPConnect(rClientConnectParameter.host, rClientConnectParameter.porta, rClientConnectParameter.TCPReceiveTimeout, debug_level);
         rClientConnect.TCPIp             = rTCPConnect.ip ;
         rClientConnect.TCPConnectMsg     = rTCPConnect.idmsg ;
         rClientConnect.TCPConnectMsgDesc = rTCPConnect.msgDesc ;
         rClientConnect.TCPConnectReturn  = rTCPConnect.return_code ;
         if ( rTCPConnect.return_code != 0)
         {
              rClientConnect.return_code = -1 ;
              return rClientConnect ;
         };

         /*
         .---------------------------------------------------------------------------------------------.
         | PASSO 2: SSL Connect                                                                        |
         +---------------------------------------------------------------------------------------------+
         | SSL Connection and Verify Certificate X.509, Authentication.                                |
         |                                                                                             |
         |                                                                                             |
         |                                                                                             |
         '---------------------------------------------------------------------------------------------' */
         if ( strcmp ( rClientConnectParameter.protocollo, "emss" ) == 0 )
         {   
              /*
              .-------------.
              | SSL Connect |
              '-------------' */
              rSSLConnect = 
                  SSLConnect ( rTCPConnect.sock, rClientConnectParameter.keystore, rClientConnectParameter.password, 
                         rClientConnectParameter.truststore, rClientConnectParameter.ciphersuite, rClientConnectParameter.sslversion, debug_level ); 

              rClientConnect.SSLVersion                       = rSSLConnect.ssl_version                            ;

              strcpy(rClientConnect.SSLCipherSuite_Client_List_Proposals , 
                          rSSLConnect.ssl_ciphersuite_client_list_proposals) ; 

              rClientConnect.SSLCipherSuite                   = rSSLConnect.ssl_ciphersuite                        ;
              rClientConnect.SSLCipherSuite_AlgSim_LenKey     = rSSLConnect.ssl_ciphersuite_algsim_lenkey          ;
              rClientConnect.SSLCipherSuite_AlgSim_SSLVersion = rSSLConnect.ssl_ciphersuite_algsim_sslversion      ;
              rClientConnect.SSLConnectMsg                    = rSSLConnect.idmsg                                  ;
              rClientConnect.SSLConnectReturn                 = rSSLConnect.return_code                            ;
              /*
              if (rEMSClientConnect.SSLConnectReturn != 0) { 
                  rEMSClientConnect.return_code = -1 ;
                  return rEMSClientConnect ;
              }; */

              /*
              .------------------------.
              | SSL Verify Certificate |
              '------------------------' */
              rSSLDoAuthentication = SSLDoAuthentication(rSSLConnect.ssl, rClientConnectParameter.host, debug_level);
              rClientConnect.SSLServerVerifyIdentityX509_Version            = rSSLDoAuthentication.x509_Version          ;
              rClientConnect.SSLServerVerifyIdentityX509_SerialNumber       = rSSLDoAuthentication.x509_SerialNumber     ;
              rClientConnect.SSLServerVerifyIdentityX509_SignatureType      = rSSLDoAuthentication.x509_SignatureType    ;
              strcpy(rClientConnect.SSLServerVerifyIdentityX509_IssuerName  , rSSLDoAuthentication.x509_IssuerName      );
              strcpy(rClientConnect.SSLServerVerifyIdentityX509_NotBefore   , rSSLDoAuthentication.x509_NotBefore       );
              strcpy(rClientConnect.SSLServerVerifyIdentityX509_NotAfter    , rSSLDoAuthentication.x509_NotAfter        );
              strcpy(rClientConnect.SSLServerVerifyIdentityX509_SubjectName , rSSLDoAuthentication.x509_SubjectName     );
              rClientConnect.SSLServerVerifyIdentityX509_CertificateType    = rSSLDoAuthentication.x509_CertificateType  ;
              rClientConnect.SSLServerVerifyIdentityMsg                     = rSSLDoAuthentication.idmsg                 ;
              rClientConnect.SSLServerVerifyIdentityReturn                  = rSSLDoAuthentication.return_code           ;
              /*
              if (rClientConnect.SSLServerVerifyIdentityReturn != 0) {
                  rClientConnect.return_code = -1 ; 
                  return rClientConnect ; 
              }; */
         };

         //================ EMS HACK
         //================ EMS HACK
         //================ EMS HACK
         //================ EMS HACK

         /*
         .--------------------------------------------.
         | INIZIO: Test di connessione all'EMS Server |
         '--------------------------------------------' */
         /*        
         PASSO 1 
         unsigned char buffer1[]= "\x77\x99\xaa\xdd\x00\x00\x00\x00\x00\x00\x00\x00"; //77 99 aa dd 00 00 00 00 00 00 00 00  
         int  lenBuffer1 = 12;
         printf("\n[STEP01] Client -> Server :: SEND: %d bytes \n", lenBuffer1);
         dump(buffer1, lenBuffer1);
         TCPSend(rTCPConnect.sock, buffer1, lenBuffer1); 
         */

         /*
         PASSO 2
         unsigned char buffer2[]= "\x00";
         int  lenBuffer2 = 1024;
         int  recv_length2 = 0;
         recv_length2 = TCPReceive(rTCPConnect.sock, buffer2, lenBuffer2) ; 
         while ( recv_length2 > 0 )
         {
                 printf("\n[STEP02] Server -> Client :: RECV: %d bytes \n", recv_length2);
                 dump(buffer2, recv_length2); 
                 recv_length2 = 0; // TCPReceive(rConnessioneTCP.sock, buffer2, lenBuffer2) ;
         };
         */          

         /*
         PASSO 3 
         unsigned char buffer3[]= "\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x1c\x76"; //00 00 00 00 00 00 00 01 00 00 1c 76 
         int  lenBuffer3 = 12;
         printf("\n[STEP03] Client -> Server :: SEND: %d bytes [ERRATI ULTIMI 2 BYTE]\n", lenBuffer3);
         dump(buffer3, lenBuffer3);
         TCPSend(rTCPConnect.sock, buffer3, lenBuffer3);
         */

         /*
         PASSO 4 
         unsigned char buffer4[]= "\x00";
         int  lenBuffer4 = 1024;
         int  recv_length4 = 0;
         //recv_length4 = TCPReceive(rTCPConnect.sock, buffer4, lenBuffer4) ;
         while ( recv_length4 > 0 )
         {
                 printf("\n[STEP04] Server -> Client :: RECV: %d bytes \n", recv_length4);
                 dump(buffer4, recv_length4);
                 recv_length4 = TCPReceive(rTCPConnect.sock, buffer4, lenBuffer4) ;
         };
         */

         /*
         PASSO 5 
         unsigned char buffer5[]= "\x00\x00\x00\x89\x99\x55\xee\xaa\x0c\x00\x00\x00\x00\x17" 
                          "\x00\x00\x00\x77\x07\x63\x6c\x74\x79\x70\x65\x00\x09\x02\x43\x00" 
                          "\x05\x75\x73\x65\x72\x00\x09\x0a\x61\x6e\x6f\x6e\x79\x6d\x6f\x75" 
                          "\x73\x00\x07\x69\x70\x61\x64\x64\x72\x00\x05\x7f\x00\x00\x01\x06" 
                          "\x6d\x69\x6e\x6f\x72\x00\x04\x00\x02\x07\x76\x62\x75\x69\x6c\x64" 
                          "\x00\x04\x00\x0c\x06\x63\x74\x79\x70\x65\x00\x05\x00\x00\x00\x00"
                          "\x07\x75\x70\x64\x61\x74\x65\x00\x04\x00\x00\x06\x6d\x61\x6a\x6f"
                          "\x72\x00\x04\x00\x04\x05\x68\x6f\x73\x74\x00\x09\x0e\x6a\x61\x63"
                          "\x6b\x30\x65\x2d\x6c\x61\x70\x74\x6f\x70\x00";
         int  lenBuffer5 = 137;
         printf("\n[STEP05] Client -> Server :: SEND: %d bytes \n", lenBuffer5);
         dump(buffer5, lenBuffer5); 
         TCPSend(rTCPConnect.sock, buffer5, lenBuffer5);
         */

         /*
         PASSO 6 
         unsigned char buffer6[]= "\x00";
         int  lenBuffer6 = 1024;
         int  recv_length6 = 0;
         recv_length6 = TCPReceive(rTCPConnect.sock, buffer6, lenBuffer6) ;
         while ( recv_length6 > 0 )
         {
                 printf("\n[STEP06] Server -> Client :: RECV: %d bytes \n", recv_length6);
                 dump(buffer6, recv_length6); 
                 recv_length6 = TCPReceive(rTCPConnect.sock, buffer6, lenBuffer6) ;
         };
         */

         //================ EMS HACK
         //================ EMS HACK
         //================ EMS HACK
         //================ EMS HACK
         //================ EMS HACK


         ///* 
         //.----------------------.
         //| Build Request String |
         //'----------------------' */ 
         //char *request;
         //char *request_template = "GET %s%s EMS/1.1\n"
         //                         "User-Agent: myClientTCP\n"
         //                         "Host: %s:%d\n\n";
         //
         //request  = (char *)malloc(1024);
         //
         //snprintf(request, 1024, request_template, risorsa_path, risorsa, host, porta); 
         //
         ///*
         //.---------------------------.
         //| Request and Response EMS |
         //'---------------------------' */ 
         //if (strcmp ( protocollo, "EMS"  ) == 0)
         //{
         //    /*
         //    .--------------.
         //    | Send Request |
         //    '--------------' */
         //    rTCPSend = TCPSend (rTCPConnect.sock, request, 0); 
         //    rEMSClientConnect.EMSRequestBuffer       = rTCPSend.request;
         //    rEMSClientConnect.EMSRequestMsg          = rTCPSend.msg;
         //    rEMSClientConnect.EMSRequestReturn       = rTCPSend.return_code;
         //
         //    /*
         //    .------------------.
         //    | Receive Response |
         //    '------------------' */
         //    rTCPReceive = TCPReceive (rTCPConnect.sock); 
         //    rEMSClientConnect.EMSResponseBufferNum   = 1;
         //    rEMSClientConnect.EMSResponseBuffer      = rTCPReceive.response;
         //    rEMSClientConnect.EMSResponseMsg         = rTCPReceive.msg;
         //    rEMSClientConnect.EMSResponseReturn      = rTCPReceive.return_code;
         //};
         //
         //if (strcmp ( protocollo, "emss" ) == 0)
         //{
         //    /*
         //    .--------------.
         //    | Send Request |
         //    '--------------' */
         //    rSSLSend = SSLSend(rSSLConnect.ssl, request);
         //    rEMSClientConnect.EMSRequestBuffer       = rSSLSend.request;
         //    rEMSClientConnect.EMSRequestMsg          = rSSLSend.msg;
         //    rEMSClientConnect.EMSRequestReturn       = rSSLSend.return_code; 
         //
         //    /*
         //    .------------------.
         //    | Receive Response |
         //    '------------------' */
         //    rSSLReceive = SSLReceive(rSSLConnect.ssl);
         //    rEMSClientConnect.EMSResponseBufferNum   = 1;
         //    rEMSClientConnect.EMSResponseBuffer      = rSSLReceive.response;
         //    rEMSClientConnect.EMSResponseMsg         = rSSLReceive.msg;
         //    rEMSClientConnect.EMSResponseReturn      = rSSLReceive.return_code;
         //};

         /*
         .-----------------------------------------------------------------.
         |                                                                 |
         |                                                                 |
         |                           SSL Disconnect                        |
         |                                                                 |
         |                                                                 |
         '-----------------------------------------------------------------' */
         if ( strcmp ( rClientConnectParameter.protocollo, "emss" ) == 0)
         {
              if ( rClientConnect.SSLConnectReturn == 0 )
              {
                   rSSLDisconnect = SSLDisconnect(rSSLConnect.ctx, rSSLConnect.ssl, debug_level );
                   rClientConnect.SSLDisconnectMsg     = rSSLDisconnect.idmsg;
                   rClientConnect.SSLDisconnectMsgDesc = rSSLDisconnect.msgDesc;
                   rClientConnect.SSLDisconnectReturn  = rSSLDisconnect.return_code;
                   if ( rSSLDisconnect.return_code != 0)
                   {
                        rClientConnect.return_code = -1 ;
                   };
              };
         };

         /*
         .-----------------------------------------------------------------.
         |                                                                 |
         |                                                                 |
         |                            TCP Disconnect                       |
         |                                                                 |
         |                                                                 |
         '-----------------------------------------------------------------' */
         rTCPDisconnect = TCPDisconnect(rTCPConnect.sock, debug_level);
         rClientConnect.TCPDisconnectMsg    = rTCPDisconnect.idmsg ;
         rClientConnect.TCPDisconnectReturn = rTCPDisconnect.return_code ;
         if ( rTCPDisconnect.return_code != 0)
         {
              rClientConnect.return_code = -1 ;
         };

         return rClientConnect ;
};


