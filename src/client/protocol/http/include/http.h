/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#define __HTTP_LEN_BUFFER_REQUEST__ 1024
#define __HTTP_VERSION__            1.0 

#define __DEFAULT_HTTP_PORT__       80
#define __DEFAULT_HTTPS_PORT__      443

/*
.----------------------------------------------------.
|                                                    |
|                    Client minimale                 |
|             (connessione/disconnessione)           |
|                                                    |
'----------------------------------------------------' */
extern struct tClientConnect HTTPClientConnect ( struct tClientConnectParameter rClientConnectParameter ); 

/*
.----------------------------------------------------.
|                                                    |
|                    Client minimale                 |
|             (connessione/disconnessione)           |
|                                                    |
'----------------------------------------------------' */
struct tHTTPHeaderProperties
{
       char variable[1024] ;
       char value[1024]    ;
};

struct tHTTPPrintHeader 
{
       struct tHTTPHeaderProperties HTTPProperties[100] ; 
       int  HeaderLen ;
       int  BodyLen ;
       char Protocol[10];
       char ProtocolVersion[10];
       char RetRequestCode[10];
       char RetRequestMsg[2048];
       int  HTTPPropertiesDim;
       long idmsg;
       char *msgDesc;
       int  return_code ;
};

extern struct tHTTPPrintHeader HTTPPrintHeader ( char *HTTPFile, int HTTPFileLen, int debug_level ); 

//  __EOF__ 
