/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 


#define __NONE__    0
#define __INFO__    1
#define __WARN__    2
#define __ERR__     3
 
/*
.--------------------------------------------------------------------------------------------.
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                   HTTP Messages Table                                      |
|                                                                                            |
|                                                                                            |
|                                                                                            |
|                                                                                            |
'--------------------------------------------------------------------------------------------' */
char *HTTP_TABMSG[][3] = {

                     /*
                     .---------------------------------.
                     |                                 |
                     |           HTTP Client           |
                     |                                 |
                     '---------------------------------' */

                     { "HTTP_CLIENT_CHECK_PARAMETERS",
                       "__INFO__",
                       "HTTP::Check Parameters...\n" },  

                     { "HTTP_CLIENT_BUILD_REQUEST",
                       "__INFO__",
                       "HTTP::Build the request...\n" },

                     { "HTTP_CLIENT_SEND_REQUEST",
                       "__INFO__",
                       "HTTP::Send Request to Server...\n" },

                     { "HTTP_CLIENT_RECEIVE_RESPONSE",
                       "__INFO__",
                       "HTTP::Received Response from Server.\n" },

                     { "HTTP_CLIENT_TCP_CONNECT_ERROR", 
                       "__INFO__",
                       "HTTP::Server TCP Connection. Error.\n" },

                     { "HTTP_CLIENT_SSL_CONNECT_ERROR",  
                       "__INFO__",
                       "HTTP::Server SSL Connection. Error.\n" },

                     { "HTTP_CLIENT_SSL_AUTH_SERVER_ERROR", 
                       "__INFO__",
                       "HTTP::Server SSL Authentication. Error.\n" },

                     { "HTTP_CLIENT_SEND_REQUEST_ERROR", 
                       "__INFO__",
                       "HTTP::Server Send Request. Error.\n" },

                     { "HTTP_CLIENT_RECEIVE_RESPONSE_ERROR", 
                       "__INFO__",
                       "HTTP::Server Receive Response. Error.\n" },

                     { "HTTP_CLIENT_SSL_DISCONNECT_ERROR", 
                       "__INFO__",
                       "HTTP::Server SSL Disconnect Error.\n" },

                     { "HTTP_CLIENT_TCP_DISCONNECT_ERROR", 
                       "__INFO__",
                       "HTTP::Server TCP Disconnect Error.\n" },

                     { "HTTP_CLIENT_UNKNOWN_ERROR", 
                       "__INFO__",
                       "HTTP::Unknown Error.\n" },

                     { "HTTP_CLIENT_RECEIVE_RESPONSE",
                       "__INFO__",
                       "HTTP::Received Response from Server.\n" },

                     { "HTTP_CLIENT_PARSEHEADER",
                       "__INFO__",
                       "HTTP::Parsing header...\n" },

                     { "HTTP_CLIENT_PRINTHEADER",
                       "__INFO__",
                       "HTTP::Printing header...\n" },



                     /*
                     .---------------------------------.
                     |                                 |
                     |        HTTP Parse Header        |
                     |                                 |
                     '---------------------------------' */

                     { "HTTP_PARSEHEADER_HEADERFILE_OPEN_ERROR",
                       "__INFO__",
                       "HTTP::I cant open the header file. Error.\n" },

                     { "HTTP_PARSEHEADER_NOHTTPHEADER_ERROR",
                       "__INFO__",
                       "HTTP::Not found http header. Error.\n" },

                     { "HTTP_PARSEHEADER_HTTPVERSION_ERROR",
                       "__INFO__",
                       "HTTP::Not found separate simbol between 'HTTP' and protocol version. Error.\n" },

                     { "HTTP_PARSEHEADER_RESPONSEEMPTY_ERROR",
                       "__INFO__",
                       "HTTP::The Response from Server is Empty. Error.\n" },

                     { "HTTP_PARSEHEADER_UNKNOWNHEADER_ERROR",
                       "__INFO__",

                       "HTTP::The Response is too short (less of 4 caracters) Error.\n" },

                     { "HTTP_PARSEHEADER_ENDHEADERNOTFOUND_ERROR",
                       "__INFO__",
                       "HTTP::The header is not terminate. Missing End Header. Error.\n" },



                     { "END", 
                       "__NONE__",
                       "End of Message Table. *Warning* Message not found.\n" }

    }; 


