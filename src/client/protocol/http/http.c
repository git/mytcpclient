/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/*
.--------------------.
| header LibC        |
'--------------------' */
#include <string.h>

/*
.-------------------------.
| header common           |
'-------------------------' */
#include "client.h"
#include "output.h"

/*
.--------------------.
| header Protocolli  |
'--------------------' */
#include "tcp.h"
#include "ssl.h"

/*
.--------------------.
| header Locale      |
'--------------------' */
#include "http.h" 
#include "http_messages.h"



/*
.------------------------------------------------------------------------.
|                                                                        |
|  Funzione    : HTTPConnect                                             |
|                                                                        |
|  Descrizione : client HTTP minimale (connessione, disconnessione)      |
|                                                                        |
|                                                                        |
|                                                                        |
|                                                                        |
|                                                                        |
|                                                                        |
'------------------------------------------------------------------------'*/
extern struct tClientConnect HTTPClientConnect ( struct tClientConnectParameter rClientConnectParameter ) 
{
    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , "", ""                                  }
    };

    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tClientConnect rClientConnect;  
    struct tClientConnect *pClientConnect;

    /*
    .------------------------------------.
    | Allocazione Spazio                 |
    '------------------------------------' */
    pClientConnect = malloc(sizeof(struct tClientConnect));  

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pClientConnect = &rClientConnect; 

    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rClientConnect, 0, sizeof(rClientConnect)); 
    rClientConnect.return_code = 0;


    /*
    .------------------------------------------.
    | Determina il livello di DEBUG            |
    '------------------------------------------' */
    int debug_level = GetDebugLevel ( rClientConnectParameter.debug, "http"  ) ; 



    /*
    .------------------------------------------------------------------------------.
    | PASSO 1. Check Parameter                                                     |
    +------------------------------------------------------------------------------+
    | I parametri obbligatori che servono per la richiesta HTTP se non impostati   |
    | vengono valorizzati con dei default. Nel caso del metodo HTTP da utilizzare  |  
    | per costruire la richiesta viene utilizzato la seguente regola per poter     |
    | applicare un valore di default nel caso in cui il parametro non risulta      |
    | impostato: [1] se il messaggio non è impostato il metodo che verrà           |
    | utilizzato è quello GET. [2] se il messaggio è impostato per inviarlo al     |
    | server è necessario il metodo POST, quindi in questo caso viene utilizzato   |
    | il metodo POST. Viene visualizzata un information in modalità di debug nei   | 
    | casi: [a] metodo POST con messaggio a lunghezza ZERO [b] metodo GET|HEAD con |
    | messaggio a lunghezza maggiore di ZERO. Nel primo caso si sta utilizzando    |
    | un metodo (POST) per inviare messaggio ma il messaggio è vuoto e nel secondo |
    | si sta utilizzando un metodo per ricevere qualcosa dal server (GET|HEAD) ed  |
    | invece il messaggio è valorizzato. Il comportamento del server Errore|Meno   |
    | probabilmente dipende dalle singole implementazioni.                         |
    |                                                                              |
    '------------------------------------------------------------------------------' */

    PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_CHECK_PARAMETERS", TabVariabiliEMPTY, 0, 0, 0);

    /*
    .------------------------------------------.
    | FileOutput set Default                   |
    '------------------------------------------' */
    if ( rClientConnectParameter.OutputFile == NULL ) {
         rClientConnectParameter.OutputFile = malloc(1024);
         strcpy (rClientConnectParameter.OutputFile, "/tmp/.myTCPClient.OutputFile.txt");
         if ( debug_level == __INFO__ )
              printf("+Setting Default Datastore [%s] \n", rClientConnectParameter.OutputFile );
    };

    /*
    .-----------------------------------------.
    | HTTP Method set Default                 |
    '-----------------------------------------' */ 
    if ( rClientConnectParameter.type_operation == NULL ) 
    {
         if ( rClientConnectParameter.message == NULL ) 
         { 
              rClientConnectParameter.type_operation = malloc(strlen("GET\0"));
              strcpy ( rClientConnectParameter.type_operation , "GET\0" ); 
              if ( debug_level == __INFO__ )
                   printf("+Setting Default for %s Method [%s] because not specified parameters|message\n", 
                                    rClientConnectParameter.protocollo, rClientConnectParameter.type_operation );
         }
         else
         {
              rClientConnectParameter.type_operation = malloc(strlen("POST\0"));
              strcpy ( rClientConnectParameter.type_operation , "POST\0" );
              if ( debug_level == __INFO__ )
                   printf("+Setting Default for %s Method [%s] because specified parameters|message\n", 
                                    rClientConnectParameter.protocollo, rClientConnectParameter.type_operation );
         };
    };

    if ( strcmp ( rClientConnectParameter.type_operation , "GET"  ) != 0 && 
         strcmp ( rClientConnectParameter.type_operation , "HEAD" ) != 0 &&  
         strcmp ( rClientConnectParameter.type_operation , "POST" ) != 0 ) { 
         if ( debug_level == __INFO__ )
              printf("+Method specified: [%s] [not supported]. *Warning*\n", rClientConnectParameter.type_operation );
    }; 

    /*
    .-----------------------------------------.
    | Check Message (POST parameters)         |
    '-----------------------------------------' */
    if ( strcmp ( rClientConnectParameter.type_operation , "POST" ) == 0 )
         if ( rClientConnectParameter.message == NULL )
              if ( debug_level == __INFO__ )
                   printf ("+The Post Request not has parameters *Warning*\n");

    if ( strcmp ( rClientConnectParameter.type_operation , "HEAD" ) == 0 ||
         strcmp ( rClientConnectParameter.type_operation , "GET"  ) == 0 )
         if ( rClientConnectParameter.message != NULL )
              if ( debug_level == __INFO__ )
                   printf ("+Message: The Get|Head Request has *Not* Empty Message [%s]. *Warning*\n", rClientConnectParameter.message);


    /*
    .-----------------------------------------.
    | Risorsa|Path set Default                |
    '-----------------------------------------' */
    if ( strlen ( rClientConnectParameter.risorsa_path   ) == 0 )
    { 
         strcpy ( rClientConnectParameter.risorsa_path , "/" );

         if ( debug_level == __INFO__ )
              printf ("+Setting Default for Used Resource Path [%s]\n", rClientConnectParameter.risorsa_path);
    };





    /*
    .------------------------.
    | QueryString            |
    '------------------------' */
    char *query_string ;
    int   query_string_len  ;

    /*
    .------------------------.
    | Message                |
    '------------------------' */
    char *send_message ;
    int   send_message_len  ;

    /*
    .------------------------.
    | Template HTTP Message  |
    '------------------------' */
    char *request_template = "%s %s%s%s HTTP/1.0\r\n"
                             "User-Agent: %s/%s\r\n"
                             "Host: %s\r\n"
                           //"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=\r\n" 
                           //"Accept-Language: it,en-us;q=0.7,en;q=0.3\r\n" 
                           //"Accept-Encoding: gzip,deflate\r\n" 
                           //"Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q =0.7\r\n" 
                           //"Keep-Alive: 300\r\n" 
                           //"Connection: keep-alive\r\n" 
                           //"If-Modified-Since: Fri, 17 Apr 20 09 09:19:16 GMT\r\n" 
                           //"Cache-Control: max-age=0\r\n" 
                             "%s"
                             "%s"
                             "%s"
                             "\r\n"
                             "%s" ;


    int RetRequestCodeInt = 0 ;
    while ( RetRequestCodeInt == 0 || RetRequestCodeInt == 302 )
    { 
            /*
            .-------------------------------------------------------------.
            | PASSO 2. Imposta|Reimposta TCP/IP Port Default              |
            +-------------------------------------------------------------+
            |                                                             |
            |                                                             |
            |                                                             |
            |                                                             |
            '-------------------------------------------------------------' */
            if ( rClientConnectParameter.porta == 0 )
            {
                if ( strcmp ( rClientConnectParameter.protocollo, "http"   ) == 0 )
                     rClientConnectParameter.porta = __DEFAULT_HTTP_PORT__  ;

                if ( strcmp ( rClientConnectParameter.protocollo, "https"  ) == 0 )
                     rClientConnectParameter.porta = __DEFAULT_HTTPS_PORT__ ;

                PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_CHECK_PARAMETERS", TabVariabiliEMPTY, 0, 0, 0);
                if ( debug_level == __INFO__ )
                     printf("+Setting Default %s Port %d\n", rClientConnectParameter.protocollo, rClientConnectParameter.porta);
            };
            rClientConnect.TCPPort = rClientConnectParameter.porta ; 

            /*
            .-------------------------------------------------------------.
            | PASSO 3. Costruisce la richiesta HTTP                       |
            +-------------------------------------------------------------+
            |                                                             |
            |                                                             |
            |                                                             |
            |                                                             |
            '-------------------------------------------------------------' */
            PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_BUILD_REQUEST", TabVariabiliEMPTY, 0, 0, 0);


            /*
            .-------------------------------------------------------.
            | Build URI (path + resource + querystring)             |
            '-------------------------------------------------------' */
            /*
            .-------------------.
            | Build QueryString |
            '-------------------' */ 
            query_string = "";
            query_string_len = 0;
            if ( rClientConnectParameter.queryStringDimTabVariable > 0 )
            {    
                 query_string     = malloc ( rClientConnectParameter.queryStringLen + 1 )  ;
                 query_string_len = rClientConnectParameter.queryStringLen                 ;
                 int i = 0;
                 while ( i < rClientConnectParameter.queryStringDimTabVariable )
                 {  
                         /*
                         .--------------------------------------------------.
                         | Inserisce la prima volta l'inizio querystring[?] |
                         '--------------------------------------------------' */
                         if ( i == 0 ) strcpy (query_string, "?\0");

                         /*
                         .--------------------------------------------.
                         | Aggiunge la Variabile trovata alla stringa |
                         '--------------------------------------------' */
                         strcat (query_string, rClientConnectParameter.queryStringTabVar[i].variable);
                         strcat (query_string, "=\0");
                         if (rClientConnectParameter.queryStringTabVar[i].value!=NULL)  
                             strcat (query_string, rClientConnectParameter.queryStringTabVar[i].value);

                         /*
                         .------------------------------------------------.
                         | Aggiunge il carattere di separazione variabile |
                         '------------------------------------------------' */ 
                         if ( i < rClientConnectParameter.queryStringDimTabVariable - 1)
                              strcat (query_string, "&\0");

                         i++ ;
                 };
            };

            /*
            .-------------------------------------------------------.
            | Content                                               |
            '-------------------------------------------------------' */
            send_message = "";
            send_message_len = 0;
            if ( rClientConnectParameter.message != NULL )     
            {
                 send_message_len = strlen ( rClientConnectParameter.message ) ;
                 send_message = malloc ( send_message_len + 1 ) ;
                 strcpy ( send_message, rClientConnectParameter.message ) ;
            };

            /*
            .-------------------------------------------------------.
            | Content-Length                                        |
            '-------------------------------------------------------' */
            char *request_content_length = "";
            if ( send_message_len > 0 ) 
            { 
                 request_content_length = malloc ( strlen("Content-Length: %s\r\n") + sizeof(send_message_len) + 1 );
                 sprintf(request_content_length, "Content-Length: %d\r\n", send_message_len);
            };

            /*
            .-------------------------------------------------------.
            | Content-language (for example: en_us, it, etc.        |
            '-------------------------------------------------------' */
            char *request_content_language = "";
            if ( rClientConnectParameter.message_language != NULL )
            {
                 request_content_language = malloc ( strlen("Content-Language: %s\r\n") + strlen(rClientConnectParameter.message_language) + 1 );
                 sprintf(request_content_language, "Content-Language: %s\r\n", rClientConnectParameter.message_language);
            };

            /*
            .-------------------------------------------------------.
            | Content-type (for example: text/html, text/xml)       |
            '-------------------------------------------------------' */
            char *request_content_type = "";
            if ( rClientConnectParameter.message_type != NULL )
            {
                 request_content_type = malloc ( strlen("Content-Type: %s\r\n") + strlen(rClientConnectParameter.message_type) + 1 );
                 sprintf(request_content_type, "Content-Type: %s\r\n", rClientConnectParameter.message_type);
            };

            /*
            .--------------------.
            | Build HTTP Message |
            '--------------------' */ 
            /*
                la lunghezza dell'header adesso è fissa __HTTP_LEN_BUFFER_REQUEST__
                dovrebbe essere invece calcolata... no? altrimenti rischio
                di sforare
            */
            rClientConnectParameter.message = malloc ( __HTTP_LEN_BUFFER_REQUEST__ + query_string_len + send_message_len );
            snprintf(rClientConnectParameter.message,
                   __HTTP_LEN_BUFFER_REQUEST__ + query_string_len + send_message_len,
                              request_template,
                                    rClientConnectParameter.type_operation,
                                    rClientConnectParameter.risorsa_path,
                                    rClientConnectParameter.risorsa,
                                    query_string,
                                    rClientConnectParameter.frontend,
                                    rClientConnectParameter.frontend_version,
                                    rClientConnectParameter.host,
                                    request_content_language,
                                    request_content_type, 
                                    request_content_length,
                                    send_message);

            /*
            .--------------------.
            | Print HTTP Message |
            '--------------------' */
            if ( debug_level == __INFO__ )
            {
                 printf(".--------------------------------------------.\n");
                 printf("|          Richiesta Server Inviata          |\n");
                 printf("'--------------------------------------------'\n");
                 printf("[%s]\n", rClientConnectParameter.message);
                 printf(".--------------------------------------------.\n");
                 printf("'--------------------------------------------'\n");
            };

            /*
            .--------------------------------------------------------------.
            | PASSO 4: Send Request and Receive Response HTTP on TCP|SSL   |
            +--------------------------------------------------------------+
            |                                                              |
            |                                                              |
            |                                                              |
            |                                                              |
            '--------------------------------------------------------------' */
            PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_SEND_REQUEST", TabVariabiliEMPTY, 0, 0, 0);

            if ( strcmp ( rClientConnectParameter.protocollo, "http"  ) == 0 ) { 
                 if ( debug_level == __INFO__ )
                      printf("+invoking TCP Client\n");
                 rClientConnect = TCPClientConnect ( rClientConnectParameter );
            };

            if ( strcmp ( rClientConnectParameter.protocollo, "https" ) == 0 ) { 
                 if ( debug_level == __INFO__ )
                      printf("+invoking SSL Client\n");
                 rClientConnect = SSLClientConnect ( rClientConnectParameter );
            };

            /*
            .--------------------------------------------------------------.
            | PASSO 5: Receive Response HTTP on TCP|SSL                    |
            +--------------------------------------------------------------+
            |                                                              |
            |                                                              |
            |                                                              |
            |                                                              |
            '--------------------------------------------------------------' */
            PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_RECEIVE_RESPONSE", TabVariabiliEMPTY, 0, 0, 0);

            if ( strcmp ( rClientConnectParameter.protocollo, "http"  ) == 0 ) {
                 if ( debug_level == __INFO__ )
                      printf("+Terminated TCP Client\n");
            };
            
            if ( strcmp ( rClientConnectParameter.protocollo, "https" ) == 0 ) {
                 if ( debug_level == __INFO__ )
                      printf("+Terminated SSL Client\n");
            };

            /*
            .--------------------------------------------------------------.
            | PASSO 6: Esito Request|Response HTTP on TCP|SSL              |
            +--------------------------------------------------------------+
            |                                                              |
            |  Return-code :  0 Ok                                         |
            |                -1 TCP Connect Error                          |
            |                -2 SSL Connect Error                          |
            |                -3 SSL Authentication Server Error            |
            |                -4 Send TCP|SSL Error                         |
            |                -5 Receive TCP|SSL Error                      |
            |                -6 SSL Disconnect Error                       |
            |                -7 TCP Disconnect                             |
            |                                                              |
            |                                                              |
            |                                                              |
            '--------------------------------------------------------------' */
            if (debug_level == __INFO__) 
            {
                switch ( rClientConnect.return_code )
                {
                         case   0  : break; 

                         case  -1  : PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_TCP_CONNECT_ERROR", TabVariabiliEMPTY, 0, 0, 0);
                                     //rClientConnect.msgDesc = malloc(strlen("HTTP.CLIENT.TCP.CONNECT.ERROR"));
                                     //rClientConnect.msgDesc = "HTTP.CLIENT.TCP.CONNECT.ERROR";
                                     break;

                         case  -2  : PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_SSL_CONNECT_ERROR", TabVariabiliEMPTY, 0, 0, 0);
                                     //rClientConnect.msgDesc = malloc(strlen("HTTP.CLIENT.SSL.CONNECT.ERROR"));
                                     //rClientConnect.msgDesc = "HTTP.CLIENT.SSL.CONNECT.ERROR";
                                     break;

                         case  -3  : PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_SSL_AUTH_SERVER_ERROR", TabVariabiliEMPTY, 0, 0, 0);
                                     //rClientConnect.msgDesc = malloc(strlen("HTTP.CLIENT.SSL.AUTH.SERVER.ERROR"));
                                     //rClientConnect.msgDesc = "HTTP.CLIENT.SSL.AUTH.SERVER.ERROR";
                                     break;

                         case  -4  : PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_SEND_REQUEST_ERROR", TabVariabiliEMPTY, 0, 0, 0);
                                     //rClientConnect.msgDesc = malloc(strlen("HTTP.CLIENT.SEND.REQUEST.ERROR"));
                                     //rClientConnect.msgDesc = "HTTP.CLIENT.SEND.REQUEST.ERROR";
                                     break;
                         
                         case  -5  : PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_RECEIVE_RESPONSE_ERROR", TabVariabiliEMPTY, 0, 0, 0);
                                     //rClientConnect.msgDesc = malloc(strlen("HTTP.CLIENT.RECEIVE.RESPONSE.ERROR"));
                                     //rClientConnect.msgDesc = "HTTP.CLIENT.RECEIVE.RESPONSE.ERROR";
                                     break;
                         
                         case  -6  : PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_SSL_DISCONNECT_ERROR", TabVariabiliEMPTY, 0, 0, 0);
                                     //rClientConnect.msgDesc = malloc(strlen("HTTP.CLIENT.SSL.DISCONNECT.ERROR"));
                                     //rClientConnect.msgDesc = "HTTP.CLIENT.SSL.DISCONNECT.ERROR";
                                     break;
                         
                         case  -7  : PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_TCP_DISCONNECT_ERROR", TabVariabiliEMPTY, 0, 0, 0);
                                     //rClientConnect.msgDesc = malloc(strlen("HTTP.CLIENT.TCP.DISCONNECT.ERROR"));
                                     //rClientConnect.msgDesc = "HTTP.CLIENT.TCP.DISCONNECT.ERROR";
                                     break;

                         default   : PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_UNKNOWN_ERROR", TabVariabiliEMPTY, 0, 0, 0);
                                     //rClientConnect.msgDesc = malloc(strlen("HTTP.CLIENT.UNKNOWN.ERROR"));
                                     //rClientConnect.msgDesc = "HTTP.CLIENT.UNKNOWN.ERROR";
                                     break;
                }
            };

            if ( rClientConnect.return_code < 0 )
            {
                 return rClientConnect ;
            };


            /*
            .--------------------------------------------------------------.
            | PASSO 7: Esito Request HTTP on TCP|SSL                       |
            +--------------------------------------------------------------+
            | Se la risposta è stata ricevuta senza problemi, prova ad     |
            | estrapolare l'header HTTP memorizzato nel dbstore creato in  |
            | precedenza che conserva la response. Da questo cerca di      |
            | leggere la riga di esito e quindi il codice di ritorno       |
            |                                                              |
            '--------------------------------------------------------------' */
            PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_PARSEHEADER", TabVariabiliEMPTY, 0, 0, 0);

            struct tHTTPPrintHeader rHTTPPrintHeader ;
            rHTTPPrintHeader = HTTPPrintHeader ( rClientConnectParameter.OutputFile, rClientConnect.ServerResponseBufferLen, debug_level );
            rClientConnect.ServerResponseMsg     = rHTTPPrintHeader.idmsg;
            rClientConnect.ServerResponseMsgDesc = rHTTPPrintHeader.msgDesc;
            rClientConnect.ServerResponseReturn  = rHTTPPrintHeader.return_code;
            if ( rHTTPPrintHeader.return_code != 0)
            {
                 rClientConnect.return_code = -1 ;
                 goto HTTPClientConnect_EXIT ;
            };

            /*
            .--------------------------------------------------------------.
            | PASSO 8: Visualizza l'Esito ridotto                          |
            +--------------------------------------------------------------+
            | Dopo aver effettuato il parse dell'header, visualizza il     |
            | ret-code HTTP.                                               |
            |                                                              |
            |                                                              |
            |                                                              |
            '--------------------------------------------------------------' */
            if ( debug_level == __INFO__ ) 
            {
                 printf("+%s://%s:%d%s%s [%s] %s\n", 
                         rClientConnectParameter.protocollo, 
                         rClientConnectParameter.host,
                         rClientConnectParameter.porta,
                         rClientConnectParameter.risorsa_path,
                         rClientConnectParameter.risorsa,
                         rHTTPPrintHeader.RetRequestCode, 
                         rHTTPPrintHeader.RetRequestMsg ); 
            };

            /*
            .--------------------------------------------------------------.
            | PASSO 9: Visualizza Esito Request HTTP on TCP|SSL            |
            +--------------------------------------------------------------+
            | Se la risposta è stata ricevuta senza problemi, prova ad     |
            | estrapolare l'header HTTP memorizzato nel dbstore creato in  |
            | precedenza che conserva la response. Da questo cerca di      |
            | leggere la riga di esito e quindi il codice di ritorno       |
            |                                                              |
            '--------------------------------------------------------------' */
            PrintMessage(debug_level, HTTP_TABMSG, "HTTP_CLIENT_PRINTHEADER", TabVariabiliEMPTY, 0, 0, 0);

            if ( debug_level == __INFO__ ) 
            { 
                 printf(".--------------------------------------------.\n");
                 printf("|          Risposta Server ricevuta          |\n");
                 printf("'--------------------------------------------'\n");
               //printf("%s\n", rClientConnect.ServerResponseBuffer);

                 printf("         [statistic] |\n");
                 printf("                     |\n");
                 printf("             receive | %d\n", rClientConnect.ServerResponseBufferLen);

                 printf("             missing | %d\n", rHTTPPrintHeader.HeaderLen + rHTTPPrintHeader.BodyLen -
                                                       rClientConnect.ServerResponseBufferLen);

                 printf("              header | %d\n", rHTTPPrintHeader.HeaderLen);
                 printf("                body | %d\n", rHTTPPrintHeader.BodyLen);
                 printf("                     |\n");
                 printf("            [header] |\n");
                 printf("                     |\n");
                 printf("            protocol | %s\n", rHTTPPrintHeader.Protocol);
                 printf("             version | %s\n", rHTTPPrintHeader.ProtocolVersion);
                 printf("                code | [%s] %s\n", rHTTPPrintHeader.RetRequestCode, rHTTPPrintHeader.RetRequestMsg);
                 printf("                     |\n");
                 printf(" [header.properties] |\n");
                 printf("                     |\n");
                 int i = 0;
                 while (i < rHTTPPrintHeader.HTTPPropertiesDim)
                 {
                        printf("%20s | %s\n",
                               rHTTPPrintHeader.HTTPProperties[i].variable,
                               rHTTPPrintHeader.HTTPProperties[i].value );
                        i++;
                 };
                 printf(".--------------------------------------------.\n");
                 printf("'--------------------------------------------'\n");
                 printf("\n");
            };

            /*
            .--------------------------------------------------------------.
            | PASSO 10: Esito Request HTTP on TCP|SSL                      |
            +--------------------------------------------------------------+
            | Se la risposta è stata ricevuta senza problemi, prova ad     |
            | estrapolare l'header HTTP memorizzato nel dbstore creato in  |
            | precedenza che conserva la response. Da questo cerca di      |
            | leggere la riga di esito e quindi il codice di ritorno       |
            |                                                              |
            '--------------------------------------------------------------' */
            rClientConnect.ServerResponseMsg     = atoi(rHTTPPrintHeader.RetRequestCode); 

            rClientConnect.ServerResponseMsgDesc = malloc(strlen("HTTP.RECV.")+strlen(rHTTPPrintHeader.RetRequestMsg)+1);
            strcpy(rClientConnect.ServerResponseMsgDesc, "HTTP.RECV.");
            strcat(rClientConnect.ServerResponseMsgDesc, rHTTPPrintHeader.RetRequestMsg);

            RetRequestCodeInt = rClientConnect.ServerResponseMsg ; 
            switch ( RetRequestCodeInt ) 
            {
               case 200 :  /* OK */ 
                           rClientConnect.ServerResponseReturn  =  0 ; 
                           break;

               case 201 :  /* Created */ 
                           rClientConnect.ServerResponseReturn  = +1 ;
                           break;

               case 202 :  /* Accepted */ 
                           rClientConnect.ServerResponseReturn  = +1 ;
                           break;

               case 204 :  /* No Content */ 
                           rClientConnect.ServerResponseReturn  = +1 ; 
                           break;

               case 301 :  /* Moved Permanently */ 
                           rClientConnect.ServerResponseReturn  = +1 ; 

                           int indice = 0;
                           while ( indice < rHTTPPrintHeader.HTTPPropertiesDim )
                           {
                                   if ( strcasecmp ( rHTTPPrintHeader.HTTPProperties[indice].variable, "Location" ) == 0 )
                                   {
                                        if ( debug_level == __INFO__ )
                                             printf ("HTTP::Read from Header the New Location: [%s]\n", rHTTPPrintHeader.HTTPProperties[indice].value) ;

                                     // ==================================================================================================
                                        struct tUri2SingleFieldConnect  rUri2SingleFieldConnect ;
                                        struct tUri2SingleFieldConnect *pUri2SingleFieldConnect ;

                                        rUri2SingleFieldConnect = Uri2SingleFieldConnect(rHTTPPrintHeader.HTTPProperties[indice].value);
                                        if ( rUri2SingleFieldConnect.return_code == 0 )
                                        {
                                             rClientConnectParameter.protocollo                = rUri2SingleFieldConnect.protocollo                    ;
                                             rClientConnectParameter.host                      = rUri2SingleFieldConnect.host                          ;
                                             rClientConnectParameter.porta                     = rUri2SingleFieldConnect.porta                         ;
                                             rClientConnectParameter.risorsa_path              = rUri2SingleFieldConnect.risorsa_path                  ;
                                             rClientConnectParameter.risorsa                   = rUri2SingleFieldConnect.risorsa                       ;
                                             rClientConnectParameter.queryStringDimTabVariable = rUri2SingleFieldConnect.queryStringDimTabVariable     ;
                                             rClientConnectParameter.queryStringLen            = rUri2SingleFieldConnect.queryStringLen                ;
                                             int i = 0;
                                             while ( i < rClientConnectParameter.queryStringDimTabVariable )
                                             {
                                                     rClientConnectParameter.queryStringTabVar[i].variable =
                                                             rUri2SingleFieldConnect.queryStringTabVariable[i].variable ;
                                                     rClientConnectParameter.queryStringTabVar[i].value =
                                                            rUri2SingleFieldConnect.queryStringTabVariable[i].value ;
                                                     i++ ;
                                             };
                                        };
                                        rClientConnectParameter.message = send_message ;
                                     // ==================================================================================================
                                        break;

                                   };
                                   indice++;
                           };

                           break;

               case 302 :  /* Moved Temporarily */ 
                           rClientConnect.ServerResponseReturn  = +1 ;                            

                           indice = 0;
                           while ( indice < rHTTPPrintHeader.HTTPPropertiesDim )
                           {
                                   if ( strcasecmp ( rHTTPPrintHeader.HTTPProperties[indice].variable, "Location" ) == 0 )
                                   {
                                        if ( debug_level == __INFO__ ) 
                                             printf ("HTTP::Read from Header the New Location: [%s]\n", rHTTPPrintHeader.HTTPProperties[indice].value) ;

                                     // ==================================================================================================
                                        struct tUri2SingleFieldConnect  rUri2SingleFieldConnect ;
                                        struct tUri2SingleFieldConnect *pUri2SingleFieldConnect ;
                                     
                                        rUri2SingleFieldConnect = Uri2SingleFieldConnect(rHTTPPrintHeader.HTTPProperties[indice].value);
                                        if ( rUri2SingleFieldConnect.return_code == 0 )
                                        {
                                             rClientConnectParameter.protocollo                = rUri2SingleFieldConnect.protocollo                    ;
                                             rClientConnectParameter.host                      = rUri2SingleFieldConnect.host                          ;
                                             rClientConnectParameter.porta                     = rUri2SingleFieldConnect.porta                         ;
                                             rClientConnectParameter.risorsa_path              = rUri2SingleFieldConnect.risorsa_path                  ;
                                             rClientConnectParameter.risorsa                   = rUri2SingleFieldConnect.risorsa                       ;
                                             rClientConnectParameter.queryStringDimTabVariable = rUri2SingleFieldConnect.queryStringDimTabVariable     ;
                                             rClientConnectParameter.queryStringLen            = rUri2SingleFieldConnect.queryStringLen                ;
                                             int i = 0;
                                             while ( i < rClientConnectParameter.queryStringDimTabVariable ) 
                                             {  
                                                     rClientConnectParameter.queryStringTabVar[i].variable = 
                                                             rUri2SingleFieldConnect.queryStringTabVariable[i].variable ;
                                                     rClientConnectParameter.queryStringTabVar[i].value = 
                                                            rUri2SingleFieldConnect.queryStringTabVariable[i].value ;
                                                     i++ ;
                                             };
                                        };
                                        rClientConnectParameter.message = send_message ; 
                                     // ==================================================================================================
                                        break; 
                                        
                                   };
                                   indice++;
                           };

                           break;

               case 304 :  /* Not Modified */ 
                           rClientConnect.ServerResponseReturn  = +1 ;                            
                           break;

               case 400 :  /* Bad Request */ 
                           rClientConnect.ServerResponseReturn  = +1 ;                            
                           break;

               case 401 :  /* Unauthorized */ 
                           rClientConnect.ServerResponseReturn  = +1 ;                            
                           break;

               case 403 :  /* Forbidden */ 
                           rClientConnect.ServerResponseReturn  = +1 ;                            
                           break;

               case 404 :  /* Not Found */ 
                           rClientConnect.ServerResponseReturn  = +1 ;                            
                           break;

               case 500 :  /* Internal Server Error */ 
                           rClientConnect.ServerResponseReturn  = +1 ;                            
                           break;

               case 501 :  /* Not Implemented */ 
                           rClientConnect.ServerResponseReturn  = +1 ;                            
                           break;

               case 502 :  /* Bad Gateway */ 
                           rClientConnect.ServerResponseReturn  = +1 ;                            
                           break;

               case 503 :  /* Service Unavailable */ 
                           rClientConnect.ServerResponseReturn  = +1 ;                            
                           break;

               default  :  /* Unknown Error */
                           rClientConnect.ServerResponseReturn  = +1 ;                            
                           break;
            };

    }

HTTPClientConnect_EXIT:

    return rClientConnect ; 
};

/*
.------------------------------------------------------------------------.
|                                                                        |
|  Funzione    : HTTPPrintHeader                                         |
|                                                                        |
|  Descrizione : Read the HTTP temp file and extract the header.         |
|                The function move the header in array.                  |
|                                                                        |
|  Parametri   :  HTTPFile    [File che contiene la risposta server]     | 
|                 HTTPFileLen [Dimensione in BYTE della risposta server] | 
|                 debug_level [Livello di debug 1-3]                     |
|                                                                        |
|  Return-code :  0 Ok                                                   |
|                -1 Open Header File Error                               |
|                -2 No Response HTTP                                     |
|                -3 Header HTTP incorrect                                |
|                -4 Header File Empty                                    | 
|                -5 Not found start header                               |
|                -6 Not found end header                                 | 
|                                                                        |
|                                                                        |
|                                                                        |
|                                                                        |
'------------------------------------------------------------------------' */
extern struct tHTTPPrintHeader HTTPPrintHeader ( char *HTTPFile, int HTTPFileLen, int debug_level ) 
{  
       /*
       .------------------------------------------.
       | dichiarazione strutture messaggi         |
       '------------------------------------------' */
       char *TabVariabiliEMPTY[][3] =  {
               { "EOT"                    , "", ""                                  }
       };

       /*
       .----------------------------------.
       | Definizione strutture utilizzate |
       '----------------------------------' */
       struct tHTTPPrintHeader  rHTTPPrintHeader ;
       struct tHTTPPrintHeader *pHTTPPrintHeader ;
    
       /*
       .------------------------------------.
       | Allocazione Spazio                 |
       '------------------------------------' */
       pHTTPPrintHeader = malloc(sizeof(struct tHTTPPrintHeader));

       /*
       .------------------------------------.
       | Utilizzo dello spazio allocato     |
       '------------------------------------' */
       pHTTPPrintHeader = &rHTTPPrintHeader ;

       /*
       .-----------------------------.
       | Inizializzazione Strutture  |
       '-----------------------------' */
       memset((void*)&rHTTPPrintHeader, 0, sizeof(rHTTPPrintHeader));

       /*
       .----------------------------------------------------.
       | Open HTTP File                                     |
       '----------------------------------------------------' */
       FILE *pHTTPFile ;
       pHTTPFile = fopen(HTTPFile, "r");
       if (!pHTTPFile)
       {
           PrintMessage(debug_level, HTTP_TABMSG, "HTTP_PARSEHEADER_HEADERFILE_OPEN_ERROR", TabVariabiliEMPTY, 0, 0, 0);
           rHTTPPrintHeader.idmsg   = 0 ;
           rHTTPPrintHeader.msgDesc = malloc(strlen("HTTP.PARSEHEADER.HEADERFILE.OPEN.ERROR"));
           rHTTPPrintHeader.msgDesc = "HTTP.PARSEHEADER.HEADERFILE.OPEN.ERROR";
           rHTTPPrintHeader.return_code = -1;

           if ( debug_level == __INFO__ ) 
                printf("+for more details, see [%s]\n", HTTPFile); 

           goto HTTPPrintHeader_EXIT ;
       }

       /*
       .----------------------------------------------------.
       | Read HTTP File                                     |
       '----------------------------------------------------' */
       char carattere[1];
       int  byte          = 0;
       long byteTot       = 0;
       char frase[1024]      ;
       int  numero_frase  = 0;

       frase[0] = 0; 
       while (fread(carattere, 1, sizeof(carattere), pHTTPFile)) 
       {      
              byteTot++;

              /*
              .------------------------------.     
              | Accumulatore                 |     
              '------------------------------' */
              frase[byte]=carattere[0];
              frase[byte+1]='\0';
              byte++;

              /*
              .-------------------------------------------------------.
              | Se è stato trovato un carattere di CR (10) allora     |
              | analizza il buffer accumulato fino a questo momento.  |
              '-------------------------------------------------------' */
              if (carattere[0]==10) 
              {
                  /*
                  .------------------------------------------------.
                  | CASISTICA 1 : EMPTY ROW (END OF HEADER)        |
                  +------------------------------------------------+
                  | Se il buffer accumulato è di lunghezza 2 e     |
                  | contiene un LF (13) ed un CR (10) è terminato  | 
                  | l'header.                                      |
                  '------------------------------------------------' */
                  if ( strlen(frase) == 2 )
                       if ( frase[0] == 13 && frase [1] == 10 )
                       {
                            rHTTPPrintHeader.HeaderLen = byteTot ; 
                            break;
                       }

                  /*
                  .--------------------.
                  | Buffer pieno.      |
                  '--------------------' */
                  numero_frase++;

                  /*
                  .------------------------------------------------.
                  | CASISTICA 2: Prima RIGA * ESITO RICHIESTA HTTP |
                  +------------------------------------------------+
                  | Prima riga del file è l'esito della richiesta  |
                  |                                                |
                  '------------------------------------------------' */
                  if ( frase[0] != 0 && numero_frase == 1 )
                  {
                       /* 
                       .------------------------.
                       | Verifica il protocollo |
                       '------------------------' */ 
                       if ( strncmp ( frase, "HTTP", 4 ) != 0 )
                       {
                            PrintMessage(debug_level, HTTP_TABMSG, "HTTP_PARSEHEADER_NOHTTPHEADER_ERROR", TabVariabiliEMPTY, 0, 0, 0);
                            rHTTPPrintHeader.idmsg   = 0 ;
                            rHTTPPrintHeader.msgDesc = malloc(strlen("HTTP.PARSEHEADER.NOHTTPHEADER.ERROR"));
                            rHTTPPrintHeader.msgDesc = "HTTP.PARSEHEADER.NOHTTPHEADER.ERROR";
                            rHTTPPrintHeader.return_code = -2;

                            if ( debug_level == __INFO__ )
                                 printf("+for more details, see [%s]\n", HTTPFile);

                            goto HTTPPrintHeader_EXIT ;
                       };
                       
                       strcpy(rHTTPPrintHeader.Protocol,"HTTP\0"); 

                       /* 
                       .-------------------------------------------. 
                       | Verifica la presenza del separatore tra   |
                       | protocollo e versione utilizzata          |
                       '-------------------------------------------' */ 
                       if ( frase[4] != '/' )
                       {
                            PrintMessage(debug_level, HTTP_TABMSG, "HTTP_PARSEHEADER_HTTPVERSION_ERROR", TabVariabiliEMPTY, 0, 0, 0);
                            rHTTPPrintHeader.idmsg   = 0 ;
                            rHTTPPrintHeader.msgDesc = malloc(strlen("HTTP.PARSEHEADER.HTTPVERSION.ERROR"));
                            rHTTPPrintHeader.msgDesc = "HTTP.PARSEHEADER.HTTPVERSION.ERROR";
                            rHTTPPrintHeader.return_code = -3;

                            if ( debug_level == __INFO__ )
                                 printf("+for more details, see [%s]\n", HTTPFile);

                            goto HTTPPrintHeader_EXIT ;
                       };

                       /* 
                       .-------------------------------------------.
                       | estrapola versione del protocollo HTTP,   |
                       | esito della richiesta                     |
                       '-------------------------------------------' */     
                       int i = 5;
                       int j = 0;
                       int passo = 0;
                       while ( frase[i]!='\0' && frase[i]!=10 && frase[i]!=13 )
                       {
                               /*
                               .-----------------------------------.
                               | estrapola protocollo versione     |
                               '-----------------------------------' */ 
                               if (passo==0) {
                                   if (frase[i]!=' ') {
                                       rHTTPPrintHeader.ProtocolVersion[j]   = frase[i];
                                       rHTTPPrintHeader.ProtocolVersion[j+1] = '\0';
                                       j++;
                                   }
                                   else
                                   {
                                       passo=1;
                                       i++;
                                       j=0; 
                                   }; 
                               }; 

                               /* 
                               .---------------------------------.
                               | estrapola esito richiesta       |
                               '---------------------------------' */
                               if (passo==1) {
                                   if (frase[i]!=' ') {
                                       rHTTPPrintHeader.RetRequestCode[j] = frase[i];
                                       rHTTPPrintHeader.RetRequestCode[j+1] ='\0';
                                       j++;
                                   }
                                   else
                                   {
                                       passo=2;
                                       i++;
                                       j=0;
                                   }
                               };

                               /* 
                               .-------------------------------------.
                               | estrapola esito richiesta stringa   |
                               '-------------------------------------' */
                               if (passo==2) {
                                   rHTTPPrintHeader.RetRequestMsg[j] = frase[i];
                                   rHTTPPrintHeader.RetRequestMsg[j+1]='\0';
                                   j++; }; 
                           
                           i++; 
                       }; 
                  };
                  

                  /*
                  .------------------------------------------------.
                  | CASISTICA 3: Riga Header                       |
                  +------------------------------------------------+
                  | Analizza la RIGA se questa risulta *NON* vuota |
                  '------------------------------------------------' */
                  if ( frase[0] != 0 && numero_frase > 1 )
                  {
                       int i = 0; 
                       int j = 0;
                       int passo = 0;
                       char variabile[1024];
                       char valore[1024];
                       variabile[0] = 0;
                       valore[0] = 0;
                       while ( frase[i]!='\0' && frase[i]!=10 && frase[i]!=13 )
                       {
                           /* 
                           .-----------------------------.
                           | estrapola VARIABILE         |
                           '-----------------------------' */
                           if (passo==0) {  
                               if (frase[i]!=':') { 
                                   variabile[j] = frase[i];
                                   variabile[j+1]='\0';
                                   j++; 
                               } 
                               else
                               {
                                   passo=1;
                                   j=0;
                               }};

                           /* 
                           .-----------------------------------------.
                           | esclude separatore VARIABILE, VALORE    |
                           '-----------------------------------------' */
                           if (passo==1) {
                               if (frase[i] != ':' && frase[i] != ' ') { 
                                   passo=2;
                                   j=0; }};                                      

                           /* 
                           .----------------------.
                           | estrapola VALORE     |
                           '----------------------' */ 
                           if (passo==2) { 
                               valore[j]=frase[i];
                               valore[j+1]='\0';
                               j++;};

                           i++;
                       };

                       strcpy(rHTTPPrintHeader.HTTPProperties[rHTTPPrintHeader.HTTPPropertiesDim].variable , variabile );
                       strcpy(rHTTPPrintHeader.HTTPProperties[rHTTPPrintHeader.HTTPPropertiesDim].value    , valore    );

                       rHTTPPrintHeader.HTTPPropertiesDim++; 

                       /* 
                       .--------------------------------------------------------.
                       | Contect-Length è la lunghezza del DOCUMENTO HTML senza |
                       | Header, pertanto conoscendo la dimensione totale della |
                       | risposta, sottraendo a questa la Contect-Length si puo |
                       | ottenere la lunghezza dell'header. Lo standard HTTP    |
                       | mi semra che dice questo, ma devo verificare.          |  
                       '--------------------------------------------------------' */ 
                       if ( strcasecmp(variabile,"Content-Length") == 0 ) 
                            rHTTPPrintHeader.BodyLen = atoi(valore); 
                  }
    
                  /*
                  .----------------------------------------------.
                  | Inizializzazione delle variabili di appoggio |
                  '----------------------------------------------' */
                  byte=0;
                  frase[0]=0;
              }
       }; 



       /*
       .-------------------------------------------------------------------------------.
       | STEP 2: Final Check HTTP Header                                               |
       +-------------------------------------------------------------------------------+
       | L'analisi del file termina in 3 casi: [1] trovato errore, [2] fine header     |
       | (se trova una riga impostata solamente con una coppia di caratteri \r\n),     |
       | [3] End Of File. Nel primo caso il codice salta all'EXIT e non passa di quà.  |
       | Nel secondo caso il fine header composta la valorizzazione del campo          |
       | headerLen nella struttura di output che altrimenti rimarrebe non valorizzato  |
       | nel caso di fine analisi per EOF. Per verificare a fine analisi se l'header   |
       | è stato trovato ed è completo è necessario verificare la presenza dell'inizio |
       | header e del fine header. L'inizio header lo si può verificare andando ad     |
       | effettuare un test sul campo Protocol sulla struttura di output.              |
       |                                                                               |
       '-------------------------------------------------------------------------------' */

       /*
       .------------------------------------------------------.
       | TEST.1: Response EMPTY                               |
       '------------------------------------------------------' */
       if ( strcmp(rHTTPPrintHeader.Protocol,"HTTP\0") != 0 )
       {
            PrintMessage(debug_level, HTTP_TABMSG, "HTTP_PARSEHEADER_RESPONSEEMPTY_ERROR", TabVariabiliEMPTY, 0, 0, 0);
            rHTTPPrintHeader.idmsg   = 0 ;
            rHTTPPrintHeader.msgDesc = malloc(strlen("HTTP.PARSEHEADER.RESPONSEEMPTY.ERROR"));
            rHTTPPrintHeader.msgDesc = "HTTP.PARSEHEADER.RESPONSEEMPTY.ERROR";
            rHTTPPrintHeader.return_code = -4;

            if ( debug_level == __INFO__ )
                 printf("+for more details, see [%s]\n", HTTPFile);

            goto HTTPPrintHeader_EXIT ;
       };

       /*
       .------------------------------------------------------.
       | TEST.2: Not Found Ini HTTP Header                    |
       '------------------------------------------------------' */
       if ( strcmp(rHTTPPrintHeader.Protocol,"HTTP\0") != 0 ) 
       {
            PrintMessage(debug_level, HTTP_TABMSG, "HTTP_PARSEHEADER_UNKNOWNHEADER_ERROR", TabVariabiliEMPTY, 0, 0, 0);
            rHTTPPrintHeader.idmsg   = 0 ;
            rHTTPPrintHeader.msgDesc = malloc(strlen("HTTP.PARSEHEADER.UNKNOWNHEADE.ERROR"));
            rHTTPPrintHeader.msgDesc = "HTTP.PARSEHEADER.UNKNOWNHEADE.ERROR";
            rHTTPPrintHeader.return_code = -5;

            if ( debug_level == __INFO__ )
                 printf("+for more details, see [%s]\n", HTTPFile);

            goto HTTPPrintHeader_EXIT ;
       };

       /*
       .------------------------------------------------------.
       | TEST.3: Not Found Fin HTTP Header                    |
       '------------------------------------------------------' */
       if ( rHTTPPrintHeader.HeaderLen == 0 ) 
       {
            PrintMessage(debug_level, HTTP_TABMSG, "HTTP_PARSEHEADER_ENDHEADERNOTFOUND_ERROR", TabVariabiliEMPTY, 0, 0, 0);
            rHTTPPrintHeader.idmsg   = 0 ;
            rHTTPPrintHeader.msgDesc = malloc(strlen("HTTP.PARSEHEADER.ENDHEADERNOTFOUND.ERROR"));
            rHTTPPrintHeader.msgDesc = "HTTP.PARSEHEADER.ENDHEADERNOTFOUND.ERROR";
            rHTTPPrintHeader.return_code = -6;

            if ( debug_level == __INFO__ )
                 printf("+for more details, see [%s]\n", HTTPFile);

            goto HTTPPrintHeader_EXIT ;
       };

       /*
       .-----------------------------------------.
       | Imposta il return-code a zero (Ok)      |
       '-----------------------------------------' */
       PrintMessage(debug_level, HTTP_TABMSG, "HTTP_PARSEHEADER__OK", TabVariabiliEMPTY, 0, 0, 0);
       rHTTPPrintHeader.return_code = 0;


HTTPPrintHeader_EXIT:

       /*
       .----------------------------------------------------.
       | Close HTTP File                                    |
       '----------------------------------------------------' */
       fclose (pHTTPFile);

       return rHTTPPrintHeader ;
}

// __EOF__

