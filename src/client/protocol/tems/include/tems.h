/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifdef HAVE_LIBTIBEMS

/*
.-----------------------.
| header libreria TIBCO |
'-----------------------' */
#include <tibems/tibems.h>


/*
.-----------------.
| Connessione EMS |
'-----------------' */
struct tConnessioneEMS 
{
       tibemsConnection connection  ;
       long             idmsg       ;
       int              return_code ;
};

extern struct tConnessioneEMS    ConnessioneEMS    ( char *serverUrl, char *auth_user, char *auth_password ) ; 


/*
.--------------------.
| Disconnessione EMS |
'--------------------' */
struct tDisconnessioneEMS
{
       long             idmsg       ;
       int              return_code ;
};

extern struct tDisconnessioneEMS DisconnessioneEMS ( tibemsConnection connection ) ; 


/*
.------------------------------------------.
|                                          |
|                                          | 
|        Connessione EMS Over SSL          |
|                                          |
|                                          |
'------------------------------------------' */
struct tConnessioneEMSS
{
       tibemsSSLParams  sslParams   ;
       tibemsConnection connection  ;
       long             idmsg       ;
       int              return_code ;
};

extern struct tConnessioneEMSS 
    ConnessioneEMSS (
       char *serverUrl,
       char *auth_user,
       char *auth_password,
       char *keystore,
       char *password,
       char *truststore ); 

/*
.-----------------------------------------.
|                                         |
|                                         |
|      Disconnessione EMS Over SSL        |
|                                         |
|                                         |
'-----------------------------------------' */
struct tDisconnessioneEMSS
{
       long             idmsg       ;
       int              return_code ;
};

extern struct tDisconnessioneEMSS DisconnessioneEMSS ( tibemsConnection connection, tibemsSSLParams sslParams ) ;



/*
.----------------------------------------.
|                                        |
|                                        |
|           Run Operation EMS            |
|                                        |
|                                        |
'----------------------------------------' */
struct tRunOperationEMS
{
       long             NumMsg            ;
       long             idmsg             ;
       int              return_code       ;
};

extern struct tRunOperationEMS RunOperationEMS ( tibemsConnection connection   ,
                                                 char            *queueName    ,
                                                 char            *operation    ,
                                                 int              MaxMsg       ,
                                                 char            *message      ,
                                                 char            *OutputFile   ); 



/*
.----------------------------------------.
|                                        |
|                                        |
|               Browser EMS              |
|                                        |
|                                        |
'----------------------------------------' */
struct tBrowserEMS 
{
       long             ServerResponseNum ; 
       long             idmsg             ;
       int              return_code       ;
};

extern struct tBrowserEMS BrowserEMS    ( tibemsConnection connection, 
                                          char *queueName, 
                                          int   MaxMsgBrowse,
                                          char *OutputFile ) ; 


/*
.----------------------------------------.
|                                        |
|                                        |
|              Producer EMS              |
|                                        |
|                                        |
'----------------------------------------' */
struct tProducerEMS
{
       long             ServerRequestNum ;
       long             idmsg             ;
       int              return_code       ;
};

extern struct tProducerEMS ProducerEMS  ( tibemsConnection connection,
                                          char *queueName, 
                                          int   MaxMsgWrite,
                                          char *message ); 


/*
.----------------------------------------.
|                                        |
|                                        |
|              Consumer EMS              |
|                                        |
|                                        |
'----------------------------------------' */
struct tConsumerEMS
{
       long             ServerResponseNum ;
       long             idmsg             ;
       int              return_code       ;
};

extern struct tConsumerEMS ConsumerEMS  ( tibemsConnection connection,
                                          char *queueName,
                                          int   MaxMsgRead,
                                          char *OutputFile ) ;

#endif

/*
.----------------------------------------------------.
|                                                    |
|                    Client minimale                 |
|             (connessione/disconnessione)           |
|                                                    |
'----------------------------------------------------' */
extern struct tClientConnect TEMSClientConnect ( struct tClientConnectParameter rClientConnectParameter ) ; 

// __EOF__
