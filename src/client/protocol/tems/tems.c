/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/*
.-----------------.
| header libC     |
'-----------------' */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

/*
.-------------------------.
| header common           |
'-------------------------' */
#include "client.h"
#include "output.h"






/*
.------------------.
! header autoconf  |
'------------------' */ 
#include "config.h"

#ifndef HAVE_LIBTIBEMS

        /*
        .--------------------------------------------------------.
        |                                                        |
        | Function  : TEMSClient                                 |
        |                                                        |
        | Remark    : Client minimale TIBCO EMS                  |
        |                                                        |
        |                                                        |
        |                                                        |
        '--------------------------------------------------------' */
        extern struct tClientConnect TEMSClientConnect ( struct tClientConnectParameter rClientConnectParameter )
        {
               printf("TIBCO EMS support is *NOT* Active\n");

               struct tClientConnect rClientConnect;

               /* 
               .-----------------------------------.
               | TIBCO EMS support is *NOT* Active | 
               '-----------------------------------' */  
               rClientConnect.return_code = -22; 
        };

#else




/*
.-----------------.
| header locale   |
'-----------------' */
#include "tems.h"
#include "tems_messages.h"





/* 
.-----------------------------------------------------------.
|                                                           |
| Function  : onException                                   |
|                                                           |
| Remark    : Verifica connessione                          |
|                                                           |
|                                                           |
'-----------------------------------------------------------' */
void onException ( tibemsConnection conn, tibems_status reason, void* closure ) 
{
    if (reason == TIBEMS_SERVER_NOT_CONNECTED)
        printf("CONNECTION EXCEPTION: Server Disconnected\n");
}; 


/*
.-------------------------------------------------------------.
|                                                             |
| Function  : _verifyHostName                                 |
|                                                             |
| Remark    : Invocata da tibemsSSLParams_SetHostNameVerifier |
|             Serve a visualizzare le regole per verificare   |
|             l'hostname.                                     |
|                                                             |
'-------------------------------------------------------------' */
static tibems_status _verifyHostName ( const char*     connected_hostname ,
                                       const char*     expected_hostname  ,
                                       const char*     certificate_name   ,
                                       void*           closure              )
{
       fprintf(stderr,"CUSTOM VERIFIER:\n"\
  "    connected: [%s]\n"\
  "    expected:  [%s]\n"\
  "    certCN:    [%s]\n",
       connected_hostname ? connected_hostname : "(null)",
       expected_hostname  ? expected_hostname  : "(null)",
       certificate_name   ? certificate_name   : "(null)");
  
       return TIBEMS_OK;
}


/*
.--------------------------------------------------------.
|                                                        |
| Function  : ConnessioneEMS                             |
|                                                        |
| Remark    : Connessione ad EMS Server                  |
|                                                        |
|                                                        |
'--------------------------------------------------------' */ 
struct tConnessioneEMS ConnessioneEMS ( char *serverUrl, char *auth_user, char *auth_password )
{
    struct tConnessioneEMS rConnessioneEMS  ;

    tibemsConnection connection = NULL      ;
    tibems_status    status     = TIBEMS_OK ;

    /*
    .------------.
    | Connection |
    '------------' */
    status = tibemsConnection_Create ( &connection, serverUrl, NULL, auth_user, auth_password ); 
    if (status != TIBEMS_OK) { 
        rConnessioneEMS.connection  = NULL   ;
        rConnessioneEMS.idmsg       = status ;
        rConnessioneEMS.return_code = -1     ;
        return rConnessioneEMS ; };

    /*
    .-----------------------------------------------.
    | Set the exception listener                    |
    '-----------------------------------------------' */
    status = tibemsConnection_SetExceptionListener(connection, onException, NULL); 
    if (status != TIBEMS_OK) {
        rConnessioneEMS.connection  = NULL   ;
        rConnessioneEMS.idmsg       = status ; 
        rConnessioneEMS.return_code = -1     ;
        return rConnessioneEMS ; }; 

    /*
    .---------------.
    | Connection Ok |
    '---------------' */ 
    rConnessioneEMS.connection  = connection ;
    rConnessioneEMS.idmsg       = status     ;
    rConnessioneEMS.return_code = 0          ;
    return rConnessioneEMS ;
};

/*
.--------------------------------------------------------.
|                                                        |
| Function  : DisconnessioneEMS                          |
|                                                        |
| Remark    : Disconnessione da EMS Server               |
|                                                        |
|                                                        |
'--------------------------------------------------------' */ 
struct tDisconnessioneEMS DisconnessioneEMS( tibemsConnection connection )
{

    struct tDisconnessioneEMS rDisconnessioneEMS  ;

    tibems_status    status     = TIBEMS_OK ;

    status = tibemsConnection_Close(connection);
    if (status != TIBEMS_OK) {
        rDisconnessioneEMS.idmsg       = status ;
        rDisconnessioneEMS.return_code = -1     ;
        return rDisconnessioneEMS ; };

    /*
    .-------------------.
    | Disconnessione Ok |
    '-------------------' */
    rDisconnessioneEMS.idmsg       = status     ;
    rDisconnessioneEMS.return_code = 0          ;

    return rDisconnessioneEMS ;
}


/*
.--------------------------------------------------------.
|                                                        |
| Function  : ConnessioneEMSS                            |
|                                                        |
| Remark    : Connessione ad EMS Over SSL Server         |
|                                                        |
|                                                        | 
'--------------------------------------------------------' */ 
extern struct tConnessioneEMSS 
       ConnessioneEMSS ( char *serverUrl,
                         char *auth_user, 
                         char *auth_password, 
                         char *keystore, 
                         char *password, 
                         char *truststore)
{
    struct tConnessioneEMSS rConnessioneEMSS;

    tibemsConnection connection = NULL;
    tibems_status    status     = TIBEMS_OK;

    /*
    .----------------------.
    | Crea il contesto SSL |
    '----------------------' */
    tibemsSSLParams sslParams = NULL;
    sslParams = tibemsSSLParams_Create();


    /*
    .-------------------------------------------------------------------.
    | Parametro: -ssl_identity                                          |
    +-------------------------------------------------------------------+
    | Registra un keystore del client (quindi privata e certificato)    |
    | nel contesto.                                                     |
    +-------------------------------------------------------------------+
    | Funzionalità Integrabile : SI                                     |
    | Funzionalità Esistente   : NO                                     |
    '-------------------------------------------------------------------' */
    if ( keystore != NULL )
    {
         status = tibemsSSLParams_SetIdentityFile ( sslParams, keystore, TIBEMS_SSL_ENCODING_AUTO ); 
         if (status != TIBEMS_OK)
         {
            rConnessioneEMSS.sslParams   = NULL   ;
            rConnessioneEMSS.connection  = NULL   ;
            rConnessioneEMSS.idmsg       = status ;
            rConnessioneEMSS.return_code = -1     ;
            return rConnessioneEMSS ; 
         }
    };


    /* 
    .-------------------------------------------------------------------.
    | Parametro: -ssl_key                                               |
    +-------------------------------------------------------------------+
    | Registra la chiave privata del client nel contesto.               |
    | Questa funzionalità è sostituibile da -ssl_identity che serve per |
    | impostare nel contesto un keystore che contiene sia la privata    |
    | sia il certificato.                                               |
    +-------------------------------------------------------------------+
    | Funzionalità Integrabile : NO                                     |
    | Funzionalità Esistente   : NO                                     |
    '-------------------------------------------------------------------' */
    /*
    if ( key != NULL )
    {
         status = tibemsSSLParams_SetPrivateKeyFile ( sslParams, key, TIBEMS_SSL_ENCODING_AUTO ); 
         if (status != TIBEMS_OK)
         {
            rConnessioneEMSS.sslParams   = NULL   ;
            rConnessioneEMSS.connection  = NULL   ;
            rConnessioneEMSS.idmsg       = status ;
            rConnessioneEMSS.return_code = -1     ;
            return rConnessioneEMSS ;
         }
    };
    */


    /*
    .-------------------------------------------------------------------.
    | Parametro: -ssl_issuer                                            |
    +-------------------------------------------------------------------+
    | Registra il certificato del client nel contesto.                  |
    | Questa funzionalità è sostituibile da -ssl_identity che serve per |
    | impostare nel contesto un keystore che contiene sia la privata    |
    | sia il certificato. Con la funzionalità                           |
    | tibemsSSLParams_AddIssuerCertFile è possibile importare più di un |
    | certificato, non so che utilità ha questa cosa...                 |
    |                                                                   |
    +-------------------------------------------------------------------+
    | Funzionalità Integrabile : NO                                     |
    | Funzionalità Esistente   : NO                                     |
    '-------------------------------------------------------------------' */
    /*
    if ( cert != NULL )
    {
         status = tibemsSSLParams_AddIssuerCertFile  (sslParams, cert, TIBEMS_SSL_ENCODING_AUTO);
         if (status != TIBEMS_OK)
         {
            rConnessioneEMSS.sslParams   = NULL   ;
            rConnessioneEMSS.connection  = NULL   ;
            rConnessioneEMSS.idmsg       = status ;
            rConnessioneEMSS.return_code = -1     ;
            return rConnessioneEMSS ;
         }
    };
    */


    /* 
    .-------------------------------------------------------------------.
    | Parametro: -ssl_trusted                                           |
    +-------------------------------------------------------------------+
    | Registra i Certificati delle CA fidate nel contesto.              |
    | Le certification authority che dovranno emettere, le sole         |  
    | abilitate, il certificato emesso dal server. Attenzione col DER   |                   
    | non funziona ho dovuto convertire il certificato CA in truststore |
    | PEM. MyTCPClient invece permette il formato DER quindi potrebbe   |
    | arrivare un formato non gestibile in questo punto.                |
    +-------------------------------------------------------------------+
    | Funzionalità Integrabile : SI                                     |
    | Funzionalità Esistente   : NO                                     |
    '-------------------------------------------------------------------' */
    if ( truststore != NULL )
    {
         status = tibemsSSLParams_AddTrustedCertFile(sslParams, truststore, TIBEMS_SSL_ENCODING_AUTO);
         if (status != TIBEMS_OK)
         {
            rConnessioneEMSS.sslParams   = NULL   ;
            rConnessioneEMSS.connection  = NULL   ;
            rConnessioneEMSS.idmsg       = status ;
            rConnessioneEMSS.return_code = -1     ;
            return rConnessioneEMSS ;
         }
    };

    /*
    .-------------------------------------------------------------------.
    | Parametro: -ssl_noverifyhost                                      |
    +-------------------------------------------------------------------+
    | Se la verifica è attiva TIBEMS_TRUE il client verifica che il     |
    | Server.Cerificato.CN e l'hostname contattato sia identici.        |
    | Nel caso siano differenti significa che il certificato inviato    |
    | dal server non è intestato correttamente oppure il client non     |
    | ha contattato il server col l'hostname corretto.                  |
    | Questo è il classico controllo che effettuano tutti i WEB Browser |
    | in cui compare il warning all'utente in caso di incongruenza.     |
    | Il client interrompe con errore se esiste l'incongruenza ed il    |
    | Server.Certificato.CN acquisito non è definito nemmeno dalla      |
    | SetExpectedHostName. Di default la funzione viene                 |
    | abilitata, quindi è necessario inserire l'istruzione con          |
    | TIBEMS_FALSE per disabilitare il controllo.                       |
    +-------------------------------------------------------------------+
    | Funzionalità Integrabile : SI                                     |
    | Funzionalità Esistente   : SI (ma non utilizzabile)               |
    '-------------------------------------------------------------------' */
    tibemsSSLParams_SetVerifyHost ( sslParams, TIBEMS_FALSE );

    /*
    .-------------------------------------------------------------------.
    | Parametro: -ssl_noverifyhostname                                  |
    +-------------------------------------------------------------------+
    | Se la verifica è attiva il client si blocca se non conosce la CA  |
    | che ha emesso il certificato ricevuto dal server. E' necessario   |
    | In caso di errore verificare se è il caso di inserire il          |
    | certificato CA del server nel proprio truststore, se questa è     |
    | stata una dimenticanza. E' evvio che attivando questo controllo   |
    | bisogna specificare un truststore tramite l'API                   | 
    | tibemsSSLParams_AddTrustedCertFile. Di default la funzione viene  | 
    | abilitata, quindi è necessario inserire l'istruzione con          |
    | TIBEMS_FALSE per disabilitare il controllo.                       |
    +-------------------------------------------------------------------+
    | Funzionalità Integrabile : SI                                     |
    | Funzionalità Esistente   : SI (ma non utilizzabile)               |
    '-------------------------------------------------------------------' */
    tibemsSSLParams_SetVerifyHostName ( sslParams, TIBEMS_TRUE );

    /*
    .-------------------------------------------------------------------.
    | Parametro: -ssl_hostname                                          |
    +-------------------------------------------------------------------+
    | Definisce il Certificato.CN che si aspetta il client di trovare   |
    | nel Certificato ricevuto dal Server. (parametro -ssl_hostname)    |
    | Si può inserire un'unico valore, quindi significa che la          |
    | forzatura può avvenire solamente su un'unica macchina server.     |
    | La verifica viene eseguita solo se è attiva la funzionalità di    |
    | verifica hostname: SetVerifyHostName (assente o a TIBEMS_TRUE).   |
    +-------------------------------------------------------------------+
    | Funzionalità Integrabile : NO                                     |
    | Funzionalità Esistente   : NO                                     |
    '-------------------------------------------------------------------' */
    tibemsSSLParams_SetExpectedHostName ( sslParams, "ems.server.local"  );

    /*
    .-------------------------------------------------------------------.
    | Parametro: -ssl_custom                                            |
    +-------------------------------------------------------------------+
    | Serve per visualizzare la regola custom costruita per verificare  |
    | l'identità del server e quindi decidere se risulta abilitato.     |
    | La regola è costruita in base ai precedenti parametri impostati   |
    | tramite tibemsSSLParams_SetExpectedHostName e                     |
    | tibemsSSLParams_SetVerifyHost. La visualizzazione della regola    | 
    | appare solamente se è attiva la funzione di verifica hostname     |
    | SetVerifyHostName assente o a TIBEMS_TRUE.                        |
    +-------------------------------------------------------------------+
    | Funzionalità Integrabile : NO                                     |
    | Funzionalità Esistente   : SI                                     |
    '-------------------------------------------------------------------' */
    tibemsSSLParams_SetHostNameVerifier ( sslParams, _verifyHostName, NULL );

    
    /*
    .-------------------------------------------------------------------.
    | Parametro: -ssl_trace                                             |
    +-------------------------------------------------------------------+
    | Mostra l'ISSUER ed il SUBJECT dei certificati caricari e          |
    | trasmessi dal client.                                             |
    +-------------------------------------------------------------------+
    | Funzionalità Integrabile : NO                                     |
    | Funzionalità Esistente   : SI (ma non utilizzabile)               | 
    '-------------------------------------------------------------------' */
    tibemsSSL_SetTrace ( TIBEMS_FALSE );
    

    /*
    .-------------------------------------------------------------------.
    | Parametro: -ssl_debug_trace                                       |
    +-------------------------------------------------------------------+
    | Mostra un livello di trace piu di dettaglio di quelli ottenuti    |
    | col SetTrace.                                                     |
    +-------------------------------------------------------------------+
    | Funzionalità Integrabile : NO                                     |
    | Funzionalità Esistente   : SI (ma non utilizzabile)               |
    '-------------------------------------------------------------------' */
    tibemsSSL_SetDebugTrace ( TIBEMS_FALSE );



    /*
    .-----------------.
    | Connessione SSL |
    '-----------------' */
    status = tibemsConnection_CreateSSL(&connection, serverUrl, NULL, auth_user, auth_password, sslParams, password);
    if (status != TIBEMS_OK) {
        rConnessioneEMSS.sslParams   = NULL   ;
        rConnessioneEMSS.connection  = NULL   ;
        rConnessioneEMSS.idmsg       = status ;
        rConnessioneEMSS.return_code = -1     ;
        return rConnessioneEMSS ; 
    };

    /*
    .-----------------------------------------------.
    | Set the exception listener                    |
    '-----------------------------------------------' */
    status = tibemsConnection_SetExceptionListener(connection, onException, NULL); 
    if (status != TIBEMS_OK) {
        rConnessioneEMSS.sslParams   = NULL   ;
        rConnessioneEMSS.connection  = NULL   ;
        rConnessioneEMSS.idmsg       = status ;
        rConnessioneEMSS.return_code = -1     ;
        return rConnessioneEMSS ; 
    };

    /*
    .---------------.
    | Connection Ok |
    '---------------' */  
    rConnessioneEMSS.sslParams   = sslParams  ;
    rConnessioneEMSS.connection  = connection ;
    rConnessioneEMSS.idmsg       = status     ;
    rConnessioneEMSS.return_code = 0          ;
    return rConnessioneEMSS ;
};


/*
.--------------------------------------------------------.
|                                                        |
| Function  : DisconnessioneEMSS                         |
|                                                        |
| Remark    : Disconnessione ad EMS Over SSL Server      |
|                                                        |
|                                                        |
'--------------------------------------------------------' */  
extern struct tDisconnessioneEMSS DisconnessioneEMSS ( tibemsConnection connection, tibemsSSLParams sslParams ) 
{
    struct tDisconnessioneEMSS rDisconnessioneEMSS;

    tibems_status status = TIBEMS_OK;

    /*
    .----------------------------------.
    | Chiude la connessione            |
    '----------------------------------' */
    status = tibemsConnection_Close(connection);
    if (status != TIBEMS_OK) {
        rDisconnessioneEMSS.idmsg       = status ;
        rDisconnessioneEMSS.return_code = -1     ;
        return rDisconnessioneEMSS ; 
    };

    /*
    .---------------------------.
    | Distrugge il contesto SSL |
    '---------------------------' */
    tibemsSSLParams_Destroy(sslParams);

    rDisconnessioneEMSS.idmsg       = status ;
    rDisconnessioneEMSS.return_code = 0      ;
    return rDisconnessioneEMSS;
}



/*
.--------------------------------------------------------.
|                                                        |
| Function  : RunOperationEMS                            |
|                                                        |
| Remark    : Chiede un'azione al server EMS.            |
|                                                        |
|                                                        |
|                                                        |
|                                                        |
'--------------------------------------------------------' */
extern struct tRunOperationEMS RunOperationEMS ( tibemsConnection connection   ,
                                                 char            *queueName    ,
                                                 char            *operation    ,
                                                 int              MaxMsg       ,
                                                 char            *message      ,
                                                 char            *OutputFile   )
{

       struct tRunOperationEMS rRunOperationEMS ;
       struct tBrowserEMS      rBrowserEMS      ;
       struct tConsumerEMS     rConsumerEMS     ; 
       struct tProducerEMS     rProducerEMS     ; 

       int idOperation = 0 ;

       if ( strcmp ( operation, "GET"    ) == 0 ) idOperation = 1;
       if ( strcmp ( operation, "PUT"    ) == 0 ) idOperation = 2;
       if ( strcmp ( operation, "GETDEL" ) == 0 ) idOperation = 3;
       if ( strcmp ( operation, "DEL"    ) == 0 ) idOperation = 4; 

       /*
       .---------------------------------------------------------------------------------.
       |                                          Browser     Consumer     Producer      | 
       +---------------------------------------------------------------------------------+ 
       |  create_queue                       -1   Yes         Yes          Yes           |        
       |  create_session                     -2   Yes         Yes          Yes           | 
       |  create_producer|consumer|browser   -3   Yes         Yes          Yes           | 
       |  start_connection                   -4   No          Yes          No            | 
       |  create output                      -5   Yes         Yes          No            | 
       |  read message                       -6   Yes         Yes          No            |  
       |  read_bodytype_message              -7   ??          Yes          No            | 
       |  verify_type_message                -8   ??          ??           No            | 
       |  read_message                       -9   Yes         Yes          No            | 
       |  create_message                    -10   ??          ??           Yes           | 
       |  set_message                       -11   No          No           Yes           | 
       |  send_message                      -12   No          No           Yes           | 
       |  destroy message                   -13   ??          ??           Yes           |  
       |  write_output                      -14   Yes         Yes          No            | 
       |  close_output                      -15   Yes         Yes          No            |  
       |  destroy_producer|consumer|browser -16   Yes         ??           ??            | 
       |  destroy_session_destination       -17   Yes         ??           Yes           |
       '---------------------------------------------------------------------------------'
       */
       switch ( idOperation )
       {
                case 1  : rBrowserEMS  = BrowserEMS  ( connection, queueName, MaxMsg, OutputFile ) ; 
                          rRunOperationEMS.NumMsg      = rBrowserEMS.ServerResponseNum;
                          rRunOperationEMS.idmsg       = rBrowserEMS.idmsg;
                          rRunOperationEMS.return_code = rBrowserEMS.return_code;
                          break;

                case 2  : rProducerEMS = ProducerEMS ( connection, queueName, MaxMsg, message ) ; 
                          rRunOperationEMS.NumMsg      = rProducerEMS.ServerRequestNum;
                          rRunOperationEMS.idmsg       = rProducerEMS.idmsg;
                          rRunOperationEMS.return_code = rProducerEMS.return_code;
                          break;

                case 3  : rConsumerEMS = ConsumerEMS ( connection, queueName, MaxMsg, OutputFile ); 
                          rRunOperationEMS.NumMsg      = rConsumerEMS.ServerResponseNum;
                          rRunOperationEMS.idmsg       = rConsumerEMS.idmsg;
                          rRunOperationEMS.return_code = rConsumerEMS.return_code;
                          break;

                default : break; 
       };

       return rRunOperationEMS ;

};



/*
.--------------------------------------------------------.
|                                                        |
| Function  : BrowserEMS                                 |
|                                                        |
| Remark    : Crea una QUEUE, ci scrive e ci legge       |
|             Il BROWSE dovrebbe leggere ma non          |
|             consumare.                                 |
|                                                        |
|                                                        | 
'--------------------------------------------------------' */ 
extern struct tBrowserEMS BrowserEMS( tibemsConnection connection   , 
                                      char            *queueName    , 
                                      int              MaxMsgBrowse ,
                                      char            *OutputFile   )
{
    struct tBrowserEMS rBrowserEMS ; 

    tibemsSession      session       = NULL      ;
    tibemsDestination  destination   = NULL      ;  
    tibemsQueueBrowser queueBrowser  = NULL      ;
    tibems_status      status        = TIBEMS_OK ;
    tibemsMsg          msg           = NULL      ;

//------------
    printf("\n ### DEBUG ### Start Browse... ");
//------------

    /*
    .---------------------------------------------------.
    | Queue_Create                                      |
    +---------------------------------------------------+
    | Creazione di una CODA.  Se esiste usa quella,  se |
    | *NON* esiste ne crea una  temporanea.  Questa API |
    | serve solo per  valorizzare  una  struttura  dati |
    | di tipo DESTINATION e quindi va sempre bene anche |
    | se non si è  connessi  o  se  non  esiste  alcuna |
    | sessione aperta.   Valorizza  semplicemente   una |
    | struttura dati e null'altro.                      |
    '---------------------------------------------------' */
    status = tibemsQueue_Create ( &destination, queueName );
    if (status != TIBEMS_OK)
    {
        rBrowserEMS.idmsg       = status;
        rBrowserEMS.return_code = -1;
        return rBrowserEMS;
    };

    /*
    .--------------------------------------------------.
    | Connection_CreateSession                         |
    +--------------------------------------------------+
    | Crea  una  sessione,  cioè un canale privata tra |
    | client  e   server  sfruttando  una  connessione |
    | aperta  tra  i  due,  in   pratica   il   server |
    | probabilmente genera  al  client  un  ID_SESSION |
    | per poi individuarlo.                            |
    '--------------------------------------------------' */
    status = tibemsConnection_CreateSession ( connection, &session, TIBEMS_FALSE, TIBEMS_AUTO_ACKNOWLEDGE );
    if (status != TIBEMS_OK)
    {
        rBrowserEMS.idmsg       = status;
        rBrowserEMS.return_code = -2;
        return rBrowserEMS;
    };

    /*
    .-------------------------------------------------.
    | Session_CreateBrowser                           |
    +-------------------------------------------------+
    | Crea per la sessione il Browser, che una        |
    | struttura dati per poter leggere contenuti di   |
    | code senza consumarle a differenza del consumer |
    '-------------------------------------------------' */ 
    status = tibemsSession_CreateBrowser ( session, &queueBrowser, destination, NULL);
    if (status != TIBEMS_OK)
    {
        rBrowserEMS.idmsg       = status;
        rBrowserEMS.return_code = -3;
        return rBrowserEMS;
    }  

    /*
    .-------------------------------------------------.
    | Connection_Start (Browser NO, Producer NO)      |
    +-------------------------------------------------+
    | Solo il Consumer sembra ne abbia bisogno        |
    |                                                 |
    '-------------------------------------------------' */
    // status = tibemsConnection_Start(connection);
    // if (status != TIBEMS_OK)
    // {
    //     rBrowserEMS.idmsg       = status;
    //     rBrowserEMS.return_code = -4;
    //     return rBrowserEMS;
    // }

    /*
    .----------------------------------------------------.
    | Create OUTPUT  (solo Browser e Consumer)           |
    +----------------------------------------------------+
    | Crea il file di Output prima di iniziare a leggere |
    | il contenuto della code. Se il file non lo si      |
    | riesce ad aprire esce per errore.                  |
    '----------------------------------------------------' */
    FILE *pOutputFile;
    pOutputFile = fopen(OutputFile, "w");
    if (!pOutputFile) 
    {
        rBrowserEMS.idmsg       =  0;
        rBrowserEMS.return_code = -5;
        return rBrowserEMS;
    }

    /*
    .-------------------------------------------------.
    | Browse QUEUE                                    |
    |                                                 |
    |                                                 |
    |                                                 |
    '-------------------------------------------------' */
    rBrowserEMS.ServerResponseNum = 0;
    while (1) 
    {
            /*
            .-----------------------------------------------.
            | QueueBrowser_GetNext                          | 
            +-----------------------------------------------+
            |                                               |
            |                                               |
            |                                               |
            |                                               |
            |                                               |
            |                                               |
            '-----------------------------------------------' */ 
            status = tibemsQueueBrowser_GetNext ( queueBrowser, &msg );

            if ( status == TIBEMS_NOT_FOUND ) {
                 break;
            }

            if ( status != TIBEMS_OK ) {
                 rBrowserEMS.idmsg       = status;
                 rBrowserEMS.return_code = -6;
                 return rBrowserEMS;
            };

            /*
            .-----------------------------------------------.
            |                                               |
            +-----------------------------------------------+
            |                                               |
            |                                               |
            '-----------------------------------------------' */
            const char *strResponse = NULL ;
            strResponse = (char *) malloc(1024);
            status = tibemsTextMsg_GetText ( msg, &strResponse );
            if (status != TIBEMS_OK)
            {
                rBrowserEMS.idmsg       = status;
                rBrowserEMS.return_code = -9;
                return rBrowserEMS;
            }

            /*
            .-----------------------------------------------.
            |                                               |
            +-----------------------------------------------+
            |                                               |
            |                                               |
            |                                               |
            |                                               |
            |                                               |
            |                                               |
            '-----------------------------------------------' */
            if ( MaxMsgBrowse == 0 || rBrowserEMS.ServerResponseNum < MaxMsgBrowse ) 
            {

//ATTENZIONE! Se raggiunge MaxMsgBrowse... non scrive si, ma continua a leggere
//Quindi andrebbe aggiunto un break per uscire fuori dal while infinito
//====================
                 strcat (strResponse, "\n\0"); 
//====================
                 fwrite(strResponse, 1, strlen(strResponse), pOutputFile); 
                 // if ( *** KO *** )
                 // {
                 //     rBrowserEMS.idmsg       = 0;
                 //     rBrowserEMS.return_code = -14;
                 //     return rBrowserEMS;
                 // }
            };

            rBrowserEMS.ServerResponseNum++;
    };

    /*
    .----------------------------------------------------.
    | Close OUTPUT                                       |
    +----------------------------------------------------+
    | Chiude il file di Output dopo aver terminato la    |
    | la lettura della coda.                             |
    '----------------------------------------------------' */
    fclose (pOutputFile);
    // if ( *** KO *** )
    // {
    //     rBrowserEMS.idmsg       = 0;
    //     rBrowserEMS.return_code = -15;
    //     return rBrowserEMS;
    // }

    /*
    .-------------------------------------------------.
    | QueueBrowser_Close                              |
    +-------------------------------------------------+
    |                                                 |
    |                                                 |
    |                                                 |
    '-------------------------------------------------' */
    status = tibemsQueueBrowser_Close(queueBrowser);
    if (status != TIBEMS_OK)
    {
        rBrowserEMS.idmsg       = status;
        rBrowserEMS.return_code = -16;
        return rBrowserEMS;
    }; 

    /*
    .-------------------------------------------------.
    | Destination_Destroy                             |
    +-------------------------------------------------+
    |                                                 |
    |                                                 |
    |                                                 |
    '-------------------------------------------------' */
    status = tibemsDestination_Destroy(destination);
    if (status != TIBEMS_OK)
    {
        rBrowserEMS.idmsg       = status;
        rBrowserEMS.return_code = -17;
        return rBrowserEMS;
    }

    /*
    .-------------------.
    | Disconnessione Ok |
    '-------------------' */
    rBrowserEMS.idmsg       = status     ;
    rBrowserEMS.return_code = 0          ;

//------------
    printf("\n ### DEBUG ### Stop Browse... letti %ld - da leggere %d ", rBrowserEMS.ServerResponseNum, MaxMsgBrowse);
//------------

    return rBrowserEMS; 
}
   

/*
.--------------------------------------------------------.
|                                                        |
| Function  : ConsumerEMS                                |
|                                                        |
| Remark    : Create, Read&Consumer Message in QUEUE.    |
|                                                        |
|                                                        |
|                                                        |
'--------------------------------------------------------' */
extern struct tConsumerEMS
         ConsumerEMS  ( tibemsConnection connection  ,
                        char            *queueName   ,
                        int              MaxMsgRead  ,
                        char            *OutputFile  )
{
    struct tConsumerEMS rConsumerEMS                            ; 
    
    tibemsSession       session       = NULL                    ;
    tibemsDestination   destination   = NULL                    ;  
    tibemsMsgConsumer   msgConsumer   = NULL                    ;
    tibems_status       status        = TIBEMS_OK               ;
    tibemsTextMsg       msg           = NULL                    ;
    tibemsMsgType       msgType       = TIBEMS_MESSAGE_UNKNOWN  ;
    char               *msgTypeName   = "UNKNOWN"               ;

    /*
    .---------------------------------------------------.
    | Queue_Create                                      |
    +---------------------------------------------------+
    | Creazione di una CODA.  Se esiste usa quella,  se |
    | *NON* esiste ne crea una  temporanea.  Questa API |
    | serve solo per  valorizzare  una  struttura  dati |
    | di tipo DESTINATION e quindi va sempre bene anche |
    | se non si è  connessi  o  se  non  esiste  alcuna |
    | sessione aperta.   Valorizza  semplicemente   una |
    | struttura dati e null'altro.                      |
    '---------------------------------------------------' */
    status = tibemsQueue_Create ( &destination, queueName );
    if (status != TIBEMS_OK)
    {
        rConsumerEMS.idmsg       = status;
        rConsumerEMS.return_code = -1;
        return rConsumerEMS;
    };

    /*
    .--------------------------------------------------. 
    | Connection_CreateSession |                       |
    | QueueConnection_CreateQueueSession               |
    +--------------------------------------------------+
    | Crea  una  sessione,  cioè un canale privata tra |
    | client  e   server  sfruttando  una  connessione |
    | aperta  tra  i  due,  in   pratica   il   server |
    | probabilmente genera  al  client  un  ID_SESSION |
    | per poi individuarlo.                            |
    '--------------------------------------------------' */
    status = tibemsConnection_CreateSession ( connection, &session, TIBEMS_FALSE, TIBEMS_AUTO_ACKNOWLEDGE );
    if (status != TIBEMS_OK)
    {
        rConsumerEMS.idmsg       = status;
        rConsumerEMS.return_code = -2;
        return rConsumerEMS;
    };

    /*
    .-------------------------------------------------.
    | Session_CreateConsumer |                        |
    | QueueSession_CreateReceiver                     |
    +-------------------------------------------------+
    | Crea per la sessione il Browser, che una        |
    | struttura dati per poter leggere contenuti di   |
    | code senza consumarle a differenza del consumer |
    '-------------------------------------------------' */
  //status = tibemsQueueSession_CreateReceiver ( session, &msgConsumer, destination, NULL);   
    status = tibemsSession_CreateConsumer      ( session, &msgConsumer, destination, NULL, TIBEMS_FALSE );
    if (status != TIBEMS_OK)
    {
        rConsumerEMS.idmsg       = status;
        rConsumerEMS.return_code = -3;
        return rConsumerEMS;
    } 

    /*
    .-------------------------------------------------.
    | Connection_Start (Browser NO, Producer NO)      |
    +-------------------------------------------------+
    | Solo il Consumer sembra ne abbia bisogno        |
    |                                                 |
    '-------------------------------------------------' */
    status = tibemsConnection_Start(connection);
    if (status != TIBEMS_OK)
    {
        rConsumerEMS.idmsg       = status;
        rConsumerEMS.return_code = -4;
        return rConsumerEMS;
    }

    /*
    .----------------------------------------------------.
    | Create OUTPUT  (solo Browser e Consumer)           |
    +----------------------------------------------------+
    | Crea il file di Output prima di iniziare a leggere |
    | il contenuto della code. Se il file non lo si      |
    | riesce ad aprire esce per errore.                  |
    '----------------------------------------------------' */
    FILE *pOutputFile;
    pOutputFile = fopen(OutputFile, "w");
    if (!pOutputFile)
    {
        rConsumerEMS.idmsg       =  0;
        rConsumerEMS.return_code = -5;
        return rConsumerEMS;
    }

    /*
    .-------------------------------------------------.
    | Consumer QUEUE                                  |
    |                                                 |
    |                                                 |
    |                                                 |
    '-------------------------------------------------' */
    rConsumerEMS.ServerResponseNum = 0;
    while (1)
    {
           status = tibemsMsgConsumer_Receive ( msgConsumer, &msg );
           if ( status != TIBEMS_OK )
           {
                rConsumerEMS.idmsg       = status;
                rConsumerEMS.return_code = -6;
                return rConsumerEMS;
           }

           /*
           .-----------------------------------------------.
           | Msg_GetBody                                   |
           +-----------------------------------------------+
           | Legge il tipo del messaggio che ha consumato  |
           | dalla coda. Se non è MESSAGE, BYTES, OBJECT,  |
           | STREAM, MAP, TEXT viene marcato come UNKNOWN  |
           | quindi sconosciuto. Successivamente non       |
           | sapendo come gestire quest'ultico caso esce   |
           | per errore chiaramente.                       |
           '-----------------------------------------------' */
           status = tibemsMsg_GetBodyType(msg,&msgType);
           if ( status != TIBEMS_OK )
           {
                 rConsumerEMS.idmsg       = status;
                 rConsumerEMS.return_code = -7;
                 return rConsumerEMS;
            }

            switch ( msgType )
            {
                     case TIBEMS_MESSAGE           : msgTypeName = "MESSAGE" ; break;
                     case TIBEMS_BYTES_MESSAGE     : msgTypeName = "BYTES"   ; break;
                     case TIBEMS_OBJECT_MESSAGE    : msgTypeName = "OBJECT"  ; break;
                     case TIBEMS_STREAM_MESSAGE    : msgTypeName = "STREAM"  ; break;
                     case TIBEMS_MAP_MESSAGE       : msgTypeName = "MAP"     ; break;
                     case TIBEMS_TEXT_MESSAGE      : msgTypeName = "TEXT"    ; break;
                     default                       : msgTypeName = "UNKNOWN" ; break;
            }

            /*
            .-----------------------------------------------.
            |                                               |
            +-----------------------------------------------+
            |                                               |
            |                                               |
            |                                               |
            |                                               |
            |                                               |
            |                                               |
            '-----------------------------------------------' */
            if ( MaxMsgRead == 0 || rConsumerEMS.ServerResponseNum < MaxMsgRead )
            {
                 const char *strResponse = NULL ;
                 strResponse = malloc(1024);

                 /*
                 .-----------------------------------------------.
                 |                                               |
                 +-----------------------------------------------+
                 |                                               |
                 |                                               |
                 '-----------------------------------------------' */
                 if (msgType != TIBEMS_TEXT_MESSAGE)
                 {
                     printf (">>> MSG NO TXT <<<\n"); 
                 }
                 else
                 {
                     /*
                     .-----------------------------------------------.
                     |                                               |
                     +-----------------------------------------------+
                     |                                               |
                     |                                               |
                     '-----------------------------------------------' */
                     status = tibemsTextMsg_GetText ( msg, &strResponse );
                     if (status != TIBEMS_OK)
                     {
                         rConsumerEMS.idmsg       = status;
                         rConsumerEMS.return_code = -9;
                         return rConsumerEMS;
                     }

                     /*
                     .-----------------------------------------------.
                     |                                               |
                     +-----------------------------------------------+
                     |                                               |
                     |                                               |
                     '-----------------------------------------------' */
                     fwrite(strResponse, 1, strlen(strResponse), pOutputFile);
                     // if ( *** KO *** )
                     // {
                     //     rConsumerEMS.idmsg       = 0;
                     //     rConsumerEMS.return_code = -14;
                     //     return rConsumerEMS;
                     // }
                 };
            };

            rConsumerEMS.ServerResponseNum++;
    };

    /*
    .----------------------------------------------------.
    | Close OUTPUT                                       |
    +----------------------------------------------------+
    | Chiude il file di Output dopo aver terminato la    |
    | la lettura della coda.                             |
    '----------------------------------------------------' */
    fclose (pOutputFile);
    // if ( *** KO *** )
    // {
    //     rConsumerEMS.idmsg       = 0;
    //     rConsumerEMS.return_code = -15;
    //     return rConsumerEMS;
    // }

    // /*
    // .-------------------------------------------------.
    // | Destroy Browser|Consumer|Producer               |
    // +-------------------------------------------------+
    // |                                                 |
    // |                                                 |
    // |                                                 |
    // '-------------------------------------------------' */
    // status = tibemsQueueBrowser_Close(queueBrowser);
    // if (status != TIBEMS_OK)
    // {
    //     rConsumerEMS.idmsg       = status;
    //     rConsumerEMS.return_code = -16;
    //     return rConsumerEMS;
    // };

    // /*
    // .-------------------------------------------------.
    // | Destination_Destroy                             |
    // +-------------------------------------------------+
    // |                                                 |
    // |                                                 |
    // |                                                 |
    // '-------------------------------------------------' */
    // status = tibemsDestination_Destroy(destination);
    // if (status != TIBEMS_OK)
    // {
    //     rConsumerEMS.idmsg       = status;
    //     rConsumerEMS.return_code = -17;
    //     return rConsumerEMS;
    // }

    /*
    .-------------------.
    | Disconnessione Ok |
    '-------------------' */
    rConsumerEMS.idmsg       = status     ;
    rConsumerEMS.return_code = 0          ;

    return rConsumerEMS;

};

/*
.--------------------------------------------------------.
|                                                        |
| Function  : ProducerEMS                                |
|                                                        |
| Remark    : Create and Write Message in QUEUE.         |
|                                                        |
|                                                        |
|                                                        | 
'--------------------------------------------------------' */ 
extern struct tProducerEMS 
         ProducerEMS  ( tibemsConnection connection  , 
                        char            *queueName   , 
                        int              MaxMsgWrite , 
                        char            *message     )
{
    struct tProducerEMS rProducerEMS                 ; 
    
    tibemsSession      session      = NULL           ;
    tibemsDestination  destination  = NULL           ;  
    tibemsMsgProducer  msgProducer  = NULL           ;
    tibems_status      status       = TIBEMS_OK      ;
    tibemsMsg          msg          = NULL           ;

    /*
    .---------------------------------------------------.
    | Queue_Create                                      |
    +---------------------------------------------------+
    | Creazione di una CODA. Se esiste usa quella, se   |
    | *NON* esiste ne crea una temporanea. Questa API   |
    | serve solo per valorizzare una struttura dati     |
    | di tipo DESTINATION e quindi va sempre bene anche |
    | se non si è connessi o se non esiste alcuna       |
    | sessione aperta. Valorizza semplicemente una      |
    | struttura dati e null'altro.                      | 
    '---------------------------------------------------' */ 
    status = tibemsQueue_Create ( &destination, queueName );
    if (status != TIBEMS_OK)
    {
        rProducerEMS.idmsg       = status;
        rProducerEMS.return_code = -1;
        return rProducerEMS;
    };
 
    /*
    .--------------------------------------------------. 
    | Connection_CreateSession |                       |
    | QueueConnection_CreateQueueSession               |
    +--------------------------------------------------+
    | Crea  una  sessione,  cioè un canale privata tra |
    | client  e   server  sfruttando  una  connessione |
    | aperta  tra  i  due,  in   pratica   il   server |
    | probabilmente genera  al  client  un  ID_SESSION |
    | per poi individuarlo.                            |
    '--------------------------------------------------' */
    status = tibemsConnection_CreateSession ( connection, &session, TIBEMS_FALSE, TIBEMS_AUTO_ACKNOWLEDGE );
    if (status != TIBEMS_OK)
    {
        rProducerEMS.idmsg       = status ;
        rProducerEMS.return_code = -2     ;
        return rProducerEMS               ;
    };

    /*
    .--------------------------------------------------.
    | Session_CreateProducer|QueueSession_CreateSender |
    +--------------------------------------------------+
    | Crea all'interno di una certa sessione un        |
    | entità chiamata producer, cioè un attore che     |
    | può scrivere su una destinazione. Esiste anche   |
    | API simili per scrivere su una destinazione e    |
    | con il medesimo prototipo come ad esempio        |
    | tibemsQueueSession_CreateSender. Forse son       |
    | semplici ridefinizioni di una medesima API....   |
    | boh! al momento non so, o forse alcune sono      |
    | deprecate e di API ne è rimasta solo una.        |
    | Leggere standard JMS che definisce le            |
    | interfaccie.                                     |
    '--------------------------------------------------' */
    status = tibemsSession_CreateProducer ( session, &msgProducer, destination ); 
    if (status != TIBEMS_OK)
    {
        rProducerEMS.idmsg       = status;
        rProducerEMS.return_code = -3;
        return rProducerEMS;
    }

    /*
    .-------------------------------------------------.
    | Connection_Start (Browser NO, Producer NO)      |
    +-------------------------------------------------+
    | Solo il Consumer sembra ne abbia bisogno        |
    |                                                 |
    '-------------------------------------------------' */
    // status = tibemsConnection_Start(connection);
    // if (status != TIBEMS_OK)
    // {
    //     rProducerEMS.idmsg       = status;
    //     rProducerEMS.return_code = -4;
    //     return rProducerEMS;
    // }

    /*
    .----------------------------------------------------.
    | Create OUTPUT  (solo Browser e Consumer)           |
    +----------------------------------------------------+
    | Crea il file di Output prima di iniziare a leggere |
    | il contenuto della code. Se il file non lo si      |
    | riesce ad aprire esce per errore.                  |
    '----------------------------------------------------' */
    // FILE *pOutputFile;
    // pOutputFile = fopen(OutputFile, "w");
    // if (!pOutputFile)
    // {
    //     rProducerEMS.idmsg       =  0;
    //     rProducerEMS.return_code = -5;
    //     return rProducerEMS;
    // }

    /*
    .-------------------------------------------------.
    | Write QUEUE                                     |
    +-------------------------------------------------+
    |                                                 |
    |                                                 |
    |                                                 |
    '-------------------------------------------------' */
    int messageInput = 0;
    while ( messageInput < MaxMsgWrite )
    {
            /*
            .-------------------------------------------------.
            | Session_CreateMessage                           |
            +-------------------------------------------------+
            | Crea nell'ambito di una specifica sessione      |
            | la struttura utilizzata per i messaggi.         |
            '-------------------------------------------------' */
            status = tibemsTextMsg_Create(&msg);
            if (status != TIBEMS_OK)
            {
                  rProducerEMS.idmsg       = status;
                  rProducerEMS.return_code = -10;
                  return rProducerEMS;
            }

            /*
            .------------------------------------------------.
            | TextMsg_SetText                                |
            +------------------------------------------------+
            |                                                |
            |                                                |
            |                                                |
            '------------------------------------------------' */ 
            status = tibemsTextMsg_SetText(msg, message);
            if ( status != TIBEMS_OK )
            {
                rProducerEMS.idmsg       = status;
                rProducerEMS.return_code = -11;
                return rProducerEMS;
            }

            /*
            .-----------------------------------------------. 
            | QueueSender_Send | MsgProducer_Send           |
            +-----------------------------------------------+
            | Il Producer trasmette un messaggio. Il primo  |
            | parametro (msgProducer) racchiude info su     |
            | connessione, sessione e destination.          | 
            '-----------------------------------------------' */ 
            status = tibemsQueueSender_Send ( msgProducer, msg );
            if ( status != TIBEMS_OK )
            {
                 rProducerEMS.idmsg       = status;
                 rProducerEMS.return_code = -12;
                 return rProducerEMS;
            }

            /*
            .---------------------------------------------.
            | Msg_Destroy                                 |
            +---------------------------------------------+
            | Distrugge il messaggio (forse è una free)   |
            | per liberare spazio di memoria se la        |
            | struttura dati non è più da utilizzare.     |
            '---------------------------------------------' */
            status = tibemsMsg_Destroy(msg);
            if ( status != TIBEMS_OK )
            {
                 rProducerEMS.idmsg       = status;
                 rProducerEMS.return_code = -13;
                 return rProducerEMS;
            }

            // /*
            // .-----------------------------------------------.
            // |                                               |
            // +-----------------------------------------------+
            // |                                               |
            // |                                               |
            // '-----------------------------------------------' */
            // fwrite(strResponse, 1, strlen(strResponse), pOutputFile);
            // if ( *** KO *** )
            // {
            //     rConsumerEMS.idmsg       = 0;
            //     rConsumerEMS.return_code = -14;
            //     return rConsumerEMS;
            // }

            messageInput++;
    };

    // /*
    // .----------------------------------------------------.
    // | Close OUTPUT                                       |
    // +----------------------------------------------------+
    // | Chiude il file di Output dopo aver terminato la    |
    // | la lettura della coda.                             |
    // '----------------------------------------------------' */
    // fclose (pOutputFile);
    // if ( *** KO *** )
    // {
    //     rConsumerEMS.idmsg       = 0;
    //     rConsumerEMS.return_code = -15;
    //     return rConsumerEMS;
    // }

    // /*
    // .-------------------------------------------------.
    // | Destroy Browser|Consumer|Producer               |
    // +-------------------------------------------------+
    // |                                                 |
    // |                                                 |
    // |                                                 |
    // '-------------------------------------------------' */
    // status = tibemsQueueBrowser_Close(queueBrowser);
    // if (status != TIBEMS_OK)
    // {
    //     rProducerEMS.idmsg       = status;
    //     rProducerEMS.return_code = -16;
    //     return rProducerEMS;
    // };  
    
    /*
    .-------------------------------------------------.
    | Destination_Destroy | Queue_Destroy             |
    +-------------------------------------------------+
    |                                                 |
    |                                                 |
    |                                                 |
    '-------------------------------------------------' */
    status = tibemsDestination_Destroy(destination);
    if (status != TIBEMS_OK)
    {
        rProducerEMS.idmsg       = status;
        rProducerEMS.return_code = -17;
        return rProducerEMS;
    }

    /*
    .-------------------------------------------.
    | Disconnessione Ok                         |
    '-------------------------------------------' */
    rProducerEMS.idmsg       = status ;
    rProducerEMS.return_code = 0      ;

    return rProducerEMS;
}



/*
.--------------------------------------------------------.
|                                                        |
| Function  : TEMSClient                                 |
|                                                        |
| Remark    : Client minimale TIBCO EMS                  |
|                                                        |
|                                                        |
|                                                        | 
'--------------------------------------------------------' */ 
extern struct tClientConnect TEMSClientConnect ( struct tClientConnectParameter rClientConnectParameter )
{
    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tClientConnect       rClientConnect      ;
    struct tClientConnect      *pClientConnect      ;

    struct tConnessioneEMS      rConnessioneEMS     ;
    struct tConnessioneEMS     *pConnessioneEMS     ;

    struct tDisconnessioneEMS   rDisconnessioneEMS  ;
    struct tDisconnessioneEMS  *pDisconnessioneEMS  ;

    struct tConnessioneEMSS     rConnessioneEMSS    ;
    struct tConnessioneEMSS    *pConnessioneEMSS    ;

    struct tDisconnessioneEMSS  rDisconnessioneEMSS ;
    struct tDisconnessioneEMSS *pDisconnessioneEMSS ;

    struct tRunOperationEMS     rRunOperationEMS    ;
    struct tRunOperationEMS    *pRunOperationEMS    ;

    /*
    .------------------------------------.
    | Allocazione Spazio                 |
    '------------------------------------' */
    pClientConnect        = malloc(sizeof(struct tClientConnect));
    pConnessioneEMS       = malloc(sizeof(struct tConnessioneEMS));
    pDisconnessioneEMS    = malloc(sizeof(struct tDisconnessioneEMS));
    pConnessioneEMSS      = malloc(sizeof(struct tConnessioneEMSS));
    pDisconnessioneEMSS   = malloc(sizeof(struct tDisconnessioneEMSS));
    pRunOperationEMS      = malloc(sizeof(struct tRunOperationEMS));
    
    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pClientConnect        = &rClientConnect;
    pConnessioneEMS       = &rConnessioneEMS;
    pDisconnessioneEMS    = &rDisconnessioneEMS;
    pConnessioneEMSS      = &rConnessioneEMSS;
    pDisconnessioneEMSS   = &rDisconnessioneEMSS;
    pRunOperationEMS      = &rRunOperationEMS;
    
    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rClientConnect      , 0, sizeof(rClientConnect));
    memset((void*)&rConnessioneEMS     , 0, sizeof(rConnessioneEMS));
    memset((void*)&rDisconnessioneEMS  , 0, sizeof(rDisconnessioneEMS));
    memset((void*)&rConnessioneEMSS    , 0, sizeof(rConnessioneEMSS));
    memset((void*)&rDisconnessioneEMSS , 0, sizeof(rDisconnessioneEMSS));
    memset((void*)&rRunOperationEMS    , 0, sizeof(rRunOperationEMS));
    
    /*
    .-------------------------.
    |                         |
    |           code          |
    |                         |
    '-------------------------' */

    /*
    .------------------------------------------.
    |  Determina il livello di DEBUG           |
    '------------------------------------------' */
    int debug_level = GetDebugLevel ( rClientConnectParameter.debug, "tems" ) ; 

    /*
    .-----------------------------------.
    | check connection                  |
    '-----------------------------------' */
    if ( rClientConnectParameter.auth_user == NULL ) {
         printf("The client did not specify any identity. *Warning* \n");
         printf("anonymous access.\n");
    }
    else
    {
         printf("The client did specify as identity [%s]\n", rClientConnectParameter.auth_user);
    };

    if ( strlen(rClientConnectParameter.risorsa) > 0 ) {
         printf("The client requested the access to resource [%s]\n", rClientConnectParameter.risorsa);
    };


    /* 
    .-----------------------------------.
    | connection string                 |
    '-----------------------------------' */
    char *strConnectTemplateEms = "%s://%s:%d" ; 
    char *strConnectEms;    
    strConnectEms = (char *)malloc(1024);

    if ( strcmp ( rClientConnectParameter.protocollo, "tems" ) == 0 )
    { 
         snprintf(strConnectEms, 1024, strConnectTemplateEms, "tcp", rClientConnectParameter.host, rClientConnectParameter.porta); 

         /*
         .------------------.
         | start connection |
         '------------------' */
         if ( strcmp ( rClientConnectParameter.protocollo, "tems" ) == 0 ) {
              rConnessioneEMS  = ConnessioneEMS  ( strConnectEms, rClientConnectParameter.auth_user, rClientConnectParameter.auth_password );
              rClientConnect.ServerConnectMsg    = rConnessioneEMS.idmsg       ;
              rClientConnect.ServerConnectReturn = rConnessioneEMS.return_code ;
         };

         /*
         .----------------------------------------.
         | visualizza esito della connessione     |
         '----------------------------------------' */
         switch(rClientConnect.ServerConnectReturn)
         {
              case  0 : PrintMessage(debug_level, TEMS_TABMSG, "CONNECT_EMS_OK"); break;
              default : PrintMessage(debug_level, TEMS_TABMSG, "CONNECT_EMS_KO"); break;
         };

         /*
         .-----------------------------------------------------------.
         | visualizza il messaggio di errore in caso di connessione  |
         | errata, prima di uscire.                                  |
         '-----------------------------------------------------------' */
         if ( rClientConnect.ServerConnectReturn != 0) { 
              char *msgConnect;         
              printf("ERROR: %s\n",msgConnect);
              printf("STATUS: %ld %s\n",rClientConnect.ServerConnectMsg,msgConnect?msgConnect:"(Undefined Error)");
              return rClientConnect ;
         };

         /*
         .--------------.
         | do operation |
         '--------------' */
         if ( rClientConnectParameter.risorsa != NULL )
         {
              rRunOperationEMS = RunOperationEMS ( rConnessioneEMS.connection,
                                                   rClientConnectParameter.risorsa,
                                                   rClientConnectParameter.type_operation,
                                                   rClientConnectParameter.MaxRequest,
                                                   rClientConnectParameter.message,
                                                   rClientConnectParameter.OutputFile );

              rClientConnect.ServerResponseBufferNum = rRunOperationEMS.NumMsg      ;
              rClientConnect.ServerResponseMsg       = rRunOperationEMS.idmsg       ;
              rClientConnect.ServerResponseReturn    = rRunOperationEMS.return_code ;

              if ( rClientConnect.ServerResponseReturn != 0)
                   return rClientConnect ;
         };

         /*
         .------------------.
         | start disconnect |
         '------------------' */
         if ( strcmp ( rClientConnectParameter.protocollo, "tems"  ) == 0 ) {
              rDisconnessioneEMS = DisconnessioneEMS  ( rConnessioneEMS.connection );
              rClientConnect.ServerDisconnectMsg    = rDisconnessioneEMS.idmsg       ;
              rClientConnect.ServerDisconnectReturn = rDisconnessioneEMS.return_code ;
         };

         /*
         .----------------------------------------.
         | visualizza esito della connessione     |
         '----------------------------------------' */
         switch(rClientConnect.ServerDisconnectReturn)
         {
              case  0 : PrintMessage(debug_level, TEMS_TABMSG, "DISCONNECT_EMS_OK"); break;
              default : PrintMessage(debug_level, TEMS_TABMSG, "DISCONNECT_EMS_KO");
         };

         /*
         .--------------------------------------------------------------.
         | visualizza il messaggio di errore in caso di disconnessione  |
         | errata, prima di uscire.                                     |
         '--------------------------------------------------------------' */
         if (rClientConnect.ServerDisconnectReturn != 0)
         {
             char *msgDisconnect ;
             printf("ERROR: %s\n",msgDisconnect);
             printf("STATUS: %ld %s\n",rClientConnect.ServerDisconnectMsg,msgDisconnect?msgDisconnect:"(Undefined Error)");
             return rClientConnect;
         };

    };

    
    if ( strcmp ( rClientConnectParameter.protocollo, "temss") == 0 )
    {
         snprintf(strConnectEms, 1024, strConnectTemplateEms, "ssl", rClientConnectParameter.host, rClientConnectParameter.porta);
  
         /*
         .------------------.
         | start connection |
         '------------------' */
         if ( strcmp ( rClientConnectParameter.protocollo, "temss") == 0 ) { 
              rConnessioneEMSS = ConnessioneEMSS ( strConnectEms, rClientConnectParameter.auth_user, rClientConnectParameter.auth_password, 
                       rClientConnectParameter.keystore, rClientConnectParameter.password, rClientConnectParameter.truststore );
              rClientConnect.ServerConnectMsg    = rConnessioneEMSS.idmsg       ;
              rClientConnect.ServerConnectReturn = rConnessioneEMSS.return_code ;
         };
   
         /*
         .----------------------------------------.
         | visualizza esito della connessione     |
         '----------------------------------------' */
         switch(rClientConnect.ServerConnectReturn)
         {
              case  0 : PrintMessage(debug_level, TEMS_TABMSG, "CONNECT_EMS_OK"); break;
              default : PrintMessage(debug_level, TEMS_TABMSG, "CONNECT_EMS_KO"); break;
         };

         /*
         .-----------------------------------------------------------.
         | visualizza il messaggio di errore in caso di connessione  |
         | errata, prima di uscire.                                  |
         '-----------------------------------------------------------' */
         if ( rClientConnect.ServerConnectReturn != 0) {
              char *msgConnect;
              printf("ERROR: %s\n",msgConnect);
              printf("STATUS: %ld %s\n",rClientConnect.ServerConnectMsg,msgConnect?msgConnect:"(Undefined Error)");
              return rClientConnect ;
         };
 
    
         /* 
         .--------------.
         | do operation |
         '--------------' */
         if ( strlen(rClientConnectParameter.risorsa) > 0 )
         {
              rRunOperationEMS = RunOperationEMS ( rConnessioneEMSS.connection,
                                                   rClientConnectParameter.risorsa,
                                                   rClientConnectParameter.type_operation,
                                                   rClientConnectParameter.MaxRequest, 
                                                   rClientConnectParameter.message,
                                                   rClientConnectParameter.OutputFile ); 
    
              rClientConnect.ServerResponseBufferNum = rRunOperationEMS.NumMsg      ; 
              rClientConnect.ServerResponseMsg       = rRunOperationEMS.idmsg       ;
              rClientConnect.ServerResponseReturn    = rRunOperationEMS.return_code ;


//======================================
    
              switch ( rClientConnect.ServerResponseReturn )
              {
                     case  0 : PrintMessage(debug_level, TEMS_TABMSG, "OPERATION_EMS_OK");
                               break;

                     default : PrintMessage(debug_level, TEMS_TABMSG, "OPERATION_EMS_KO");
              }

              if ( rClientConnect.ServerResponseReturn == 0 || 
                   rClientConnect.ServerResponseReturn < -1 )
                   printf("+Create Destination %s... ok\n", rClientConnectParameter.risorsa);
 
              if ( rClientConnect.ServerResponseReturn == 0 ||
                   rClientConnect.ServerResponseReturn < -2 )
                   printf("+Create Connection.Session... ok\n");
 
              if ( rClientConnect.ServerResponseReturn == 0 ||
                   rClientConnect.ServerResponseReturn < -3 )
                   printf("+Create Connection.Session.Browser... ok\n");

              /*
              .-----------------------------------------.
              | Output File                             |
              '-----------------------------------------' */
              if ( rClientConnect.ServerResponseReturn == 0 ||
                   rClientConnect.ServerResponseReturn < -4 )
              {
                   printf ("+Create Output File %s ... ok\n", rClientConnectParameter.OutputFile );
              };

              if ( rClientConnect.ServerResponseReturn == -4 )
              {
                   printf ("+Create Output File %s ... ko\n", rClientConnectParameter.OutputFile );
                   printf ("Open error.\n");
              };

              /*
              .-----------------------------------------.
              | Browse Response                         |
              '-----------------------------------------' */ 
              if ( rClientConnect.ServerResponseReturn == 0 ||
                   rClientConnect.ServerResponseReturn < -5 )
              {
                   if (rClientConnectParameter.MaxRequest == 0)
                   { 
                       printf("+Browse Queue %s... (ALL messages) *Warning*\n", rClientConnectParameter.risorsa); 
                   }
                   else
                   {
                       printf("+Browse Queue %s... (max %ld messages)\n",rClientConnectParameter.risorsa,rClientConnectParameter.MaxRequest);
                   }
              };

              /*
              .----------------------------------------.
              | Extract data.                          |
              '----------------------------------------' */
              if ( rClientConnect.ServerResponseReturn == 0 ||
                   rClientConnect.ServerResponseReturn < -6 )
                   printf("++Extract data from resource... ok\n"); 

              /* 
              .----------------------------------------.
              | Num. Response Read                     |
              '----------------------------------------' */
              if ( rClientConnect.ServerResponseReturn == 0 ||
                   rClientConnect.ServerResponseReturn < -6 )
              {
                   if ( rClientConnectParameter.MaxRequest == 0 )
                        printf ("+Browsed [%ld/%ld] Messages.\n", 
                               rClientConnect.ServerResponseBufferNum, 
                               rClientConnect.ServerResponseBufferNum );

                   if ( rClientConnectParameter.MaxRequest > 0 && 
                        rClientConnectParameter.MaxRequest <= rClientConnect.ServerResponseBufferNum )
                        printf ("+Browsed [%ld/%ld] Messages.\n",
                               rClientConnectParameter.MaxRequest, 
                               rClientConnect.ServerResponseBufferNum );

                   if ( rClientConnectParameter.MaxRequest  > 0 && 
                        rClientConnectParameter.MaxRequest >  rClientConnect.ServerResponseBufferNum )
                        printf ("+Browsed [%ld/%ld] Messages.\n",
                               rClientConnect.ServerResponseBufferNum, 
                               rClientConnect.ServerResponseBufferNum );
              }; 

              if ( rClientConnect.ServerResponseReturn == 0 ||
                   rClientConnect.ServerResponseReturn < -7 )
                   printf("+Close Connection.Session.Browser... ok\n");

              if ( rClientConnect.ServerResponseReturn == 0 ||
                   rClientConnect.ServerResponseReturn < -8 )
                   printf("+Destroy Destination... ok\n");

              char *msgResponse ;
              if ( rClientConnect.ServerResponseReturn != 0 )
              {
                   //%%%%LEVARE%%%% msgResponse = (char *) tibemsStatus_GetText(rClientConnect.ServerResponseMsg);
                   printf("ERROR: %s\n",msgResponse);
                   printf("STATUS: %ld %s\n",rClientConnect.ServerResponseMsg,msgResponse?msgResponse:"(Undefined Error)");
              };

//===========================


              if ( rClientConnect.ServerResponseReturn != 0)
                   return rClientConnect ;
         };
    
         /*
         .------------------.
         | start disconnect |
         '------------------' */ 
         if ( strcmp ( rClientConnectParameter.protocollo, "temss" ) == 0 ) { 
              rDisconnessioneEMSS = DisconnessioneEMSS ( rConnessioneEMSS.connection, rConnessioneEMSS.sslParams );
              rClientConnect.ServerDisconnectMsg    = rDisconnessioneEMSS.idmsg       ;
              rClientConnect.ServerDisconnectReturn = rDisconnessioneEMSS.return_code ;
         };

         /*
         .----------------------------------------.
         | visualizza esito della connessione     |
         '----------------------------------------' */
         switch(rClientConnect.ServerDisconnectReturn)
         {
              case  0 : PrintMessage(debug_level, TEMS_TABMSG, "DISCONNECT_EMS_OK"); break;
              default : PrintMessage(debug_level, TEMS_TABMSG, "DISCONNECT_EMS_KO");
         };

         /*
         .--------------------------------------------------------------.
         | visualizza il messaggio di errore in caso di disconnessione  |
         | errata, prima di uscire.                                     |
         '--------------------------------------------------------------' */
         if (rClientConnect.ServerDisconnectReturn != 0)
         {
             char *msgDisconnect ;
             printf("ERROR: %s\n",msgDisconnect);
             printf("STATUS: %ld %s\n",rClientConnect.ServerDisconnectMsg,msgDisconnect?msgDisconnect:"(Undefined Error)");
             return rClientConnect;
         };
     
    };

    return rClientConnect;

};


#endif
