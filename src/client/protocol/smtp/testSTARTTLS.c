 
#include <stdio.h>
#include <stdlib.h> 
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#define BUFFEr_SIZE 4096

void error(char *msg)
{
  perror(msg);
  exit(0);
}

int main(int argc, char *argv[])
{
 int sockfd, portno, n;
 struct sockaddr_in serv_addr;
 struct hostent *server;

 char buffer[BUFFEr_SIZE];
 if (argc < 3) {
    fprintf(stderr,"usage %s hostname port\n", argv[0]);
    exit(0);
 }
 portno = atoi(argv[2]);
 sockfd = socket(AF_INET, SOCK_STREAM, 0);
if (sockfd < 0) 
    error("ERROR opening socket");
server = gethostbyname(argv[1]);
if (server == NULL) {
    fprintf(stderr,"ERROR, no such host\n");
    exit(0);
}

bzero((char *) &serv_addr, sizeof(serv_addr));
serv_addr.sin_family = AF_INET;
bcopy((char *)server->h_addr, 
     (char *)&serv_addr.sin_addr.s_addr,
     server->h_length);
serv_addr.sin_port = htons(portno);
if (connect(sockfd,&serv_addr,sizeof(serv_addr)) < 0) 
    error("ERROR connecting");

n = read(sockfd,buffer,BUFFEr_SIZE-1);
if (n < 0) 
     error("ERROR reading from socket");
printf("\nCONNECT (Response): %s\n",buffer);

bzero(buffer,BUFFEr_SIZE);

/*------------------------------*/
printf("\nDONE\n");
printf("EHLO");

strcpy(buffer,"ehlo localhost\r\n");

n = write(sockfd,buffer,strlen(buffer));
if (n < 0) 
     error("ERROR writing to socket");
bzero(buffer,BUFFEr_SIZE);
n = read(sockfd,buffer,BUFFEr_SIZE-1);
if (n < 0) 
     error("ERROR reading from socket");
printf("\nHELO (Response): %s\n",buffer);
/*------------------------------*/    

/*------------------------------*/
printf("\nDONE\n");
printf("STARTTLS");

strcpy(buffer,"STARTTLS\r\n");

n = write(sockfd,buffer,strlen(buffer));
if (n < 0)
     error("ERROR writing to socket");
bzero(buffer,BUFFEr_SIZE);
n = read(sockfd,buffer,BUFFEr_SIZE-1);
if (n < 0)
     error("ERROR reading from socket");
printf("\nSTARTTLS (Response): %s\n",buffer);

printf("OKKKKKKKKKKKKKKKKKKKKKKK STARTTLS");
return 0;
/*------------------------------*/

return 0;
}  
