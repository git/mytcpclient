/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/*
.--------------------.
| header LibC        |
'--------------------' */
#include <string.h>

/*
.-------------------------.
| header common           |
'-------------------------' */
#include "client.h"
#include "output.h"

/*
.--------------------.
| header Protocolli  |
'--------------------' */
#include "tcp.h"
#include "ssl.h"

/*
.--------------------.
| header Locale      |
'--------------------' */
#include "smtp.h" 
#include "smtp_messages.h"


/*
.------------------------------------------------------------------------.
|                                                                        |
|  Funzione    : SMTPConnect                                             |
|                                                                        |
|  Descrizione : client SMTP minimale (connessione, disconnessione)      |
|                                                                        |
|                                                                        |
|                                                                        |
|                                                                        |
|                                                                        |
|                                                                        |
'------------------------------------------------------------------------'*/
extern struct tClientConnect SMTPClientConnect ( struct tClientConnectParameter rClientConnectParameter ) 
{
    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tClientConnect rClientConnect;
    struct tClientConnect *pClientConnect;

    /*
    .------------------------------------.
    | Allocazione Spazio                 |
    '------------------------------------' */
    pClientConnect = malloc(sizeof(struct tClientConnect));

    /*
    .------------------------------------.
    | Utilizzo dello spazio allocato     |
    '------------------------------------' */
    pClientConnect = &rClientConnect;

    /*
    .-----------------------------.
    | Inizializzazione Strutture  |
    '-----------------------------' */
    memset((void*)&rClientConnect, 0, sizeof(rClientConnect));
    rClientConnect.return_code = 0;


    /*
    .------------------------------------------.
    | Determina il livello di DEBUG            |
    '------------------------------------------' */
    int debug_level = GetDebugLevel ( rClientConnectParameter.debug, "smtp"  ) ;


    return rClientConnect ; 
};

