 
#include <stdio.h>
#include <stdlib.h> 
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#define BUFFEr_SIZE 4096

void error(char *msg)
{
  perror(msg);
  exit(0);
}

int main(int argc, char *argv[])
{
 int sockfd, portno, n;
 struct sockaddr_in serv_addr;
 struct hostent *server;

 char buffer[BUFFEr_SIZE];
 if (argc < 3) {
    fprintf(stderr,"usage %s hostname port\n", argv[0]);
    exit(0);
 }
 portno = atoi(argv[2]);
 sockfd = socket(AF_INET, SOCK_STREAM, 0);
if (sockfd < 0) 
    error("ERROR opening socket");
server = gethostbyname(argv[1]);
if (server == NULL) {
    fprintf(stderr,"ERROR, no such host\n");
    exit(0);
}

bzero((char *) &serv_addr, sizeof(serv_addr));
serv_addr.sin_family = AF_INET;
bcopy((char *)server->h_addr, 
     (char *)&serv_addr.sin_addr.s_addr,
     server->h_length);
serv_addr.sin_port = htons(portno);
if (connect(sockfd,&serv_addr,sizeof(serv_addr)) < 0) 
    error("ERROR connecting");

n = read(sockfd,buffer,BUFFEr_SIZE-1);
if (n < 0) 
     error("ERROR reading from socket");
printf("\nCONNECT (Response): %s\n",buffer);

bzero(buffer,BUFFEr_SIZE);

/*------------------------------*/
printf("\nDONE\n");
printf("EHLO");

strcpy(buffer,"ehlo localhost\r\n");

n = write(sockfd,buffer,strlen(buffer));
if (n < 0) 
     error("ERROR writing to socket");
bzero(buffer,BUFFEr_SIZE);
n = read(sockfd,buffer,BUFFEr_SIZE-1);
if (n < 0) 
     error("ERROR reading from socket");
printf("\nHELO (Response): %s\n",buffer);
/*------------------------------*/    

/*------------------------------*/
printf("\nDONE\n");
printf("STARTTLS");

strcpy(buffer,"STARTTLS\r\n");

n = write(sockfd,buffer,strlen(buffer));
if (n < 0)
     error("ERROR writing to socket");
bzero(buffer,BUFFEr_SIZE);
n = read(sockfd,buffer,BUFFEr_SIZE-1);
if (n < 0)
     error("ERROR reading from socket");
printf("\nSTARTTLS (Response): %s\n",buffer);

printf("OKKKKKKKKKKKKKKKKKKKKKKK STARTTLS");
return 0;
/*------------------------------*/

/*------------------------------*/
printf("\nDONE EHLO\n");
printf("AUTH");

strcpy(buffer,"AUTH LOGIN\n");

n = write(sockfd,buffer,strlen(buffer)+1);
if (n < 0) 
     error("ERROR writing to socket");
bzero(buffer,BUFFEr_SIZE);
n = read(sockfd,buffer,BUFFEr_SIZE-1);
if (n < 0) 
     error("ERROR reading from socket");
printf("%s\n",buffer);

printf("OKKKKKKKKKKKKKKKKKKKKKKK AUTHLOGIN");
return 0;

/*------------------------------*/ 

/*------------------------------*/
printf("\nDONE AUTH LOGIN\n");
printf("AUTH UID");

strcpy(buffer,"Mzc1MDIzMDc=");

n = write(sockfd,buffer,strlen(buffer));
if (n < 0) 
     error("ERROR writing to socket");
bzero(buffer,BUFFEr_SIZE);
n = read(sockfd,buffer,BUFFEr_SIZE-1);
if (n < 0) 
     error("ERROR reading from socket");
printf("%s\n",buffer);

/*------------------------------*/ 

/*------------------------------*/
printf("\nDONE UID\n");
printf("AUTH PWD");

strcpy(buffer,"Y2FtcGFuaWEuIUFnMQ==");

n = write(sockfd,buffer,strlen(buffer)+1);
if (n < 0) 
     error("ERROR writing to socket");
bzero(buffer,BUFFEr_SIZE);
n = read(sockfd,buffer,BUFFEr_SIZE-1);
if (n < 0) 
     error("ERROR reading from socket");
printf("%s\n",buffer);

/*------------------------------*/ 

/*------------------------------*/
printf("\nAUTH PWD DONE\n");
printf("MAIL FROM");

strcpy(buffer,"MAIL FROM:<angeloraffaele.granito@telecomitalia.it>");

n = write(sockfd,buffer,strlen(buffer));
if (n < 0) 
     error("ERROR writing to socket");
bzero(buffer,BUFFEr_SIZE);
n = read(sockfd,buffer,BUFFEr_SIZE-1);
if (n < 0) 
     error("ERROR reading from socket");
printf("%s\n",buffer);

/*------------------------------*/ 

/*------------------------------*/
printf("\nMAIL FROM DONE\n");
printf("RCPT TO");

strcpy(buffer,"RCPT TO:<angeloraffaele.granito@telecomitalia.it>");

n = write(sockfd,buffer,strlen(buffer));
if (n < 0) 
     error("ERROR writing to socket");
bzero(buffer,BUFFEr_SIZE);
n = read(sockfd,buffer,BUFFEr_SIZE-1);
if (n < 0) 
     error("ERROR reading from socket");
printf("%s\n",buffer);

/*------------------------------*/       

/*------------------------------*/
printf("RCPT TO MAILTO\n");

printf("DATA");

strcpy(buffer,"DATA\r\n");
n = write(sockfd,buffer,strlen(buffer));

strcpy(buffer,"Subject: test\r\n");
n = write(sockfd,buffer,strlen(buffer));

strcpy(buffer,"SMTP MAIL TOOL TEST WORKS!!!\r\n");
n = write(sockfd,buffer,strlen(buffer));

strcpy(buffer,"\n\n");
n = write(sockfd,buffer,strlen(buffer));

strcpy(buffer,".\n");
n = write(sockfd,buffer,strlen(buffer));

/*
Date: Tue, 05 Jun 2018 17:38:28 +0200
 ~> To: francesco.cecconi@telecomitalia.it
 ~> From: angeloraffaele.granito@telecomitalia.it
 ~> Subject: test Tue, 05 Jun 2018 17:38:28 +0200
 ~> Message-Id: <20180605173828.009572@100F00PF0VAEHM.telecomitalia.local>
 ~> X-Mailer: swaks v20170101.0 jetmore.org/john/code/swaks/
 ~>
 ~> This is a test mailing
 ~>
 ~> .
*/
/*------------------------------*/ 

/*------------------------------*/ 
printf("SON DONE");
strcpy(buffer,"quit\n");

n = write(sockfd,buffer,strlen(buffer));
if (n < 0) 
     error("ERROR writing to socket");
bzero(buffer,BUFFEr_SIZE);
n = read(sockfd,buffer,BUFFEr_SIZE-1);
if (n < 0) 
     error("ERROR reading from socket");
puts(buffer);

/*------------------------------*/

return 0;
}  
