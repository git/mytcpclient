/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it>
#
# This file is part of myTCPClient:
# Universal TCP Client
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/*
.-------------------------------------------------.
| Sub Struct Query String (Application Parameter) | 
'-------------------------------------------------' */
struct t_queryStringTabVar
{
   char                            *variable                      ;
   char                            *value                         ;
};

/*
.-------------------------------------.
| Input Struct Generic Connect Client |
'-------------------------------------' */
struct tClientConnectParameter
{
   char                            *frontend                      ;
   char                            *frontend_version              ;
   char                            *url                           ;
   char                            *protocollo                    ;
   char                            *host                          ;
   unsigned int                     netmask                       ;
   unsigned int                     porta                         ;
   char                            *risorsa_path                  ;
   char                            *risorsa                       ;
   int                              queryStringDimTabVariable     ;
   int                              queryStringLen                ;
   struct t_queryStringTabVar       queryStringTabVar[10]         ;
   char                            *httpProxyHost                 ;
   char                            *httpProxyPort                 ;
   char                            *httpProxyUser                 ;
   char                            *httpProxyPassword             ;
   char                            *auth_user                     ;
   char                            *auth_password                 ;
   char                            *keystore                      ;
   char                            *password                      ;
   char                            *truststore                    ;
   char                            *ciphersuite                   ;
   char                            *sslversion                    ;
   char                            *type_operation                ;
   char                            *message_language              ;
   char                            *message_type                  ;
   char                            *message                       ;
   char                            *SSLCipherSuiteServerScan      ;
   long                             MaxRequest                    ;
   long                             TCPReceiveTimeout             ;
   char                            *OutputFile                    ;
   char                            *debug                         ;
};


/*
.---------------------------------------.
| Output Struct Generic Connect Client  |
'---------------------------------------' */ 
struct tClientConnect
{
   char        *TCPIp                                                   ;
   int          TCPPort                                                 ;
   long         TCPConnectMsg                                           ;
   char        *TCPConnectMsgDesc                                       ;
   int          TCPConnectReturn                                        ;
// long         TCPReceiveTimeout                                       ; 
   long         TCPDisconnectMsg                                        ; 
   char        *TCPDisconnectMsgDesc                                    ;
   int          TCPDisconnectReturn                                     ;
   char        *SSLVersion                                              ;
   char         SSLCipherSuite_Client_List_Proposals[2048]              ; 
   int          SSLCipherSuite_Client_List_Proposals_Num                ;
   char        *SSLCipherSuite                                          ;
   int          SSLCipherSuite_AlgSim_LenKey                            ;
   char        *SSLCipherSuite_AlgSim_SSLVersion                        ; 
   char         SSLCipherSuite_NoUsed[2048]                             ;
   int          SSLCipherSuite_NoUsed_Num                               ; 
   long         SSLConnectMsg                                           ;
   char        *SSLConnectMsgDesc                                       ;
   int          SSLConnectReturn                                        ;
   long         SSLServerVerifyIdentityX509_Version                     ;
   long         SSLServerVerifyIdentityX509_SerialNumber                ;
   int          SSLServerVerifyIdentityX509_SignatureType               ;
   char         SSLServerVerifyIdentityX509_IssuerName[256]             ;
   char         SSLServerVerifyIdentityX509_NotBefore[15]               ;
   char         SSLServerVerifyIdentityX509_NotAfter[15]                ;
   char         SSLServerVerifyIdentityX509_SubjectName[256]            ;
   char         SSLServerVerifyIdentityX509_SubjectName_commonname[256] ;
   int          SSLServerVerifyIdentityX509_CertificateType             ;
   long         SSLServerVerifyIdentityMsg                              ;
   char        *SSLServerVerifyIdentityMsgDesc                          ; 
   int          SSLServerVerifyIdentityReturn                           ;
   long         SSLDisconnectMsg                                        ;
   char        *SSLDisconnectMsgDesc                                    ;
   int          SSLDisconnectReturn                                     ;
   long         ServerConnectMsg                                        ;
   char        *ServerConnectMsgDesc                                    ;
   int          ServerConnectReturn                                     ;
   int          ServerRequestBufferLen                                  ;
   char        *ServerRequestBuffer                                     ;
   long         ServerRequestMsg                                        ;
   char        *ServerRequestMsgDesc                                    ;
   int          ServerRequestReturn                                     ;
   long         ServerResponseBufferNum                                 ; 
   int          ServerResponseBufferLen                                 ;
   char        *ServerResponseBuffer                                    ;
   long         ServerResponseMsg                                       ;
   char        *ServerResponseMsgDesc                                   ; 
   int          ServerResponseReturn                                    ;
   long         ServerDisconnectMsg                                     ;
   char        *ServerDisconnectMsgDesc                                 ;
   int          ServerDisconnectReturn                                  ;
   int          return_code                                             ;
};

extern struct tClientConnect ClientConnect ( struct tClientConnectParameter ); 



// __EOF__
