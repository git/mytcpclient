/*
# Copyright (C) 2008-2009 
# Raffaele Granito <raffaele.granito@tiscali.it> 
#
# This file is part of myTCPClient:
# Universal TCP Client 
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/*
.--------------.
| headers LIBC |
'--------------' */
#include <sys/time.h>
#include <string.h>
#include <stdlib.h>

/*
.--------------------.
| header locale      |
'--------------------' */
#include "client.h"

/*
.--------------------.
| header _EXT        |
'--------------------' */
#include "output.h" 
#include "output_message2.h"

/*
.--------------------.
| header protocolli  | 
'--------------------' */
#include "tcp.h"
#include "ssl.h"
#include "http.h"
#include "ems.h"
#include "tems.h"

/*
.----------------------------------------------------------.
|                                                          |
|  Funzione   : INThandler                                 |
|                                                          |
|  Descrizione: Intercetta i segnali (example Control^C).  | 
|                                                          |
'----------------------------------------------------------'*/
/*
static volatile sig_atomic_t keep_running = 1;
void  INThandler(int sig)
{
     char  c;

     signal(sig, SIG_IGN);
     printf("OUCH, did you hit Ctrl-C?\n"
            "Do you really want to quit? [y/n] ");
     c = getchar();
     if (c == 'y' || c == 'Y')
  //    exit(0);
        keep_running = 0; 
     else
        signal(SIGINT, INThandler);
     getchar(); // Get new line character
}
*/

/*
.----------------------------------------------------------.
|                                                          |
|  Funzione   : clientConnect                              |
|                                                          |
|  Descrizione: Effettua la connessione al server          |
|                                                          |
'----------------------------------------------------------'*/
extern struct tClientConnect
ClientConnect ( struct tClientConnectParameter rClientConnectParameter )
{
    /*
    .--------------------------------------------.
    | Intercettare i segnali (example Control+C) |
    | Invocazione funzione INThandler [...]      | 
    '--------------------------------------------' */
// NON FUNZIONA
//    signal(SIGINT, INThandler);

    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliEMPTY[][3] =  {
            { "EOT"                    , "", "" }
    };
 
    /*
    .------------------------------------------.
    | Dichiarazione delle STRUTTURE            |
    '------------------------------------------' */
    struct tClientConnect  rClientConnect;
    struct tClientConnect *pClientConnect;

    struct tUri2SingleFieldConnect  rUri2SingleFieldConnect ;
    struct tUri2SingleFieldConnect *pUri2SingleFieldConnect ;

    /*
    .--------------------------------------------------.
    | Alloca lo spazio per contenere la struttura dati |
    | che va a riempire la funzione ClientConnect. La  |
    | seconda istruzione serve per dedicare lo spazio  |
    | allocato alla struttura dati. Successivamente    |
    | inizializza l'intera struttura con NULL ed il    |
    | campo return_code a ZERO.                        |
    '--------------------------------------------------' */
    pClientConnect = malloc(sizeof(struct tClientConnect));
    pClientConnect = &rClientConnect;
    memset((void*)&rClientConnect, 0, sizeof(rClientConnect));
    rClientConnect.return_code = 0;

    pUri2SingleFieldConnect = malloc(sizeof(struct tUri2SingleFieldConnect));
    pUri2SingleFieldConnect = &rUri2SingleFieldConnect;
    memset((void*)&rUri2SingleFieldConnect,0,sizeof(rUri2SingleFieldConnect));
    rUri2SingleFieldConnect.return_code = 0;

    /*
    .------------------------------------------.
    |  Determina il livello di DEBUG           |
    '------------------------------------------' */
    int debug_level = GetDebugLevel ( rClientConnectParameter.debug, "client" ) ;
    if ( rClientConnectParameter.debug == 0 )
           debug_level=1;

    /*
    .------------------------------------------------------------.
    | Tempi dell'intera sessione. ...::: Inizio :::...           |
    +------------------------------------------------------------+
    |                                                            |
    |                                                            |
    '------------------------------------------------------------' */
    struct timespec tstartSession={0,0}, tendSession={0,0}, tstart={0,0}, tend={0,0};                                              
    clock_gettime(CLOCK_MONOTONIC, &tstartSession);

    /*
    .-----------------------------------------------------------------------------------------.
    |  Verifica esistenza [OBBLIGATORIA URL] e spezzettamento stringa di connessione.         |
    +-----------------------------------------------------------------------------------------+
    |                                                                                         |
    |  Return Code:  -1 errore: Generale    * URI nullo                                       | 
    |                -2 errore: Protocollo  * Non Specificato (len 0)                         |
    |                -3 errore: Protocollo  * Missing Separatore con Hostname (://)           |
    |                -4 errore: Hostname    * Non Specificato (len 0)                         |
    |                -5 errore: Porta       * Valore non numerico                             |
    |                -6 errore: Porta       * Valore non valido ( ! 1..65535 )                |
    |                -7 errore: QueryString * Missing separatore URL/Query [?]                |
    |                -8 errore: QueryString * Missing QueryString (len 0)                     |
    |                -9 errore: QueryString * Lunga più di __DIM_QUERY_STRING__ caratteri     |
    |               -10 errore: QueryString * Missing separatore variabile                    |
    |               -11 errore: QueryString * Missing variable (len 0)                        |
    |               -12 errore: QueryString * Missing carattere di assegnazione valore        |
    |               -13 errore: Netmask     * Valore non numerico                             |
    |               -14 errore: Natmask     * Valore non valido ( ! 1..32 )                   |
    |                                                                                         |
    '-----------------------------------------------------------------------------------------' */
    rUri2SingleFieldConnect = Uri2SingleFieldConnect(rClientConnectParameter.url);
    switch ( rUri2SingleFieldConnect.return_code )
    {
         case 0 :

                  /*
                  .------------------------------------------.
                  |                protocollo                |
                  '------------------------------------------' */
                  rClientConnectParameter.protocollo = malloc(strlen(rUri2SingleFieldConnect.protocollo)+1);
                  memset(rClientConnectParameter.protocollo, 0, strlen(rClientConnectParameter.protocollo)+1);
                  strcpy(rClientConnectParameter.protocollo, rUri2SingleFieldConnect.protocollo);

                  /*
                  .------------------------------------------.
                  |                   host                   |
                  '------------------------------------------' */
                  rClientConnectParameter.host = malloc(strlen(rUri2SingleFieldConnect.host)+1);
                  memset(rClientConnectParameter.host, 0, strlen(rClientConnectParameter.host)+1);
                  strcpy(rClientConnectParameter.host, rUri2SingleFieldConnect.host);
				  
                  /*
                  .-----------------------------------------.
                  |                  netmask                |
                  '-----------------------------------------' */
                  rClientConnectParameter.netmask = rUri2SingleFieldConnect.netmask;
				  
                  /*
                  .-----------------------------------------.
                  |                   porta                 |
                  '-----------------------------------------' */
                  rClientConnectParameter.porta = rUri2SingleFieldConnect.porta;

                  /*
                  .-----------------------------------------.
                  |                path risorsa             |
                  '-----------------------------------------' */
                  rClientConnectParameter.risorsa_path = malloc(strlen(rUri2SingleFieldConnect.risorsa_path)+1);
                  memset(rClientConnectParameter.risorsa_path,0,
                  strlen(rClientConnectParameter.risorsa_path)+1);
                  strcpy(rClientConnectParameter.risorsa_path, rUri2SingleFieldConnect.risorsa_path);

                  /*
                  .-----------------------------------------.
                  |                  risorsa                |
                  '-----------------------------------------' */
                  rClientConnectParameter.risorsa = malloc(strlen(rUri2SingleFieldConnect.risorsa)+1);
                  memset(rClientConnectParameter.risorsa, 0, strlen(rClientConnectParameter.risorsa)+1);
                  strcpy(rClientConnectParameter.risorsa, rUri2SingleFieldConnect.risorsa);

                  /*
                  .-----------------------------------------.
                  |                 Parametri               |
                  '-----------------------------------------' */
                  int indice = 0 ;
                  while ( indice < rUri2SingleFieldConnect.queryStringDimTabVariable )
                  {
                          /*
                          .-------------------------------------------.
                          | Variabile                                 |
                          '-------------------------------------------' */
                          rClientConnectParameter.queryStringTabVar[indice].variable =
                               malloc(strlen(rUri2SingleFieldConnect.queryStringTabVariable[indice].variable)+1);

                          memset(rClientConnectParameter.queryStringTabVar[indice].variable,0,
                             strlen(rUri2SingleFieldConnect.queryStringTabVariable[indice].variable)+1);

                          strcpy(rClientConnectParameter.queryStringTabVar[indice].variable,
                                    rUri2SingleFieldConnect.queryStringTabVariable[indice].variable);
   
                          /*
                          .-------------------------------------------.
                          | Value                                     |
                          '-------------------------------------------' */
                          rClientConnectParameter.queryStringTabVar[indice].value =
                                malloc(strlen(rUri2SingleFieldConnect.queryStringTabVariable[indice].value)+1);

                          memset(rClientConnectParameter.queryStringTabVar[indice].value,0,
                                     strlen(rUri2SingleFieldConnect.queryStringTabVariable[indice].value)+1);

                          strcpy(rClientConnectParameter.queryStringTabVar[indice].value,
                                    rUri2SingleFieldConnect.queryStringTabVariable[indice].value);

                          indice++;
                  };

                  /*
                  .---------------------------------------.
                  | Dimensione della tabella              |
                  '---------------------------------------' */
                  rClientConnectParameter.queryStringDimTabVariable =
                      rUri2SingleFieldConnect.queryStringDimTabVariable ;


                  /*
                  .--------------------------------------------.
                  | Lunghezza totale della Query String        |
                  '--------------------------------------------' */
                  rClientConnectParameter.queryStringLen =
                        rUri2SingleFieldConnect.queryStringLen ;

                  break;

         case  -1 : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_MISSING"         ,TabVariabiliEMPTY, 0, 0, 0); break;
         case  -2 : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_PROTOCOLLO"      ,TabVariabiliEMPTY, 0, 0, 0); break;
         case  -3 : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_PROTOCOLLO_NOSEP",TabVariabiliEMPTY, 0, 0, 0); break;
         case  -4 : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_HOSTNAME"        ,TabVariabiliEMPTY, 0, 0, 0); break;
         case  -5 : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_PORTA_NONUM"     ,TabVariabiliEMPTY, 0, 0, 0); break;
         case  -6 : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_PORTA_NORANGE"   ,TabVariabiliEMPTY, 0, 0, 0); break;
         case  -7 : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_NOSEPURL"  ,TabVariabiliEMPTY, 0, 0, 0); break;
         case  -8 : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_LEN0"      ,TabVariabiliEMPTY, 0, 0, 0); break;
         case  -9 : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_NOTLENMAX" ,TabVariabiliEMPTY, 0, 0, 0); break;
         case -10 : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_NOSEPVAR"  ,TabVariabiliEMPTY, 0, 0, 0); break;
         case -11 : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_VARLEN0"   ,TabVariabiliEMPTY, 0, 0, 0); break;
         case -12 : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_QUERY_NOASSIGN"  ,TabVariabiliEMPTY, 0, 0, 0); break;
         case -13 : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_NETMASK_NONUM"   ,TabVariabiliEMPTY, 0, 0, 0); break;
         case -14 : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_NETMASK_NORANGE" ,TabVariabiliEMPTY, 0, 0, 0); break;
         default  : PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__ERRORE_STRINGA_CONNESSIONE_UNKNOWN"         ,TabVariabiliEMPTY, 0, 0, 0); break;
    };

    if ( rUri2SingleFieldConnect.return_code < 0 )
    {
         PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__FORMATO", TabVariabiliEMPTY, 0, 0, 0);
         return rClientConnect ;
    };



	/*
	.------------------------------------------------------------------------.
    |                                                                        |
	|                           Analisi Network                              |
	|                                                                        |
	'------------------------------------------------------------------------' */

	#include "strIPv4.h"
	
	unsigned long IPv4LongGateway  ;
	
	/* Si tratta di una Network? */
	if ( rClientConnectParameter.netmask > 0 ) 
	{
		/*
		.-----------------------------------------------.
		| Risoluzione Hostname                          |
		'-----------------------------------------------' */
		struct tTCPGetHostByName  rTCPGetHostByName;
		rTCPGetHostByName = TCPGetHostByName ( rClientConnectParameter.host, 0 ); // debug_level );
		if ( rTCPGetHostByName.return_code < 0 )
		{
			printf("\nTCPGetHostByName Error [%d]", rTCPGetHostByName.return_code);
			exit(1);
		}

		/*
		.-----------------------------------------------------------------.
		| converte formato IPv4 Host/Network (string->stringBinary)       | 
		'-----------------------------------------------------------------' */
		struct tConvIPv4Str2StringBinary rConvIPv4Str2StringBinary__HOST;
		rConvIPv4Str2StringBinary__HOST = convIPv4Str2StringBinary (rTCPGetHostByName.tabAddress[0]); // rClientConnectParameter.host);
		if (rConvIPv4Str2StringBinary__HOST.ret < 0) {
			printf("\nconvIPv4Str2StringBinary Error [%d]", rConvIPv4Str2StringBinary__HOST.ret);
			exit(1);
		}
		
		/*
		.-----------------------------------------------------------.
		| converte formato IPv4 Netmask (string->stringBinary)      | 
		'-----------------------------------------------------------' */
		struct tIPv4LenNet2Netmask rIPv4LenNet2Netmask ;
		struct tConvIPv4Str2StringBinary rConvIPv4Str2StringBinary__NETMASK;
		rIPv4LenNet2Netmask = IPv4LenNet2Netmask (rClientConnectParameter.netmask) ;
		if (rIPv4LenNet2Netmask.ret < 0) {
			printf("\nIPv4LenNet2Netmask Error [%d]", rIPv4LenNet2Netmask.ret);
			exit(1);
		}
		strcpy (rConvIPv4Str2StringBinary__NETMASK.IPv4binaryString, rIPv4LenNet2Netmask.IPv4binaryString);

		/*
		.------------------------.
		| Analisi delle Network  |
		'------------------------' */
		struct tGetListHostNetworkIPv4 rGetListHostNetworkIPv4;
		rGetListHostNetworkIPv4 = getListHostNetworkIPv4
			( rConvIPv4Str2StringBinary__HOST.IPv4binaryString,
				rConvIPv4Str2StringBinary__NETMASK.IPv4binaryString);
		if (rGetListHostNetworkIPv4.ret < 0) {
			printf("\nError [%d]", rGetListHostNetworkIPv4.ret);
			exit(1);
		}

//==============================================================

    /*
    .--------------------------------------.
    | converte l'IPv4 Long in string       |
    '--------------------------------------' */
    struct tConvIPv4Long2Str rConvIPv4Long2Str__NETWORK;
    rConvIPv4Long2Str__NETWORK = convIPv4Long2Str (rGetListHostNetworkIPv4.IPv4LongNetwork);
    if (rConvIPv4Long2Str__NETWORK.ret < 0) {
       printf("\nError [%d]", rConvIPv4Long2Str__NETWORK.ret);
       exit(1);
    }

    /*
    .--------------------------------------.
    | converte l'IPv4 Long in string       |
    '--------------------------------------' */
    struct tConvIPv4Long2Str rConvIPv4Long2Str__GATEWAY;
    rConvIPv4Long2Str__GATEWAY = convIPv4Long2Str (rGetListHostNetworkIPv4.IPv4LongGateway);
    if (rConvIPv4Long2Str__GATEWAY.ret < 0) {
       printf("\nError [%d]", rConvIPv4Long2Str__GATEWAY.ret);
       exit(1);
    }
	
    /*
    .-----------------------------------------.
    | converte l'IPv4 stringBinary in Stringa |
    '-----------------------------------------' */
    struct tConvIPv4StringBinary2Str rConvIPv4StringBinary2Str__NETMASK;
    rConvIPv4StringBinary2Str__NETMASK = convIPv4StringBinary2Str (rConvIPv4Str2StringBinary__NETMASK.IPv4binaryString);
    if (rConvIPv4StringBinary2Str__NETMASK.ret < 0) {
       printf("\nError [%d]", rConvIPv4StringBinary2Str__NETMASK.ret);
       exit(1);
    }
	
    /*
    .--------------------------------------.
    | converte l'IPv4 Long in string       |
    '--------------------------------------' */
    struct tConvIPv4Long2Str rConvIPv4Long2Str__BROADCAST;
    rConvIPv4Long2Str__BROADCAST = convIPv4Long2Str (rGetListHostNetworkIPv4.IPv4LongBroadcast);
    if (rConvIPv4Long2Str__BROADCAST.ret < 0) {
       printf("\nError [%d]", rConvIPv4Long2Str__BROADCAST.ret);
       exit(1);
    }


//==============================================================
/*
	     printf(
	        "\nIPv4 getListHostNetwork * "
            "\n - HostStringBinary[%s]"
            "\n - NetmaskStringBinary[%s][%s] : "
            "\n - networkLen[%d]"
            "\n - NetworkLong[%lu][%s]"
            "\n - GatewayLong[%lu][%s]"
            "\n - BroadcastLong[%lu][%s]"
            "\n - numHostNetwork[%lu]"
            "\n - numHost[%lu]"
            "\n - ret[%d]",
			rClientConnectParameter.host,
			rConvIPv4Str2StringBinary__NETMASK.IPv4binaryString, rConvIPv4StringBinary2Str__NETMASK.IPv4Str,
            rGetListHostNetworkIPv4.networkLen,
            rGetListHostNetworkIPv4.IPv4LongNetwork            , rConvIPv4Long2Str__NETWORK.IPv4Str, 
            rGetListHostNetworkIPv4.IPv4LongGateway            , rConvIPv4Long2Str__GATEWAY.IPv4Str,
            rGetListHostNetworkIPv4.IPv4LongBroadcast          , rConvIPv4Long2Str__BROADCAST.IPv4Str,
            rGetListHostNetworkIPv4.numHostNetwork,
            rGetListHostNetworkIPv4.numHost,
            rGetListHostNetworkIPv4.ret );
*/

	     printf( "Netmask[%s] Net[%s] GW[%s] Broadcast[%s] Numero Host[%lu]\n\n", 
					rConvIPv4StringBinary2Str__NETMASK.IPv4Str,
						rConvIPv4Long2Str__NETWORK.IPv4Str,
							rConvIPv4Long2Str__GATEWAY.IPv4Str,
								rConvIPv4Long2Str__BROADCAST.IPv4Str,
									rGetListHostNetworkIPv4.numHostNetwork );

//=============================================================================					

		/*
		.----------------------------------------------.
		| Get Network.Gateway (primo host delle rete   |
		'----------------------------------------------' */
		IPv4LongGateway = rGetListHostNetworkIPv4.IPv4LongGateway ;

		/*
		.----------------------------------------------.
		| Setting MaxRequest -- Get Network.numHost    |
		'----------------------------------------------' */
		rClientConnectParameter.MaxRequest = rGetListHostNetworkIPv4.numHostNetwork ;  /*
		--------------------------------------------------------------------------------*/
	}
	
	struct tConvIPv4Long2Str rConvIPv4Long2Str__HOST;
	


    /*
    .------------------------------------------------------------.
    |  Connessioni al SERVER [Ciclo]                             |
    +------------------------------------------------------------+
    |                                                            |
    |                                                            |
    '------------------------------------------------------------' */
    /*
    .------------------.
    | Inizializzazioni | 
    '------------------' */
    int flag_unsupported_protocol = 0;
    int sequence = 0 ;
    int contaPacchettiInviati = 0 ;
    int contaPacchettiRicevuti = 0 ;
    double tempo_min_singola_connessione = 0 ;
    double tempo_max_singola_connessione = 0 ;

    /* Default Setting :: rClientConnectParameter.SSLCipherSuiteServerScan */
    if ( rClientConnectParameter.SSLCipherSuiteServerScan == NULL ) { 
         rClientConnectParameter.SSLCipherSuiteServerScan = malloc(strlen("Off\0")+1);
         strcpy(rClientConnectParameter.SSLCipherSuiteServerScan, "Off\0"); 
    };

    /* Default Setting :: rClientConnectParameter.MaxRequest ONLY no-used SSLCipherSuiteServerScan function */
    if ( rClientConnectParameter.MaxRequest == 0 && ( strcasecmp(rClientConnectParameter.SSLCipherSuiteServerScan,"ON") != 0 )) 
          rClientConnectParameter.MaxRequest = 1 ; 

    /*
    .-----------------------------.
    | Cliclo Infinito Connessioni |
    '-----------------------------' */ 
    for (;;)
    {
            sequence++;
            /*
            **************************************************************
            ***                                                        ***
            ***                 C O N N E S S I O N E                  ***
            ***                                                        ***
            ************************************************************** */

            /*
			.-------------------------------------------------.
			| Per ANALISI di NETWORK - Incrementa HOST        |
			'-------------------------------------------------' */
			if ( rClientConnectParameter.netmask > 0 ) 
			{
				/*
				.--------------------------------------.
				| converte l'IPv4 Long in string       |
				'--------------------------------------' */
				rConvIPv4Long2Str__HOST = convIPv4Long2Str (IPv4LongGateway);
				if (rConvIPv4Long2Str__HOST.ret < 0) {
					printf("\nError [%d]", rConvIPv4Long2Str__HOST.ret);
					exit(1);
				}
				/*
				----------------------------------------------------------------------------- */
				strcpy ( rClientConnectParameter.host , rConvIPv4Long2Str__HOST.IPv4Str );   /*
				------------------------------------------------------------------------------*/

				IPv4LongGateway++;
			}

            /*
            .------------------------------------------------------------------.
            | Print Parametri Connessione.                                     |
            +------------------------------------------------------------------+
            | Visualizza i parametri impostati analizzati dalla routine per    |
            | attività di DEBUG. (DEBUG = 1|0 * Attiva|Non Attiva).            |
            '------------------------------------------------------------------' */
            int DEBUG = 0 ;
            if ( DEBUG == 1 )
                  clientConnectParameterPrint ( rClientConnectParameter ) ;


            /*
            .------------------------------------------------------------.
            | Tempo INIZIALE * Connessione al Server                     |
            +------------------------------------------------------------+
            |                                                            |
            |                                                            |
            '------------------------------------------------------------' */
            clock_gettime(CLOCK_MONOTONIC, &tstart);


            /*
            .------------------------------------------------------------.
            | Connessione al Server                                      |
            +------------------------------------------------------------+
            |                                                            |
            |                                                            |
            '------------------------------------------------------------' */
            /* TCP Protocol */ 
            if ( strcmp ( rClientConnectParameter.protocollo , "tcp" ) == 0 )  
            	rClientConnect = TCPClientConnect ( rClientConnectParameter );

            /* SSL Protocol */ 
            if ( strcmp ( rClientConnectParameter.protocollo , "ssl" ) == 0 )  
                 rClientConnect = SSLClientConnect ( rClientConnectParameter );

            /* HTTP/HTTPS Protocol */ 
            if ( strcmp ( rClientConnectParameter.protocollo , "http"  ) == 0 || 
                 strcmp ( rClientConnectParameter.protocollo , "https" ) == 0 )  
                 rClientConnect = HTTPClientConnect ( rClientConnectParameter ); 

            /* EMS/EMSS Protocol */ 
            if ( strcmp ( rClientConnectParameter.protocollo , "ems"  ) == 0 || 
                 strcmp ( rClientConnectParameter.protocollo , "emss" ) == 0 )  
                 rClientConnect = EMSClientConnect ( rClientConnectParameter );

            /* TIBCO EMS Protocol */ 
            if ( strcmp ( rClientConnectParameter.protocollo , "tems" ) == 0 ||
                 strcmp ( rClientConnectParameter.protocollo , "temss") == 0 )  
                 rClientConnect = TEMSClientConnect ( rClientConnectParameter );

            /* UNSUPPORTED Protocol */
            if ( strcmp ( rClientConnectParameter.protocollo , "tcp"   ) != 0 && 
                 strcmp ( rClientConnectParameter.protocollo , "ssl"   ) != 0 && 
                 strcmp ( rClientConnectParameter.protocollo , "http"  ) != 0 && 
                 strcmp ( rClientConnectParameter.protocollo , "https" ) != 0 && 
                 strcmp ( rClientConnectParameter.protocollo , "ems"   ) != 0 && 
                 strcmp ( rClientConnectParameter.protocollo , "emss"  ) != 0 && 
                 strcmp ( rClientConnectParameter.protocollo , "tems"  ) != 0 && 
                 strcmp ( rClientConnectParameter.protocollo , "temss" ) != 0 )
                 flag_unsupported_protocol = 1; 


				 
            /*
            .------------------------------------------------------------.
            | Uniforma Return-Code                                       |
            +------------------------------------------------------------+
            |                                                            |
            |                                                            |
            '------------------------------------------------------------' */
            long msg = 0; 
            char *msgDesc = "";

            if ( rClientConnect.return_code < 0 ) 
            {  
                 /* ...::: TCP Connect Error :::... */
                 if ( rClientConnect.TCPConnectReturn != 0 ) { 
                      msg = rClientConnect.TCPConnectMsg;
                      msgDesc = rClientConnect.TCPConnectMsgDesc; 
                 };

                 /* ...::: SSL Connect Error :::... */
                 if ( rClientConnect.TCPConnectReturn == 0 &&
                      rClientConnect.SSLConnectReturn != 0 ) { 
                      msg = rClientConnect.SSLConnectMsg; 
                      msgDesc = rClientConnect.SSLConnectMsgDesc; 
                 };

                 /* ...::: SSL Verify Identity Error :::... */ 
                 if ( rClientConnect.TCPConnectReturn == 0 &&
                      rClientConnect.SSLConnectReturn == 0 &&
                      rClientConnect.SSLServerVerifyIdentityReturn <  0 ) { 
                      msg = rClientConnect.SSLServerVerifyIdentityMsg; 
                      msgDesc = rClientConnect.SSLServerVerifyIdentityMsgDesc; 
                 };

                 /* ...::: Server Connect Error :::... */
                 if ( rClientConnect.TCPConnectReturn == 0 &&
                      rClientConnect.SSLConnectReturn == 0 &&
                      rClientConnect.SSLServerVerifyIdentityReturn >= 0 &&
                      rClientConnect.ServerConnectReturn != 0 ) { 
                      msg = rClientConnect.ServerConnectMsg; 
                      msgDesc = rClientConnect.ServerConnectMsgDesc; 
                 }; 

                 /* ...::: Server Send Request Error :::... */
                 if ( rClientConnect.TCPConnectReturn == 0 &&
                      rClientConnect.SSLConnectReturn == 0 &&
                      rClientConnect.SSLServerVerifyIdentityReturn >= 0 &&
                      rClientConnect.ServerConnectReturn == 0 &&
                      rClientConnect.ServerRequestReturn != 0 ) { 
                      msg = rClientConnect.ServerRequestMsg; 
                      msgDesc = rClientConnect.ServerRequestMsgDesc; 
                 }; 

                 /* ...::: Server Receive Response Error :::... */
                 if ( rClientConnect.TCPConnectReturn == 0 &&
                      rClientConnect.SSLConnectReturn == 0 &&
                      rClientConnect.SSLServerVerifyIdentityReturn >= 0 &&
                      rClientConnect.ServerConnectReturn == 0 &&
                      rClientConnect.ServerRequestReturn == 0 &&
                      rClientConnect.ServerResponseReturn != 0 ) { 
                      msg = rClientConnect.ServerResponseMsg; 
                      msgDesc = rClientConnect.ServerResponseMsgDesc; 
                 };

                 /* ...::: Server Disconnect Error :::... */
                 if ( rClientConnect.TCPConnectReturn == 0 &&
                      rClientConnect.SSLConnectReturn == 0 &&
                      rClientConnect.SSLServerVerifyIdentityReturn >= 0 &&
                      rClientConnect.ServerConnectReturn == 0 &&
                      rClientConnect.ServerRequestReturn == 0 &&
                      rClientConnect.ServerResponseReturn == 0 &&
                      rClientConnect.ServerDisconnectReturn != 0 ) { 
                      msg = rClientConnect.ServerDisconnectMsg; 
                      msgDesc = rClientConnect.ServerDisconnectMsgDesc; 
                 };

                 /* ...::: SSL Disconnect Error :::... */
                 if ( rClientConnect.TCPConnectReturn == 0 &&
                      rClientConnect.SSLConnectReturn == 0 &&
                      rClientConnect.SSLServerVerifyIdentityReturn >= 0 &&
                      rClientConnect.ServerConnectReturn == 0 &&
                      rClientConnect.ServerRequestReturn == 0 &&
                      rClientConnect.ServerResponseReturn == 0 &&
                      rClientConnect.ServerDisconnectReturn == 0 &&
                      rClientConnect.SSLDisconnectReturn != 0 ) { 
                      msg = rClientConnect.SSLDisconnectMsg; 
                      msgDesc = rClientConnect.SSLDisconnectMsgDesc; 
                 };

                 /* ...::: TCP Disconnect Error :::... */
                 if ( rClientConnect.TCPConnectReturn == 0 &&
                      rClientConnect.SSLConnectReturn == 0 &&
                      rClientConnect.SSLServerVerifyIdentityReturn >= 0 &&
                      rClientConnect.ServerConnectReturn == 0 &&
                      rClientConnect.ServerRequestReturn == 0 &&
                      rClientConnect.ServerResponseReturn == 0 &&
                      rClientConnect.ServerDisconnectReturn == 0 &&
                      rClientConnect.SSLDisconnectReturn == 0 &&
                      rClientConnect.TCPDisconnectReturn != 0 ) { 
                      msg = rClientConnect.TCPDisconnectMsg; 
                      msgDesc = rClientConnect.TCPDisconnectMsgDesc; 
                 };
            }; 

            if ( flag_unsupported_protocol == 1 ) {
                 msg     = -1;
                 msgDesc = "CLIENT.PROTOCOL.USED.IS.NOT.SUPPORTED";
            };
 
            /*
            .------------------------------------------------------------.
            | Tempo FINALE * Connessione al Server                       |
            +------------------------------------------------------------+
            |                                                            |
            |                                                            |
            '------------------------------------------------------------' */
            clock_gettime(CLOCK_MONOTONIC, &tend);


            /*
            .------------------------------------------------------------.
            | Statistica Singole Connessioni                             |
            +------------------------------------------------------------+
            |                                                            |
            | 1. Pacchetti entranti (request)                            |
            | 2. Pacchetti uscenti (response)                            |
            | 3. Tempo di connessione al server (tempo di risposta)      |
            | 4. Tempo di connessione al server MIN (parziale)           |
            | 5. Tempo di connessione al server MAX (parziale)           |
            |                                                            |
            '------------------------------------------------------------' */

            /* ...::: Conteggia Pacchetti :: Entranti :::... */
            if ( rClientConnect.ServerRequestBufferLen > 0 )
                 contaPacchettiInviati++ ;

            /* ...::: Conteggia Pacchetti :: Uscenti :::... */
            if ( rClientConnect.ServerResponseBufferLen > 0 )
                 contaPacchettiRicevuti++ ;

            /* ...::: Calcola Tempo FINALE * Connessione al Server :::... */
   			double tempo_totale_singola_connessione = 
					(((double)tend.tv_sec   + 1.0e-9*tend.tv_nsec) - 
                     ((double)tstart.tv_sec + 1.0e-9*tstart.tv_nsec));

            /* ...::: Registra il tempo di connessione parziale MINORE (MIN) :::... */
            if ( tempo_totale_singola_connessione < tempo_min_singola_connessione || sequence == 0 )
                 tempo_min_singola_connessione = tempo_totale_singola_connessione ;

            /* ...::: Registra il tempo di connessione parziale MAGGIORE (MAX) :::... */
            if ( tempo_totale_singola_connessione > tempo_max_singola_connessione || sequence == 0 ) 
                 tempo_max_singola_connessione = tempo_totale_singola_connessione ;




            /*
            .------------------------------------------------------------.
            | Verify Exit Condition                                      |
            +------------------------------------------------------------+
            |                                                            |
            |                                                            |
            '------------------------------------------------------------' */

            /*
            .---------------------------------------------------.
            |                                                   |
            |             Gestione Scan CipherSuite             |
            |                                                   |
            '---------------------------------------------------' */
            if ( strcasecmp(rClientConnectParameter.SSLCipherSuiteServerScan,"ON") == 0 )
            {
                 /*
                 .-------------------------------------------.
                 | Stop Scanning for HandShake Error         |
                 '-------------------------------------------' */
                 if ( rClientConnect.SSLConnectReturn == -5 )
                 {
                    /* --- stampa CipherSuite proposals al server in caso di handshake errore --- */
                    if ( rClientConnect.SSLCipherSuite_Client_List_Proposals_Num > 0 )
                         clientConnectSSLCipherSCliePropErr ( rClientConnect );

                    break; 
                 }; 

                 /*
                 .-------------------------------------------.
                 | Stop Scanning for Cipher terminated       |
                 '-------------------------------------------' */
                 if ( rClientConnect.SSLCipherSuite_NoUsed_Num == 0 )  
                 {
                    /* --- stampa riga di dettaglio --- */
                    clientConnectPrintDetails 
                            ( rClientConnectParameter, rClientConnect,
                                  tempo_totale_singola_connessione, sequence, msg, msgDesc ) ;
                    break;
                 };

                 /*
                 .-------------------------------------------------------------------------.
                 | Propone come CipherSuite l'intera lista di quelle non ancora utilizzate |
                 '-------------------------------------------------------------------------' */
                 rClientConnectParameter.ciphersuite = 
                     malloc(strlen(rClientConnect.SSLCipherSuite_NoUsed)+1);
                 strcpy ( rClientConnectParameter.ciphersuite, 
                     rClientConnect.SSLCipherSuite_NoUsed );
            };
			
            /*
            .---------------------------------------------------.
            |                                                   |
            |                Gestione MaxRequest                |
            |                                                   |
            '---------------------------------------------------' */
            if ( rClientConnectParameter.MaxRequest > 0 )
            {
  		         /*
                 .--------------------------------.
                 | Se impostato MAX-Tentativi.    |
                 '--------------------------------' */
                 if ( sequence >= rClientConnectParameter.MaxRequest )   
                 {
                      /* --- stampa riga di dettaglio --- */
                      clientConnectPrintDetails
                        ( rClientConnectParameter, rClientConnect,
                             tempo_totale_singola_connessione, sequence, msg, msgDesc ) ;
                      break;
                 }; 
            };
			
			
            /*
            .---------------------------------------------------.
            |                                                   |
            |             Interruzione (control^C)              |
            |                                                   |
            '---------------------------------------------------' */
// NON FUNZIONA
//            if (keep_running == 0) break; 


            /*
            .------------------------------------------------------------.
            | Print                                                      |
            +------------------------------------------------------------+
            |                                                            |
            |                                                            |
            '------------------------------------------------------------' */

            /* ...::: stampa riga di testa :::... */
            if ( sequence == 1 )  
                clientConnectPrintHead 
                    ( rClientConnectParameter, rClientConnect ) ; 
					
            /* ...::: stampa riga di dettaglio :::... */
            clientConnectPrintDetails
                    ( rClientConnectParameter, rClientConnect, 
                          tempo_totale_singola_connessione, sequence, msg, msgDesc ) ; 

            /*
            .------------------------------------------------------------.
            | Pause [NOP] ...::: da parametrizzare :::...                |
            +------------------------------------------------------------+
            |                                                            |
            |                                                            |
            '------------------------------------------------------------' */
            // ---- sleep(1);
    };



    /*
    .------------------------------------------------------------.
    | Tempi dell'intera sessione. ...::: Fine :::...             |
    +------------------------------------------------------------+
    |                                                            |
    |                                                            |
    '------------------------------------------------------------' */
    clock_gettime(CLOCK_MONOTONIC, &tendSession);

    /* ...::: Calcola il Tempo dell'Intera Sessione :::... */ 
    double tempo_totale_intera_sessione = 
 		(((double)tendSession.tv_sec   + 1.0e-9*tendSession.tv_nsec) - 
         ((double)tstartSession.tv_sec + 1.0e-9*tstartSession.tv_nsec));

    /*
    .------------------------------------------------------------.
    | Print Statistiche                                          |
    +------------------------------------------------------------+
    | Se ci è stata almeno una connessione.                      |
    |                                                            |
    |                                                            |
    '------------------------------------------------------------' */
    if ( sequence > 1 ) { 
         clientConnectPrintStatistic ( rClientConnectParameter,
                                       rClientConnect,
                                       tempo_totale_intera_sessione, 
                                       contaPacchettiInviati,
                                       contaPacchettiRicevuti,
                                       tempo_min_singola_connessione,
                                       tempo_max_singola_connessione ) ; 
    };

    return rClientConnect;
}



/*
.---------------------------------------------------------------------------.
|                                                                           |
|  Funzione    : clientConnectPrintHead                                     |
|                                                                           |
|  Descrizione : Print Head Row                                             |
|                                                                           |
|  Parameter.                                                               |
|                                                                           |
|     %protocollo%              - Protocol                                  |
|     %host%                    - Host Target                               |
|     %porta%                   - Port Target                               |
|     %risorsa_path%            - Path Resouce Target                       |
|     %risorsa%                 - Resource Target                           |
|     %TCPIp%                   - Host Target Resolved (IP)                 |
|     %ServerRequestBufferLen%  - Package Request Len                       | 
|                                                                           |
|  Mask.                                                                    |
|                                                                           |
|     PING  %protocollo%://%host%:%porta%%risorsa_path%%risorsa%            |
|      (%TCPIp%) %ServerRequestBufferLen%(%ServerRequestBufferLen%)         |
|          bytes of data.                                                   |
|                                                                           |
|  Example.                                                                 |
|                                                                           |
|     PING http://www.domain.it:80/ (192.168.1.1) 77(77) bytes of data.     | 
|                                                                           |
'---------------------------------------------------------------------------'*/
int clientConnectPrintHead ( struct tClientConnectParameter rClientConnectParameter,
                         struct tClientConnect rClientConnect ) 
{

    //----------------------------------------- estrapolare
    register short debug_level = 1 ;
    //======================================================================================

    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliConnectPrintHead[][3] =  {
          { "%protocollo%"                 , rClientConnectParameter.protocollo                            , "string" },
          { "%host%"                       , rClientConnectParameter.host                                  , "string" },
          { "%netmask%"                    , my_itoa(rClientConnectParameter.netmask, "%d")                , "numeric"},          
          { "%porta%"                      , my_itoa(rClientConnectParameter.porta, "%d")                  , "numeric"},
          { "%risorsa_path%"               , rClientConnectParameter.risorsa_path                          , "string" },
          { "%risorsa%"                    , rClientConnectParameter.risorsa                               , "string" },
          { "%TCPIp%"                      , rClientConnect.TCPIp                                          , "string" },
          { "%ServerRequestBufferLen%"     , my_itoa(rClientConnect.ServerRequestBufferLen, "%d")          , "numeric"},
          { "%ServerRequestBufferLen%"     , my_itoa(rClientConnect.ServerRequestBufferLen, "%d")          , "numeric"}, 
          { "EOT"                          , ""                                                            , ""       }
    };

	/* analisi HOST */
	if ( rClientConnectParameter.netmask == 0 )
         PrintMessage ( debug_level, TABMSG2, "MY_CLIENT_TCP__MSG_HEAD", TabVariabiliConnectPrintHead, 0, 0, 0);

	/* analisi NETWORK */
	if ( rClientConnectParameter.netmask > 0 ) {
         PrintMessage ( debug_level, TABMSG2, "MY_CLIENT_TCP__MSG_HEAD_NETWORK", TabVariabiliConnectPrintHead, 0, 0, 0);		 
    };
};



/*
.-------------------------------------------------------------------------------.
|                                                                               |
|  Funzione    : clientConnectPrintDetails                                      |
|                                                                               |
|  Descrizione : Print Detail RoW                                               |
|                                                                               |
|  Parameter   :                                                                |
|                                                                               |
|     %protocollo%                       - Protocol                             |
|     %host%                             - Host Target                          |
|     %porta%                            - Port Target                          |
|     %ServerResponseBufferLen%          - Server Response Buffer Len           |
|     %sequence%                         - Sequenza                             |
|     %tempoTotaleSingolaConnessione%    - Tempo totale connessione             |
|     %SSLCipherSuite_AlgSim_SSLVersion% - SSL CipherSuite AlgSim ed SSLVersion | 
|     %SSLCipherSuite%                   - SSL CipherSuite Algorit. Simmetrico  |
|     %SSLCipherSuite_AlgSim_LenKey%     - SSL CipherSuite Algorit. Simm/Lenkey |
|     %msg%                              - Server Message Response              | 
|     %msgDesc%                          - Server Message Response/Description  | 
|     %returnClientConnect%              - Esito Connessione                    | 
|                                                                               |
|  mask        :                                                                |
|                                                                               |
|     "%ServerResponseBufferLen% byte from %host%:%porta%                       | 
|         %protocollo%_seq=%sequence% time=%tempoTotaleSingolaConnessione% ms\t |
|              [ %SSLCipherSuite_AlgSim_SSLVersion% | %SSLCipherSuite% |        |
|                  %SSLCipherSuite_AlgSim_LenKey% ]\t [%msg%] %msgDesc          |
|                          %returnClientConnect%                                |
|  example     :                                                                |
|                                                                               |
|     000128 byte from open.org:443 http_seq=001 time=27 ms                     | 
|             [ TLSv1/SSLv3 | RC4-MD5 | 128 ]                                   | 
|                   [405] HTTP.RECV.Method Not Allowed   fail                   |
|                                                                               |
|  TODO        :                                                                |
|                                                                               |
|     Nel caso di connessione con SSL nel messagio è presente versione e        |
|     ciphersuite. Questi campi compaiono a null anche quando non si tratta     |
|     di ssl. Bisognerebbe implemenetare in ambito PrintMessage la possibilita  |
|     di condizionare un blocco del mask. Idem per il %msg% ed %msgDesc%.       |
|                                                                               | 
'-------------------------------------------------------------------------------'*/ 
int clientConnectPrintDetails ( struct tClientConnectParameter rClientConnectParameter,
                                struct tClientConnect rClientConnect,
                                double tempo_totale_singola_connessione,
                                int sequence,  
                                long msg,
                                char *msgDesc ) 
{
    char *stringaOUT ;
    stringaOUT = malloc(1024);
    memset (stringaOUT, 0, 1024);

//----------------------------------------- estrapolare
    register short debug_level = 1 ;
//======================================================================================

    /*
    .------------------------------------------.
    | Imposta la descrizione dell'esito        |
    | della connessione in caso di fallimento. |  
    '------------------------------------------' */
    char *returnClientConnectDesc = malloc(10); 
    memset(returnClientConnectDesc,0,10); 
    if ( rClientConnect.return_code < 0 )
          strcpy(returnClientConnectDesc, "fail\0"); 

    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliConnectPrintDetails[][3] =  {
          { "%protocollo%"                       , rClientConnectParameter.protocollo                               , "string"  },
          { "%host%"                             , rClientConnectParameter.host                                     , "string"  },
          { "%porta%"                            , my_itoa(rClientConnect.TCPPort, "%d")                            , "numeric" },
          { "%ServerResponseBufferLen%"          , my_itoa(rClientConnect.ServerResponseBufferLen, "%06d")          , "numeric" },
          { "%sequence%"                         , my_itoa(sequence, "%03d")                                        , "numeric" },
          { "%tempoTotaleSingolaConnessione%"    , my_dtoa(tempo_totale_singola_connessione, "%.3g")                , "numeric" },
          { "%SSLCipherSuite_AlgSim_SSLVersion%" , my_stos(rClientConnect.SSLCipherSuite_AlgSim_SSLVersion, "%11s") , "string"  },
          { "%SSLCipherSuite%"                   , my_stos(rClientConnect.SSLCipherSuite, "%20s")                   , "string"  },
          { "%SSLCipherSuite_AlgSim_LenKey%"     , my_itoa(rClientConnect.SSLCipherSuite_AlgSim_LenKey, "%3d")      , "numeric" }, 
          { "%msg%"                              , my_ltoa(msg,"%ld")                                               , "string"  },
          { "%msgDesc%"                          , msgDesc                                                          , "string"  },
          { "%returnClientConnect%"              , returnClientConnectDesc                                          , "numeric" }, 
          { "EOT"                                , ""                                                               , ""        }
    };

    PrintMessage ( debug_level, TABMSG2, "MY_CLIENT_TCP__MSG_DETAILS", TabVariabiliConnectPrintDetails, 0, 0, 0);
};


/*
.---------------------------------------------------------------------------.
|                                                                           |
|  Funzione    : clientConnectPrintStatistic                                |
|                                                                           |
|  Descrizione : Print Statistic Session Terminated                         |
|                                                                           | 
|  Parameter.                                                               |
|                                                                           |
|     %A - Protocol                                                         |
|     %B - Host Target                                                      |
|     %C - Port Target                                                      |
|     %D - Path Resouce Target                                              |
|     %E - Resource Target                                                  |
|     %F - Packet Trasmitted                                                |
|     %G - Packet Received                                                  |
|     %H - Durata Sessione                                                  |
|     %I - Min Single Connection                                            |
|     %L - Max Single Connection                                            |
|                                                                           |
|  Mask.                                                                    |
|                                                                           |
|     --- %A://%B:%C%D%E ping statistics ---                                |
|     %F packets transmitted, %G received, 0% packet loss, time %Hms        |
|     rtt min/avg/max/mdev = %I/0.000/%L/0.000 ms                           |
|                                                                           |
|  Example.                                                                 |
|                                                                           |
|     --- ssl://www.domain.it:443 ping statistics ---                       |
|     14 packets transmitted, 14 received, 0% packet loss, time 1.97e+04ms  |
|     rtt min/avg/max/mdev = 0/0.000/1.22e+03/0.000 ms                      |
|                                                                           |
'---------------------------------------------------------------------------'*/
int clientConnectPrintStatistic ( struct tClientConnectParameter rClientConnectParameter,
                              struct tClientConnect rClientConnect,
                              double tempo_totale_intera_sessione, 
                              int  contaPacchettiInviati, 
                              int contaPacchettiRicevuti, 
                              double tempo_min_singola_connessione,
                              double tempo_max_singola_connessione ) 
{
    //----------------------------------------- estrapolare
    register short debug_level = 1 ;
    //======================================================================================

    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliConnectPrintStatistic[][3] =  {
          { "%protocollo%"                 , rClientConnectParameter.protocollo                            , "string" },
          { "%host%"                       , rClientConnectParameter.host                                  , "string" },
          { "%porta%"                      , my_itoa(rClientConnect.TCPPort, "%d")                         , "numeric"},
          { "%risorsa_path%"               , rClientConnectParameter.risorsa_path                          , "string" },
          { "%risorsa%"                    , rClientConnectParameter.risorsa                               , "string" },
          { "%contaPacchettiInviati%"      , my_itoa(contaPacchettiInviati, "%d")                          , "numeric"},
          { "%contaPacchettiRicevuti%"     , my_itoa(contaPacchettiRicevuti, "%d")                         , "numeric"},
          { "%SessionDurationTime%"        , my_dtoa(tempo_totale_intera_sessione, "%.3g")                 , "numeric"},
          { "%MinConnectionTime%"          , my_dtoa(tempo_min_singola_connessione, "%.3g")                , "numeric"},
          { "%MaxConnectionTime%"          , my_dtoa(tempo_max_singola_connessione, "%.3g")                , "numeric"},
          { "EOT"                          , ""                                                            , ""       }
    };

    PrintMessage ( debug_level, TABMSG2, "MY_CLIENT_TCP__MSG_STATISTIC", TabVariabiliConnectPrintStatistic, 0, 0, 0);
}

/*
.---------------------------------------------------------------------------.
|                                                                           |
|  Funzione    : clientConnectSSLCipherSCliePropErr                         |
|                                                                           |
|  Descrizione : Visualizza la lista delle ciphers proposte dal client      |
|                quando avviene un errore in fase di handshake              |
|                                                                           |
|  Parameter.                                                               |
|                                                                           |
|     %ClientListProposalsNum% - numero di ciphers proposte al server       |
|     %ClientListProposals%    - elenco delle ciphers proposte al server    |
|                                                                           |
|  Mask.                                                                    |
|                                                                           |
|     +++ ciphersuite handshake error                                       |
|             [%ClientListProposalsNum%][%ClientListProposals%]             |
|                                                                           |
|  Example.                                                                 |
|                                                                           |
|     +++ ciphersuite handshake error [79][ECDHE-ECDSA-AES256-GCM-SHA384:   |
|     ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:                     |
|     SRP-DSS-AES-256-CBC-SHA:SRP-RSA-AES-256-CBC-SHA:SRP-AES-256-CBC-SHA:  |
|     DH-DSS-AES256-GCM-SHA384:DHE-DSS-AES256-GCM-SHA384:                   |
|     DH-RSA-AES256-GCM-SHA384:DHE-DSS-AES256-SHA256:DH-RSA-AES256-SHA256:  |
|     ...                                                                   |
|     CBC3-SHA:ECDH-RSA-DES-CBC3-SHA:ECDH-ECDSA-DES-CBC3-SHA:DES-CBC3-SHA]  |
|                                                                           |
'---------------------------------------------------------------------------'*/
int clientConnectSSLCipherSCliePropErr ( struct tClientConnect rClientConnect )
{
    //----------------------------------------- estrapolare
    register short debug_level = 1 ;
    //======================================================================================

    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
    char *TabVariabiliConnectSSLCipherSClieProp[][3] =  {
       { "%ClientListProposalsNum%", my_itoa(rClientConnect.SSLCipherSuite_Client_List_Proposals_Num,"%d"), "numeric" },
       { "%ClientListProposals%"   , rClientConnect.SSLCipherSuite_Client_List_Proposals                  , "string"  },
       { "EOT"                     , ""                                                                   , ""        }
    };
    PrintMessage ( debug_level, TABMSG2, "MY_CLIENT_SSL__CIPHERS_CLIENT_PROP", TabVariabiliConnectSSLCipherSClieProp, 0, 0, 0);
}


/*
.---------------------------------------------------------------------------.
|                                                                           |
|  Funzione    : clientConnectparameterPrint                                |
|                                                                           |
|  Descrizione : Visualizza la struttura di input.                          |
|                                                                           |
|  Parameter. ( DA SISTEMARE ===================================== )        |
|                                                                           |
|     %A - Protocol                                                         |
|     %B - Host Target                                                      |
|     %C - Port Target                                                      |
|     %D - Path Resouce Target                                              |
|     %E - Resource Target                                                  |
|     %F - Packet Trasmitted                                                |
|     %G - Packet Received                                                  |
|     %H - Durata Sessione                                                  |
|     %I - Min Single Connection                                            |
|     %L - Max Single Connection                                            |
|                                                                           |
|  Mask.                                                                    |
|                                                                           |
|     --- %A://%B:%C%D%E ping statistics ---                                |
|     %F packets transmitted, %G received, 0% packet loss, time %Hms        |
|     rtt min/avg/max/mdev = %I/0.000/%L/0.000 s                            |
|                                                                           |
|  Example.                                                                 |
|                                                                           |
|     --- ssl://www.domain.it:443 ping statistics ---                       |
|     14 packets transmitted, 14 received, 0% packet loss, time 1.97e+04ms  |
|     rtt min/avg/max/mdev = 0/0.000/1.22e+03/0.000 s                       |
|                                                                           |
'---------------------------------------------------------------------------'*/
int clientConnectParameterPrint ( struct tClientConnectParameter rClientConnectParameter ) 
{
    //----------------------------------------- estrapolare
    register short debug_level = 1 ;
    //======================================================================================

    /*
    .------------------------------------------.
    | dichiarazione strutture messaggi         |
    '------------------------------------------' */
	printf("\nnetmask: %d", rClientConnectParameter.netmask);
    char *TabVariabiliConnectParameter[][3] =  {
          { "%frontend%"                   , rClientConnectParameter.frontend                                        , "string" }, 
          { "%frontend_version%"           , rClientConnectParameter.frontend_version                                , "string" }, 
          { "%url%"                        , rClientConnectParameter.url                                             , "string" },
          { "%protocollo%"                 , rClientConnectParameter.protocollo                                      , "string" },
          { "%host%"                       , rClientConnectParameter.host                                            , "string" }, 
          { "%netmask%"                    , my_itoa(rClientConnectParameter.netmask, "%d")                          , "numeric"}, 
          { "%porta%"                      , my_itoa(rClientConnectParameter.porta, "%d")                            , "numeric"},
          { "%risorsa_path%"               , rClientConnectParameter.risorsa_path                                    , "string" }, 
          { "%risorsa%"                    , rClientConnectParameter.risorsa                                         , "string" }, 
          { "%queryStringLen%"             , my_itoa(rClientConnectParameter.queryStringLen, "%d")                   , "numeric"}, 
          { "%queryStringDimTabVariable%"  , my_itoa(rClientConnectParameter.queryStringDimTabVariable, "%d")        , "numeric"}, 
     /**/ { "%queryStringTabVar_index%"    , "A\0"                                                                   , "numeric"},
     /**/ { "%queryStringTabVar_variable%" , my_stos(rClientConnectParameter.queryStringTabVar[0].variable, "%30s")  , "string" }, 
     /**/ { "%queryStringTabVar_value%"    , my_stos(rClientConnectParameter.queryStringTabVar[0].value, "%30s")     , "string" },
          { "%auth_user%"                  , rClientConnectParameter.auth_user                                       , "string" }, 
          { "%auth_password%"              , rClientConnectParameter.auth_password                                   , "string" }, 
          { "%keystore%"                   , rClientConnectParameter.keystore                                        , "string" }, 
          { "%password%"                   , rClientConnectParameter.password                                        , "string" }, 
          { "%truststore%"                 , rClientConnectParameter.truststore                                      , "string" }, 
          { "%ciphersuite%"                , rClientConnectParameter.ciphersuite                                     , "string" }, 
          { "%sslversion%"                 , rClientConnectParameter.sslversion                                      , "string" }, 
          { "%type_operation%"             , rClientConnectParameter.type_operation                                  , "string" }, 
          { "%message_language%"           , rClientConnectParameter.message_language                                , "string" }, 
          { "%message_type%"               , rClientConnectParameter.message_type                                    , "string" }, 
          { "%message%"                    , rClientConnectParameter.message                                         , "string" }, 
          { "%SSLCipherSuiteServerScan%"   , rClientConnectParameter.SSLCipherSuiteServerScan                        , "string" }, 
          { "%MaxRequest%"                 , my_itoa(rClientConnectParameter.MaxRequest, "%d")                       , "numeric"}, 
          { "%TCPReceiveTimeout%"          , my_itoa(rClientConnectParameter.TCPReceiveTimeout, "%d")                , "numeric"}, 
          { "%OutputFile%"                 , rClientConnectParameter.OutputFile                                      , "string" }, 
          { "%debug%"                      , rClientConnectParameter.debug                                           , "string" }, 
          { "EOT"                          , ""                                                                      , ""       }
    };

    PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__CLIENT_CONNECT_PARAMETER_TESTA"  , TabVariabiliConnectParameter, 0, 0, 0);

    register short indice = 0;
    while ( indice < rClientConnectParameter.queryStringDimTabVariable )
    {
          /*
          .----------------------------.
          |    %queryStringTabVar_     |
          '----------------------------' */ 
          TabVariabiliConnectParameter[9][1]  = my_itoa(indice, "%3d.");                                                     // _index% 
          TabVariabiliConnectParameter[10][1] = my_stos(rClientConnectParameter.queryStringTabVar[indice].variable, "%30s"); // _variable%
          TabVariabiliConnectParameter[11][1] = my_stos(rClientConnectParameter.queryStringTabVar[indice].value, "%30s");    // _value% 

          PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__CLIENT_CONNECT_PARAMETER_VARURL" , TabVariabiliConnectParameter, 0, 0, 0);
          indice++;
    }

    PrintMessage(debug_level, TABMSG2, "MY_CLIENT_TCP__CLIENT_CONNECT_PARAMETER_CODA"   , TabVariabiliConnectParameter, 0, 0, 0);
  
}


// __EOF__

